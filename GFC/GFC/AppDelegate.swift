//
//  AppDelegate.swift
//  GFC
//
//  Created by Parth Anghan on 18/10/21.
//

//com.GFCGlobalhealth

import UIKit
import IQKeyboardManager
import LGSideMenuController
import FirebaseAuth
import Firebase
import GoogleSignIn
import GoogleMaps
import GooglePlaces
import Quickblox
import QuickbloxWebRTC

struct TimeIntervalConstant {
    static let answerTimeInterval: TimeInterval = 30.0
    static let dialingTimeInterval: TimeInterval = 3.0
}

struct AppDelegateConstant {
    static let enableStatsReports: UInt = 1
}

@main

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var dicUserLoginData = GFCLoginData()
    
    var currentCountry = GFCCountryListData()
    
    var isPopupHidden = false
    
    var strCartCount = ""
    
    var strCurrentSelectCountry = "₹"
    
    var objOtherUserId = 0
    var objOtherUserName = ""
    var objOtherUserLogin = ""

    var objSelectAddress = GFCAddressListData()
    
    var payment_access_token = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        
        GIDSignIn.sharedInstance().clientID = "383455892930-6cgqo0j732em7503hbsvv413it44fkvu.apps.googleusercontent.com"
        
        GMSServices.provideAPIKey(GOOGLE_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_KEY)
        
        //Client
        Quickblox.initWithApplicationId(103844, authKey: "ak_sgMJd79kA7NR9Tu", authSecret: "as_ywZB62F924dUL8v", accountKey: "ack_CWhVVybjuzpVX1YanvgH")
        
        //Demo
       // Quickblox.initWithApplicationId(104292, authKey: "ak_cyw3ZyCHe47dwxX", authSecret: "as_XnAXs9Ad9Mc4ftw", accountKey: "ack_cTZor1kxapvhsg8bswss")
     
        
        QBSettings.logLevel = .debug
        
        QBRTCConfig.setAnswerTimeInterval(30.0)
        QBRTCConfig.setDialingTimeInterval(3.0)
        
        QBSettings.enableXMPPLogging()
        QBSettings.disableXMPPLogging()
        
        QBRTCConfig.setLogLevel(QBRTCLogLevel.verboseWithWebRTC)
        
        QBSettings.autoReconnectEnabled = true
        QBSettings.carbonsEnabled = true
        
        QBRTCClient.initializeRTC()
        
        
        dicUserLoginData = getCurrentUserData()
        
        setSemantricFlow()
        
        sleep(2)
        
        IQKeyboardManager.shared().isEnabled = true
        
        if let isSelectLanguage = UserDefaults.standard.value(forKey: "isSelectLanguage") as? Bool
        {
            if isSelectLanguage == true
            {
                if let isIntroFinished = UserDefaults.standard.value(forKey: "isIntroFinished") as? Bool
                {
                    if isIntroFinished == true
                    {
                        let UserLogin = UserDefaults.standard.value(forKey: "UserLogin") as? Bool
                        
                        if UserLogin == true
                        {
                            self.setUpSideMenu()
                        }
                        else
                        {
                            self.setUpLoginData()
                        }
                    }
                }
            }
            else
            {
                setUpLanguageData()
            }
        }
        else
        {
            setUpLanguageData()
        }
        
        try? isUpdateAvailable {[self] (update, error) in
            if let error = error {
                print(error)
            } else if update ?? false {
                // show alert
                
                DispatchQueue.main.async {
                    let alertMessage = "An update to the app is required to continue.\nPlease go to the app store and upgrade your application."
                    
                    let alertController = UIAlertController(title: "You have new updates", message: alertMessage, preferredStyle: .alert)
                    
                    let updateButton = UIAlertAction(title: "Update App", style: .default) { (action:UIAlertAction) in
                        guard let url = URL(string: "https://itunes.apple.com/app/id6448429453") else {
                            return
                        }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                    
                    alertController.addAction(updateButton)
                    self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                }
                
                
            }
        }
        
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        try? isUpdateAvailable {[self] (update, error) in
            if let error = error {
                print(error)
            } else if update ?? false {
                // show alert
                
                DispatchQueue.main.async {
                    let alertMessage = "An update to the app is required to continue.\nPlease go to the app store and upgrade your application."
                    
                    let alertController = UIAlertController(title: "You have new updates", message: alertMessage, preferredStyle: .alert)
                    
                    let updateButton = UIAlertAction(title: "Update App", style: .default) { (action:UIAlertAction) in
                        guard let url = URL(string: "https://itunes.apple.com/app/id6448429453") else {
                            return
                        }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                    
                    alertController.addAction(updateButton)
                    self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                }
                
                
            }
        }
    }
    
    func setUpSideMenu(){
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let home: TabbarViewController = mainStoryboard.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        
        if Language.shared.isArabic == true{
            let homeNavigation = UINavigationController(rootViewController: home)
            let leftViewController: SideMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
            
            let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: nil, rightViewController: leftViewController)
            controller.modalPresentationStyle = .popover
            controller.navigationController?.navigationBar.isHidden = true
            
            homeNavigation.navigationBar.isHidden = true
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
        }else{
            let homeNavigation = UINavigationController(rootViewController: home)
            let leftViewController: SideMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
            
            let controller = LGSideMenuController.init(rootViewController: homeNavigation, leftViewController: leftViewController, rightViewController: nil)
            controller.modalPresentationStyle = .popover
            controller.navigationController?.navigationBar.isHidden = true
            
            homeNavigation.navigationBar.isHidden = true
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
        }
        
    }
    
    func setUpLoginData(){
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationController?.navigationBar.isHidden = true
        self.window?.rootViewController = homeNavigation
        self.window?.makeKeyAndVisible()
    }
    
    func setUpLanguageData(){
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = mainStoryboard.instantiateViewController(withIdentifier: "SelectLanguageVC") as! SelectLanguageVC
        
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationController?.navigationBar.isHidden = true
        self.window?.rootViewController = homeNavigation
        self.window?.makeKeyAndVisible()
    }
    
    func saveCurrentUserData(dic: GFCLoginData)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "currentUserDataGFC")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> GFCLoginData
    {
        if let data = UserDefaults.standard.object(forKey: "currentUserDataGFC"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! GFCLoginData
        }
        return GFCLoginData()
    }
    
    // Add any other AppDelegate methods below if needed
}

enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}

@discardableResult
func isUpdateAvailable(completion: @escaping (Bool?, Error?) -> Void) throws -> URLSessionDataTask {
    guard let info = Bundle.main.infoDictionary,
          let currentVersion = info["CFBundleShortVersionString"] as? String,
          let identifier = info["CFBundleIdentifier"] as? String,
          let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
        throw VersionError.invalidBundleInfo
    }
    
    let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData)
    
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        do {
            if let error = error { throw error }
            
            guard let data = data else { throw VersionError.invalidResponse }
            
            let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
            
            guard let result = (json?["results"] as? [Any])?.first as? [String: Any], let lastVersion = result["version"] as? String else {
                throw VersionError.invalidResponse
            }
            completion(lastVersion > currentVersion, nil)
        } catch {
            completion(nil, error)
        }
    }
    
    task.resume()
    return task
}
