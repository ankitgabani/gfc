//
//  TabbarViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 01/04/22.
//

import UIKit
import Quickblox
class TabbarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.items?[0].title = "Doctors".localizeString(string: Language.shared.currentAppLang)
        tabBar.items?[1].title = "Hospitals".localizeString(string: Language.shared.currentAppLang)
        tabBar.items?[2].title = "Pharmacy".localizeString(string: Language.shared.currentAppLang)
        tabBar.items?[3].title = "Account".localizeString(string: Language.shared.currentAppLang)

        tabBarController?.tabBar.backgroundColor = UIColor.white
        
        if #available(iOS 13.0, *) {
            let tabBarAppearance: UITabBarAppearance = UITabBarAppearance()
            tabBarAppearance.configureWithDefaultBackground()
            tabBarAppearance.backgroundColor = UIColor.white
            UITabBar.appearance().standardAppearance = tabBarAppearance

            if #available(iOS 15.0, *) {
                UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(tabChange(_:)), name: Notification.Name("tabChange"), object: nil)
 
    }
    
    @objc func tabChange(_ notification: Notification){
        self.selectedIndex = notification.userInfo?["index"] as! Int
    }
    
}
