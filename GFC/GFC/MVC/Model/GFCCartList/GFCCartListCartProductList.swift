//
//	GFCCartListCartProductList.swift
//
//	Create by iMac on 13/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCCartListCartProductList : NSObject, NSCoding{

	var addedDate : String!
	var cartId : String!
	var consumerId : String!
	var createdBy : String!
	var discountedPrice : String!
    var discountedPrice1 : String!
    var discountedPrice2 : String!
    var discountedPrice3 : String!
	var image : String!
	var isOffer : String!
	var nettotal : Int!
	var offerId : String!
	var orderType : String!
	var orderedDate : String!
	var pId : String!
	var price : String!
    var price1 : String!
    var price2 : String!
    var price3 : String!
	var productName : String!
	var quantity : String!
	var size : String!
    var size1 : String!
    var size2 : String!
    var size3 : String!
	var status : String!
	var varient : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		addedDate = dictionary["added_date"] as? String == nil ? "" : dictionary["added_date"] as? String
		cartId = dictionary["cart_id"] as? String == nil ? "" : dictionary["cart_id"] as? String
		consumerId = dictionary["consumerId"] as? String == nil ? "" : dictionary["consumerId"] as? String
		createdBy = dictionary["created_by"] as? String == nil ? "" : dictionary["created_by"] as? String
		discountedPrice = dictionary["discounted_price"] as? String == nil ? "" : dictionary["discounted_price"] as? String
        discountedPrice1 = dictionary["discounted_price1"] as? String == nil ? "" : dictionary["discounted_price1"] as? String
        discountedPrice2 = dictionary["discounted_price2"] as? String == nil ? "" : dictionary["discounted_price2"] as? String
        discountedPrice3 = dictionary["discounted_price3"] as? String == nil ? "" : dictionary["discounted_price3"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		isOffer = dictionary["is_offer"] as? String == nil ? "" : dictionary["is_offer"] as? String
		nettotal = dictionary["nettotal"] as? Int == nil ? 0 : dictionary["nettotal"] as? Int
		offerId = dictionary["offerId"] as? String == nil ? "" : dictionary["offerId"] as? String
		orderType = dictionary["order_type"] as? String == nil ? "" : dictionary["order_type"] as? String
		orderedDate = dictionary["ordered_date"] as? String == nil ? "" : dictionary["ordered_date"] as? String
		pId = dictionary["pId"] as? String == nil ? "" : dictionary["pId"] as? String
		price = dictionary["price"] as? String == nil ? "" : dictionary["price"] as? String
        price1 = dictionary["price1"] as? String == nil ? "" : dictionary["price1"] as? String
        price2 = dictionary["price2"] as? String == nil ? "" : dictionary["price2"] as? String
        price3 = dictionary["price3"] as? String == nil ? "" : dictionary["price3"] as? String
		productName = dictionary["productName"] as? String == nil ? "" : dictionary["productName"] as? String
		quantity = dictionary["quantity"] as? String == nil ? "" : dictionary["quantity"] as? String
		size = dictionary["size"] as? String == nil ? "" : dictionary["size"] as? String
        size1 = dictionary["size1"] as? String == nil ? "" : dictionary["size1"] as? String
        size2 = dictionary["size2"] as? String == nil ? "" : dictionary["size2"] as? String
        size3 = dictionary["size3"] as? String == nil ? "" : dictionary["size3"] as? String

		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		varient = dictionary["varient"] as? String == nil ? "" : dictionary["varient"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if addedDate != nil{
			dictionary["added_date"] = addedDate
		}
		if cartId != nil{
			dictionary["cart_id"] = cartId
		}
		if consumerId != nil{
			dictionary["consumerId"] = consumerId
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if discountedPrice != nil{
			dictionary["discounted_price"] = discountedPrice
		}
        if discountedPrice1 != nil{
            dictionary["discounted_price1"] = discountedPrice1
        }
        if discountedPrice2 != nil{
            dictionary["discounted_price2"] = discountedPrice2
        }
        if discountedPrice3 != nil{
            dictionary["discounted_price3"] = discountedPrice3
        }
		if image != nil{
			dictionary["image"] = image
		}
		if isOffer != nil{
			dictionary["is_offer"] = isOffer
		}
		if nettotal != nil{
			dictionary["nettotal"] = nettotal
		}
		if offerId != nil{
			dictionary["offerId"] = offerId
		}
		if orderType != nil{
			dictionary["order_type"] = orderType
		}
		if orderedDate != nil{
			dictionary["ordered_date"] = orderedDate
		}
		if pId != nil{
			dictionary["pId"] = pId
		}
		if price != nil{
			dictionary["price"] = price
		}
        if price1 != nil{
            dictionary["price1"] = price1
        }
        if price2 != nil{
            dictionary["price2"] = price2
        }
        if price3 != nil{
            dictionary["price3"] = price3
        }
		if productName != nil{
			dictionary["productName"] = productName
		}
		if quantity != nil{
			dictionary["quantity"] = quantity
		}
		if size != nil{
			dictionary["size"] = size
		}
        if size1 != nil{
            dictionary["size1"] = size1
        }
        if size2 != nil{
            dictionary["size2"] = size2
        }
        if size3 != nil{
            dictionary["size3"] = size3
        }
		if status != nil{
			dictionary["status"] = status
		}
		if varient != nil{
			dictionary["varient"] = varient
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addedDate = aDecoder.decodeObject(forKey: "added_date") as? String
         cartId = aDecoder.decodeObject(forKey: "cart_id") as? String
         consumerId = aDecoder.decodeObject(forKey: "consumerId") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         discountedPrice = aDecoder.decodeObject(forKey: "discounted_price") as? String
        discountedPrice1 = aDecoder.decodeObject(forKey: "discounted_price1") as? String
        discountedPrice2 = aDecoder.decodeObject(forKey: "discounted_price2") as? String
        discountedPrice3 = aDecoder.decodeObject(forKey: "discounted_price3") as? String

         image = aDecoder.decodeObject(forKey: "image") as? String
         isOffer = aDecoder.decodeObject(forKey: "is_offer") as? String
         nettotal = aDecoder.decodeObject(forKey: "nettotal") as? Int
         offerId = aDecoder.decodeObject(forKey: "offerId") as? String
         orderType = aDecoder.decodeObject(forKey: "order_type") as? String
         orderedDate = aDecoder.decodeObject(forKey: "ordered_date") as? String
         pId = aDecoder.decodeObject(forKey: "pId") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
        price1 = aDecoder.decodeObject(forKey: "price1") as? String
        price2 = aDecoder.decodeObject(forKey: "price2") as? String
        price3 = aDecoder.decodeObject(forKey: "price3") as? String

        
         productName = aDecoder.decodeObject(forKey: "productName") as? String
         quantity = aDecoder.decodeObject(forKey: "quantity") as? String
         size = aDecoder.decodeObject(forKey: "size") as? String
        size1 = aDecoder.decodeObject(forKey: "size1") as? String
        size2 = aDecoder.decodeObject(forKey: "size2") as? String
        size3 = aDecoder.decodeObject(forKey: "size3") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         varient = aDecoder.decodeObject(forKey: "varient") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if addedDate != nil{
			aCoder.encode(addedDate, forKey: "added_date")
		}
		if cartId != nil{
			aCoder.encode(cartId, forKey: "cart_id")
		}
		if consumerId != nil{
			aCoder.encode(consumerId, forKey: "consumerId")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if discountedPrice != nil{
			aCoder.encode(discountedPrice, forKey: "discounted_price")
		}
        if discountedPrice1 != nil{
            aCoder.encode(discountedPrice1, forKey: "discounted_price1")
        }
        if discountedPrice2 != nil{
            aCoder.encode(discountedPrice2, forKey: "discounted_price2")
        }
        if discountedPrice3 != nil{
            aCoder.encode(discountedPrice3, forKey: "discounted_price3")
        }
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if isOffer != nil{
			aCoder.encode(isOffer, forKey: "is_offer")
		}
		if nettotal != nil{
			aCoder.encode(nettotal, forKey: "nettotal")
		}
		if offerId != nil{
			aCoder.encode(offerId, forKey: "offerId")
		}
		if orderType != nil{
			aCoder.encode(orderType, forKey: "order_type")
		}
		if orderedDate != nil{
			aCoder.encode(orderedDate, forKey: "ordered_date")
		}
		if pId != nil{
			aCoder.encode(pId, forKey: "pId")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
        if price1 != nil{
            aCoder.encode(price1, forKey: "price1")
        }
        if price2 != nil{
            aCoder.encode(price2, forKey: "price2")
        }
        if price3 != nil{
            aCoder.encode(price3, forKey: "price3")
        }
		if productName != nil{
			aCoder.encode(productName, forKey: "productName")
		}
		if quantity != nil{
			aCoder.encode(quantity, forKey: "quantity")
		}
		if size != nil{
			aCoder.encode(size, forKey: "size")
		}
        if size1 != nil{
            aCoder.encode(size1, forKey: "size1")
        }
        if size2 != nil{
            aCoder.encode(size2, forKey: "size2")
        }
        if size3 != nil{
            aCoder.encode(size3, forKey: "size3")
        }
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if varient != nil{
			aCoder.encode(varient, forKey: "varient")
		}

	}

}
