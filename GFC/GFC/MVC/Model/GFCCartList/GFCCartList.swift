//
//	GFCCartList.swift
//
//	Create by iMac on 13/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCCartList : NSObject, NSCoding{

	var cartProductList : [GFCCartListCartProductList]!
	var message : String!
	var offer : String!
	var status : String!
	var subTotal : Int!
	var total : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		cartProductList = [GFCCartListCartProductList]()
		if let cartProductListArray = dictionary["cartProductList"] as? [NSDictionary]{
			for dic in cartProductListArray{
				let value = GFCCartListCartProductList(fromDictionary: dic)
				cartProductList.append(value)
			}
		}
		message = dictionary["message"] as? String == nil ? "" : dictionary["message"] as? String
		offer = dictionary["offer"] as? String == nil ? "" : dictionary["offer"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		subTotal = dictionary["sub_total"] as? Int == nil ? 0 : dictionary["sub_total"] as? Int
		total = dictionary["total"] as? Int == nil ? 0 : dictionary["total"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if cartProductList != nil{
			var dictionaryElements = [NSDictionary]()
			for cartProductListElement in cartProductList {
				dictionaryElements.append(cartProductListElement.toDictionary())
			}
			dictionary["cartProductList"] = dictionaryElements
		}
		if message != nil{
			dictionary["message"] = message
		}
		if offer != nil{
			dictionary["offer"] = offer
		}
		if status != nil{
			dictionary["status"] = status
		}
		if subTotal != nil{
			dictionary["sub_total"] = subTotal
		}
		if total != nil{
			dictionary["total"] = total
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cartProductList = aDecoder.decodeObject(forKey: "cartProductList") as? [GFCCartListCartProductList]
         message = aDecoder.decodeObject(forKey: "message") as? String
         offer = aDecoder.decodeObject(forKey: "offer") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         subTotal = aDecoder.decodeObject(forKey: "sub_total") as? Int
         total = aDecoder.decodeObject(forKey: "total") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if cartProductList != nil{
			aCoder.encode(cartProductList, forKey: "cartProductList")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if offer != nil{
			aCoder.encode(offer, forKey: "offer")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if subTotal != nil{
			aCoder.encode(subTotal, forKey: "sub_total")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}

	}

}