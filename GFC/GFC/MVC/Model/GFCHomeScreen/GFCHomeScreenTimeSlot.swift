//
//	GFCHomeScreenTimeSlot.swift
//
//	Create by mac on 9/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCHomeScreenTimeSlot : NSObject, NSCoding{

	var timeSlot : String!
	var timeSlotId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		timeSlot = dictionary["time_slot"] as? String == nil ? "" : dictionary["time_slot"] as? String
		timeSlotId = dictionary["time_slot_id"] as? String == nil ? "" : dictionary["time_slot_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if timeSlot != nil{
			dictionary["time_slot"] = timeSlot
		}
		if timeSlotId != nil{
			dictionary["time_slot_id"] = timeSlotId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         timeSlot = aDecoder.decodeObject(forKey: "time_slot") as? String
         timeSlotId = aDecoder.decodeObject(forKey: "time_slot_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if timeSlot != nil{
			aCoder.encode(timeSlot, forKey: "time_slot")
		}
		if timeSlotId != nil{
			aCoder.encode(timeSlotId, forKey: "time_slot_id")
		}

	}

}