//
//	GFCHomeScreenDocArrFeatured.swift
//
//	Create by mac on 9/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCHomeScreenDocArrFeatured : NSObject, NSCoding{

	var about : String!
	var achievements : String!
	var address : String!
	var age : String!
	var city : String!
	var coOrdinatorMobileNumber : String!
	var country : String!
	var createdAt : String!
	var doctorId : String!
	var email : String!
	var experience : String!
	var gender : String!
	var mobileNumber : String!
	var name : String!
	var profileImage : String!
	var qualifications : String!
	var ratings : Int!
	var speciality : [GFCHomeScreenSpeciality]!
	var state : String!
	var timeSlot : [GFCHomeScreenTimeSlot]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		about = dictionary["about"] as? String == nil ? "" : dictionary["about"] as? String
		achievements = dictionary["achievements"] as? String == nil ? "" : dictionary["achievements"] as? String
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		age = dictionary["age"] as? String == nil ? "" : dictionary["age"] as? String
		city = dictionary["city"] as? String == nil ? "" : dictionary["city"] as? String
		coOrdinatorMobileNumber = dictionary["coOrdinatorMobileNumber"] as? String == nil ? "" : dictionary["coOrdinatorMobileNumber"] as? String
		country = dictionary["country"] as? String == nil ? "" : dictionary["country"] as? String
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		doctorId = dictionary["doctorId"] as? String == nil ? "" : dictionary["doctorId"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		experience = dictionary["experience"] as? String == nil ? "" : dictionary["experience"] as? String
		gender = dictionary["gender"] as? String == nil ? "" : dictionary["gender"] as? String
		mobileNumber = dictionary["mobileNumber"] as? String == nil ? "" : dictionary["mobileNumber"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		profileImage = dictionary["profileImage"] as? String == nil ? "" : dictionary["profileImage"] as? String
		qualifications = dictionary["qualifications"] as? String == nil ? "" : dictionary["qualifications"] as? String
		ratings = dictionary["ratings"] as? Int == nil ? 0 : dictionary["ratings"] as? Int
		speciality = [GFCHomeScreenSpeciality]()
		if let specialityArray = dictionary["speciality"] as? [NSDictionary]{
			for dic in specialityArray{
				let value = GFCHomeScreenSpeciality(fromDictionary: dic)
				speciality.append(value)
			}
		}
		state = dictionary["state"] as? String == nil ? "" : dictionary["state"] as? String
		timeSlot = [GFCHomeScreenTimeSlot]()
		if let timeSlotArray = dictionary["time_slot"] as? [NSDictionary]{
			for dic in timeSlotArray{
				let value = GFCHomeScreenTimeSlot(fromDictionary: dic)
				timeSlot.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if about != nil{
			dictionary["about"] = about
		}
		if achievements != nil{
			dictionary["achievements"] = achievements
		}
		if address != nil{
			dictionary["address"] = address
		}
		if age != nil{
			dictionary["age"] = age
		}
		if city != nil{
			dictionary["city"] = city
		}
		if coOrdinatorMobileNumber != nil{
			dictionary["coOrdinatorMobileNumber"] = coOrdinatorMobileNumber
		}
		if country != nil{
			dictionary["country"] = country
		}
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if doctorId != nil{
			dictionary["doctorId"] = doctorId
		}
		if email != nil{
			dictionary["email"] = email
		}
		if experience != nil{
			dictionary["experience"] = experience
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if mobileNumber != nil{
			dictionary["mobileNumber"] = mobileNumber
		}
		if name != nil{
			dictionary["name"] = name
		}
		if profileImage != nil{
			dictionary["profileImage"] = profileImage
		}
		if qualifications != nil{
			dictionary["qualifications"] = qualifications
		}
		if ratings != nil{
			dictionary["ratings"] = ratings
		}
		if speciality != nil{
			var dictionaryElements = [NSDictionary]()
			for specialityElement in speciality {
				dictionaryElements.append(specialityElement.toDictionary())
			}
			dictionary["speciality"] = dictionaryElements
		}
		if state != nil{
			dictionary["state"] = state
		}
		if timeSlot != nil{
			var dictionaryElements = [NSDictionary]()
			for timeSlotElement in timeSlot {
				dictionaryElements.append(timeSlotElement.toDictionary())
			}
			dictionary["time_slot"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         about = aDecoder.decodeObject(forKey: "about") as? String
         achievements = aDecoder.decodeObject(forKey: "achievements") as? String
         address = aDecoder.decodeObject(forKey: "address") as? String
         age = aDecoder.decodeObject(forKey: "age") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         coOrdinatorMobileNumber = aDecoder.decodeObject(forKey: "coOrdinatorMobileNumber") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         doctorId = aDecoder.decodeObject(forKey: "doctorId") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         experience = aDecoder.decodeObject(forKey: "experience") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         profileImage = aDecoder.decodeObject(forKey: "profileImage") as? String
         qualifications = aDecoder.decodeObject(forKey: "qualifications") as? String
         ratings = aDecoder.decodeObject(forKey: "ratings") as? Int
         speciality = aDecoder.decodeObject(forKey: "speciality") as? [GFCHomeScreenSpeciality]
         state = aDecoder.decodeObject(forKey: "state") as? String
         timeSlot = aDecoder.decodeObject(forKey: "time_slot") as? [GFCHomeScreenTimeSlot]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if about != nil{
			aCoder.encode(about, forKey: "about")
		}
		if achievements != nil{
			aCoder.encode(achievements, forKey: "achievements")
		}
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if age != nil{
			aCoder.encode(age, forKey: "age")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if coOrdinatorMobileNumber != nil{
			aCoder.encode(coOrdinatorMobileNumber, forKey: "coOrdinatorMobileNumber")
		}
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if doctorId != nil{
			aCoder.encode(doctorId, forKey: "doctorId")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if experience != nil{
			aCoder.encode(experience, forKey: "experience")
		}
		if gender != nil{
			aCoder.encode(gender, forKey: "gender")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobileNumber")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if profileImage != nil{
			aCoder.encode(profileImage, forKey: "profileImage")
		}
		if qualifications != nil{
			aCoder.encode(qualifications, forKey: "qualifications")
		}
		if ratings != nil{
			aCoder.encode(ratings, forKey: "ratings")
		}
		if speciality != nil{
			aCoder.encode(speciality, forKey: "speciality")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if timeSlot != nil{
			aCoder.encode(timeSlot, forKey: "time_slot")
		}

	}

}