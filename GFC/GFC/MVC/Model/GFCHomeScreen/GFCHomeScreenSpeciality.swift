//
//	GFCHomeScreenSpeciality.swift
//
//	Create by mac on 9/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCHomeScreenSpeciality : NSObject, NSCoding{

	var specialityId : String!
	var specialityName : String!
	var image : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		specialityId = dictionary["specialityId"] as? String == nil ? "" : dictionary["specialityId"] as? String
		specialityName = dictionary["specialityName"] as? String == nil ? "" : dictionary["specialityName"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if specialityId != nil{
			dictionary["specialityId"] = specialityId
		}
		if specialityName != nil{
			dictionary["specialityName"] = specialityName
		}
		if image != nil{
			dictionary["image"] = image
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         specialityId = aDecoder.decodeObject(forKey: "specialityId") as? String
         specialityName = aDecoder.decodeObject(forKey: "specialityName") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if specialityId != nil{
			aCoder.encode(specialityId, forKey: "specialityId")
		}
		if specialityName != nil{
			aCoder.encode(specialityName, forKey: "specialityName")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}

	}

}