//
//	GFCHomeScreenData.swift
//
//	Create by mac on 9/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCHomeScreenData : NSObject, NSCoding{

	var docArrFeatured : [GFCHomeScreenDocArrFeatured]!
	var docArrPopular : [GFCHomeScreenDocArrFeatured]!
	var liveVideos : [GFCHomeScreenLiveVideo]!
	var speciality : [GFCHomeScreenSpeciality]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		docArrFeatured = [GFCHomeScreenDocArrFeatured]()
		if let docArrFeaturedArray = dictionary["DocArrFeatured"] as? [NSDictionary]{
			for dic in docArrFeaturedArray{
				let value = GFCHomeScreenDocArrFeatured(fromDictionary: dic)
				docArrFeatured.append(value)
			}
		}
		docArrPopular = [GFCHomeScreenDocArrFeatured]()
		if let docArrPopularArray = dictionary["DocArrPopular"] as? [NSDictionary]{
			for dic in docArrPopularArray{
				let value = GFCHomeScreenDocArrFeatured(fromDictionary: dic)
				docArrPopular.append(value)
			}
		}
		liveVideos = [GFCHomeScreenLiveVideo]()
		if let liveVideosArray = dictionary["live_videos"] as? [NSDictionary]{
			for dic in liveVideosArray{
				let value = GFCHomeScreenLiveVideo(fromDictionary: dic)
				liveVideos.append(value)
			}
		}
		speciality = [GFCHomeScreenSpeciality]()
		if let specialityArray = dictionary["speciality"] as? [NSDictionary]{
			for dic in specialityArray{
				let value = GFCHomeScreenSpeciality(fromDictionary: dic)
				speciality.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if docArrFeatured != nil{
			var dictionaryElements = [NSDictionary]()
			for docArrFeaturedElement in docArrFeatured {
				dictionaryElements.append(docArrFeaturedElement.toDictionary())
			}
			dictionary["DocArrFeatured"] = dictionaryElements
		}
		if docArrPopular != nil{
			var dictionaryElements = [NSDictionary]()
			for docArrPopularElement in docArrPopular {
				dictionaryElements.append(docArrPopularElement.toDictionary())
			}
			dictionary["DocArrPopular"] = dictionaryElements
		}
		if liveVideos != nil{
			var dictionaryElements = [NSDictionary]()
			for liveVideosElement in liveVideos {
				dictionaryElements.append(liveVideosElement.toDictionary())
			}
			dictionary["live_videos"] = dictionaryElements
		}
		if speciality != nil{
			var dictionaryElements = [NSDictionary]()
			for specialityElement in speciality {
				dictionaryElements.append(specialityElement.toDictionary())
			}
			dictionary["speciality"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         docArrFeatured = aDecoder.decodeObject(forKey: "DocArrFeatured") as? [GFCHomeScreenDocArrFeatured]
         docArrPopular = aDecoder.decodeObject(forKey: "DocArrPopular") as? [GFCHomeScreenDocArrFeatured]
         liveVideos = aDecoder.decodeObject(forKey: "live_videos") as? [GFCHomeScreenLiveVideo]
         speciality = aDecoder.decodeObject(forKey: "speciality") as? [GFCHomeScreenSpeciality]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if docArrFeatured != nil{
			aCoder.encode(docArrFeatured, forKey: "DocArrFeatured")
		}
		if docArrPopular != nil{
			aCoder.encode(docArrPopular, forKey: "DocArrPopular")
		}
		if liveVideos != nil{
			aCoder.encode(liveVideos, forKey: "live_videos")
		}
		if speciality != nil{
			aCoder.encode(speciality, forKey: "speciality")
		}

	}

}
