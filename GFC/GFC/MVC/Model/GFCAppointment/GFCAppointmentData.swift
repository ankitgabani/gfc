//
//	GFCAppointmentData.swift
//
//	Create by iMac on 29/11/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCAppointmentData : NSObject, NSCoding{

	var doctorsAppointment : [GFCAppointmentDoctorsAppointment]!
	var hospitalAppointment : [GFCAppointmentHospitalAppointment]!
	var labAppointment : [GFCAppointmentLabAppointment]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		doctorsAppointment = [GFCAppointmentDoctorsAppointment]()
		if let doctorsAppointmentArray = dictionary["doctors_appointment"] as? [NSDictionary]{
			for dic in doctorsAppointmentArray{
				let value = GFCAppointmentDoctorsAppointment(fromDictionary: dic)
				doctorsAppointment.append(value)
			}
		}
		hospitalAppointment = [GFCAppointmentHospitalAppointment]()
		if let hospitalAppointmentArray = dictionary["hospital_appointment"] as? [NSDictionary]{
			for dic in hospitalAppointmentArray{
				let value = GFCAppointmentHospitalAppointment(fromDictionary: dic)
				hospitalAppointment.append(value)
			}
		}
		labAppointment = [GFCAppointmentLabAppointment]()
		if let labAppointmentArray = dictionary["lab_appointment"] as? [NSDictionary]{
			for dic in labAppointmentArray{
				let value = GFCAppointmentLabAppointment(fromDictionary: dic)
				labAppointment.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if doctorsAppointment != nil{
			var dictionaryElements = [NSDictionary]()
			for doctorsAppointmentElement in doctorsAppointment {
				dictionaryElements.append(doctorsAppointmentElement.toDictionary())
			}
			dictionary["doctors_appointment"] = dictionaryElements
		}
		if hospitalAppointment != nil{
			var dictionaryElements = [NSDictionary]()
			for hospitalAppointmentElement in hospitalAppointment {
				dictionaryElements.append(hospitalAppointmentElement.toDictionary())
			}
			dictionary["hospital_appointment"] = dictionaryElements
		}
		if labAppointment != nil{
			var dictionaryElements = [NSDictionary]()
			for labAppointmentElement in labAppointment {
				dictionaryElements.append(labAppointmentElement.toDictionary())
			}
			dictionary["lab_appointment"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         doctorsAppointment = aDecoder.decodeObject(forKey: "doctors_appointment") as? [GFCAppointmentDoctorsAppointment]
         hospitalAppointment = aDecoder.decodeObject(forKey: "hospital_appointment") as? [GFCAppointmentHospitalAppointment]
         labAppointment = aDecoder.decodeObject(forKey: "lab_appointment") as? [GFCAppointmentLabAppointment]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if doctorsAppointment != nil{
			aCoder.encode(doctorsAppointment, forKey: "doctors_appointment")
		}
		if hospitalAppointment != nil{
			aCoder.encode(hospitalAppointment, forKey: "hospital_appointment")
		}
		if labAppointment != nil{
			aCoder.encode(labAppointment, forKey: "lab_appointment")
		}

	}

}