//
//	GFCAppointmentDoctorsAppointment.swift
//
//	Create by iMac on 29/11/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCAppointmentDoctorsAppointment : NSObject, NSCoding{

	var appointmentId : String!
	var consultantFee : String!
	var createdAt : String!
	var date : String!
	var doctorAddress : String!
	var doctorId : String!
	var doctorName : String!
	var id : String!
	var patientId : String!
	var paymentMode : String!
	var paymentStatus : String!
    var prescriptionDocument1 : [GFCAppointmentDoctorsAppointmentPrescriptionDocument1]!
    var prescriptionDocument1Url : String!
	var prescriptionDocument2 : String!
	var rating : String!
	var time : String!
	var uhid : String!
	var updatedAt : String!
    var appointment_type : String!
    var image_url : String!
    var image : String!
    var quickblox_id : String!
    var email : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		appointmentId = dictionary["appointment_id"] as? String == nil ? "" : dictionary["appointment_id"] as? String
		consultantFee = dictionary["consultant_fee"] as? String == nil ? "" : dictionary["consultant_fee"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		date = dictionary["date"] as? String == nil ? "" : dictionary["date"] as? String
		doctorAddress = dictionary["doctor_address"] as? String == nil ? "" : dictionary["doctor_address"] as? String
		doctorId = dictionary["doctor_id"] as? String == nil ? "" : dictionary["doctor_id"] as? String
		doctorName = dictionary["doctor_name"] as? String == nil ? "" : dictionary["doctor_name"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		patientId = dictionary["patient_id"] as? String == nil ? "" : dictionary["patient_id"] as? String
		paymentMode = dictionary["payment_mode"] as? String == nil ? "" : dictionary["payment_mode"] as? String
		paymentStatus = dictionary["payment_status"] as? String == nil ? "" : dictionary["payment_status"] as? String
        prescriptionDocument1 = [GFCAppointmentDoctorsAppointmentPrescriptionDocument1]()
        if let prescriptionDocument1Array = dictionary["prescription_document1"] as? [NSDictionary]{
            for dic in prescriptionDocument1Array{
                let value = GFCAppointmentDoctorsAppointmentPrescriptionDocument1(fromDictionary: dic)
                prescriptionDocument1.append(value)
            }
        }
		prescriptionDocument1Url = dictionary["prescription_document1_url"] as? String == nil ? "" : dictionary["prescription_document1_url"] as? String
		prescriptionDocument2 = dictionary["prescription_document2"] as? String == nil ? "" : dictionary["prescription_document2"] as? String
		rating = dictionary["rating"] as? String == nil ? "" : dictionary["rating"] as? String
		time = dictionary["time"] as? String == nil ? "" : dictionary["time"] as? String
		uhid = dictionary["uhid"] as? String == nil ? "" : dictionary["uhid"] as? String
		updatedAt = dictionary["updated_at"] as? String == nil ? "" : dictionary["updated_at"] as? String
        appointment_type = dictionary["appointment_type"] as? String == nil ? "" : dictionary["appointment_type"] as? String
        image_url = dictionary["image_url"] as? String == nil ? "" : dictionary["image_url"] as? String
        image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
        quickblox_id = dictionary["quickblox_id"] as? String == nil ? "" : dictionary["quickblox_id"] as? String
        email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String


	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if appointmentId != nil{
			dictionary["appointment_id"] = appointmentId
		}
		if consultantFee != nil{
			dictionary["consultant_fee"] = consultantFee
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if date != nil{
			dictionary["date"] = date
		}
		if doctorAddress != nil{
			dictionary["doctor_address"] = doctorAddress
		}
		if doctorId != nil{
			dictionary["doctor_id"] = doctorId
		}
		if doctorName != nil{
			dictionary["doctor_name"] = doctorName
		}
		if id != nil{
			dictionary["id"] = id
		}
		if patientId != nil{
			dictionary["patient_id"] = patientId
		}
		if paymentMode != nil{
			dictionary["payment_mode"] = paymentMode
		}
		if paymentStatus != nil{
			dictionary["payment_status"] = paymentStatus
		}
		if prescriptionDocument1 != nil{
            var dictionaryElements = [NSDictionary]()
            for prescriptionDocument1Element in prescriptionDocument1 {
                dictionaryElements.append(prescriptionDocument1Element.toDictionary())
            }
            dictionary["prescription_document1"] = dictionaryElements

		}
		if prescriptionDocument1Url != nil{
			dictionary["prescription_document1_url"] = prescriptionDocument1Url
		}
		if prescriptionDocument2 != nil{
			dictionary["prescription_document2"] = prescriptionDocument2
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if time != nil{
			dictionary["time"] = time
		}
		if uhid != nil{
			dictionary["uhid"] = uhid
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
        if appointment_type != nil{
            dictionary["appointment_type"] = appointment_type
        }
        if image_url != nil{
            dictionary["image_url"] = image_url
        } 
        if image != nil{
            dictionary["image"] = image
        }
        if quickblox_id != nil{
            dictionary["quickblox_id"] = quickblox_id
        }
        if email != nil{
            dictionary["email"] = email
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         appointmentId = aDecoder.decodeObject(forKey: "appointment_id") as? String
         consultantFee = aDecoder.decodeObject(forKey: "consultant_fee") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         date = aDecoder.decodeObject(forKey: "date") as? String
         doctorAddress = aDecoder.decodeObject(forKey: "doctor_address") as? String
         doctorId = aDecoder.decodeObject(forKey: "doctor_id") as? String
         doctorName = aDecoder.decodeObject(forKey: "doctor_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         patientId = aDecoder.decodeObject(forKey: "patient_id") as? String
         paymentMode = aDecoder.decodeObject(forKey: "payment_mode") as? String
         paymentStatus = aDecoder.decodeObject(forKey: "payment_status") as? String
        prescriptionDocument1 = aDecoder.decodeObject(forKey: "prescription_document1") as? [GFCAppointmentDoctorsAppointmentPrescriptionDocument1]
        prescriptionDocument1Url = aDecoder.decodeObject(forKey: "prescription_document1_url") as? String
         prescriptionDocument2 = aDecoder.decodeObject(forKey: "prescription_document2") as? String
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         time = aDecoder.decodeObject(forKey: "time") as? String
         uhid = aDecoder.decodeObject(forKey: "uhid") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        appointment_type = aDecoder.decodeObject(forKey: "appointment_type") as? String
        image_url = aDecoder.decodeObject(forKey: "image_url") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        quickblox_id = aDecoder.decodeObject(forKey: "quickblox_id") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if appointmentId != nil{
			aCoder.encode(appointmentId, forKey: "appointment_id")
		}
		if consultantFee != nil{
			aCoder.encode(consultantFee, forKey: "consultant_fee")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if date != nil{
			aCoder.encode(date, forKey: "date")
		}
		if doctorAddress != nil{
			aCoder.encode(doctorAddress, forKey: "doctor_address")
		}
		if doctorId != nil{
			aCoder.encode(doctorId, forKey: "doctor_id")
		}
		if doctorName != nil{
			aCoder.encode(doctorName, forKey: "doctor_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if patientId != nil{
			aCoder.encode(patientId, forKey: "patient_id")
		}
		if paymentMode != nil{
			aCoder.encode(paymentMode, forKey: "payment_mode")
		}
		if paymentStatus != nil{
			aCoder.encode(paymentStatus, forKey: "payment_status")
		}
		if prescriptionDocument1 != nil{
			aCoder.encode(prescriptionDocument1, forKey: "prescription_document1")
		}
		if prescriptionDocument1Url != nil{
			aCoder.encode(prescriptionDocument1Url, forKey: "prescription_document1_url")
		}
		if prescriptionDocument2 != nil{
			aCoder.encode(prescriptionDocument2, forKey: "prescription_document2")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if time != nil{
			aCoder.encode(time, forKey: "time")
		}
		if uhid != nil{
			aCoder.encode(uhid, forKey: "uhid")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
        if appointment_type != nil{
            aCoder.encode(appointment_type, forKey: "appointment_type")
        } 
        if image_url != nil{
            aCoder.encode(image_url, forKey: "image_url")
        } 
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if quickblox_id != nil{
            aCoder.encode(quickblox_id, forKey: "quickblox_id")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }

	}

}



class GFCAppointmentDoctorsAppointmentPrescriptionDocument1 : NSObject, NSCoding{

    var prescriptionFiles : String!


    /**
     * Overiding init method
     */
    init(fromDictionary dictionary: NSDictionary)
    {
        super.init()
        parseJSONData(fromDictionary: dictionary)
    }

    /**
     * Overiding init method
     */
    override init(){
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    @objc func parseJSONData(fromDictionary dictionary: NSDictionary)
    {
        prescriptionFiles = dictionary["prescription_files"] as? String == nil ? "" : dictionary["prescription_files"] as? String
    }

    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if prescriptionFiles != nil{
            dictionary["prescription_files"] = prescriptionFiles
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         prescriptionFiles = aDecoder.decodeObject(forKey: "prescription_files") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder)
    {
        if prescriptionFiles != nil{
            aCoder.encode(prescriptionFiles, forKey: "prescription_files")
        }

    }

}
