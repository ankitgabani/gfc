//
//	GFCAppointmentHospitalAppointment.swift
//
//	Create by iMac on 29/11/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCAppointmentHospitalAppointment : NSObject, NSCoding{

	var accountStatus : String!
	var address : String!
	var addressType : String!
	var appointId : String!
	var appointmentDate : String!
	var appointmentId : String!
	var appointmentTime : String!
	var archivePatients : String!
	var archiveReports : String!
	var assignUser : String!
	var attendantAddress : String!
	var attendantName : String!
	var attendantPhone : String!
	var bestDays : String!
	var bestTime : String!
	var createdBy : String!
	var createdAt : String!
	var gender : String!
	var hospitalId : String!
	var noOfPerson : String!
	var patientId : String!
	var patientCity : String!
	var patientCountry : String!
	var patientEmail : String!
	var patientName : String!
	var patientPassport : String!
	var patientState : String!
	var patientStreet : String!
	var paymentMode : String!
	var paymentStatus : String!
	var phone : String!
	var pincode : String!
	var profileImage : String!
	var rating : String!
	var relation : String!
	var servicesInterestedIn : String!
	var source : String!
	var status : String!
	var testId : String!
	var updatedAt : String!
	var updatedBy : String!
    var hospital_name : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		accountStatus = dictionary["accountStatus"] as? String == nil ? "" : dictionary["accountStatus"] as? String
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		addressType = dictionary["address_type"] as? String == nil ? "" : dictionary["address_type"] as? String
		appointId = dictionary["appoint_id"] as? String == nil ? "" : dictionary["appoint_id"] as? String
		appointmentDate = dictionary["appointment_date"] as? String == nil ? "" : dictionary["appointment_date"] as? String
		appointmentId = dictionary["appointment_id"] as? String == nil ? "" : dictionary["appointment_id"] as? String
		appointmentTime = dictionary["appointment_time"] as? String == nil ? "" : dictionary["appointment_time"] as? String
		archivePatients = dictionary["archive_patients"] as? String == nil ? "" : dictionary["archive_patients"] as? String
		archiveReports = dictionary["archive_reports"] as? String == nil ? "" : dictionary["archive_reports"] as? String
		assignUser = dictionary["assign_user"] as? String == nil ? "" : dictionary["assign_user"] as? String
		attendantAddress = dictionary["attendant_address"] as? String == nil ? "" : dictionary["attendant_address"] as? String
		attendantName = dictionary["attendant_name"] as? String == nil ? "" : dictionary["attendant_name"] as? String
		attendantPhone = dictionary["attendant_phone"] as? String == nil ? "" : dictionary["attendant_phone"] as? String
		bestDays = dictionary["best_days"] as? String == nil ? "" : dictionary["best_days"] as? String
		bestTime = dictionary["best_time"] as? String == nil ? "" : dictionary["best_time"] as? String
		createdBy = dictionary["createdBy"] as? String == nil ? "" : dictionary["createdBy"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		gender = dictionary["gender"] as? String == nil ? "" : dictionary["gender"] as? String
		hospitalId = dictionary["hospitalId"] as? String == nil ? "" : dictionary["hospitalId"] as? String
		noOfPerson = dictionary["no_of_person"] as? String == nil ? "" : dictionary["no_of_person"] as? String
		patientId = dictionary["patientId"] as? String == nil ? "" : dictionary["patientId"] as? String
		patientCity = dictionary["patient_city"] as? String == nil ? "" : dictionary["patient_city"] as? String
		patientCountry = dictionary["patient_country"] as? String == nil ? "" : dictionary["patient_country"] as? String
		patientEmail = dictionary["patient_email"] as? String == nil ? "" : dictionary["patient_email"] as? String
		patientName = dictionary["patient_name"] as? String == nil ? "" : dictionary["patient_name"] as? String
		patientPassport = dictionary["patient_passport"] as? String == nil ? "" : dictionary["patient_passport"] as? String
		patientState = dictionary["patient_state"] as? String == nil ? "" : dictionary["patient_state"] as? String
		patientStreet = dictionary["patient_street"] as? String == nil ? "" : dictionary["patient_street"] as? String
		paymentMode = dictionary["payment_mode"] as? String == nil ? "" : dictionary["payment_mode"] as? String
		paymentStatus = dictionary["payment_status"] as? String == nil ? "" : dictionary["payment_status"] as? String
		phone = dictionary["phone"] as? String == nil ? "" : dictionary["phone"] as? String
		pincode = dictionary["pincode"] as? String == nil ? "" : dictionary["pincode"] as? String
		profileImage = dictionary["profileImage"] as? String == nil ? "" : dictionary["profileImage"] as? String
        rating = dictionary["rating"] as? String == nil ? "" : dictionary["rating"] as? String
		relation = dictionary["relation"] as? String == nil ? "" : dictionary["relation"] as? String
		servicesInterestedIn = dictionary["services_interested_in"] as? String == nil ? "" : dictionary["services_interested_in"] as? String
		source = dictionary["source"] as? String == nil ? "" : dictionary["source"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		testId = dictionary["testId"] as? String == nil ? "" : dictionary["testId"] as? String
		updatedAt = dictionary["updatedAt"] as? String == nil ? "" : dictionary["updatedAt"] as? String
		updatedBy = dictionary["updatedBy"] as? String == nil ? "" : dictionary["updatedBy"] as? String
        hospital_name = dictionary["hospital_name"] as? String == nil ? "" : dictionary["hospital_name"] as? String

	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if accountStatus != nil{
			dictionary["accountStatus"] = accountStatus
		}
		if address != nil{
			dictionary["address"] = address
		}
		if addressType != nil{
			dictionary["address_type"] = addressType
		}
		if appointId != nil{
			dictionary["appoint_id"] = appointId
		}
		if appointmentDate != nil{
			dictionary["appointment_date"] = appointmentDate
		}
		if appointmentId != nil{
			dictionary["appointment_id"] = appointmentId
		}
		if appointmentTime != nil{
			dictionary["appointment_time"] = appointmentTime
		}
		if archivePatients != nil{
			dictionary["archive_patients"] = archivePatients
		}
		if archiveReports != nil{
			dictionary["archive_reports"] = archiveReports
		}
		if assignUser != nil{
			dictionary["assign_user"] = assignUser
		}
		if attendantAddress != nil{
			dictionary["attendant_address"] = attendantAddress
		}
		if attendantName != nil{
			dictionary["attendant_name"] = attendantName
		}
		if attendantPhone != nil{
			dictionary["attendant_phone"] = attendantPhone
		}
		if bestDays != nil{
			dictionary["best_days"] = bestDays
		}
		if bestTime != nil{
			dictionary["best_time"] = bestTime
		}
		if createdBy != nil{
			dictionary["createdBy"] = createdBy
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if hospitalId != nil{
			dictionary["hospitalId"] = hospitalId
		}
		if noOfPerson != nil{
			dictionary["no_of_person"] = noOfPerson
		}
		if patientId != nil{
			dictionary["patientId"] = patientId
		}
		if patientCity != nil{
			dictionary["patient_city"] = patientCity
		}
		if patientCountry != nil{
			dictionary["patient_country"] = patientCountry
		}
		if patientEmail != nil{
			dictionary["patient_email"] = patientEmail
		}
		if patientName != nil{
			dictionary["patient_name"] = patientName
		}
		if patientPassport != nil{
			dictionary["patient_passport"] = patientPassport
		}
		if patientState != nil{
			dictionary["patient_state"] = patientState
		}
		if patientStreet != nil{
			dictionary["patient_street"] = patientStreet
		}
		if paymentMode != nil{
			dictionary["payment_mode"] = paymentMode
		}
		if paymentStatus != nil{
			dictionary["payment_status"] = paymentStatus
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if pincode != nil{
			dictionary["pincode"] = pincode
		}
		if profileImage != nil{
			dictionary["profileImage"] = profileImage
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if relation != nil{
			dictionary["relation"] = relation
		}
		if servicesInterestedIn != nil{
			dictionary["services_interested_in"] = servicesInterestedIn
		}
		if source != nil{
			dictionary["source"] = source
		}
		if status != nil{
			dictionary["status"] = status
		}
		if testId != nil{
			dictionary["testId"] = testId
		}
		if updatedAt != nil{
			dictionary["updatedAt"] = updatedAt
		}
		if updatedBy != nil{
			dictionary["updatedBy"] = updatedBy
		}
        if hospital_name != nil{
            dictionary["hospital_name"] = hospital_name
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accountStatus = aDecoder.decodeObject(forKey: "accountStatus") as? String
         address = aDecoder.decodeObject(forKey: "address") as? String
         addressType = aDecoder.decodeObject(forKey: "address_type") as? String
         appointId = aDecoder.decodeObject(forKey: "appoint_id") as? String
         appointmentDate = aDecoder.decodeObject(forKey: "appointment_date") as? String
         appointmentId = aDecoder.decodeObject(forKey: "appointment_id") as? String
         appointmentTime = aDecoder.decodeObject(forKey: "appointment_time") as? String
         archivePatients = aDecoder.decodeObject(forKey: "archive_patients") as? String
         archiveReports = aDecoder.decodeObject(forKey: "archive_reports") as? String
         assignUser = aDecoder.decodeObject(forKey: "assign_user") as? String
         attendantAddress = aDecoder.decodeObject(forKey: "attendant_address") as? String
         attendantName = aDecoder.decodeObject(forKey: "attendant_name") as? String
         attendantPhone = aDecoder.decodeObject(forKey: "attendant_phone") as? String
         bestDays = aDecoder.decodeObject(forKey: "best_days") as? String
         bestTime = aDecoder.decodeObject(forKey: "best_time") as? String
         createdBy = aDecoder.decodeObject(forKey: "createdBy") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         hospitalId = aDecoder.decodeObject(forKey: "hospitalId") as? String
         noOfPerson = aDecoder.decodeObject(forKey: "no_of_person") as? String
         patientId = aDecoder.decodeObject(forKey: "patientId") as? String
         patientCity = aDecoder.decodeObject(forKey: "patient_city") as? String
         patientCountry = aDecoder.decodeObject(forKey: "patient_country") as? String
         patientEmail = aDecoder.decodeObject(forKey: "patient_email") as? String
         patientName = aDecoder.decodeObject(forKey: "patient_name") as? String
         patientPassport = aDecoder.decodeObject(forKey: "patient_passport") as? String
         patientState = aDecoder.decodeObject(forKey: "patient_state") as? String
         patientStreet = aDecoder.decodeObject(forKey: "patient_street") as? String
         paymentMode = aDecoder.decodeObject(forKey: "payment_mode") as? String
         paymentStatus = aDecoder.decodeObject(forKey: "payment_status") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         pincode = aDecoder.decodeObject(forKey: "pincode") as? String
         profileImage = aDecoder.decodeObject(forKey: "profileImage") as? String
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         relation = aDecoder.decodeObject(forKey: "relation") as? String
         servicesInterestedIn = aDecoder.decodeObject(forKey: "services_interested_in") as? String
         source = aDecoder.decodeObject(forKey: "source") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         testId = aDecoder.decodeObject(forKey: "testId") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updatedBy") as? String
        hospital_name = aDecoder.decodeObject(forKey: "hospital_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if accountStatus != nil{
			aCoder.encode(accountStatus, forKey: "accountStatus")
		}
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if addressType != nil{
			aCoder.encode(addressType, forKey: "address_type")
		}
		if appointId != nil{
			aCoder.encode(appointId, forKey: "appoint_id")
		}
		if appointmentDate != nil{
			aCoder.encode(appointmentDate, forKey: "appointment_date")
		}
		if appointmentId != nil{
			aCoder.encode(appointmentId, forKey: "appointment_id")
		}
		if appointmentTime != nil{
			aCoder.encode(appointmentTime, forKey: "appointment_time")
		}
		if archivePatients != nil{
			aCoder.encode(archivePatients, forKey: "archive_patients")
		}
		if archiveReports != nil{
			aCoder.encode(archiveReports, forKey: "archive_reports")
		}
		if assignUser != nil{
			aCoder.encode(assignUser, forKey: "assign_user")
		}
		if attendantAddress != nil{
			aCoder.encode(attendantAddress, forKey: "attendant_address")
		}
		if attendantName != nil{
			aCoder.encode(attendantName, forKey: "attendant_name")
		}
		if attendantPhone != nil{
			aCoder.encode(attendantPhone, forKey: "attendant_phone")
		}
		if bestDays != nil{
			aCoder.encode(bestDays, forKey: "best_days")
		}
		if bestTime != nil{
			aCoder.encode(bestTime, forKey: "best_time")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "createdBy")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if gender != nil{
			aCoder.encode(gender, forKey: "gender")
		}
		if hospitalId != nil{
			aCoder.encode(hospitalId, forKey: "hospitalId")
		}
		if noOfPerson != nil{
			aCoder.encode(noOfPerson, forKey: "no_of_person")
		}
		if patientId != nil{
			aCoder.encode(patientId, forKey: "patientId")
		}
		if patientCity != nil{
			aCoder.encode(patientCity, forKey: "patient_city")
		}
		if patientCountry != nil{
			aCoder.encode(patientCountry, forKey: "patient_country")
		}
		if patientEmail != nil{
			aCoder.encode(patientEmail, forKey: "patient_email")
		}
		if patientName != nil{
			aCoder.encode(patientName, forKey: "patient_name")
		}
		if patientPassport != nil{
			aCoder.encode(patientPassport, forKey: "patient_passport")
		}
		if patientState != nil{
			aCoder.encode(patientState, forKey: "patient_state")
		}
		if patientStreet != nil{
			aCoder.encode(patientStreet, forKey: "patient_street")
		}
		if paymentMode != nil{
			aCoder.encode(paymentMode, forKey: "payment_mode")
		}
		if paymentStatus != nil{
			aCoder.encode(paymentStatus, forKey: "payment_status")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if pincode != nil{
			aCoder.encode(pincode, forKey: "pincode")
		}
		if profileImage != nil{
			aCoder.encode(profileImage, forKey: "profileImage")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if relation != nil{
			aCoder.encode(relation, forKey: "relation")
		}
		if servicesInterestedIn != nil{
			aCoder.encode(servicesInterestedIn, forKey: "services_interested_in")
		}
		if source != nil{
			aCoder.encode(source, forKey: "source")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if testId != nil{
			aCoder.encode(testId, forKey: "testId")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updatedAt")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updatedBy")
		}
        if hospital_name != nil{
            aCoder.encode(hospital_name, forKey: "hospital_name")
        }
	}

}
