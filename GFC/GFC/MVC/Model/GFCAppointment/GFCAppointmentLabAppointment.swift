//
//	GFCAppointmentLabAppointment.swift
//
//	Create by iMac on 29/11/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCAppointmentLabAppointment : NSObject, NSCoding{

	var address : String!
	var addressType : String!
	var amount : String!
	var appointmentId : String!
	var bookingDate : String!
	var bookingStatus : String!
	var city : String!
	var country : String!
	var created : String!
	var createdBy : String!
	var id : String!
	var labId : String!
	var labReport : [String]!
	var labname : String!
	var lat : String!
	var lon : String!
	var mobile : String!
	var name : String!
	var noOfPerson : String!
	var pateintId : String!
	var paymentMode : String!
	var paymentStatus : String!
	var pincode : String!
	var rating : String!
	var slotEndTime : String!
	var slotStartTime : String!
	var state : String!
	var status : String!
	var testId : String!
	var testname : String!
	var title : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		addressType = dictionary["address_type"] as? String == nil ? "" : dictionary["address_type"] as? String
		amount = dictionary["amount"] as? String == nil ? "" : dictionary["amount"] as? String
		appointmentId = dictionary["appointment_id"] as? String == nil ? "" : dictionary["appointment_id"] as? String
		bookingDate = dictionary["booking_date"] as? String == nil ? "" : dictionary["booking_date"] as? String
		bookingStatus = dictionary["booking_status"] as? String == nil ? "" : dictionary["booking_status"] as? String
		city = dictionary["city"] as? String == nil ? "" : dictionary["city"] as? String
		country = dictionary["country"] as? String == nil ? "" : dictionary["country"] as? String
		created = dictionary["created"] as? String == nil ? "" : dictionary["created"] as? String
		createdBy = dictionary["created_by"] as? String == nil ? "" : dictionary["created_by"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		labId = dictionary["lab_id"] as? String == nil ? "" : dictionary["lab_id"] as? String
		labReport = dictionary["lab_report"] as? [String] == nil ? [] : dictionary["lab_report"] as? [String]
		labname = dictionary["labname"] as? String == nil ? "" : dictionary["labname"] as? String
		lat = dictionary["lat"] as? String == nil ? "" : dictionary["lat"] as? String
		lon = dictionary["lon"] as? String == nil ? "" : dictionary["lon"] as? String
		mobile = dictionary["mobile"] as? String == nil ? "" : dictionary["mobile"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		noOfPerson = dictionary["no_of_person"] as? String == nil ? "" : dictionary["no_of_person"] as? String
		pateintId = dictionary["pateintId"] as? String == nil ? "" : dictionary["pateintId"] as? String
		paymentMode = dictionary["payment_mode"] as? String == nil ? "" : dictionary["payment_mode"] as? String
		paymentStatus = dictionary["payment_status"] as? String == nil ? "" : dictionary["payment_status"] as? String
		pincode = dictionary["pincode"] as? String == nil ? "" : dictionary["pincode"] as? String
		rating = dictionary["rating"] as? String == nil ? "" : dictionary["rating"] as? String
		slotEndTime = dictionary["slot_end_time"] as? String == nil ? "" : dictionary["slot_end_time"] as? String
		slotStartTime = dictionary["slot_start_time"] as? String == nil ? "" : dictionary["slot_start_time"] as? String
		state = dictionary["state"] as? String == nil ? "" : dictionary["state"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		testId = dictionary["testId"] as? String == nil ? "" : dictionary["testId"] as? String
		testname = dictionary["testname"] as? String == nil ? "" : dictionary["testname"] as? String
		title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if addressType != nil{
			dictionary["address_type"] = addressType
		}
		if amount != nil{
			dictionary["amount"] = amount
		}
		if appointmentId != nil{
			dictionary["appointment_id"] = appointmentId
		}
		if bookingDate != nil{
			dictionary["booking_date"] = bookingDate
		}
		if bookingStatus != nil{
			dictionary["booking_status"] = bookingStatus
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if created != nil{
			dictionary["created"] = created
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if id != nil{
			dictionary["id"] = id
		}
		if labId != nil{
			dictionary["lab_id"] = labId
		}
		if labReport != nil{
			dictionary["lab_report"] = labReport
		}
		if labname != nil{
			dictionary["labname"] = labname
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if lon != nil{
			dictionary["lon"] = lon
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if name != nil{
			dictionary["name"] = name
		}
		if noOfPerson != nil{
			dictionary["no_of_person"] = noOfPerson
		}
		if pateintId != nil{
			dictionary["pateintId"] = pateintId
		}
		if paymentMode != nil{
			dictionary["payment_mode"] = paymentMode
		}
		if paymentStatus != nil{
			dictionary["payment_status"] = paymentStatus
		}
		if pincode != nil{
			dictionary["pincode"] = pincode
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if slotEndTime != nil{
			dictionary["slot_end_time"] = slotEndTime
		}
		if slotStartTime != nil{
			dictionary["slot_start_time"] = slotStartTime
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if testId != nil{
			dictionary["testId"] = testId
		}
		if testname != nil{
			dictionary["testname"] = testname
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         addressType = aDecoder.decodeObject(forKey: "address_type") as? String
         amount = aDecoder.decodeObject(forKey: "amount") as? String
         appointmentId = aDecoder.decodeObject(forKey: "appointment_id") as? String
         bookingDate = aDecoder.decodeObject(forKey: "booking_date") as? String
         bookingStatus = aDecoder.decodeObject(forKey: "booking_status") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         created = aDecoder.decodeObject(forKey: "created") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         labId = aDecoder.decodeObject(forKey: "lab_id") as? String
         labReport = aDecoder.decodeObject(forKey: "lab_report") as? [String]
         labname = aDecoder.decodeObject(forKey: "labname") as? String
         lat = aDecoder.decodeObject(forKey: "lat") as? String
         lon = aDecoder.decodeObject(forKey: "lon") as? String
         mobile = aDecoder.decodeObject(forKey: "mobile") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         noOfPerson = aDecoder.decodeObject(forKey: "no_of_person") as? String
         pateintId = aDecoder.decodeObject(forKey: "pateintId") as? String
         paymentMode = aDecoder.decodeObject(forKey: "payment_mode") as? String
         paymentStatus = aDecoder.decodeObject(forKey: "payment_status") as? String
         pincode = aDecoder.decodeObject(forKey: "pincode") as? String
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         slotEndTime = aDecoder.decodeObject(forKey: "slot_end_time") as? String
         slotStartTime = aDecoder.decodeObject(forKey: "slot_start_time") as? String
         state = aDecoder.decodeObject(forKey: "state") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         testId = aDecoder.decodeObject(forKey: "testId") as? String
         testname = aDecoder.decodeObject(forKey: "testname") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if addressType != nil{
			aCoder.encode(addressType, forKey: "address_type")
		}
		if amount != nil{
			aCoder.encode(amount, forKey: "amount")
		}
		if appointmentId != nil{
			aCoder.encode(appointmentId, forKey: "appointment_id")
		}
		if bookingDate != nil{
			aCoder.encode(bookingDate, forKey: "booking_date")
		}
		if bookingStatus != nil{
			aCoder.encode(bookingStatus, forKey: "booking_status")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if created != nil{
			aCoder.encode(created, forKey: "created")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if labId != nil{
			aCoder.encode(labId, forKey: "lab_id")
		}
		if labReport != nil{
			aCoder.encode(labReport, forKey: "lab_report")
		}
		if labname != nil{
			aCoder.encode(labname, forKey: "labname")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lon != nil{
			aCoder.encode(lon, forKey: "lon")
		}
		if mobile != nil{
			aCoder.encode(mobile, forKey: "mobile")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if noOfPerson != nil{
			aCoder.encode(noOfPerson, forKey: "no_of_person")
		}
		if pateintId != nil{
			aCoder.encode(pateintId, forKey: "pateintId")
		}
		if paymentMode != nil{
			aCoder.encode(paymentMode, forKey: "payment_mode")
		}
		if paymentStatus != nil{
			aCoder.encode(paymentStatus, forKey: "payment_status")
		}
		if pincode != nil{
			aCoder.encode(pincode, forKey: "pincode")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if slotEndTime != nil{
			aCoder.encode(slotEndTime, forKey: "slot_end_time")
		}
		if slotStartTime != nil{
			aCoder.encode(slotStartTime, forKey: "slot_start_time")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if testId != nil{
			aCoder.encode(testId, forKey: "testId")
		}
		if testname != nil{
			aCoder.encode(testname, forKey: "testname")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}
