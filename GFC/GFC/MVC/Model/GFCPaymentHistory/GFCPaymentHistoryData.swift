//
//	GFCPaymentHistoryData.swift
//
//	Create by iMac on 11/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCPaymentHistoryData : NSObject, NSCoding{

	var amount : String!
	var currency : String!
	var date : String!
	var image : String!
	var note : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		amount = dictionary["amount"] as? String == nil ? "" : dictionary["amount"] as? String
		currency = dictionary["currency"] as? String == nil ? "" : dictionary["currency"] as? String
		date = dictionary["date"] as? String == nil ? "" : dictionary["date"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		note = dictionary["note"] as? String == nil ? "" : dictionary["note"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if amount != nil{
			dictionary["amount"] = amount
		}
		if currency != nil{
			dictionary["currency"] = currency
		}
		if date != nil{
			dictionary["date"] = date
		}
		if image != nil{
			dictionary["image"] = image
		}
		if note != nil{
			dictionary["note"] = note
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         amount = aDecoder.decodeObject(forKey: "amount") as? String
         currency = aDecoder.decodeObject(forKey: "currency") as? String
         date = aDecoder.decodeObject(forKey: "date") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         note = aDecoder.decodeObject(forKey: "note") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if amount != nil{
			aCoder.encode(amount, forKey: "amount")
		}
		if currency != nil{
			aCoder.encode(currency, forKey: "currency")
		}
		if date != nil{
			aCoder.encode(date, forKey: "date")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if note != nil{
			aCoder.encode(note, forKey: "note")
		}

	}

}