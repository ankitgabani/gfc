//
//	GFCHospitalDetailTreatment.swift
//
//	Create by iMac on 12/4/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCHospitalDetailTreatment : NSObject, NSCoding{

	var createdAt : String!
	var images : [GFCHospitalDetailImage]!
	var status : String!
	var treatmentDescription : String!
	var treatmentId : String!
	var treatmentName : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		images = [GFCHospitalDetailImage]()
		if let imagesArray = dictionary["images"] as? [NSDictionary]{
			for dic in imagesArray{
				let value = GFCHospitalDetailImage(fromDictionary: dic)
				images.append(value)
			}
		}
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		treatmentDescription = dictionary["treatmentDescription"] as? String == nil ? "" : dictionary["treatmentDescription"] as? String
		treatmentId = dictionary["treatmentId"] as? String == nil ? "" : dictionary["treatmentId"] as? String
		treatmentName = dictionary["treatmentName"] as? String == nil ? "" : dictionary["treatmentName"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if images != nil{
			var dictionaryElements = [NSDictionary]()
			for imagesElement in images {
				dictionaryElements.append(imagesElement.toDictionary())
			}
			dictionary["images"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		if treatmentDescription != nil{
			dictionary["treatmentDescription"] = treatmentDescription
		}
		if treatmentId != nil{
			dictionary["treatmentId"] = treatmentId
		}
		if treatmentName != nil{
			dictionary["treatmentName"] = treatmentName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         images = aDecoder.decodeObject(forKey: "images") as? [GFCHospitalDetailImage]
         status = aDecoder.decodeObject(forKey: "status") as? String
         treatmentDescription = aDecoder.decodeObject(forKey: "treatmentDescription") as? String
         treatmentId = aDecoder.decodeObject(forKey: "treatmentId") as? String
         treatmentName = aDecoder.decodeObject(forKey: "treatmentName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if images != nil{
			aCoder.encode(images, forKey: "images")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if treatmentDescription != nil{
			aCoder.encode(treatmentDescription, forKey: "treatmentDescription")
		}
		if treatmentId != nil{
			aCoder.encode(treatmentId, forKey: "treatmentId")
		}
		if treatmentName != nil{
			aCoder.encode(treatmentName, forKey: "treatmentName")
		}

	}

}