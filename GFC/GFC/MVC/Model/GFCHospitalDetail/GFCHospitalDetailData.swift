//
//	GFCHospitalDetailData.swift
//
//	Create by iMac on 12/4/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCHospitalDetailData : NSObject, NSCoding{

	var accountStatus : String!
	var address : String!
	var body : String!
	var cityId : String!
	var cityName : String!
	var countryId : String!
	var countryName : String!
	var createdAt : String!
	var createdDate : String!
	var currency : String!
	var descriptionField : String!
	var email : String!
	var fcmToken : String!
	var hospitalImgArr : [String]!
	var hospitalImgURL : String!
	var id : String!
	var latitude : String!
	var longitude : String!
	var mobileNumber : String!
	var name : String!
	var passport : String!
	var phoneCode : String!
	var profileImage : String!
	var service : String!
	var servicesImgURL : String!
	var sortname : String!
	var stateId : String!
	var stateName : String!
	var status : String!
	var treatment : String!
	var treatmentImgURL : String!
	var treatments : [GFCHospitalDetailTreatment]!
	var updatedAt : String!
	var updatedBy : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		accountStatus = dictionary["accountStatus"] as? String == nil ? "" : dictionary["accountStatus"] as? String
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		body = dictionary["body"] as? String == nil ? "" : dictionary["body"] as? String
		cityId = dictionary["city_id"] as? String == nil ? "" : dictionary["city_id"] as? String
		cityName = dictionary["city_name"] as? String == nil ? "" : dictionary["city_name"] as? String
		countryId = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
		countryName = dictionary["country_name"] as? String == nil ? "" : dictionary["country_name"] as? String
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		createdDate = dictionary["created_date"] as? String == nil ? "" : dictionary["created_date"] as? String
		currency = dictionary["currency"] as? String == nil ? "" : dictionary["currency"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		fcmToken = dictionary["fcmToken"] as? String == nil ? "" : dictionary["fcmToken"] as? String
		hospitalImgArr = dictionary["hospital_imgArr"] as? [String] == nil ? [] : dictionary["hospital_imgArr"] as? [String]
		hospitalImgURL = dictionary["hospital_imgURL"] as? String == nil ? "" : dictionary["hospital_imgURL"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		latitude = dictionary["latitude"] as? String == nil ? "" : dictionary["latitude"] as? String
		longitude = dictionary["longitude"] as? String == nil ? "" : dictionary["longitude"] as? String
		mobileNumber = dictionary["mobileNumber"] as? String == nil ? "" : dictionary["mobileNumber"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		passport = dictionary["passport"] as? String == nil ? "" : dictionary["passport"] as? String
		phoneCode = dictionary["phone_code"] as? String == nil ? "" : dictionary["phone_code"] as? String
		profileImage = dictionary["profileImage"] as? String == nil ? "" : dictionary["profileImage"] as? String
		service = dictionary["service"] as? String == nil ? "" : dictionary["service"] as? String
		servicesImgURL = dictionary["services_imgURL"] as? String == nil ? "" : dictionary["services_imgURL"] as? String
		sortname = dictionary["sortname"] as? String == nil ? "" : dictionary["sortname"] as? String
		stateId = dictionary["state_id"] as? String == nil ? "" : dictionary["state_id"] as? String
		stateName = dictionary["state_name"] as? String == nil ? "" : dictionary["state_name"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		treatment = dictionary["treatment"] as? String == nil ? "" : dictionary["treatment"] as? String
		treatmentImgURL = dictionary["treatment_imgURL"] as? String == nil ? "" : dictionary["treatment_imgURL"] as? String
		treatments = [GFCHospitalDetailTreatment]()
		if let treatmentsArray = dictionary["treatments"] as? [NSDictionary]{
			for dic in treatmentsArray{
				let value = GFCHospitalDetailTreatment(fromDictionary: dic)
				treatments.append(value)
			}
		}
		updatedAt = dictionary["updatedAt"] as? String == nil ? "" : dictionary["updatedAt"] as? String
		updatedBy = dictionary["updatedBy"] as? String == nil ? "" : dictionary["updatedBy"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if accountStatus != nil{
			dictionary["accountStatus"] = accountStatus
		}
		if address != nil{
			dictionary["address"] = address
		}
		if body != nil{
			dictionary["body"] = body
		}
		if cityId != nil{
			dictionary["city_id"] = cityId
		}
		if cityName != nil{
			dictionary["city_name"] = cityName
		}
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if countryName != nil{
			dictionary["country_name"] = countryName
		}
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if createdDate != nil{
			dictionary["created_date"] = createdDate
		}
		if currency != nil{
			dictionary["currency"] = currency
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if email != nil{
			dictionary["email"] = email
		}
		if fcmToken != nil{
			dictionary["fcmToken"] = fcmToken
		}
		if hospitalImgArr != nil{
			dictionary["hospital_imgArr"] = hospitalImgArr
		}
		if hospitalImgURL != nil{
			dictionary["hospital_imgURL"] = hospitalImgURL
		}
		if id != nil{
			dictionary["id"] = id
		}
		if latitude != nil{
			dictionary["latitude"] = latitude
		}
		if longitude != nil{
			dictionary["longitude"] = longitude
		}
		if mobileNumber != nil{
			dictionary["mobileNumber"] = mobileNumber
		}
		if name != nil{
			dictionary["name"] = name
		}
		if passport != nil{
			dictionary["passport"] = passport
		}
		if phoneCode != nil{
			dictionary["phone_code"] = phoneCode
		}
		if profileImage != nil{
			dictionary["profileImage"] = profileImage
		}
		if service != nil{
			dictionary["service"] = service
		}
		if servicesImgURL != nil{
			dictionary["services_imgURL"] = servicesImgURL
		}
		if sortname != nil{
			dictionary["sortname"] = sortname
		}
		if stateId != nil{
			dictionary["state_id"] = stateId
		}
		if stateName != nil{
			dictionary["state_name"] = stateName
		}
		if status != nil{
			dictionary["status"] = status
		}
		if treatment != nil{
			dictionary["treatment"] = treatment
		}
		if treatmentImgURL != nil{
			dictionary["treatment_imgURL"] = treatmentImgURL
		}
		if treatments != nil{
			var dictionaryElements = [NSDictionary]()
			for treatmentsElement in treatments {
				dictionaryElements.append(treatmentsElement.toDictionary())
			}
			dictionary["treatments"] = dictionaryElements
		}
		if updatedAt != nil{
			dictionary["updatedAt"] = updatedAt
		}
		if updatedBy != nil{
			dictionary["updatedBy"] = updatedBy
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accountStatus = aDecoder.decodeObject(forKey: "accountStatus") as? String
         address = aDecoder.decodeObject(forKey: "address") as? String
         body = aDecoder.decodeObject(forKey: "body") as? String
         cityId = aDecoder.decodeObject(forKey: "city_id") as? String
         cityName = aDecoder.decodeObject(forKey: "city_name") as? String
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         countryName = aDecoder.decodeObject(forKey: "country_name") as? String
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         createdDate = aDecoder.decodeObject(forKey: "created_date") as? String
         currency = aDecoder.decodeObject(forKey: "currency") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         fcmToken = aDecoder.decodeObject(forKey: "fcmToken") as? String
         hospitalImgArr = aDecoder.decodeObject(forKey: "hospital_imgArr") as? [String]
         hospitalImgURL = aDecoder.decodeObject(forKey: "hospital_imgURL") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         latitude = aDecoder.decodeObject(forKey: "latitude") as? String
         longitude = aDecoder.decodeObject(forKey: "longitude") as? String
         mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         passport = aDecoder.decodeObject(forKey: "passport") as? String
         phoneCode = aDecoder.decodeObject(forKey: "phone_code") as? String
         profileImage = aDecoder.decodeObject(forKey: "profileImage") as? String
         service = aDecoder.decodeObject(forKey: "service") as? String
         servicesImgURL = aDecoder.decodeObject(forKey: "services_imgURL") as? String
         sortname = aDecoder.decodeObject(forKey: "sortname") as? String
         stateId = aDecoder.decodeObject(forKey: "state_id") as? String
         stateName = aDecoder.decodeObject(forKey: "state_name") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         treatment = aDecoder.decodeObject(forKey: "treatment") as? String
         treatmentImgURL = aDecoder.decodeObject(forKey: "treatment_imgURL") as? String
         treatments = aDecoder.decodeObject(forKey: "treatments") as? [GFCHospitalDetailTreatment]
         updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updatedBy") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if accountStatus != nil{
			aCoder.encode(accountStatus, forKey: "accountStatus")
		}
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if body != nil{
			aCoder.encode(body, forKey: "body")
		}
		if cityId != nil{
			aCoder.encode(cityId, forKey: "city_id")
		}
		if cityName != nil{
			aCoder.encode(cityName, forKey: "city_name")
		}
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if countryName != nil{
			aCoder.encode(countryName, forKey: "country_name")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if createdDate != nil{
			aCoder.encode(createdDate, forKey: "created_date")
		}
		if currency != nil{
			aCoder.encode(currency, forKey: "currency")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if fcmToken != nil{
			aCoder.encode(fcmToken, forKey: "fcmToken")
		}
		if hospitalImgArr != nil{
			aCoder.encode(hospitalImgArr, forKey: "hospital_imgArr")
		}
		if hospitalImgURL != nil{
			aCoder.encode(hospitalImgURL, forKey: "hospital_imgURL")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if latitude != nil{
			aCoder.encode(latitude, forKey: "latitude")
		}
		if longitude != nil{
			aCoder.encode(longitude, forKey: "longitude")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobileNumber")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if passport != nil{
			aCoder.encode(passport, forKey: "passport")
		}
		if phoneCode != nil{
			aCoder.encode(phoneCode, forKey: "phone_code")
		}
		if profileImage != nil{
			aCoder.encode(profileImage, forKey: "profileImage")
		}
		if service != nil{
			aCoder.encode(service, forKey: "service")
		}
		if servicesImgURL != nil{
			aCoder.encode(servicesImgURL, forKey: "services_imgURL")
		}
		if sortname != nil{
			aCoder.encode(sortname, forKey: "sortname")
		}
		if stateId != nil{
			aCoder.encode(stateId, forKey: "state_id")
		}
		if stateName != nil{
			aCoder.encode(stateName, forKey: "state_name")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if treatment != nil{
			aCoder.encode(treatment, forKey: "treatment")
		}
		if treatmentImgURL != nil{
			aCoder.encode(treatmentImgURL, forKey: "treatment_imgURL")
		}
		if treatments != nil{
			aCoder.encode(treatments, forKey: "treatments")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updatedAt")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updatedBy")
		}

	}

}
