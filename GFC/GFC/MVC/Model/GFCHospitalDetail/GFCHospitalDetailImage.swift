//
//	GFCHospitalDetailImage.swift
//
//	Create by iMac on 12/4/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCHospitalDetailImage : NSObject, NSCoding{

	var createdAt : String!
	var imgId : String!
	var imgName : String!
	var imgTreatmentId : String!
	var treatmentDetailId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		imgId = dictionary["imgId"] as? String == nil ? "" : dictionary["imgId"] as? String
		imgName = dictionary["imgName"] as? String == nil ? "" : dictionary["imgName"] as? String
		imgTreatmentId = dictionary["imgTreatmentId"] as? String == nil ? "" : dictionary["imgTreatmentId"] as? String
		treatmentDetailId = dictionary["treatment_detail_id"] as? String == nil ? "" : dictionary["treatment_detail_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if imgId != nil{
			dictionary["imgId"] = imgId
		}
		if imgName != nil{
			dictionary["imgName"] = imgName
		}
		if imgTreatmentId != nil{
			dictionary["imgTreatmentId"] = imgTreatmentId
		}
		if treatmentDetailId != nil{
			dictionary["treatment_detail_id"] = treatmentDetailId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         imgId = aDecoder.decodeObject(forKey: "imgId") as? String
         imgName = aDecoder.decodeObject(forKey: "imgName") as? String
         imgTreatmentId = aDecoder.decodeObject(forKey: "imgTreatmentId") as? String
         treatmentDetailId = aDecoder.decodeObject(forKey: "treatment_detail_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if imgId != nil{
			aCoder.encode(imgId, forKey: "imgId")
		}
		if imgName != nil{
			aCoder.encode(imgName, forKey: "imgName")
		}
		if imgTreatmentId != nil{
			aCoder.encode(imgTreatmentId, forKey: "imgTreatmentId")
		}
		if treatmentDetailId != nil{
			aCoder.encode(treatmentDetailId, forKey: "treatment_detail_id")
		}

	}

}