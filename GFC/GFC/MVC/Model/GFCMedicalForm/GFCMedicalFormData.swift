//
//	GFCMedicalFormData.swift
//
//	Create by iMac on 2/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCMedicalFormData : NSObject, NSCoding{

	var age : String!
	var alcoholism : String!
	var allergyMedicine : String!
	var asthma : String!
	var bloodTansfusion : String!
	var cardio : String!
	var createdAt : String!
	var diabetes : String!
	var diagnosisName : String!
	var diagnosisTime : String!
	var dob : String!
	var drugs : String!
	var familyHistory : String!
	var hypertension : String!
	var id : String!
	var images : [String]!
	var liverDiseases : String!
	var otherHistory : String!
	var patientDetailId : String!
	var patientid : String!
	var patinetSuffering : String!
	var renalFailure : String!
	var smoking : String!
	var thyroid : String!
	var updatedAt : String!
	var weight : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		age = dictionary["age"] as? String == nil ? "" : dictionary["age"] as? String
		alcoholism = dictionary["alcoholism"] as? String == nil ? "" : dictionary["alcoholism"] as? String
		allergyMedicine = dictionary["allergy_medicine"] as? String == nil ? "" : dictionary["allergy_medicine"] as? String
		asthma = dictionary["asthma"] as? String == nil ? "" : dictionary["asthma"] as? String
		bloodTansfusion = dictionary["blood_tansfusion"] as? String == nil ? "" : dictionary["blood_tansfusion"] as? String
		cardio = dictionary["cardio"] as? String == nil ? "" : dictionary["cardio"] as? String
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		diabetes = dictionary["diabetes"] as? String == nil ? "" : dictionary["diabetes"] as? String
		diagnosisName = dictionary["diagnosis_name"] as? String == nil ? "" : dictionary["diagnosis_name"] as? String
		diagnosisTime = dictionary["diagnosis_time"] as? String == nil ? "" : dictionary["diagnosis_time"] as? String
		dob = dictionary["dob"] as? String == nil ? "" : dictionary["dob"] as? String
		drugs = dictionary["drugs"] as? String == nil ? "" : dictionary["drugs"] as? String
		familyHistory = dictionary["family_history"] as? String == nil ? "" : dictionary["family_history"] as? String
		hypertension = dictionary["hypertension"] as? String == nil ? "" : dictionary["hypertension"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		images = dictionary["images"] as? [String] == nil ? [] : dictionary["images"] as? [String]
		liverDiseases = dictionary["liver_diseases"] as? String == nil ? "" : dictionary["liver_diseases"] as? String
		otherHistory = dictionary["other_history"] as? String == nil ? "" : dictionary["other_history"] as? String
		patientDetailId = dictionary["patient_detail_id"] as? String == nil ? "" : dictionary["patient_detail_id"] as? String
		patientid = dictionary["patientid"] as? String == nil ? "" : dictionary["patientid"] as? String
		patinetSuffering = dictionary["patinet_suffering"] as? String == nil ? "" : dictionary["patinet_suffering"] as? String
		renalFailure = dictionary["renal_failure"] as? String == nil ? "" : dictionary["renal_failure"] as? String
		smoking = dictionary["smoking"] as? String == nil ? "" : dictionary["smoking"] as? String
		thyroid = dictionary["thyroid"] as? String == nil ? "" : dictionary["thyroid"] as? String
		updatedAt = dictionary["updatedAt"] as? String == nil ? "" : dictionary["updatedAt"] as? String
		weight = dictionary["weight"] as? String == nil ? "" : dictionary["weight"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if age != nil{
			dictionary["age"] = age
		}
		if alcoholism != nil{
			dictionary["alcoholism"] = alcoholism
		}
		if allergyMedicine != nil{
			dictionary["allergy_medicine"] = allergyMedicine
		}
		if asthma != nil{
			dictionary["asthma"] = asthma
		}
		if bloodTansfusion != nil{
			dictionary["blood_tansfusion"] = bloodTansfusion
		}
		if cardio != nil{
			dictionary["cardio"] = cardio
		}
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if diabetes != nil{
			dictionary["diabetes"] = diabetes
		}
		if diagnosisName != nil{
			dictionary["diagnosis_name"] = diagnosisName
		}
		if diagnosisTime != nil{
			dictionary["diagnosis_time"] = diagnosisTime
		}
		if dob != nil{
			dictionary["dob"] = dob
		}
		if drugs != nil{
			dictionary["drugs"] = drugs
		}
		if familyHistory != nil{
			dictionary["family_history"] = familyHistory
		}
		if hypertension != nil{
			dictionary["hypertension"] = hypertension
		}
		if id != nil{
			dictionary["id"] = id
		}
		if images != nil{
			dictionary["images"] = images
		}
		if liverDiseases != nil{
			dictionary["liver_diseases"] = liverDiseases
		}
		if otherHistory != nil{
			dictionary["other_history"] = otherHistory
		}
		if patientDetailId != nil{
			dictionary["patient_detail_id"] = patientDetailId
		}
		if patientid != nil{
			dictionary["patientid"] = patientid
		}
		if patinetSuffering != nil{
			dictionary["patinet_suffering"] = patinetSuffering
		}
		if renalFailure != nil{
			dictionary["renal_failure"] = renalFailure
		}
		if smoking != nil{
			dictionary["smoking"] = smoking
		}
		if thyroid != nil{
			dictionary["thyroid"] = thyroid
		}
		if updatedAt != nil{
			dictionary["updatedAt"] = updatedAt
		}
		if weight != nil{
			dictionary["weight"] = weight
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         age = aDecoder.decodeObject(forKey: "age") as? String
         alcoholism = aDecoder.decodeObject(forKey: "alcoholism") as? String
         allergyMedicine = aDecoder.decodeObject(forKey: "allergy_medicine") as? String
         asthma = aDecoder.decodeObject(forKey: "asthma") as? String
         bloodTansfusion = aDecoder.decodeObject(forKey: "blood_tansfusion") as? String
         cardio = aDecoder.decodeObject(forKey: "cardio") as? String
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         diabetes = aDecoder.decodeObject(forKey: "diabetes") as? String
         diagnosisName = aDecoder.decodeObject(forKey: "diagnosis_name") as? String
         diagnosisTime = aDecoder.decodeObject(forKey: "diagnosis_time") as? String
         dob = aDecoder.decodeObject(forKey: "dob") as? String
         drugs = aDecoder.decodeObject(forKey: "drugs") as? String
         familyHistory = aDecoder.decodeObject(forKey: "family_history") as? String
         hypertension = aDecoder.decodeObject(forKey: "hypertension") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         images = aDecoder.decodeObject(forKey: "images") as? [String]
         liverDiseases = aDecoder.decodeObject(forKey: "liver_diseases") as? String
         otherHistory = aDecoder.decodeObject(forKey: "other_history") as? String
         patientDetailId = aDecoder.decodeObject(forKey: "patient_detail_id") as? String
         patientid = aDecoder.decodeObject(forKey: "patientid") as? String
         patinetSuffering = aDecoder.decodeObject(forKey: "patinet_suffering") as? String
         renalFailure = aDecoder.decodeObject(forKey: "renal_failure") as? String
         smoking = aDecoder.decodeObject(forKey: "smoking") as? String
         thyroid = aDecoder.decodeObject(forKey: "thyroid") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as? String
         weight = aDecoder.decodeObject(forKey: "weight") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if age != nil{
			aCoder.encode(age, forKey: "age")
		}
		if alcoholism != nil{
			aCoder.encode(alcoholism, forKey: "alcoholism")
		}
		if allergyMedicine != nil{
			aCoder.encode(allergyMedicine, forKey: "allergy_medicine")
		}
		if asthma != nil{
			aCoder.encode(asthma, forKey: "asthma")
		}
		if bloodTansfusion != nil{
			aCoder.encode(bloodTansfusion, forKey: "blood_tansfusion")
		}
		if cardio != nil{
			aCoder.encode(cardio, forKey: "cardio")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if diabetes != nil{
			aCoder.encode(diabetes, forKey: "diabetes")
		}
		if diagnosisName != nil{
			aCoder.encode(diagnosisName, forKey: "diagnosis_name")
		}
		if diagnosisTime != nil{
			aCoder.encode(diagnosisTime, forKey: "diagnosis_time")
		}
		if dob != nil{
			aCoder.encode(dob, forKey: "dob")
		}
		if drugs != nil{
			aCoder.encode(drugs, forKey: "drugs")
		}
		if familyHistory != nil{
			aCoder.encode(familyHistory, forKey: "family_history")
		}
		if hypertension != nil{
			aCoder.encode(hypertension, forKey: "hypertension")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if images != nil{
			aCoder.encode(images, forKey: "images")
		}
		if liverDiseases != nil{
			aCoder.encode(liverDiseases, forKey: "liver_diseases")
		}
		if otherHistory != nil{
			aCoder.encode(otherHistory, forKey: "other_history")
		}
		if patientDetailId != nil{
			aCoder.encode(patientDetailId, forKey: "patient_detail_id")
		}
		if patientid != nil{
			aCoder.encode(patientid, forKey: "patientid")
		}
		if patinetSuffering != nil{
			aCoder.encode(patinetSuffering, forKey: "patinet_suffering")
		}
		if renalFailure != nil{
			aCoder.encode(renalFailure, forKey: "renal_failure")
		}
		if smoking != nil{
			aCoder.encode(smoking, forKey: "smoking")
		}
		if thyroid != nil{
			aCoder.encode(thyroid, forKey: "thyroid")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updatedAt")
		}
		if weight != nil{
			aCoder.encode(weight, forKey: "weight")
		}

	}

}
