//
//    GFCTreatmentListData.swift
//
//    Create by iMac on 6/3/2023
//    Copyright © 2023. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCTreatmentListData : NSObject, NSCoding{

    var appointmentId : String!
    var createdAt : String!
    var doctorName : String!
    var id : String!
    var images : [String]!
    var treatmentId : String!
    var treatmentName : String!


    /**
     * Overiding init method
     */
    init(fromDictionary dictionary: NSDictionary)
    {
        super.init()
        parseJSONData(fromDictionary: dictionary)
    }

    /**
     * Overiding init method
     */
    override init(){
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    @objc func parseJSONData(fromDictionary dictionary: NSDictionary)
    {
        appointmentId = dictionary["appointment_id"] as? String == nil ? "" : dictionary["appointment_id"] as? String
        createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
        doctorName = dictionary["doctor_name"] as? String == nil ? "" : dictionary["doctor_name"] as? String
        id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
        images = dictionary["images"] as? [String] == nil ? [] : dictionary["images"] as? [String]
        treatmentId = dictionary["treatmentId"] as? String == nil ? "" : dictionary["treatmentId"] as? String
        treatmentName = dictionary["treatment_name"] as? String == nil ? "" : dictionary["treatment_name"] as? String
    }

    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if appointmentId != nil{
            dictionary["appointment_id"] = appointmentId
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if doctorName != nil{
            dictionary["doctor_name"] = doctorName
        }
        if id != nil{
            dictionary["id"] = id
        }
        if images != nil{
            dictionary["images"] = images
        }
        if treatmentId != nil{
            dictionary["treatmentId"] = treatmentId
        }
        if treatmentName != nil{
            dictionary["treatment_name"] = treatmentName
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         appointmentId = aDecoder.decodeObject(forKey: "appointment_id") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         doctorName = aDecoder.decodeObject(forKey: "doctor_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         images = aDecoder.decodeObject(forKey: "images") as? [String]
         treatmentId = aDecoder.decodeObject(forKey: "treatmentId") as? String
         treatmentName = aDecoder.decodeObject(forKey: "treatment_name") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder)
    {
        if appointmentId != nil{
            aCoder.encode(appointmentId, forKey: "appointment_id")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if doctorName != nil{
            aCoder.encode(doctorName, forKey: "doctor_name")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if images != nil{
            aCoder.encode(images, forKey: "images")
        }
        if treatmentId != nil{
            aCoder.encode(treatmentId, forKey: "treatmentId")
        }
        if treatmentName != nil{
            aCoder.encode(treatmentName, forKey: "treatment_name")
        }

    }

}
