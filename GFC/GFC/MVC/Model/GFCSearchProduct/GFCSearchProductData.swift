//
//	GFCSearchProductData.swift
//
//	Create by iMac on 13/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCSearchProductData : NSObject, NSCoding{

	var category : String!
	var childCategoryName : String!
	var createdAt : String!
	var discountedPrice : String!
	var image : String!
	var moqPerUser : String!
	var name : String!
	var pId : String!
	var pharmacy : String!
	var price : String!
	var productStock : String!
	var subCategoryName : String!
    var rating : String!

	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		category = dictionary["category"] as? String == nil ? "" : dictionary["category"] as? String
		childCategoryName = dictionary["childCategoryName"] as? String == nil ? "" : dictionary["childCategoryName"] as? String
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		discountedPrice = dictionary["discounted_price"] as? String == nil ? "" : dictionary["discounted_price"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		moqPerUser = dictionary["moq_per_user"] as? String == nil ? "" : dictionary["moq_per_user"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		pId = dictionary["pId"] as? String == nil ? "" : dictionary["pId"] as? String
		pharmacy = dictionary["pharmacy"] as? String == nil ? "" : dictionary["pharmacy"] as? String
		price = dictionary["price"] as? String == nil ? "" : dictionary["price"] as? String
		productStock = dictionary["productStock"] as? String == nil ? "" : dictionary["productStock"] as? String
		subCategoryName = dictionary["subCategoryName"] as? String == nil ? "" : dictionary["subCategoryName"] as? String
        rating = dictionary["rating"] as? String == nil ? "" : dictionary["rating"] as? String

	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if category != nil{
			dictionary["category"] = category
		}
		if childCategoryName != nil{
			dictionary["childCategoryName"] = childCategoryName
		}
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if discountedPrice != nil{
			dictionary["discounted_price"] = discountedPrice
		}
		if image != nil{
			dictionary["image"] = image
		}
		if moqPerUser != nil{
			dictionary["moq_per_user"] = moqPerUser
		}
		if name != nil{
			dictionary["name"] = name
		}
		if pId != nil{
			dictionary["pId"] = pId
		}
		if pharmacy != nil{
			dictionary["pharmacy"] = pharmacy
		}
		if price != nil{
			dictionary["price"] = price
		}
		if productStock != nil{
			dictionary["productStock"] = productStock
		}
		if subCategoryName != nil{
			dictionary["subCategoryName"] = subCategoryName
		}
        if rating != nil{
            dictionary["rating"] = rating
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         category = aDecoder.decodeObject(forKey: "category") as? String
         childCategoryName = aDecoder.decodeObject(forKey: "childCategoryName") as? String
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         discountedPrice = aDecoder.decodeObject(forKey: "discounted_price") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         moqPerUser = aDecoder.decodeObject(forKey: "moq_per_user") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         pId = aDecoder.decodeObject(forKey: "pId") as? String
         pharmacy = aDecoder.decodeObject(forKey: "pharmacy") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         productStock = aDecoder.decodeObject(forKey: "productStock") as? String
         subCategoryName = aDecoder.decodeObject(forKey: "subCategoryName") as? String
        rating = aDecoder.decodeObject(forKey: "rating") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if childCategoryName != nil{
			aCoder.encode(childCategoryName, forKey: "childCategoryName")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if discountedPrice != nil{
			aCoder.encode(discountedPrice, forKey: "discounted_price")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if moqPerUser != nil{
			aCoder.encode(moqPerUser, forKey: "moq_per_user")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if pId != nil{
			aCoder.encode(pId, forKey: "pId")
		}
		if pharmacy != nil{
			aCoder.encode(pharmacy, forKey: "pharmacy")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if productStock != nil{
			aCoder.encode(productStock, forKey: "productStock")
		}
		if subCategoryName != nil{
			aCoder.encode(subCategoryName, forKey: "subCategoryName")
		}
        if rating != nil{
            aCoder.encode(rating, forKey: "rating")
        }

	}

}
