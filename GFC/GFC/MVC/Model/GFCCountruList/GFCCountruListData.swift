//
//	GFCCountruListData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCCountruListData : NSObject, NSCoding{

	var code : String!
	var country : String!
	var createdDate : String!
	var id : String!
	var ipAddress : String!
	var modifiedDate : String!
	var phoneCode : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		code = dictionary["code"] as? String == nil ? "" : dictionary["code"] as? String
		country = dictionary["country"] as? String == nil ? "" : dictionary["country"] as? String
		createdDate = dictionary["created_date"] as? String == nil ? "" : dictionary["created_date"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		ipAddress = dictionary["ip_address"] as? String == nil ? "" : dictionary["ip_address"] as? String
		modifiedDate = dictionary["modified_date"] as? String == nil ? "" : dictionary["modified_date"] as? String
		phoneCode = dictionary["phone_code"] as? String == nil ? "" : dictionary["phone_code"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if code != nil{
			dictionary["code"] = code
		}
		if country != nil{
			dictionary["country"] = country
		}
		if createdDate != nil{
			dictionary["created_date"] = createdDate
		}
		if id != nil{
			dictionary["id"] = id
		}
		if ipAddress != nil{
			dictionary["ip_address"] = ipAddress
		}
		if modifiedDate != nil{
			dictionary["modified_date"] = modifiedDate
		}
		if phoneCode != nil{
			dictionary["phone_code"] = phoneCode
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         code = aDecoder.decodeObject(forKey: "code") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         createdDate = aDecoder.decodeObject(forKey: "created_date") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         ipAddress = aDecoder.decodeObject(forKey: "ip_address") as? String
         modifiedDate = aDecoder.decodeObject(forKey: "modified_date") as? String
         phoneCode = aDecoder.decodeObject(forKey: "phone_code") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if code != nil{
			aCoder.encode(code, forKey: "code")
		}
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if createdDate != nil{
			aCoder.encode(createdDate, forKey: "created_date")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if ipAddress != nil{
			aCoder.encode(ipAddress, forKey: "ip_address")
		}
		if modifiedDate != nil{
			aCoder.encode(modifiedDate, forKey: "modified_date")
		}
		if phoneCode != nil{
			aCoder.encode(phoneCode, forKey: "phone_code")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}