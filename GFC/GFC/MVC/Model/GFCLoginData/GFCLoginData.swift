//
//	GFCLoginData.swift
//
//	Create by mac on 9/12/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCLoginData : NSObject, NSCoding{

	var accountStatus : String!
	var address : String!
	var age : String!
	var archivePatients : String!
	var archiveReports : String!
	var assignUser : String!
	var cityId : String!
	var countryId : String!
	var createdAt : String!
	var email : String!
	var emailVerification : String!
	var fbId : String!
	var fcmToken : String!
	var gId : String!
	var gender : String!
	var id : String!
	var isLogin : String!
	var latitude : String!
	var longitude : String!
	var mobileNumber : String!
	var name : String!
	var passport : String!
	var password : String!
	var phoneVerification : String!
	var profileImage : String!
	var role : String!
	var signuptype : String!
	var stateId : String!
	var updatedAt : String!
	var updatedBy : String!
    var country_code : String!
    var quickblox_id : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		accountStatus = dictionary["accountStatus"] as? String == nil ? "" : dictionary["accountStatus"] as? String
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		age = dictionary["age"] as? String == nil ? "" : dictionary["age"] as? String
		archivePatients = dictionary["archive_patients"] as? String == nil ? "" : dictionary["archive_patients"] as? String
		archiveReports = dictionary["archive_reports"] as? String == nil ? "" : dictionary["archive_reports"] as? String
		assignUser = dictionary["assign_user"] as? String == nil ? "" : dictionary["assign_user"] as? String
		cityId = dictionary["city_id"] as? String == nil ? "" : dictionary["city_id"] as? String
		countryId = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		emailVerification = dictionary["emailVerification"] as? String == nil ? "" : dictionary["emailVerification"] as? String
		fbId = dictionary["fb_id"] as? String == nil ? "" : dictionary["fb_id"] as? String
		fcmToken = dictionary["fcmToken"] as? String == nil ? "" : dictionary["fcmToken"] as? String
		gId = dictionary["g_id"] as? String == nil ? "" : dictionary["g_id"] as? String
		gender = dictionary["gender"] as? String == nil ? "" : dictionary["gender"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		isLogin = dictionary["isLogin"] as? String == nil ? "" : dictionary["isLogin"] as? String
		latitude = dictionary["latitude"] as? String == nil ? "" : dictionary["latitude"] as? String
		longitude = dictionary["longitude"] as? String == nil ? "" : dictionary["longitude"] as? String
		mobileNumber = dictionary["mobileNumber"] as? String == nil ? "" : dictionary["mobileNumber"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		passport = dictionary["passport"] as? String == nil ? "" : dictionary["passport"] as? String
		password = dictionary["password"] as? String == nil ? "" : dictionary["password"] as? String
		phoneVerification = dictionary["phoneVerification"] as? String == nil ? "" : dictionary["phoneVerification"] as? String
		profileImage = dictionary["profileImage"] as? String == nil ? "" : dictionary["profileImage"] as? String
		role = dictionary["role"] as? String == nil ? "" : dictionary["role"] as? String
		signuptype = dictionary["signuptype"] as? String == nil ? "" : dictionary["signuptype"] as? String
		stateId = dictionary["state_id"] as? String == nil ? "" : dictionary["state_id"] as? String
		updatedAt = dictionary["updatedAt"] as? String == nil ? "" : dictionary["updatedAt"] as? String
		updatedBy = dictionary["updatedBy"] as? String == nil ? "" : dictionary["updatedBy"] as? String
        country_code = dictionary["country_code"] as? String == nil ? "" : dictionary["country_code"] as? String
        quickblox_id = dictionary["quickblox_id"] as? String == nil ? "" : dictionary["quickblox_id"] as? String

	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if accountStatus != nil{
			dictionary["accountStatus"] = accountStatus
		}
		if address != nil{
			dictionary["address"] = address
		}
		if age != nil{
			dictionary["age"] = age
		}
		if archivePatients != nil{
			dictionary["archive_patients"] = archivePatients
		}
		if archiveReports != nil{
			dictionary["archive_reports"] = archiveReports
		}
		if assignUser != nil{
			dictionary["assign_user"] = assignUser
		}
		if cityId != nil{
			dictionary["city_id"] = cityId
		}
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if email != nil{
			dictionary["email"] = email
		}
		if emailVerification != nil{
			dictionary["emailVerification"] = emailVerification
		}
		if fbId != nil{
			dictionary["fb_id"] = fbId
		}
		if fcmToken != nil{
			dictionary["fcmToken"] = fcmToken
		}
		if gId != nil{
			dictionary["g_id"] = gId
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if id != nil{
			dictionary["id"] = id
		}
		if isLogin != nil{
			dictionary["isLogin"] = isLogin
		}
		if latitude != nil{
			dictionary["latitude"] = latitude
		}
		if longitude != nil{
			dictionary["longitude"] = longitude
		}
		if mobileNumber != nil{
			dictionary["mobileNumber"] = mobileNumber
		}
		if name != nil{
			dictionary["name"] = name
		}
		if passport != nil{
			dictionary["passport"] = passport
		}
		if password != nil{
			dictionary["password"] = password
		}
		if phoneVerification != nil{
			dictionary["phoneVerification"] = phoneVerification
		}
		if profileImage != nil{
			dictionary["profileImage"] = profileImage
		}
		if role != nil{
			dictionary["role"] = role
		}
		if signuptype != nil{
			dictionary["signuptype"] = signuptype
		}
		if stateId != nil{
			dictionary["state_id"] = stateId
		}
		if updatedAt != nil{
			dictionary["updatedAt"] = updatedAt
		}
		if updatedBy != nil{
			dictionary["updatedBy"] = updatedBy
		}
        if country_code != nil{
            dictionary["country_code"] = country_code
        }
        if quickblox_id != nil{
            dictionary["quickblox_id"] = quickblox_id
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accountStatus = aDecoder.decodeObject(forKey: "accountStatus") as? String
         address = aDecoder.decodeObject(forKey: "address") as? String
         age = aDecoder.decodeObject(forKey: "age") as? String
         archivePatients = aDecoder.decodeObject(forKey: "archive_patients") as? String
         archiveReports = aDecoder.decodeObject(forKey: "archive_reports") as? String
         assignUser = aDecoder.decodeObject(forKey: "assign_user") as? String
         cityId = aDecoder.decodeObject(forKey: "city_id") as? String
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         emailVerification = aDecoder.decodeObject(forKey: "emailVerification") as? String
         fbId = aDecoder.decodeObject(forKey: "fb_id") as? String
         fcmToken = aDecoder.decodeObject(forKey: "fcmToken") as? String
         gId = aDecoder.decodeObject(forKey: "g_id") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         isLogin = aDecoder.decodeObject(forKey: "isLogin") as? String
         latitude = aDecoder.decodeObject(forKey: "latitude") as? String
         longitude = aDecoder.decodeObject(forKey: "longitude") as? String
         mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         passport = aDecoder.decodeObject(forKey: "passport") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         phoneVerification = aDecoder.decodeObject(forKey: "phoneVerification") as? String
         profileImage = aDecoder.decodeObject(forKey: "profileImage") as? String
         role = aDecoder.decodeObject(forKey: "role") as? String
         signuptype = aDecoder.decodeObject(forKey: "signuptype") as? String
         stateId = aDecoder.decodeObject(forKey: "state_id") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updatedBy") as? String
        country_code = aDecoder.decodeObject(forKey: "country_code") as? String
        quickblox_id = aDecoder.decodeObject(forKey: "quickblox_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if accountStatus != nil{
			aCoder.encode(accountStatus, forKey: "accountStatus")
		}
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if age != nil{
			aCoder.encode(age, forKey: "age")
		}
		if archivePatients != nil{
			aCoder.encode(archivePatients, forKey: "archive_patients")
		}
		if archiveReports != nil{
			aCoder.encode(archiveReports, forKey: "archive_reports")
		}
		if assignUser != nil{
			aCoder.encode(assignUser, forKey: "assign_user")
		}
		if cityId != nil{
			aCoder.encode(cityId, forKey: "city_id")
		}
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if emailVerification != nil{
			aCoder.encode(emailVerification, forKey: "emailVerification")
		}
		if fbId != nil{
			aCoder.encode(fbId, forKey: "fb_id")
		}
		if fcmToken != nil{
			aCoder.encode(fcmToken, forKey: "fcmToken")
		}
		if gId != nil{
			aCoder.encode(gId, forKey: "g_id")
		}
		if gender != nil{
			aCoder.encode(gender, forKey: "gender")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isLogin != nil{
			aCoder.encode(isLogin, forKey: "isLogin")
		}
		if latitude != nil{
			aCoder.encode(latitude, forKey: "latitude")
		}
		if longitude != nil{
			aCoder.encode(longitude, forKey: "longitude")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobileNumber")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if passport != nil{
			aCoder.encode(passport, forKey: "passport")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if phoneVerification != nil{
			aCoder.encode(phoneVerification, forKey: "phoneVerification")
		}
		if profileImage != nil{
			aCoder.encode(profileImage, forKey: "profileImage")
		}
		if role != nil{
			aCoder.encode(role, forKey: "role")
		}
		if signuptype != nil{
			aCoder.encode(signuptype, forKey: "signuptype")
		}
		if stateId != nil{
			aCoder.encode(stateId, forKey: "state_id")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updatedAt")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updatedBy")
		}
        if country_code != nil{
            aCoder.encode(country_code, forKey: "country_code")
        }
        if quickblox_id != nil{
            aCoder.encode(quickblox_id, forKey: "quickblox_id")
        }

	}

}
