//
//	GFCServicesData.swift
//
//	Create by Code on 1/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCServicesData : NSObject, NSCoding{

	var createdAt : String!
	var image : String!
	var serviceDescription : String!
	var serviceId : String!
	var serviceName : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		serviceDescription = dictionary["serviceDescription"] as? String == nil ? "" : dictionary["serviceDescription"] as? String
		serviceId = dictionary["serviceId"] as? String == nil ? "" : dictionary["serviceId"] as? String
		serviceName = dictionary["serviceName"] as? String == nil ? "" : dictionary["serviceName"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if image != nil{
			dictionary["image"] = image
		}
		if serviceDescription != nil{
			dictionary["serviceDescription"] = serviceDescription
		}
		if serviceId != nil{
			dictionary["serviceId"] = serviceId
		}
		if serviceName != nil{
			dictionary["serviceName"] = serviceName
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         serviceDescription = aDecoder.decodeObject(forKey: "serviceDescription") as? String
         serviceId = aDecoder.decodeObject(forKey: "serviceId") as? String
         serviceName = aDecoder.decodeObject(forKey: "serviceName") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if serviceDescription != nil{
			aCoder.encode(serviceDescription, forKey: "serviceDescription")
		}
		if serviceId != nil{
			aCoder.encode(serviceId, forKey: "serviceId")
		}
		if serviceName != nil{
			aCoder.encode(serviceName, forKey: "serviceName")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}