//
//	GFCHospitalListData.swift
//
//	Create by iMac on 24/11/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCHospitalListData : NSObject, NSCoding{

	var accountStatus : String!
	var address : String!
	var body : String!
	var cityId : String!
	var countryId : String!
	var createdAt : String!
	var descriptionField : String!
	var email : String!
	var fcmToken : String!
	var id : String!
	var imageUrl : String!
	var images : [GFCHospitalListImage]!
	var latitude : String!
	var longitude : String!
	var mobileNumber : String!
	var name : String!
	var passport : String!
	var profileImage : String!
	var service : String!
	var stateId : String!
	var treatment : [GFCHospitalListTreatment]!
	var updatedAt : String!
	var updatedBy : String!
    var rating : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		accountStatus = dictionary["accountStatus"] as? String == nil ? "" : dictionary["accountStatus"] as? String
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		body = dictionary["body"] as? String == nil ? "" : dictionary["body"] as? String
		cityId = dictionary["city_id"] as? String == nil ? "" : dictionary["city_id"] as? String
		countryId = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		fcmToken = dictionary["fcmToken"] as? String == nil ? "" : dictionary["fcmToken"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		imageUrl = dictionary["image_url"] as? String == nil ? "" : dictionary["image_url"] as? String
		images = [GFCHospitalListImage]()
		if let imagesArray = dictionary["images"] as? [NSDictionary]{
			for dic in imagesArray{
				let value = GFCHospitalListImage(fromDictionary: dic)
				images.append(value)
			}
		}
		latitude = dictionary["latitude"] as? String == nil ? "" : dictionary["latitude"] as? String
		longitude = dictionary["longitude"] as? String == nil ? "" : dictionary["longitude"] as? String
		mobileNumber = dictionary["mobileNumber"] as? String == nil ? "" : dictionary["mobileNumber"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		passport = dictionary["passport"] as? String == nil ? "" : dictionary["passport"] as? String
		profileImage = dictionary["profileImage"] as? String == nil ? "" : dictionary["profileImage"] as? String
		service = dictionary["service"] as? String == nil ? "" : dictionary["service"] as? String
		stateId = dictionary["state_id"] as? String == nil ? "" : dictionary["state_id"] as? String
		treatment = [GFCHospitalListTreatment]()
		if let treatmentArray = dictionary["treatment"] as? [NSDictionary]{
			for dic in treatmentArray{
				let value = GFCHospitalListTreatment(fromDictionary: dic)
				treatment.append(value)
			}
		}
		updatedAt = dictionary["updatedAt"] as? String == nil ? "" : dictionary["updatedAt"] as? String
		updatedBy = dictionary["updatedBy"] as? String == nil ? "" : dictionary["updatedBy"] as? String
        rating = dictionary["rating"] as? String == nil ? "" : dictionary["rating"] as? String

	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if accountStatus != nil{
			dictionary["accountStatus"] = accountStatus
		}
		if address != nil{
			dictionary["address"] = address
		}
		if body != nil{
			dictionary["body"] = body
		}
		if cityId != nil{
			dictionary["city_id"] = cityId
		}
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if email != nil{
			dictionary["email"] = email
		}
		if fcmToken != nil{
			dictionary["fcmToken"] = fcmToken
		}
		if id != nil{
			dictionary["id"] = id
		}
		if imageUrl != nil{
			dictionary["image_url"] = imageUrl
		}
		if images != nil{
			var dictionaryElements = [NSDictionary]()
			for imagesElement in images {
				dictionaryElements.append(imagesElement.toDictionary())
			}
			dictionary["images"] = dictionaryElements
		}
		if latitude != nil{
			dictionary["latitude"] = latitude
		}
		if longitude != nil{
			dictionary["longitude"] = longitude
		}
		if mobileNumber != nil{
			dictionary["mobileNumber"] = mobileNumber
		}
		if name != nil{
			dictionary["name"] = name
		}
		if passport != nil{
			dictionary["passport"] = passport
		}
		if profileImage != nil{
			dictionary["profileImage"] = profileImage
		}
		if service != nil{
			dictionary["service"] = service
		}
		if stateId != nil{
			dictionary["state_id"] = stateId
		}
		if treatment != nil{
			var dictionaryElements = [NSDictionary]()
			for treatmentElement in treatment {
				dictionaryElements.append(treatmentElement.toDictionary())
			}
			dictionary["treatment"] = dictionaryElements
		}
		if updatedAt != nil{
			dictionary["updatedAt"] = updatedAt
		}
		if updatedBy != nil{
			dictionary["updatedBy"] = updatedBy
		}
        if rating != nil{
            dictionary["rating"] = rating
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accountStatus = aDecoder.decodeObject(forKey: "accountStatus") as? String
         address = aDecoder.decodeObject(forKey: "address") as? String
         body = aDecoder.decodeObject(forKey: "body") as? String
         cityId = aDecoder.decodeObject(forKey: "city_id") as? String
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         fcmToken = aDecoder.decodeObject(forKey: "fcmToken") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
         images = aDecoder.decodeObject(forKey: "images") as? [GFCHospitalListImage]
         latitude = aDecoder.decodeObject(forKey: "latitude") as? String
         longitude = aDecoder.decodeObject(forKey: "longitude") as? String
         mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         passport = aDecoder.decodeObject(forKey: "passport") as? String
         profileImage = aDecoder.decodeObject(forKey: "profileImage") as? String
         service = aDecoder.decodeObject(forKey: "service") as? String
         stateId = aDecoder.decodeObject(forKey: "state_id") as? String
         treatment = aDecoder.decodeObject(forKey: "treatment") as? [GFCHospitalListTreatment]
         updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updatedBy") as? String
        rating = aDecoder.decodeObject(forKey: "rating") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if accountStatus != nil{
			aCoder.encode(accountStatus, forKey: "accountStatus")
		}
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if body != nil{
			aCoder.encode(body, forKey: "body")
		}
		if cityId != nil{
			aCoder.encode(cityId, forKey: "city_id")
		}
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if fcmToken != nil{
			aCoder.encode(fcmToken, forKey: "fcmToken")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if imageUrl != nil{
			aCoder.encode(imageUrl, forKey: "image_url")
		}
		if images != nil{
			aCoder.encode(images, forKey: "images")
		}
		if latitude != nil{
			aCoder.encode(latitude, forKey: "latitude")
		}
		if longitude != nil{
			aCoder.encode(longitude, forKey: "longitude")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobileNumber")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if passport != nil{
			aCoder.encode(passport, forKey: "passport")
		}
		if profileImage != nil{
			aCoder.encode(profileImage, forKey: "profileImage")
		}
		if service != nil{
			aCoder.encode(service, forKey: "service")
		}
		if stateId != nil{
			aCoder.encode(stateId, forKey: "state_id")
		}
		if treatment != nil{
			aCoder.encode(treatment, forKey: "treatment")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updatedAt")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updatedBy")
		}
        if rating != nil{
            aCoder.encode(rating, forKey: "rating")
        }

	}

}
