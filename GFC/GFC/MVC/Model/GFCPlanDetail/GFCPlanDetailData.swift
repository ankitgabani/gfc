//
//	GFCPlanDetailData.swift
//
//	Create by iMac on 11/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCPlanDetailData : NSObject, NSCoding{

	var appointmentId : String!
	var costOfStaying : String!
	var costOfTreatment : String!
	var createdAt : String!
	var dayToStay : String!
	var daysInHospital : String!
	var doctorName : String!
	var hospitalName : String!
	var id : String!
	var images : [String]!
	var otherNote : String!
	var success : String!
	var treatmentName : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		appointmentId = dictionary["appointment_id"] as? String == nil ? "" : dictionary["appointment_id"] as? String
		costOfStaying = dictionary["cost_of_staying"] as? String == nil ? "" : dictionary["cost_of_staying"] as? String
		costOfTreatment = dictionary["cost_of_treatment"] as? String == nil ? "" : dictionary["cost_of_treatment"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		dayToStay = dictionary["day_to_stay"] as? String == nil ? "" : dictionary["day_to_stay"] as? String
		daysInHospital = dictionary["days_in_hospital"] as? String == nil ? "" : dictionary["days_in_hospital"] as? String
		doctorName = dictionary["doctor_name"] as? String == nil ? "" : dictionary["doctor_name"] as? String
		hospitalName = dictionary["hospital_name"] as? String == nil ? "" : dictionary["hospital_name"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		images = dictionary["images"] as? [String] == nil ? [] : dictionary["images"] as? [String]
		otherNote = dictionary["other_note"] as? String == nil ? "" : dictionary["other_note"] as? String
		success = dictionary["success"] as? String == nil ? "" : dictionary["success"] as? String
		treatmentName = dictionary["treatment_name"] as? String == nil ? "" : dictionary["treatment_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if appointmentId != nil{
			dictionary["appointment_id"] = appointmentId
		}
		if costOfStaying != nil{
			dictionary["cost_of_staying"] = costOfStaying
		}
		if costOfTreatment != nil{
			dictionary["cost_of_treatment"] = costOfTreatment
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if dayToStay != nil{
			dictionary["day_to_stay"] = dayToStay
		}
		if daysInHospital != nil{
			dictionary["days_in_hospital"] = daysInHospital
		}
		if doctorName != nil{
			dictionary["doctor_name"] = doctorName
		}
		if hospitalName != nil{
			dictionary["hospital_name"] = hospitalName
		}
		if id != nil{
			dictionary["id"] = id
		}
		if images != nil{
			dictionary["images"] = images
		}
		if otherNote != nil{
			dictionary["other_note"] = otherNote
		}
		if success != nil{
			dictionary["success"] = success
		}
		if treatmentName != nil{
			dictionary["treatment_name"] = treatmentName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         appointmentId = aDecoder.decodeObject(forKey: "appointment_id") as? String
         costOfStaying = aDecoder.decodeObject(forKey: "cost_of_staying") as? String
         costOfTreatment = aDecoder.decodeObject(forKey: "cost_of_treatment") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         dayToStay = aDecoder.decodeObject(forKey: "day_to_stay") as? String
         daysInHospital = aDecoder.decodeObject(forKey: "days_in_hospital") as? String
         doctorName = aDecoder.decodeObject(forKey: "doctor_name") as? String
         hospitalName = aDecoder.decodeObject(forKey: "hospital_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         images = aDecoder.decodeObject(forKey: "images") as? [String]
         otherNote = aDecoder.decodeObject(forKey: "other_note") as? String
         success = aDecoder.decodeObject(forKey: "success") as? String
         treatmentName = aDecoder.decodeObject(forKey: "treatment_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if appointmentId != nil{
			aCoder.encode(appointmentId, forKey: "appointment_id")
		}
		if costOfStaying != nil{
			aCoder.encode(costOfStaying, forKey: "cost_of_staying")
		}
		if costOfTreatment != nil{
			aCoder.encode(costOfTreatment, forKey: "cost_of_treatment")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if dayToStay != nil{
			aCoder.encode(dayToStay, forKey: "day_to_stay")
		}
		if daysInHospital != nil{
			aCoder.encode(daysInHospital, forKey: "days_in_hospital")
		}
		if doctorName != nil{
			aCoder.encode(doctorName, forKey: "doctor_name")
		}
		if hospitalName != nil{
			aCoder.encode(hospitalName, forKey: "hospital_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if images != nil{
			aCoder.encode(images, forKey: "images")
		}
		if otherNote != nil{
			aCoder.encode(otherNote, forKey: "other_note")
		}
		if success != nil{
			aCoder.encode(success, forKey: "success")
		}
		if treatmentName != nil{
			aCoder.encode(treatmentName, forKey: "treatment_name")
		}

	}

}
