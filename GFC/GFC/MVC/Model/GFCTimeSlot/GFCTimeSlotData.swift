//
//	GFCTimeSlotData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCTimeSlotData : NSObject, NSCoding{

	var availableDay : String!
	var branchId : String!
	var dayName : String!
	var doctorId : String!
	var fromTime : String!
	var id : String!
	var toTime : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		availableDay = dictionary["available_day"] as? String == nil ? "" : dictionary["available_day"] as? String
		branchId = dictionary["branch_id"] as? String == nil ? "" : dictionary["branch_id"] as? String
		dayName = dictionary["day_name"] as? String == nil ? "" : dictionary["day_name"] as? String
		doctorId = dictionary["doctor_id"] as? String == nil ? "" : dictionary["doctor_id"] as? String
		fromTime = dictionary["from_time"] as? String == nil ? "" : dictionary["from_time"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		toTime = dictionary["to_time"] as? String == nil ? "" : dictionary["to_time"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if availableDay != nil{
			dictionary["available_day"] = availableDay
		}
		if branchId != nil{
			dictionary["branch_id"] = branchId
		}
		if dayName != nil{
			dictionary["day_name"] = dayName
		}
		if doctorId != nil{
			dictionary["doctor_id"] = doctorId
		}
		if fromTime != nil{
			dictionary["from_time"] = fromTime
		}
		if id != nil{
			dictionary["id"] = id
		}
		if toTime != nil{
			dictionary["to_time"] = toTime
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         availableDay = aDecoder.decodeObject(forKey: "available_day") as? String
         branchId = aDecoder.decodeObject(forKey: "branch_id") as? String
         dayName = aDecoder.decodeObject(forKey: "day_name") as? String
         doctorId = aDecoder.decodeObject(forKey: "doctor_id") as? String
         fromTime = aDecoder.decodeObject(forKey: "from_time") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         toTime = aDecoder.decodeObject(forKey: "to_time") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if availableDay != nil{
			aCoder.encode(availableDay, forKey: "available_day")
		}
		if branchId != nil{
			aCoder.encode(branchId, forKey: "branch_id")
		}
		if dayName != nil{
			aCoder.encode(dayName, forKey: "day_name")
		}
		if doctorId != nil{
			aCoder.encode(doctorId, forKey: "doctor_id")
		}
		if fromTime != nil{
			aCoder.encode(fromTime, forKey: "from_time")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if toTime != nil{
			aCoder.encode(toTime, forKey: "to_time")
		}

	}

}