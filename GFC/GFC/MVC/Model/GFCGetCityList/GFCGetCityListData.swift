//
//	GFCGetCityListData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCGetCityListData : NSObject, NSCoding{

	var city : String!
	var countryId : String!
	var createdDate : String!
	var id : String!
	var ipAddress : String!
	var modifiedDate : String!
	var stateId : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		city = dictionary["city"] as? String == nil ? "" : dictionary["city"] as? String
		countryId = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
		createdDate = dictionary["created_date"] as? String == nil ? "" : dictionary["created_date"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		ipAddress = dictionary["ip_address"] as? String == nil ? "" : dictionary["ip_address"] as? String
		modifiedDate = dictionary["modified_date"] as? String == nil ? "" : dictionary["modified_date"] as? String
		stateId = dictionary["state_id"] as? String == nil ? "" : dictionary["state_id"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if city != nil{
			dictionary["city"] = city
		}
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if createdDate != nil{
			dictionary["created_date"] = createdDate
		}
		if id != nil{
			dictionary["id"] = id
		}
		if ipAddress != nil{
			dictionary["ip_address"] = ipAddress
		}
		if modifiedDate != nil{
			dictionary["modified_date"] = modifiedDate
		}
		if stateId != nil{
			dictionary["state_id"] = stateId
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         city = aDecoder.decodeObject(forKey: "city") as? String
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         createdDate = aDecoder.decodeObject(forKey: "created_date") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         ipAddress = aDecoder.decodeObject(forKey: "ip_address") as? String
         modifiedDate = aDecoder.decodeObject(forKey: "modified_date") as? String
         stateId = aDecoder.decodeObject(forKey: "state_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if createdDate != nil{
			aCoder.encode(createdDate, forKey: "created_date")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if ipAddress != nil{
			aCoder.encode(ipAddress, forKey: "ip_address")
		}
		if modifiedDate != nil{
			aCoder.encode(modifiedDate, forKey: "modified_date")
		}
		if stateId != nil{
			aCoder.encode(stateId, forKey: "state_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}