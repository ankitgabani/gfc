//
//	GFCSpecialitySpecialityData.swift
//
//	Create by iMac on 28/2/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCSpecialitySpecialityData : NSObject, NSCoding{

	var branchId : String!
	var createdBy : String!
	var createdDate : String!
	var defaultValue : String!
	var deletedBy : String!
	var deletedDate : String!
	var downloadId : String!
	var id : String!
	var ipAddress : String!
	var isDeleted : String!
	var modifiedBy : String!
	var modifiedDate : String!
	var specialization : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		branchId = dictionary["branch_id"] as? String == nil ? "" : dictionary["branch_id"] as? String
		createdBy = dictionary["created_by"] as? String == nil ? "" : dictionary["created_by"] as? String
		createdDate = dictionary["created_date"] as? String == nil ? "" : dictionary["created_date"] as? String
		defaultValue = dictionary["default_value"] as? String == nil ? "" : dictionary["default_value"] as? String
		deletedBy = dictionary["deleted_by"] as? String == nil ? "" : dictionary["deleted_by"] as? String
		deletedDate = dictionary["deleted_date"] as? String == nil ? "" : dictionary["deleted_date"] as? String
		downloadId = dictionary["download_id"] as? String == nil ? "" : dictionary["download_id"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		ipAddress = dictionary["ip_address"] as? String == nil ? "" : dictionary["ip_address"] as? String
		isDeleted = dictionary["is_deleted"] as? String == nil ? "" : dictionary["is_deleted"] as? String
		modifiedBy = dictionary["modified_by"] as? String == nil ? "" : dictionary["modified_by"] as? String
		modifiedDate = dictionary["modified_date"] as? String == nil ? "" : dictionary["modified_date"] as? String
		specialization = dictionary["specialization"] as? String == nil ? "" : dictionary["specialization"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if branchId != nil{
			dictionary["branch_id"] = branchId
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if createdDate != nil{
			dictionary["created_date"] = createdDate
		}
		if defaultValue != nil{
			dictionary["default_value"] = defaultValue
		}
		if deletedBy != nil{
			dictionary["deleted_by"] = deletedBy
		}
		if deletedDate != nil{
			dictionary["deleted_date"] = deletedDate
		}
		if downloadId != nil{
			dictionary["download_id"] = downloadId
		}
		if id != nil{
			dictionary["id"] = id
		}
		if ipAddress != nil{
			dictionary["ip_address"] = ipAddress
		}
		if isDeleted != nil{
			dictionary["is_deleted"] = isDeleted
		}
		if modifiedBy != nil{
			dictionary["modified_by"] = modifiedBy
		}
		if modifiedDate != nil{
			dictionary["modified_date"] = modifiedDate
		}
		if specialization != nil{
			dictionary["specialization"] = specialization
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         branchId = aDecoder.decodeObject(forKey: "branch_id") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         createdDate = aDecoder.decodeObject(forKey: "created_date") as? String
         defaultValue = aDecoder.decodeObject(forKey: "default_value") as? String
         deletedBy = aDecoder.decodeObject(forKey: "deleted_by") as? String
         deletedDate = aDecoder.decodeObject(forKey: "deleted_date") as? String
         downloadId = aDecoder.decodeObject(forKey: "download_id") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         ipAddress = aDecoder.decodeObject(forKey: "ip_address") as? String
         isDeleted = aDecoder.decodeObject(forKey: "is_deleted") as? String
         modifiedBy = aDecoder.decodeObject(forKey: "modified_by") as? String
         modifiedDate = aDecoder.decodeObject(forKey: "modified_date") as? String
         specialization = aDecoder.decodeObject(forKey: "specialization") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if branchId != nil{
			aCoder.encode(branchId, forKey: "branch_id")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if createdDate != nil{
			aCoder.encode(createdDate, forKey: "created_date")
		}
		if defaultValue != nil{
			aCoder.encode(defaultValue, forKey: "default_value")
		}
		if deletedBy != nil{
			aCoder.encode(deletedBy, forKey: "deleted_by")
		}
		if deletedDate != nil{
			aCoder.encode(deletedDate, forKey: "deleted_date")
		}
		if downloadId != nil{
			aCoder.encode(downloadId, forKey: "download_id")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if ipAddress != nil{
			aCoder.encode(ipAddress, forKey: "ip_address")
		}
		if isDeleted != nil{
			aCoder.encode(isDeleted, forKey: "is_deleted")
		}
		if modifiedBy != nil{
			aCoder.encode(modifiedBy, forKey: "modified_by")
		}
		if modifiedDate != nil{
			aCoder.encode(modifiedDate, forKey: "modified_date")
		}
		if specialization != nil{
			aCoder.encode(specialization, forKey: "specialization")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}