//
//	GFCSpecialityData.swift
//
//	Create by iMac on 28/2/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCSpecialityData : NSObject, NSCoding{

	var featuredDoctor : [GFCSpecialityFeaturedDoctor]!
	var popularDoctor : [GFCSpecialityFeaturedDoctor]!
	var specialityData : [GFCSpecialitySpecialityData]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		featuredDoctor = [GFCSpecialityFeaturedDoctor]()
		if let featuredDoctorArray = dictionary["featured_doctor"] as? [NSDictionary]{
			for dic in featuredDoctorArray{
				let value = GFCSpecialityFeaturedDoctor(fromDictionary: dic)
				featuredDoctor.append(value)
			}
		}
		popularDoctor = [GFCSpecialityFeaturedDoctor]()
		if let popularDoctorArray = dictionary["popular_doctor"] as? [NSDictionary]{
			for dic in popularDoctorArray{
				let value = GFCSpecialityFeaturedDoctor(fromDictionary: dic)
				popularDoctor.append(value)
			}
		}
		specialityData = [GFCSpecialitySpecialityData]()
		if let specialityDataArray = dictionary["specialityData"] as? [NSDictionary]{
			for dic in specialityDataArray{
				let value = GFCSpecialitySpecialityData(fromDictionary: dic)
				specialityData.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if featuredDoctor != nil{
			var dictionaryElements = [NSDictionary]()
			for featuredDoctorElement in featuredDoctor {
				dictionaryElements.append(featuredDoctorElement.toDictionary())
			}
			dictionary["featured_doctor"] = dictionaryElements
		}
		if popularDoctor != nil{
			var dictionaryElements = [NSDictionary]()
			for popularDoctorElement in popularDoctor {
				dictionaryElements.append(popularDoctorElement.toDictionary())
			}
			dictionary["popular_doctor"] = dictionaryElements
		}
		if specialityData != nil{
			var dictionaryElements = [NSDictionary]()
			for specialityDataElement in specialityData {
				dictionaryElements.append(specialityDataElement.toDictionary())
			}
			dictionary["specialityData"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         featuredDoctor = aDecoder.decodeObject(forKey: "featured_doctor") as? [GFCSpecialityFeaturedDoctor]
         popularDoctor = aDecoder.decodeObject(forKey: "popular_doctor") as? [GFCSpecialityFeaturedDoctor]
         specialityData = aDecoder.decodeObject(forKey: "specialityData") as? [GFCSpecialitySpecialityData]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if featuredDoctor != nil{
			aCoder.encode(featuredDoctor, forKey: "featured_doctor")
		}
		if popularDoctor != nil{
			aCoder.encode(popularDoctor, forKey: "popular_doctor")
		}
		if specialityData != nil{
			aCoder.encode(specialityData, forKey: "specialityData")
		}

	}

}