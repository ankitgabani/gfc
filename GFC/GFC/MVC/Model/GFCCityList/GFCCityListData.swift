//
//	GFCCityListData.swift
//
//	Create by iMac on 1/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCCityListData : NSObject, NSCoding{

	var cityId : String!
	var cityName : String!
	var stateId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		cityId = dictionary["city_id"] as? String == nil ? "" : dictionary["city_id"] as? String
		cityName = dictionary["city_name"] as? String == nil ? "" : dictionary["city_name"] as? String
		stateId = dictionary["state_id"] as? String == nil ? "" : dictionary["state_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if cityId != nil{
			dictionary["city_id"] = cityId
		}
		if cityName != nil{
			dictionary["city_name"] = cityName
		}
		if stateId != nil{
			dictionary["state_id"] = stateId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cityId = aDecoder.decodeObject(forKey: "city_id") as? String
         cityName = aDecoder.decodeObject(forKey: "city_name") as? String
         stateId = aDecoder.decodeObject(forKey: "state_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if cityId != nil{
			aCoder.encode(cityId, forKey: "city_id")
		}
		if cityName != nil{
			aCoder.encode(cityName, forKey: "city_name")
		}
		if stateId != nil{
			aCoder.encode(stateId, forKey: "state_id")
		}

	}

}