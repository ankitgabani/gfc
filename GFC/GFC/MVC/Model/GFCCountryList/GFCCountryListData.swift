//
//	GFCCountryListData.swift
//
//	Create by iMac on 28/2/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCCountryListData : NSObject, NSCoding{

	var countryId : String!
	var countryName : String!
	var createdDate : String!
	var currency : String!
	var phoneCode : String!
	var sortname : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		countryId = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
		countryName = dictionary["country_name"] as? String == nil ? "" : dictionary["country_name"] as? String
		createdDate = dictionary["created_date"] as? String == nil ? "" : dictionary["created_date"] as? String
		currency = dictionary["currency"] as? String == nil ? "" : dictionary["currency"] as? String
		phoneCode = dictionary["phone_code"] as? String == nil ? "" : dictionary["phone_code"] as? String
		sortname = dictionary["sortname"] as? String == nil ? "" : dictionary["sortname"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if countryName != nil{
			dictionary["country_name"] = countryName
		}
		if createdDate != nil{
			dictionary["created_date"] = createdDate
		}
		if currency != nil{
			dictionary["currency"] = currency
		}
		if phoneCode != nil{
			dictionary["phone_code"] = phoneCode
		}
		if sortname != nil{
			dictionary["sortname"] = sortname
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         countryName = aDecoder.decodeObject(forKey: "country_name") as? String
         createdDate = aDecoder.decodeObject(forKey: "created_date") as? String
         currency = aDecoder.decodeObject(forKey: "currency") as? String
         phoneCode = aDecoder.decodeObject(forKey: "phone_code") as? String
         sortname = aDecoder.decodeObject(forKey: "sortname") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if countryName != nil{
			aCoder.encode(countryName, forKey: "country_name")
		}
		if createdDate != nil{
			aCoder.encode(createdDate, forKey: "created_date")
		}
		if currency != nil{
			aCoder.encode(currency, forKey: "currency")
		}
		if phoneCode != nil{
			aCoder.encode(phoneCode, forKey: "phone_code")
		}
		if sortname != nil{
			aCoder.encode(sortname, forKey: "sortname")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
