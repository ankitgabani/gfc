//
//	GFCStateListData.swift
//
//	Create by iMac on 1/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCStateListData : NSObject, NSCoding{

	var countryId : String!
	var stateId : String!
	var stateName : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		countryId = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
		stateId = dictionary["state_id"] as? String == nil ? "" : dictionary["state_id"] as? String
		stateName = dictionary["state_name"] as? String == nil ? "" : dictionary["state_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if stateId != nil{
			dictionary["state_id"] = stateId
		}
		if stateName != nil{
			dictionary["state_name"] = stateName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         stateId = aDecoder.decodeObject(forKey: "state_id") as? String
         stateName = aDecoder.decodeObject(forKey: "state_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if stateId != nil{
			aCoder.encode(stateId, forKey: "state_id")
		}
		if stateName != nil{
			aCoder.encode(stateName, forKey: "state_name")
		}

	}

}