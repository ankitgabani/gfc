//
//	GFCAddressListData.swift
//
//	Create by admin on 1/2/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCAddressListData : NSObject, NSCoding{

	var address : String!
	var city : String!
	var country : String!
	var countryName : String!
	var createdAt : String!
	var id : String!
	var lat : String!
	var lng : String!
	var mobile : String!
	var name : String!
	var patientId : String!
	var pincode : String!
	var state : String!
	var status : String!
	var title : String!
    var locality : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		city = dictionary["city"] as? String == nil ? "" : dictionary["city"] as? String
		country = dictionary["country"] as? String == nil ? "" : dictionary["country"] as? String
		countryName = dictionary["country_name"] as? String == nil ? "" : dictionary["country_name"] as? String
		createdAt = dictionary["created_at"] as? String == nil ? "" : dictionary["created_at"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		lat = dictionary["lat"] as? String == nil ? "" : dictionary["lat"] as? String
		lng = dictionary["lng"] as? String == nil ? "" : dictionary["lng"] as? String
		mobile = dictionary["mobile"] as? String == nil ? "" : dictionary["mobile"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		patientId = dictionary["patient_id"] as? String == nil ? "" : dictionary["patient_id"] as? String
		pincode = dictionary["pincode"] as? String == nil ? "" : dictionary["pincode"] as? String
		state = dictionary["state"] as? String == nil ? "" : dictionary["state"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
        locality = dictionary["locality"] as? String == nil ? "" : dictionary["locality"] as? String

	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if countryName != nil{
			dictionary["country_name"] = countryName
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if id != nil{
			dictionary["id"] = id
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if lng != nil{
			dictionary["lng"] = lng
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if name != nil{
			dictionary["name"] = name
		}
		if patientId != nil{
			dictionary["patient_id"] = patientId
		}
		if pincode != nil{
			dictionary["pincode"] = pincode
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if title != nil{
			dictionary["title"] = title
		}
        if locality != nil{
            dictionary["locality"] = locality
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         countryName = aDecoder.decodeObject(forKey: "country_name") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         lat = aDecoder.decodeObject(forKey: "lat") as? String
         lng = aDecoder.decodeObject(forKey: "lng") as? String
         mobile = aDecoder.decodeObject(forKey: "mobile") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         patientId = aDecoder.decodeObject(forKey: "patient_id") as? String
         pincode = aDecoder.decodeObject(forKey: "pincode") as? String
         state = aDecoder.decodeObject(forKey: "state") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
        locality = aDecoder.decodeObject(forKey: "locality") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if countryName != nil{
			aCoder.encode(countryName, forKey: "country_name")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lng != nil{
			aCoder.encode(lng, forKey: "lng")
		}
		if mobile != nil{
			aCoder.encode(mobile, forKey: "mobile")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if patientId != nil{
			aCoder.encode(patientId, forKey: "patient_id")
		}
		if pincode != nil{
			aCoder.encode(pincode, forKey: "pincode")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
        if locality != nil{
            aCoder.encode(locality, forKey: "locality")
        }

	}

}
