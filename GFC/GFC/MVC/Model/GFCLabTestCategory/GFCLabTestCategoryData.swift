//
//	GFCLabTestCategoryData.swift
//
//	Create by iMac on 30/11/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCLabTestCategoryData : NSObject, NSCoding{

	var createdAt : String!
	var status : String!
	var testCateDescription : String!
	var testCateId : String!
	var testCateName : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		testCateDescription = dictionary["test_cate_description"] as? String == nil ? "" : dictionary["test_cate_description"] as? String
		testCateId = dictionary["test_cate_id"] as? String == nil ? "" : dictionary["test_cate_id"] as? String
		testCateName = dictionary["test_cate_name"] as? String == nil ? "" : dictionary["test_cate_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if status != nil{
			dictionary["status"] = status
		}
		if testCateDescription != nil{
			dictionary["test_cate_description"] = testCateDescription
		}
		if testCateId != nil{
			dictionary["test_cate_id"] = testCateId
		}
		if testCateName != nil{
			dictionary["test_cate_name"] = testCateName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         testCateDescription = aDecoder.decodeObject(forKey: "test_cate_description") as? String
         testCateId = aDecoder.decodeObject(forKey: "test_cate_id") as? String
         testCateName = aDecoder.decodeObject(forKey: "test_cate_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if testCateDescription != nil{
			aCoder.encode(testCateDescription, forKey: "test_cate_description")
		}
		if testCateId != nil{
			aCoder.encode(testCateId, forKey: "test_cate_id")
		}
		if testCateName != nil{
			aCoder.encode(testCateName, forKey: "test_cate_name")
		}

	}

}