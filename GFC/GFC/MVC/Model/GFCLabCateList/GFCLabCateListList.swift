//
//	GFCLabCateListList.swift
//
//	Create by iMac on 1/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCLabCateListList : NSObject, NSCoding{

	var address : String!
	var createdAt : String!
	var createdBy : String!
	var descriptionField : String!
	var discountedPrice : String!
	var image : String!
	var includedTests : String!
	var labId : String!
	var labName : String!
	var name : String!
	var precautions : String!
	var price : String!
	var status : String!
	var testCategoryName : String!
	var testId : String!
	var testCateId : String!
    var rating : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		createdBy = dictionary["created_by"] as? String == nil ? "" : dictionary["created_by"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		discountedPrice = dictionary["discounted_price"] as? String == nil ? "" : dictionary["discounted_price"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		includedTests = dictionary["included_tests"] as? String == nil ? "" : dictionary["included_tests"] as? String
		labId = dictionary["labId"] as? String == nil ? "" : dictionary["labId"] as? String
		labName = dictionary["labName"] as? String == nil ? "" : dictionary["labName"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		precautions = dictionary["precautions"] as? String == nil ? "" : dictionary["precautions"] as? String
		price = dictionary["price"] as? String == nil ? "" : dictionary["price"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		testCategoryName = dictionary["testCategoryName"] as? String == nil ? "" : dictionary["testCategoryName"] as? String
		testId = dictionary["testId"] as? String == nil ? "" : dictionary["testId"] as? String
		testCateId = dictionary["test_cate_id"] as? String == nil ? "" : dictionary["test_cate_id"] as? String
        rating = dictionary["rating"] as? String == nil ? "" : dictionary["rating"] as? String

	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if discountedPrice != nil{
			dictionary["discounted_price"] = discountedPrice
		}
		if image != nil{
			dictionary["image"] = image
		}
		if includedTests != nil{
			dictionary["included_tests"] = includedTests
		}
		if labId != nil{
			dictionary["labId"] = labId
		}
		if labName != nil{
			dictionary["labName"] = labName
		}
		if name != nil{
			dictionary["name"] = name
		}
		if precautions != nil{
			dictionary["precautions"] = precautions
		}
		if price != nil{
			dictionary["price"] = price
		}
		if status != nil{
			dictionary["status"] = status
		}
		if testCategoryName != nil{
			dictionary["testCategoryName"] = testCategoryName
		}
		if testId != nil{
			dictionary["testId"] = testId
		}
		if testCateId != nil{
			dictionary["test_cate_id"] = testCateId
		}
        if rating != nil{
            dictionary["rating"] = rating
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         discountedPrice = aDecoder.decodeObject(forKey: "discounted_price") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         includedTests = aDecoder.decodeObject(forKey: "included_tests") as? String
         labId = aDecoder.decodeObject(forKey: "labId") as? String
         labName = aDecoder.decodeObject(forKey: "labName") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         precautions = aDecoder.decodeObject(forKey: "precautions") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         testCategoryName = aDecoder.decodeObject(forKey: "testCategoryName") as? String
         testId = aDecoder.decodeObject(forKey: "testId") as? String
         testCateId = aDecoder.decodeObject(forKey: "test_cate_id") as? String
        rating = aDecoder.decodeObject(forKey: "rating") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if discountedPrice != nil{
			aCoder.encode(discountedPrice, forKey: "discounted_price")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if includedTests != nil{
			aCoder.encode(includedTests, forKey: "included_tests")
		}
		if labId != nil{
			aCoder.encode(labId, forKey: "labId")
		}
		if labName != nil{
			aCoder.encode(labName, forKey: "labName")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if precautions != nil{
			aCoder.encode(precautions, forKey: "precautions")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if testCategoryName != nil{
			aCoder.encode(testCategoryName, forKey: "testCategoryName")
		}
		if testId != nil{
			aCoder.encode(testId, forKey: "testId")
		}
		if testCateId != nil{
			aCoder.encode(testCateId, forKey: "test_cate_id")
		}
        if rating != nil{
            aCoder.encode(rating, forKey: "rating")
        }

	}

}
