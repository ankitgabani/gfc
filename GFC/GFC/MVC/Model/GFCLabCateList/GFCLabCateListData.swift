//
//	GFCLabCateListData.swift
//
//	Create by iMac on 1/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCLabCateListData : NSObject, NSCoding{

	var imageUrl : String!
	var lists : [GFCLabCateListList]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		imageUrl = dictionary["imageUrl"] as? String == nil ? "" : dictionary["imageUrl"] as? String
		lists = [GFCLabCateListList]()
		if let listsArray = dictionary["lists"] as? [NSDictionary]{
			for dic in listsArray{
				let value = GFCLabCateListList(fromDictionary: dic)
				lists.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if imageUrl != nil{
			dictionary["imageUrl"] = imageUrl
		}
		if lists != nil{
			var dictionaryElements = [NSDictionary]()
			for listsElement in lists {
				dictionaryElements.append(listsElement.toDictionary())
			}
			dictionary["lists"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         imageUrl = aDecoder.decodeObject(forKey: "imageUrl") as? String
         lists = aDecoder.decodeObject(forKey: "lists") as? [GFCLabCateListList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if imageUrl != nil{
			aCoder.encode(imageUrl, forKey: "imageUrl")
		}
		if lists != nil{
			aCoder.encode(lists, forKey: "lists")
		}

	}

}