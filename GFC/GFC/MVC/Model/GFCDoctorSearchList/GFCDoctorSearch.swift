//
//	GFCDoctorSearch.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCDoctorSearch : NSObject, NSCoding{

	var doctorList : [GFCDoctorSearchDoctorList]!
	var message : String!
	var status : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		doctorList = [GFCDoctorSearchDoctorList]()
		if let doctorListArray = dictionary["doctor_list"] as? [NSDictionary]{
			for dic in doctorListArray{
				let value = GFCDoctorSearchDoctorList(fromDictionary: dic)
				doctorList.append(value)
			}
		}
		message = dictionary["message"] as? String == nil ? "" : dictionary["message"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if doctorList != nil{
			var dictionaryElements = [NSDictionary]()
			for doctorListElement in doctorList {
				dictionaryElements.append(doctorListElement.toDictionary())
			}
			dictionary["doctor_list"] = dictionaryElements
		}
		if message != nil{
			dictionary["message"] = message
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         doctorList = aDecoder.decodeObject(forKey: "doctor_list") as? [GFCDoctorSearchDoctorList]
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if doctorList != nil{
			aCoder.encode(doctorList, forKey: "doctor_list")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}