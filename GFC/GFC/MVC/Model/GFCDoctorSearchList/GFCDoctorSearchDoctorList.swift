//
//	GFCDoctorSearchDoctorList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCDoctorSearchDoctorList : NSObject, NSCoding{

	var address : String!
	var address2 : String!
	var address3 : String!
	var altMobileNo : String!
	var anniEmailSendYear : String!
	var anniSmsYear : String!
	var anniversary : String!
	var billingHeader : String!
	var birthEmailYear : String!
	var birthSmsYear : String!
	var branchId : String!
	var cityId : String!
	var consultantCharge : String!
	var countryId : String!
	var createdBy : String!
	var createdDate : String!
	var days : String!
	var deletedBy : String!
	var deletedDate : String!
	var descriptionField : String!
	var dob : String!
	var docRegNo : String!
	var doctorCode : String!
	var doctorName : String!
	var doctorPanelType : String!
	var doctorPayType : String!
	var doctorType : String!
	var email : String!
	var emergencyCharge : String!
	var experience : String!
	var headerContent : String!
	var hospitalName : String!
	var id : String!
	var image : String!
	var ipAddress : String!
	var isDeleted : String!
	var landlineNo : String!
	var latitude : String!
	var longitude : String!
	var marketingPersonId : String!
	var mobileNo : String!
	var modifiedBy : String!
	var modifiedDate : String!
	var oldCharge : String!
	var opdHeader : String!
	var panNo : String!
	var password : String!
	var perDayApt : String!
	var perPatientTiming : String!
	var pincode : String!
	var prescriptionHeader : String!
	var qualification : String!
	var ratePlanId : String!
	var scheduleType : String!
	var seprateHeader : String!
	var signature : String!
	var specializationName : String!
	var specilizationId : String!
	var stateId : String!
	var status : String!
	var timings : String!
	var username : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		address2 = dictionary["address2"] as? String == nil ? "" : dictionary["address2"] as? String
		address3 = dictionary["address3"] as? String == nil ? "" : dictionary["address3"] as? String
		altMobileNo = dictionary["alt_mobile_no"] as? String == nil ? "" : dictionary["alt_mobile_no"] as? String
		anniEmailSendYear = dictionary["anni_email_send_year"] as? String == nil ? "" : dictionary["anni_email_send_year"] as? String
		anniSmsYear = dictionary["anni_sms_year"] as? String == nil ? "" : dictionary["anni_sms_year"] as? String
		anniversary = dictionary["anniversary"] as? String == nil ? "" : dictionary["anniversary"] as? String
		billingHeader = dictionary["billing_header"] as? String == nil ? "" : dictionary["billing_header"] as? String
		birthEmailYear = dictionary["birth_email_year"] as? String == nil ? "" : dictionary["birth_email_year"] as? String
		birthSmsYear = dictionary["birth_sms_year"] as? String == nil ? "" : dictionary["birth_sms_year"] as? String
		branchId = dictionary["branch_id"] as? String == nil ? "" : dictionary["branch_id"] as? String
		cityId = dictionary["city_id"] as? String == nil ? "" : dictionary["city_id"] as? String
		consultantCharge = dictionary["consultant_charge"] as? String == nil ? "" : dictionary["consultant_charge"] as? String
		countryId = dictionary["country_id"] as? String == nil ? "" : dictionary["country_id"] as? String
		createdBy = dictionary["created_by"] as? String == nil ? "" : dictionary["created_by"] as? String
		createdDate = dictionary["created_date"] as? String == nil ? "" : dictionary["created_date"] as? String
		days = dictionary["days"] as? String == nil ? "" : dictionary["days"] as? String
		deletedBy = dictionary["deleted_by"] as? String == nil ? "" : dictionary["deleted_by"] as? String
		deletedDate = dictionary["deleted_date"] as? String == nil ? "" : dictionary["deleted_date"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		dob = dictionary["dob"] as? String == nil ? "" : dictionary["dob"] as? String
		docRegNo = dictionary["doc_reg_no"] as? String == nil ? "" : dictionary["doc_reg_no"] as? String
		doctorCode = dictionary["doctor_code"] as? String == nil ? "" : dictionary["doctor_code"] as? String
		doctorName = dictionary["doctor_name"] as? String == nil ? "" : dictionary["doctor_name"] as? String
		doctorPanelType = dictionary["doctor_panel_type"] as? String == nil ? "" : dictionary["doctor_panel_type"] as? String
		doctorPayType = dictionary["doctor_pay_type"] as? String == nil ? "" : dictionary["doctor_pay_type"] as? String
		doctorType = dictionary["doctor_type"] as? String == nil ? "" : dictionary["doctor_type"] as? String
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		emergencyCharge = dictionary["emergency_charge"] as? String == nil ? "" : dictionary["emergency_charge"] as? String
		experience = dictionary["experience"] as? String == nil ? "" : dictionary["experience"] as? String
		headerContent = dictionary["header_content"] as? String == nil ? "" : dictionary["header_content"] as? String
		hospitalName = dictionary["hospital_name"] as? String == nil ? "" : dictionary["hospital_name"] as? String
		id = dictionary["id"] as? String == nil ? "" : dictionary["id"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		ipAddress = dictionary["ip_address"] as? String == nil ? "" : dictionary["ip_address"] as? String
		isDeleted = dictionary["is_deleted"] as? String == nil ? "" : dictionary["is_deleted"] as? String
		landlineNo = dictionary["landline_no"] as? String == nil ? "" : dictionary["landline_no"] as? String
		latitude = dictionary["latitude"] as? String == nil ? "" : dictionary["latitude"] as? String
		longitude = dictionary["longitude"] as? String == nil ? "" : dictionary["longitude"] as? String
		marketingPersonId = dictionary["marketing_person_id"] as? String == nil ? "" : dictionary["marketing_person_id"] as? String
		mobileNo = dictionary["mobile_no"] as? String == nil ? "" : dictionary["mobile_no"] as? String
		modifiedBy = dictionary["modified_by"] as? String == nil ? "" : dictionary["modified_by"] as? String
		modifiedDate = dictionary["modified_date"] as? String == nil ? "" : dictionary["modified_date"] as? String
		oldCharge = dictionary["old_charge"] as? String == nil ? "" : dictionary["old_charge"] as? String
		opdHeader = dictionary["opd_header"] as? String == nil ? "" : dictionary["opd_header"] as? String
		panNo = dictionary["pan_no"] as? String == nil ? "" : dictionary["pan_no"] as? String
		password = dictionary["password"] as? String == nil ? "" : dictionary["password"] as? String
		perDayApt = dictionary["per_day_apt"] as? String == nil ? "" : dictionary["per_day_apt"] as? String
		perPatientTiming = dictionary["per_patient_timing"] as? String == nil ? "" : dictionary["per_patient_timing"] as? String
		pincode = dictionary["pincode"] as? String == nil ? "" : dictionary["pincode"] as? String
		prescriptionHeader = dictionary["prescription_header"] as? String == nil ? "" : dictionary["prescription_header"] as? String
		qualification = dictionary["qualification"] as? String == nil ? "" : dictionary["qualification"] as? String
		ratePlanId = dictionary["rate_plan_id"] as? String == nil ? "" : dictionary["rate_plan_id"] as? String
		scheduleType = dictionary["schedule_type"] as? String == nil ? "" : dictionary["schedule_type"] as? String
		seprateHeader = dictionary["seprate_header"] as? String == nil ? "" : dictionary["seprate_header"] as? String
		signature = dictionary["signature"] as? String == nil ? "" : dictionary["signature"] as? String
		specializationName = dictionary["specialization_name"] as? String == nil ? "" : dictionary["specialization_name"] as? String
		specilizationId = dictionary["specilization_id"] as? String == nil ? "" : dictionary["specilization_id"] as? String
		stateId = dictionary["state_id"] as? String == nil ? "" : dictionary["state_id"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		timings = dictionary["timings"] as? String == nil ? "" : dictionary["timings"] as? String
		username = dictionary["username"] as? String == nil ? "" : dictionary["username"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if address2 != nil{
			dictionary["address2"] = address2
		}
		if address3 != nil{
			dictionary["address3"] = address3
		}
		if altMobileNo != nil{
			dictionary["alt_mobile_no"] = altMobileNo
		}
		if anniEmailSendYear != nil{
			dictionary["anni_email_send_year"] = anniEmailSendYear
		}
		if anniSmsYear != nil{
			dictionary["anni_sms_year"] = anniSmsYear
		}
		if anniversary != nil{
			dictionary["anniversary"] = anniversary
		}
		if billingHeader != nil{
			dictionary["billing_header"] = billingHeader
		}
		if birthEmailYear != nil{
			dictionary["birth_email_year"] = birthEmailYear
		}
		if birthSmsYear != nil{
			dictionary["birth_sms_year"] = birthSmsYear
		}
		if branchId != nil{
			dictionary["branch_id"] = branchId
		}
		if cityId != nil{
			dictionary["city_id"] = cityId
		}
		if consultantCharge != nil{
			dictionary["consultant_charge"] = consultantCharge
		}
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if createdDate != nil{
			dictionary["created_date"] = createdDate
		}
		if days != nil{
			dictionary["days"] = days
		}
		if deletedBy != nil{
			dictionary["deleted_by"] = deletedBy
		}
		if deletedDate != nil{
			dictionary["deleted_date"] = deletedDate
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if dob != nil{
			dictionary["dob"] = dob
		}
		if docRegNo != nil{
			dictionary["doc_reg_no"] = docRegNo
		}
		if doctorCode != nil{
			dictionary["doctor_code"] = doctorCode
		}
		if doctorName != nil{
			dictionary["doctor_name"] = doctorName
		}
		if doctorPanelType != nil{
			dictionary["doctor_panel_type"] = doctorPanelType
		}
		if doctorPayType != nil{
			dictionary["doctor_pay_type"] = doctorPayType
		}
		if doctorType != nil{
			dictionary["doctor_type"] = doctorType
		}
		if email != nil{
			dictionary["email"] = email
		}
		if emergencyCharge != nil{
			dictionary["emergency_charge"] = emergencyCharge
		}
		if experience != nil{
			dictionary["experience"] = experience
		}
		if headerContent != nil{
			dictionary["header_content"] = headerContent
		}
		if hospitalName != nil{
			dictionary["hospital_name"] = hospitalName
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if ipAddress != nil{
			dictionary["ip_address"] = ipAddress
		}
		if isDeleted != nil{
			dictionary["is_deleted"] = isDeleted
		}
		if landlineNo != nil{
			dictionary["landline_no"] = landlineNo
		}
		if latitude != nil{
			dictionary["latitude"] = latitude
		}
		if longitude != nil{
			dictionary["longitude"] = longitude
		}
		if marketingPersonId != nil{
			dictionary["marketing_person_id"] = marketingPersonId
		}
		if mobileNo != nil{
			dictionary["mobile_no"] = mobileNo
		}
		if modifiedBy != nil{
			dictionary["modified_by"] = modifiedBy
		}
		if modifiedDate != nil{
			dictionary["modified_date"] = modifiedDate
		}
		if oldCharge != nil{
			dictionary["old_charge"] = oldCharge
		}
		if opdHeader != nil{
			dictionary["opd_header"] = opdHeader
		}
		if panNo != nil{
			dictionary["pan_no"] = panNo
		}
		if password != nil{
			dictionary["password"] = password
		}
		if perDayApt != nil{
			dictionary["per_day_apt"] = perDayApt
		}
		if perPatientTiming != nil{
			dictionary["per_patient_timing"] = perPatientTiming
		}
		if pincode != nil{
			dictionary["pincode"] = pincode
		}
		if prescriptionHeader != nil{
			dictionary["prescription_header"] = prescriptionHeader
		}
		if qualification != nil{
			dictionary["qualification"] = qualification
		}
		if ratePlanId != nil{
			dictionary["rate_plan_id"] = ratePlanId
		}
		if scheduleType != nil{
			dictionary["schedule_type"] = scheduleType
		}
		if seprateHeader != nil{
			dictionary["seprate_header"] = seprateHeader
		}
		if signature != nil{
			dictionary["signature"] = signature
		}
		if specializationName != nil{
			dictionary["specialization_name"] = specializationName
		}
		if specilizationId != nil{
			dictionary["specilization_id"] = specilizationId
		}
		if stateId != nil{
			dictionary["state_id"] = stateId
		}
		if status != nil{
			dictionary["status"] = status
		}
		if timings != nil{
			dictionary["timings"] = timings
		}
		if username != nil{
			dictionary["username"] = username
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         address2 = aDecoder.decodeObject(forKey: "address2") as? String
         address3 = aDecoder.decodeObject(forKey: "address3") as? String
         altMobileNo = aDecoder.decodeObject(forKey: "alt_mobile_no") as? String
         anniEmailSendYear = aDecoder.decodeObject(forKey: "anni_email_send_year") as? String
         anniSmsYear = aDecoder.decodeObject(forKey: "anni_sms_year") as? String
         anniversary = aDecoder.decodeObject(forKey: "anniversary") as? String
         billingHeader = aDecoder.decodeObject(forKey: "billing_header") as? String
         birthEmailYear = aDecoder.decodeObject(forKey: "birth_email_year") as? String
         birthSmsYear = aDecoder.decodeObject(forKey: "birth_sms_year") as? String
         branchId = aDecoder.decodeObject(forKey: "branch_id") as? String
         cityId = aDecoder.decodeObject(forKey: "city_id") as? String
         consultantCharge = aDecoder.decodeObject(forKey: "consultant_charge") as? String
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         createdDate = aDecoder.decodeObject(forKey: "created_date") as? String
         days = aDecoder.decodeObject(forKey: "days") as? String
         deletedBy = aDecoder.decodeObject(forKey: "deleted_by") as? String
         deletedDate = aDecoder.decodeObject(forKey: "deleted_date") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         dob = aDecoder.decodeObject(forKey: "dob") as? String
         docRegNo = aDecoder.decodeObject(forKey: "doc_reg_no") as? String
         doctorCode = aDecoder.decodeObject(forKey: "doctor_code") as? String
         doctorName = aDecoder.decodeObject(forKey: "doctor_name") as? String
         doctorPanelType = aDecoder.decodeObject(forKey: "doctor_panel_type") as? String
         doctorPayType = aDecoder.decodeObject(forKey: "doctor_pay_type") as? String
         doctorType = aDecoder.decodeObject(forKey: "doctor_type") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         emergencyCharge = aDecoder.decodeObject(forKey: "emergency_charge") as? String
         experience = aDecoder.decodeObject(forKey: "experience") as? String
         headerContent = aDecoder.decodeObject(forKey: "header_content") as? String
         hospitalName = aDecoder.decodeObject(forKey: "hospital_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         ipAddress = aDecoder.decodeObject(forKey: "ip_address") as? String
         isDeleted = aDecoder.decodeObject(forKey: "is_deleted") as? String
         landlineNo = aDecoder.decodeObject(forKey: "landline_no") as? String
         latitude = aDecoder.decodeObject(forKey: "latitude") as? String
         longitude = aDecoder.decodeObject(forKey: "longitude") as? String
         marketingPersonId = aDecoder.decodeObject(forKey: "marketing_person_id") as? String
         mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String
         modifiedBy = aDecoder.decodeObject(forKey: "modified_by") as? String
         modifiedDate = aDecoder.decodeObject(forKey: "modified_date") as? String
         oldCharge = aDecoder.decodeObject(forKey: "old_charge") as? String
         opdHeader = aDecoder.decodeObject(forKey: "opd_header") as? String
         panNo = aDecoder.decodeObject(forKey: "pan_no") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         perDayApt = aDecoder.decodeObject(forKey: "per_day_apt") as? String
         perPatientTiming = aDecoder.decodeObject(forKey: "per_patient_timing") as? String
         pincode = aDecoder.decodeObject(forKey: "pincode") as? String
         prescriptionHeader = aDecoder.decodeObject(forKey: "prescription_header") as? String
         qualification = aDecoder.decodeObject(forKey: "qualification") as? String
         ratePlanId = aDecoder.decodeObject(forKey: "rate_plan_id") as? String
         scheduleType = aDecoder.decodeObject(forKey: "schedule_type") as? String
         seprateHeader = aDecoder.decodeObject(forKey: "seprate_header") as? String
         signature = aDecoder.decodeObject(forKey: "signature") as? String
         specializationName = aDecoder.decodeObject(forKey: "specialization_name") as? String
         specilizationId = aDecoder.decodeObject(forKey: "specilization_id") as? String
         stateId = aDecoder.decodeObject(forKey: "state_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         timings = aDecoder.decodeObject(forKey: "timings") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if address2 != nil{
			aCoder.encode(address2, forKey: "address2")
		}
		if address3 != nil{
			aCoder.encode(address3, forKey: "address3")
		}
		if altMobileNo != nil{
			aCoder.encode(altMobileNo, forKey: "alt_mobile_no")
		}
		if anniEmailSendYear != nil{
			aCoder.encode(anniEmailSendYear, forKey: "anni_email_send_year")
		}
		if anniSmsYear != nil{
			aCoder.encode(anniSmsYear, forKey: "anni_sms_year")
		}
		if anniversary != nil{
			aCoder.encode(anniversary, forKey: "anniversary")
		}
		if billingHeader != nil{
			aCoder.encode(billingHeader, forKey: "billing_header")
		}
		if birthEmailYear != nil{
			aCoder.encode(birthEmailYear, forKey: "birth_email_year")
		}
		if birthSmsYear != nil{
			aCoder.encode(birthSmsYear, forKey: "birth_sms_year")
		}
		if branchId != nil{
			aCoder.encode(branchId, forKey: "branch_id")
		}
		if cityId != nil{
			aCoder.encode(cityId, forKey: "city_id")
		}
		if consultantCharge != nil{
			aCoder.encode(consultantCharge, forKey: "consultant_charge")
		}
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if createdDate != nil{
			aCoder.encode(createdDate, forKey: "created_date")
		}
		if days != nil{
			aCoder.encode(days, forKey: "days")
		}
		if deletedBy != nil{
			aCoder.encode(deletedBy, forKey: "deleted_by")
		}
		if deletedDate != nil{
			aCoder.encode(deletedDate, forKey: "deleted_date")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if dob != nil{
			aCoder.encode(dob, forKey: "dob")
		}
		if docRegNo != nil{
			aCoder.encode(docRegNo, forKey: "doc_reg_no")
		}
		if doctorCode != nil{
			aCoder.encode(doctorCode, forKey: "doctor_code")
		}
		if doctorName != nil{
			aCoder.encode(doctorName, forKey: "doctor_name")
		}
		if doctorPanelType != nil{
			aCoder.encode(doctorPanelType, forKey: "doctor_panel_type")
		}
		if doctorPayType != nil{
			aCoder.encode(doctorPayType, forKey: "doctor_pay_type")
		}
		if doctorType != nil{
			aCoder.encode(doctorType, forKey: "doctor_type")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if emergencyCharge != nil{
			aCoder.encode(emergencyCharge, forKey: "emergency_charge")
		}
		if experience != nil{
			aCoder.encode(experience, forKey: "experience")
		}
		if headerContent != nil{
			aCoder.encode(headerContent, forKey: "header_content")
		}
		if hospitalName != nil{
			aCoder.encode(hospitalName, forKey: "hospital_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if ipAddress != nil{
			aCoder.encode(ipAddress, forKey: "ip_address")
		}
		if isDeleted != nil{
			aCoder.encode(isDeleted, forKey: "is_deleted")
		}
		if landlineNo != nil{
			aCoder.encode(landlineNo, forKey: "landline_no")
		}
		if latitude != nil{
			aCoder.encode(latitude, forKey: "latitude")
		}
		if longitude != nil{
			aCoder.encode(longitude, forKey: "longitude")
		}
		if marketingPersonId != nil{
			aCoder.encode(marketingPersonId, forKey: "marketing_person_id")
		}
		if mobileNo != nil{
			aCoder.encode(mobileNo, forKey: "mobile_no")
		}
		if modifiedBy != nil{
			aCoder.encode(modifiedBy, forKey: "modified_by")
		}
		if modifiedDate != nil{
			aCoder.encode(modifiedDate, forKey: "modified_date")
		}
		if oldCharge != nil{
			aCoder.encode(oldCharge, forKey: "old_charge")
		}
		if opdHeader != nil{
			aCoder.encode(opdHeader, forKey: "opd_header")
		}
		if panNo != nil{
			aCoder.encode(panNo, forKey: "pan_no")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if perDayApt != nil{
			aCoder.encode(perDayApt, forKey: "per_day_apt")
		}
		if perPatientTiming != nil{
			aCoder.encode(perPatientTiming, forKey: "per_patient_timing")
		}
		if pincode != nil{
			aCoder.encode(pincode, forKey: "pincode")
		}
		if prescriptionHeader != nil{
			aCoder.encode(prescriptionHeader, forKey: "prescription_header")
		}
		if qualification != nil{
			aCoder.encode(qualification, forKey: "qualification")
		}
		if ratePlanId != nil{
			aCoder.encode(ratePlanId, forKey: "rate_plan_id")
		}
		if scheduleType != nil{
			aCoder.encode(scheduleType, forKey: "schedule_type")
		}
		if seprateHeader != nil{
			aCoder.encode(seprateHeader, forKey: "seprate_header")
		}
		if signature != nil{
			aCoder.encode(signature, forKey: "signature")
		}
		if specializationName != nil{
			aCoder.encode(specializationName, forKey: "specialization_name")
		}
		if specilizationId != nil{
			aCoder.encode(specilizationId, forKey: "specilization_id")
		}
		if stateId != nil{
			aCoder.encode(stateId, forKey: "state_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if timings != nil{
			aCoder.encode(timings, forKey: "timings")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}