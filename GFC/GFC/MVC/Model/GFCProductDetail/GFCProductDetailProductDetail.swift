//
//	GFCProductDetailProductDetail.swift
//
//	Create by iMac on 20/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCProductDetailProductDetail : NSObject, NSCoding{

	var brandName : String!
	var category : String!
	var childCatId : String!
	var childCategoryName : String!
	var createdAt : String!
	var createdBy : String!
	var descriptionField : String!
	var discountedPrice : String!
	var discountedPrice1 : String!
	var discountedPrice2 : String!
	var discountedPrice3 : String!
	var image : String!
	var images : [GFCProductDetailImage]!
	var moqPerUser : String!
	var name : String!
	var pId : String!
	var pharmacy : String!
	var pharmacyId : String!
	var pharmacyCateId : String!
	var price : String!
	var price1 : String!
	var price2 : String!
	var price3 : String!
	var productStock : String!
	var productStock1 : String!
	var productStock2 : String!
	var productStock3 : String!
	var size : String!
	var size1 : String!
	var size2 : String!
	var size3 : String!
	var status : String!
	var subCatId : String!
	var subCategoryName : String!
	var tags : String!
	var varients : [GFCProductDetailVarient]!
    var rating : Int!

	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		brandName = dictionary["brandName"] as? String == nil ? "" : dictionary["brandName"] as? String
		category = dictionary["category"] as? String == nil ? "" : dictionary["category"] as? String
		childCatId = dictionary["childCatId"] as? String == nil ? "" : dictionary["childCatId"] as? String
		childCategoryName = dictionary["childCategoryName"] as? String == nil ? "" : dictionary["childCategoryName"] as? String
		createdAt = dictionary["createdAt"] as? String == nil ? "" : dictionary["createdAt"] as? String
		createdBy = dictionary["created_by"] as? String == nil ? "" : dictionary["created_by"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		discountedPrice = dictionary["discounted_price"] as? String == nil ? "" : dictionary["discounted_price"] as? String
		discountedPrice1 = dictionary["discounted_price1"] as? String == nil ? "" : dictionary["discounted_price1"] as? String
		discountedPrice2 = dictionary["discounted_price2"] as? String == nil ? "" : dictionary["discounted_price2"] as? String
        discountedPrice3 = dictionary["discounted_price3"] as? String == nil ? "" : dictionary["discounted_price3"] as? String
        rating = dictionary["rating"] as? Int == nil ? 0 : dictionary["rating"] as? Int
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		images = [GFCProductDetailImage]()
		if let imagesArray = dictionary["images"] as? [NSDictionary]{
			for dic in imagesArray{
				let value = GFCProductDetailImage(fromDictionary: dic)
				images.append(value)
			}
		}
		moqPerUser = dictionary["moq_per_user"] as? String == nil ? "" : dictionary["moq_per_user"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		pId = dictionary["pId"] as? String == nil ? "" : dictionary["pId"] as? String
		pharmacy = dictionary["pharmacy"] as? String == nil ? "" : dictionary["pharmacy"] as? String
		pharmacyId = dictionary["pharmacyId"] as? String == nil ? "" : dictionary["pharmacyId"] as? String
		pharmacyCateId = dictionary["pharmacy_cate_id"] as? String == nil ? "" : dictionary["pharmacy_cate_id"] as? String
		price = dictionary["price"] as? String == nil ? "" : dictionary["price"] as? String
		price1 = dictionary["price1"] as? String == nil ? "" : dictionary["price1"] as? String
		price2 = dictionary["price2"] as? String == nil ? "" : dictionary["price2"] as? String
		price3 = dictionary["price3"] as? String == nil ? "" : dictionary["price3"] as? String
		productStock = dictionary["productStock"] as? String == nil ? "" : dictionary["productStock"] as? String
		productStock1 = dictionary["productStock1"] as? String == nil ? "" : dictionary["productStock1"] as? String
		productStock2 = dictionary["productStock2"] as? String == nil ? "" : dictionary["productStock2"] as? String
		productStock3 = dictionary["productStock3"] as? String == nil ? "" : dictionary["productStock3"] as? String
		size = dictionary["size"] as? String == nil ? "" : dictionary["size"] as? String
		size1 = dictionary["size1"] as? String == nil ? "" : dictionary["size1"] as? String
		size2 = dictionary["size2"] as? String == nil ? "" : dictionary["size2"] as? String
		size3 = dictionary["size3"] as? String == nil ? "" : dictionary["size3"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		subCatId = dictionary["subCatId"] as? String == nil ? "" : dictionary["subCatId"] as? String
		subCategoryName = dictionary["subCategoryName"] as? String == nil ? "" : dictionary["subCategoryName"] as? String
		tags = dictionary["tags"] as? String == nil ? "" : dictionary["tags"] as? String
		varients = [GFCProductDetailVarient]()
		if let varientsArray = dictionary["varients"] as? [NSDictionary]{
			for dic in varientsArray{
				let value = GFCProductDetailVarient(fromDictionary: dic)
				varients.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if brandName != nil{
			dictionary["brandName"] = brandName
		}
		if category != nil{
			dictionary["category"] = category
		}
		if childCatId != nil{
			dictionary["childCatId"] = childCatId
		}
		if childCategoryName != nil{
			dictionary["childCategoryName"] = childCategoryName
		}
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if discountedPrice != nil{
			dictionary["discounted_price"] = discountedPrice
		}
		if discountedPrice1 != nil{
			dictionary["discounted_price1"] = discountedPrice1
		}
		if discountedPrice2 != nil{
			dictionary["discounted_price2"] = discountedPrice2
		}
		if discountedPrice3 != nil{
			dictionary["discounted_price3"] = discountedPrice3
		}
		if image != nil{
			dictionary["image"] = image
		}
		if images != nil{
			var dictionaryElements = [NSDictionary]()
			for imagesElement in images {
				dictionaryElements.append(imagesElement.toDictionary())
			}
			dictionary["images"] = dictionaryElements
		}
		if moqPerUser != nil{
			dictionary["moq_per_user"] = moqPerUser
		}
		if name != nil{
			dictionary["name"] = name
		}
		if pId != nil{
			dictionary["pId"] = pId
		}
		if pharmacy != nil{
			dictionary["pharmacy"] = pharmacy
		}
		if pharmacyId != nil{
			dictionary["pharmacyId"] = pharmacyId
		}
		if pharmacyCateId != nil{
			dictionary["pharmacy_cate_id"] = pharmacyCateId
		}
		if price != nil{
			dictionary["price"] = price
		}
		if price1 != nil{
			dictionary["price1"] = price1
		}
		if price2 != nil{
			dictionary["price2"] = price2
		}
		if price3 != nil{
			dictionary["price3"] = price3
		}
		if productStock != nil{
			dictionary["productStock"] = productStock
		}
		if productStock1 != nil{
			dictionary["productStock1"] = productStock1
		}
		if productStock2 != nil{
			dictionary["productStock2"] = productStock2
		}
		if productStock3 != nil{
			dictionary["productStock3"] = productStock3
		}
		if size != nil{
			dictionary["size"] = size
		}
		if size1 != nil{
			dictionary["size1"] = size1
		}
		if size2 != nil{
			dictionary["size2"] = size2
		}
		if size3 != nil{
			dictionary["size3"] = size3
		}
		if status != nil{
			dictionary["status"] = status
		}
		if subCatId != nil{
			dictionary["subCatId"] = subCatId
		}
		if subCategoryName != nil{
			dictionary["subCategoryName"] = subCategoryName
		}
        if rating != nil{
            dictionary["rating"] = rating
        }
		if tags != nil{
			dictionary["tags"] = tags
		}
		if varients != nil{
			var dictionaryElements = [NSDictionary]()
			for varientsElement in varients {
				dictionaryElements.append(varientsElement.toDictionary())
			}
			dictionary["varients"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         brandName = aDecoder.decodeObject(forKey: "brandName") as? String
         category = aDecoder.decodeObject(forKey: "category") as? String
         childCatId = aDecoder.decodeObject(forKey: "childCatId") as? String
         childCategoryName = aDecoder.decodeObject(forKey: "childCategoryName") as? String
         createdAt = aDecoder.decodeObject(forKey: "createdAt") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         discountedPrice = aDecoder.decodeObject(forKey: "discounted_price") as? String
         discountedPrice1 = aDecoder.decodeObject(forKey: "discounted_price1") as? String
         discountedPrice2 = aDecoder.decodeObject(forKey: "discounted_price2") as? String
         discountedPrice3 = aDecoder.decodeObject(forKey: "discounted_price3") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         images = aDecoder.decodeObject(forKey: "images") as? [GFCProductDetailImage]
         moqPerUser = aDecoder.decodeObject(forKey: "moq_per_user") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         pId = aDecoder.decodeObject(forKey: "pId") as? String
         pharmacy = aDecoder.decodeObject(forKey: "pharmacy") as? String
         pharmacyId = aDecoder.decodeObject(forKey: "pharmacyId") as? String
         pharmacyCateId = aDecoder.decodeObject(forKey: "pharmacy_cate_id") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         price1 = aDecoder.decodeObject(forKey: "price1") as? String
         price2 = aDecoder.decodeObject(forKey: "price2") as? String
         price3 = aDecoder.decodeObject(forKey: "price3") as? String
         productStock = aDecoder.decodeObject(forKey: "productStock") as? String
         productStock1 = aDecoder.decodeObject(forKey: "productStock1") as? String
         productStock2 = aDecoder.decodeObject(forKey: "productStock2") as? String
         productStock3 = aDecoder.decodeObject(forKey: "productStock3") as? String
         size = aDecoder.decodeObject(forKey: "size") as? String
         size1 = aDecoder.decodeObject(forKey: "size1") as? String
         size2 = aDecoder.decodeObject(forKey: "size2") as? String
         size3 = aDecoder.decodeObject(forKey: "size3") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         subCatId = aDecoder.decodeObject(forKey: "subCatId") as? String
         subCategoryName = aDecoder.decodeObject(forKey: "subCategoryName") as? String
         tags = aDecoder.decodeObject(forKey: "tags") as? String
        rating = aDecoder.decodeObject(forKey: "rating") as? Int
         varients = aDecoder.decodeObject(forKey: "varients") as? [GFCProductDetailVarient]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if brandName != nil{
			aCoder.encode(brandName, forKey: "brandName")
		}
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if childCatId != nil{
			aCoder.encode(childCatId, forKey: "childCatId")
		}
		if childCategoryName != nil{
			aCoder.encode(childCategoryName, forKey: "childCategoryName")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "createdAt")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if discountedPrice != nil{
			aCoder.encode(discountedPrice, forKey: "discounted_price")
		}
		if discountedPrice1 != nil{
			aCoder.encode(discountedPrice1, forKey: "discounted_price1")
		}
		if discountedPrice2 != nil{
			aCoder.encode(discountedPrice2, forKey: "discounted_price2")
		}
		if discountedPrice3 != nil{
			aCoder.encode(discountedPrice3, forKey: "discounted_price3")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if images != nil{
			aCoder.encode(images, forKey: "images")
		}
		if moqPerUser != nil{
			aCoder.encode(moqPerUser, forKey: "moq_per_user")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if pId != nil{
			aCoder.encode(pId, forKey: "pId")
		}
		if pharmacy != nil{
			aCoder.encode(pharmacy, forKey: "pharmacy")
		}
		if pharmacyId != nil{
			aCoder.encode(pharmacyId, forKey: "pharmacyId")
		}
		if pharmacyCateId != nil{
			aCoder.encode(pharmacyCateId, forKey: "pharmacy_cate_id")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if price1 != nil{
			aCoder.encode(price1, forKey: "price1")
		}
		if price2 != nil{
			aCoder.encode(price2, forKey: "price2")
		}
		if price3 != nil{
			aCoder.encode(price3, forKey: "price3")
		}
		if productStock != nil{
			aCoder.encode(productStock, forKey: "productStock")
		}
		if productStock1 != nil{
			aCoder.encode(productStock1, forKey: "productStock1")
		}
		if productStock2 != nil{
			aCoder.encode(productStock2, forKey: "productStock2")
		}
		if productStock3 != nil{
			aCoder.encode(productStock3, forKey: "productStock3")
		}
		if size != nil{
			aCoder.encode(size, forKey: "size")
		}
		if size1 != nil{
			aCoder.encode(size1, forKey: "size1")
		}
		if size2 != nil{
			aCoder.encode(size2, forKey: "size2")
		}
		if size3 != nil{
			aCoder.encode(size3, forKey: "size3")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if subCatId != nil{
			aCoder.encode(subCatId, forKey: "subCatId")
		}
		if subCategoryName != nil{
			aCoder.encode(subCategoryName, forKey: "subCategoryName")
		}
		if tags != nil{
			aCoder.encode(tags, forKey: "tags")
		}
		if varients != nil{
			aCoder.encode(varients, forKey: "varients")
		}
        if rating != nil{
            aCoder.encode(rating, forKey: "rating")
        }

	}

}
