//
//	GFCProductDetailVarient.swift
//
//	Create by iMac on 20/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCProductDetailVarient : NSObject, NSCoding{

	var discountedPrice1 : String!
	var discountedPrice2 : String!
	var discountedPrice3 : String!
	var price1 : String!
	var price2 : String!
	var price3 : String!
	var productStock1 : String!
	var productStock2 : String!
	var productStock3 : String!
	var size1 : String!
	var size2 : String!
	var size3 : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		discountedPrice1 = dictionary["discounted_price1"] as? String == nil ? "" : dictionary["discounted_price1"] as? String
		discountedPrice2 = dictionary["discounted_price2"] as? String == nil ? "" : dictionary["discounted_price2"] as? String
		discountedPrice3 = dictionary["discounted_price3"] as? String == nil ? "" : dictionary["discounted_price3"] as? String
		price1 = dictionary["price1"] as? String == nil ? "" : dictionary["price1"] as? String
		price2 = dictionary["price2"] as? String == nil ? "" : dictionary["price2"] as? String
		price3 = dictionary["price3"] as? String == nil ? "" : dictionary["price3"] as? String
		productStock1 = dictionary["productStock1"] as? String == nil ? "" : dictionary["productStock1"] as? String
		productStock2 = dictionary["productStock2"] as? String == nil ? "" : dictionary["productStock2"] as? String
		productStock3 = dictionary["productStock3"] as? String == nil ? "" : dictionary["productStock3"] as? String
		size1 = dictionary["size1"] as? String == nil ? "" : dictionary["size1"] as? String
		size2 = dictionary["size2"] as? String == nil ? "" : dictionary["size2"] as? String
		size3 = dictionary["size3"] as? String == nil ? "" : dictionary["size3"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if discountedPrice1 != nil{
			dictionary["discounted_price1"] = discountedPrice1
		}
		if discountedPrice2 != nil{
			dictionary["discounted_price2"] = discountedPrice2
		}
		if discountedPrice3 != nil{
			dictionary["discounted_price3"] = discountedPrice3
		}
		if price1 != nil{
			dictionary["price1"] = price1
		}
		if price2 != nil{
			dictionary["price2"] = price2
		}
		if price3 != nil{
			dictionary["price3"] = price3
		}
		if productStock1 != nil{
			dictionary["productStock1"] = productStock1
		}
		if productStock2 != nil{
			dictionary["productStock2"] = productStock2
		}
		if productStock3 != nil{
			dictionary["productStock3"] = productStock3
		}
		if size1 != nil{
			dictionary["size1"] = size1
		}
		if size2 != nil{
			dictionary["size2"] = size2
		}
		if size3 != nil{
			dictionary["size3"] = size3
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         discountedPrice1 = aDecoder.decodeObject(forKey: "discounted_price1") as? String
         discountedPrice2 = aDecoder.decodeObject(forKey: "discounted_price2") as? String
         discountedPrice3 = aDecoder.decodeObject(forKey: "discounted_price3") as? String
         price1 = aDecoder.decodeObject(forKey: "price1") as? String
         price2 = aDecoder.decodeObject(forKey: "price2") as? String
         price3 = aDecoder.decodeObject(forKey: "price3") as? String
         productStock1 = aDecoder.decodeObject(forKey: "productStock1") as? String
         productStock2 = aDecoder.decodeObject(forKey: "productStock2") as? String
         productStock3 = aDecoder.decodeObject(forKey: "productStock3") as? String
         size1 = aDecoder.decodeObject(forKey: "size1") as? String
         size2 = aDecoder.decodeObject(forKey: "size2") as? String
         size3 = aDecoder.decodeObject(forKey: "size3") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if discountedPrice1 != nil{
			aCoder.encode(discountedPrice1, forKey: "discounted_price1")
		}
		if discountedPrice2 != nil{
			aCoder.encode(discountedPrice2, forKey: "discounted_price2")
		}
		if discountedPrice3 != nil{
			aCoder.encode(discountedPrice3, forKey: "discounted_price3")
		}
		if price1 != nil{
			aCoder.encode(price1, forKey: "price1")
		}
		if price2 != nil{
			aCoder.encode(price2, forKey: "price2")
		}
		if price3 != nil{
			aCoder.encode(price3, forKey: "price3")
		}
		if productStock1 != nil{
			aCoder.encode(productStock1, forKey: "productStock1")
		}
		if productStock2 != nil{
			aCoder.encode(productStock2, forKey: "productStock2")
		}
		if productStock3 != nil{
			aCoder.encode(productStock3, forKey: "productStock3")
		}
		if size1 != nil{
			aCoder.encode(size1, forKey: "size1")
		}
		if size2 != nil{
			aCoder.encode(size2, forKey: "size2")
		}
		if size3 != nil{
			aCoder.encode(size3, forKey: "size3")
		}

	}

}