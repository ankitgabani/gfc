//
//	GFCProductDetailData.swift
//
//	Create by iMac on 20/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCProductDetailData : NSObject, NSCoding{

	var imgURL : String!
	var productDetails : GFCProductDetailProductDetail!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		imgURL = dictionary["imgURL"] as? String == nil ? "" : dictionary["imgURL"] as? String
		if let productDetailsData = dictionary["product_details"] as? NSDictionary{
			productDetails = GFCProductDetailProductDetail(fromDictionary: productDetailsData)
		}
		else
		{
			productDetails = GFCProductDetailProductDetail(fromDictionary: NSDictionary.init())
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if imgURL != nil{
			dictionary["imgURL"] = imgURL
		}
		if productDetails != nil{
			dictionary["product_details"] = productDetails.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         imgURL = aDecoder.decodeObject(forKey: "imgURL") as? String
         productDetails = aDecoder.decodeObject(forKey: "product_details") as? GFCProductDetailProductDetail

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if imgURL != nil{
			aCoder.encode(imgURL, forKey: "imgURL")
		}
		if productDetails != nil{
			aCoder.encode(productDetails, forKey: "product_details")
		}

	}

}