//
//	GFCProductDetailImage.swift
//
//	Create by iMac on 20/3/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCProductDetailImage : NSObject, NSCoding{

	var creetedAt : String!
	var imgId : String!
	var imgName : String!
	var imgProductId : String!
	var userId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		creetedAt = dictionary["creeted_at"] as? String == nil ? "" : dictionary["creeted_at"] as? String
		imgId = dictionary["imgId"] as? String == nil ? "" : dictionary["imgId"] as? String
		imgName = dictionary["imgName"] as? String == nil ? "" : dictionary["imgName"] as? String
		imgProductId = dictionary["imgProductId"] as? String == nil ? "" : dictionary["imgProductId"] as? String
		userId = dictionary["user_id"] as? String == nil ? "" : dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if creetedAt != nil{
			dictionary["creeted_at"] = creetedAt
		}
		if imgId != nil{
			dictionary["imgId"] = imgId
		}
		if imgName != nil{
			dictionary["imgName"] = imgName
		}
		if imgProductId != nil{
			dictionary["imgProductId"] = imgProductId
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         creetedAt = aDecoder.decodeObject(forKey: "creeted_at") as? String
         imgId = aDecoder.decodeObject(forKey: "imgId") as? String
         imgName = aDecoder.decodeObject(forKey: "imgName") as? String
         imgProductId = aDecoder.decodeObject(forKey: "imgProductId") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if creetedAt != nil{
			aCoder.encode(creetedAt, forKey: "creeted_at")
		}
		if imgId != nil{
			aCoder.encode(imgId, forKey: "imgId")
		}
		if imgName != nil{
			aCoder.encode(imgName, forKey: "imgName")
		}
		if imgProductId != nil{
			aCoder.encode(imgProductId, forKey: "imgProductId")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}