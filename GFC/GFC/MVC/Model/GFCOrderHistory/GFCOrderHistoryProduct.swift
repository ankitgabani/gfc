//
//	GFCOrderHistoryProduct.swift
//
//	Create by iMac on 29/11/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCOrderHistoryProduct : NSObject, NSCoding{

	var category : String!
	var childCategoryName : String!
	var image : String!
	var name : String!
	var orderedDate : String!
	var pId : String!
	var pharmacy : String!
	var price : String!
	var quantity : String!
	var rating : String!
	var subCategoryName : String!
	var varient : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		category = dictionary["category"] as? String == nil ? "" : dictionary["category"] as? String
		childCategoryName = dictionary["childCategoryName"] as? String == nil ? "" : dictionary["childCategoryName"] as? String
		image = dictionary["image"] as? String == nil ? "" : dictionary["image"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		orderedDate = dictionary["ordered_date"] as? String == nil ? "" : dictionary["ordered_date"] as? String
		pId = dictionary["pId"] as? String == nil ? "" : dictionary["pId"] as? String
		pharmacy = dictionary["pharmacy"] as? String == nil ? "" : dictionary["pharmacy"] as? String
		price = dictionary["price"] as? String == nil ? "" : dictionary["price"] as? String
		quantity = dictionary["quantity"] as? String == nil ? "" : dictionary["quantity"] as? String
		rating = dictionary["rating"] as? String == nil ? "" : dictionary["rating"] as? String
		subCategoryName = dictionary["subCategoryName"] as? String == nil ? "" : dictionary["subCategoryName"] as? String
		varient = dictionary["varient"] as? String == nil ? "" : dictionary["varient"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if category != nil{
			dictionary["category"] = category
		}
		if childCategoryName != nil{
			dictionary["childCategoryName"] = childCategoryName
		}
		if image != nil{
			dictionary["image"] = image
		}
		if name != nil{
			dictionary["name"] = name
		}
		if orderedDate != nil{
			dictionary["ordered_date"] = orderedDate
		}
		if pId != nil{
			dictionary["pId"] = pId
		}
		if pharmacy != nil{
			dictionary["pharmacy"] = pharmacy
		}
		if price != nil{
			dictionary["price"] = price
		}
		if quantity != nil{
			dictionary["quantity"] = quantity
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if subCategoryName != nil{
			dictionary["subCategoryName"] = subCategoryName
		}
		if varient != nil{
			dictionary["varient"] = varient
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         category = aDecoder.decodeObject(forKey: "category") as? String
         childCategoryName = aDecoder.decodeObject(forKey: "childCategoryName") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         orderedDate = aDecoder.decodeObject(forKey: "ordered_date") as? String
         pId = aDecoder.decodeObject(forKey: "pId") as? String
         pharmacy = aDecoder.decodeObject(forKey: "pharmacy") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         quantity = aDecoder.decodeObject(forKey: "quantity") as? String
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         subCategoryName = aDecoder.decodeObject(forKey: "subCategoryName") as? String
         varient = aDecoder.decodeObject(forKey: "varient") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if childCategoryName != nil{
			aCoder.encode(childCategoryName, forKey: "childCategoryName")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if orderedDate != nil{
			aCoder.encode(orderedDate, forKey: "ordered_date")
		}
		if pId != nil{
			aCoder.encode(pId, forKey: "pId")
		}
		if pharmacy != nil{
			aCoder.encode(pharmacy, forKey: "pharmacy")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if quantity != nil{
			aCoder.encode(quantity, forKey: "quantity")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if subCategoryName != nil{
			aCoder.encode(subCategoryName, forKey: "subCategoryName")
		}
		if varient != nil{
			aCoder.encode(varient, forKey: "varient")
		}

	}

}
