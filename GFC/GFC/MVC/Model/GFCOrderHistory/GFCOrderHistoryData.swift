//
//	GFCOrderHistoryData.swift
//
//	Create by iMac on 29/11/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCOrderHistoryData : NSObject, NSCoding{

	var order : GFCOrderHistoryOrder!
	var products : [GFCOrderHistoryProduct]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		if let orderData = dictionary["order"] as? NSDictionary{
			order = GFCOrderHistoryOrder(fromDictionary: orderData)
		}
		else
		{
			order = GFCOrderHistoryOrder(fromDictionary: NSDictionary.init())
		}
		products = [GFCOrderHistoryProduct]()
		if let productsArray = dictionary["products"] as? [NSDictionary]{
			for dic in productsArray{
				let value = GFCOrderHistoryProduct(fromDictionary: dic)
				products.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if order != nil{
			dictionary["order"] = order.toDictionary()
		}
		if products != nil{
			var dictionaryElements = [NSDictionary]()
			for productsElement in products {
				dictionaryElements.append(productsElement.toDictionary())
			}
			dictionary["products"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         order = aDecoder.decodeObject(forKey: "order") as? GFCOrderHistoryOrder
         products = aDecoder.decodeObject(forKey: "products") as? [GFCOrderHistoryProduct]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if order != nil{
			aCoder.encode(order, forKey: "order")
		}
		if products != nil{
			aCoder.encode(products, forKey: "products")
		}

	}

}