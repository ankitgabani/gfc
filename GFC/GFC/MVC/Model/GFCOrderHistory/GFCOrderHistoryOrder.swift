//
//	GFCOrderHistoryOrder.swift
//
//	Create by iMac on 29/11/2023
//	Copyright © 2023. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GFCOrderHistoryOrder : NSObject, NSCoding{

	var address : String!
	var addressType : String!
	var amount : String!
	var cartIds : String!
	var city : String!
	var country : String!
	var created : String!
	var lat : String!
	var longField : String!
	var mobile : String!
	var name : String!
	var orderId : String!
	var orderStatus : String!
	var patientId : String!
	var paymentMode : String!
	var paymentStatus : String!
	var pincode : String!
	var state : String!
	var status : String!
	var title : String!
	var txnId : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		address = dictionary["address"] as? String == nil ? "" : dictionary["address"] as? String
		addressType = dictionary["address_type"] as? String == nil ? "" : dictionary["address_type"] as? String
		amount = dictionary["amount"] as? String == nil ? "" : dictionary["amount"] as? String
		cartIds = dictionary["cartIds"] as? String == nil ? "" : dictionary["cartIds"] as? String
		city = dictionary["city"] as? String == nil ? "" : dictionary["city"] as? String
		country = dictionary["country"] as? String == nil ? "" : dictionary["country"] as? String
		created = dictionary["created"] as? String == nil ? "" : dictionary["created"] as? String
		lat = dictionary["lat"] as? String == nil ? "" : dictionary["lat"] as? String
		longField = dictionary["long"] as? String == nil ? "" : dictionary["long"] as? String
		mobile = dictionary["mobile"] as? String == nil ? "" : dictionary["mobile"] as? String
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		orderId = dictionary["orderId"] as? String == nil ? "" : dictionary["orderId"] as? String
		orderStatus = dictionary["orderStatus"] as? String == nil ? "" : dictionary["orderStatus"] as? String
		patientId = dictionary["patientId"] as? String == nil ? "" : dictionary["patientId"] as? String
		paymentMode = dictionary["payment_mode"] as? String == nil ? "" : dictionary["payment_mode"] as? String
		paymentStatus = dictionary["payment_status"] as? String == nil ? "" : dictionary["payment_status"] as? String
		pincode = dictionary["pincode"] as? String == nil ? "" : dictionary["pincode"] as? String
		state = dictionary["state"] as? String == nil ? "" : dictionary["state"] as? String
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
		txnId = dictionary["txn_id"] as? String == nil ? "" : dictionary["txn_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if address != nil{
			dictionary["address"] = address
		}
		if addressType != nil{
			dictionary["address_type"] = addressType
		}
		if amount != nil{
			dictionary["amount"] = amount
		}
		if cartIds != nil{
			dictionary["cartIds"] = cartIds
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if created != nil{
			dictionary["created"] = created
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if longField != nil{
			dictionary["long"] = longField
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if name != nil{
			dictionary["name"] = name
		}
		if orderId != nil{
			dictionary["orderId"] = orderId
		}
		if orderStatus != nil{
			dictionary["orderStatus"] = orderStatus
		}
		if patientId != nil{
			dictionary["patientId"] = patientId
		}
		if paymentMode != nil{
			dictionary["payment_mode"] = paymentMode
		}
		if paymentStatus != nil{
			dictionary["payment_status"] = paymentStatus
		}
		if pincode != nil{
			dictionary["pincode"] = pincode
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if title != nil{
			dictionary["title"] = title
		}
		if txnId != nil{
			dictionary["txn_id"] = txnId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         addressType = aDecoder.decodeObject(forKey: "address_type") as? String
         amount = aDecoder.decodeObject(forKey: "amount") as? String
         cartIds = aDecoder.decodeObject(forKey: "cartIds") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         created = aDecoder.decodeObject(forKey: "created") as? String
         lat = aDecoder.decodeObject(forKey: "lat") as? String
         longField = aDecoder.decodeObject(forKey: "long") as? String
         mobile = aDecoder.decodeObject(forKey: "mobile") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         orderId = aDecoder.decodeObject(forKey: "orderId") as? String
         orderStatus = aDecoder.decodeObject(forKey: "orderStatus") as? String
         patientId = aDecoder.decodeObject(forKey: "patientId") as? String
         paymentMode = aDecoder.decodeObject(forKey: "payment_mode") as? String
         paymentStatus = aDecoder.decodeObject(forKey: "payment_status") as? String
         pincode = aDecoder.decodeObject(forKey: "pincode") as? String
         state = aDecoder.decodeObject(forKey: "state") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         txnId = aDecoder.decodeObject(forKey: "txn_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if addressType != nil{
			aCoder.encode(addressType, forKey: "address_type")
		}
		if amount != nil{
			aCoder.encode(amount, forKey: "amount")
		}
		if cartIds != nil{
			aCoder.encode(cartIds, forKey: "cartIds")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if created != nil{
			aCoder.encode(created, forKey: "created")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if longField != nil{
			aCoder.encode(longField, forKey: "long")
		}
		if mobile != nil{
			aCoder.encode(mobile, forKey: "mobile")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if orderId != nil{
			aCoder.encode(orderId, forKey: "orderId")
		}
		if orderStatus != nil{
			aCoder.encode(orderStatus, forKey: "orderStatus")
		}
		if patientId != nil{
			aCoder.encode(patientId, forKey: "patientId")
		}
		if paymentMode != nil{
			aCoder.encode(paymentMode, forKey: "payment_mode")
		}
		if paymentStatus != nil{
			aCoder.encode(paymentStatus, forKey: "payment_status")
		}
		if pincode != nil{
			aCoder.encode(pincode, forKey: "pincode")
		}
		if state != nil{
			aCoder.encode(state, forKey: "state")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if txnId != nil{
			aCoder.encode(txnId, forKey: "txn_id")
		}

	}

}