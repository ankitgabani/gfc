//
//  OnboardingScreenFVC.swift
//  GFC
//
//  Created by Parth Anghan on 18/10/21.
//

import UIKit
import Quickblox
class OnboardingScreenFVC: UIViewController {

    @IBOutlet weak var btnGetStarted: UIButton!
    
    @IBOutlet weak var lblTFindDoc: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnGetStarted.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.19), alpha: 1, x: 1, y: 1, blur: 5, spread: 0)
    }
    
    @IBAction func clickedStarted(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "OnboardingScreenSVC") as! OnboardingScreenSVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func clickedSkio(_ sender: Any) {
        UserDefaults.standard.setValue(true, forKey: "isIntroFinished")
        UserDefaults.standard.synchronize()

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
               return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = "\(t?.prefix(maxLength) ?? "")"
    }
}
