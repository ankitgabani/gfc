//
//  SignupViewController.swift
//  GFC
//
//  Created by Parth Anghan on 21/10/21.
//

import UIKit
import LGSideMenuController
import Toast_Swift
import DropDown
import DialCountries
import GoogleSignIn
import AuthenticationServices
import FirebaseAuth
import ADCountryPicker
import OTPFieldView
import CoreLocation
import CoreTelephony
import Quickblox
import QuickbloxWebRTC

class SignupViewController: UIViewController, UITextFieldDelegate,GIDSignInDelegate, ASAuthorizationControllerPresentationContextProviding, CLLocationManagerDelegate {
    @IBOutlet weak var btnBG: UIButton!
    
    
    @IBOutlet weak var viewBGG: UIView!
    
    @IBOutlet weak var mainViewOTP: UIView!
    @IBOutlet weak var viewOTP: OTPFieldView!
    
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmailPhone: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    
    @IBOutlet weak var imgPass: UIImageView!
    
    @IBOutlet weak var imgCheckUncheck: UIImageView!
    
    @IBOutlet weak var btnAgreementCheckBox: UIButton!
    
    @IBOutlet weak var txtSelectCountry: UITextField!
    @IBOutlet weak var viewSelectCountry: UIView!
    @IBOutlet weak var imgCountry: UIImageView!
    
    @IBOutlet weak var txtSelectState: UITextField!
    @IBOutlet weak var viewSelectState: UIView!
    @IBOutlet weak var imgState: UIImageView!
    
    @IBOutlet weak var txtSelecteCity: UITextField!
    @IBOutlet weak var viewSelectCity: UIView!
    @IBOutlet weak var imgCity: UIImageView!
    
    @IBOutlet weak var btnSelectCountry: UIButton!
    @IBOutlet weak var btnSelectState: UIButton!
    @IBOutlet weak var btnSelectCity: UIButton!
    @IBOutlet weak var lblAgree: UILabel!
    
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var viewTimer: UIView!
    @IBOutlet weak var lblTimer: UILabel!
    
    @IBOutlet weak var lblTOtpVerify: UILabel!
    @IBOutlet weak var lblTOtpDes: UILabel!
    @IBOutlet weak var lblTResend: UILabel!
    
    @IBOutlet weak var btnTVerify: UIButton!
    
    @IBOutlet weak var lblTSignUp: UILabel!
    @IBOutlet weak var lblTGoogle: UILabel!
    @IBOutlet weak var lblTApple: UILabel!
    
    @IBOutlet weak var btnTSignUp: UIButton!
    @IBOutlet weak var btnTHaveAcc: UIButton!
    @IBOutlet weak var btnTPrivacyPolicy: UIButton!
    
    let dropDownCountry = DropDown()
    let dropDownState = DropDown()
    let dropDownCity = DropDown()
    
    var country_id = ""
    var state_id = ""
    var city_id = ""
    
    var enterOTP = ""
    
    var isAppleLogin:Bool = false
    fileprivate var currentNonce: String?
    
    var arrCountryList = NSMutableArray()
    var arrStateList = NSMutableArray()
    var arrCityList = NSMutableArray()
    
    var myTimer: Timer?

    var counter = 59
    
    private var locationManager: CLLocationManager!
    var objLatitude = "21.00"
    var objLongitude = "72.00"
    
    private let authModule = AuthModule()
    private var connection = ConnectionModule()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        authModule.delegate = self
        connection.delegate = self
        
//        DispatchQueue.main.async {
//            if CLLocationManager.locationServicesEnabled(){
//                self.locationManager.delegate = self
//                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
//                self.locationManager.distanceFilter = 10
//                self.locationManager.requestWhenInUseAuthorization()
//                self.locationManager.requestAlwaysAuthorization()
//                self.locationManager.startUpdatingLocation()
//            }
//        }
        
        lblTSignUp.text = "Sign Up".localizeString(string: Language.shared.currentAppLang)
        lblTGoogle.text = "Google".localizeString(string: Language.shared.currentAppLang)
        lblTApple.text = "Sign in with Apple".localizeString(string: Language.shared.currentAppLang)
        lblAgree.text = "I agree with the".localizeString(string: Language.shared.currentAppLang)
        
        txtName.placeholder = "Name".localizeString(string: Language.shared.currentAppLang)
        txtEmailPhone.placeholder = "Email Address".localizeString(string: Language.shared.currentAppLang)
        txtPhone.placeholder = "Phone Number".localizeString(string: Language.shared.currentAppLang)
        txtSelectCountry.text = "Select Country".localizeString(string: Language.shared.currentAppLang)
        txtSelectState.text = "Select State".localizeString(string: Language.shared.currentAppLang)
        txtSelecteCity.placeholder = "Enter City".localizeString(string: Language.shared.currentAppLang)
        txtPass.placeholder = "Password".localizeString(string: Language.shared.currentAppLang)
        
        btnTPrivacyPolicy.setTitle("Privacy Policy".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnTSignUp.setTitle("Sign Up".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnTHaveAcc.setTitle("Have an account? Log in".localizeString(string: Language.shared.currentAppLang), for: .normal)

        if Language.shared.isArabic == false{
            txtName.textAlignment = .left
            txtEmailPhone.textAlignment = .left
            txtPhone.textAlignment = .left
            txtSelectCountry.textAlignment = .left
            txtSelectState.textAlignment = .left
            txtSelecteCity.textAlignment = .left
            txtPass.textAlignment = .left
        }else{
            txtName.textAlignment = .right
            txtEmailPhone.textAlignment = .right
            txtPhone.textAlignment = .right
            txtSelectCountry.textAlignment = .right
            txtSelectState.textAlignment = .right
            txtSelecteCity.textAlignment = .right
            txtPass.textAlignment = .right
        }

        btnBG.isHidden = true
        viewBGG.isHidden = true
        mainViewOTP.isHidden = true
        
        mainViewOTP.clipsToBounds = true
        mainViewOTP.layer.cornerRadius = 30
        mainViewOTP.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        txtPass.isSecureTextEntry = false
        imgPass.image = UIImage(named: "ic_HidePass")
        
        mainViewOTP.isHidden = true
        
        txtCountryCode.delegate = self
        
        btnAgreementCheckBox.isSelected = false
        
        txtSelecteCity.delegate = self
      
        callGetCountryAPI()
       // setupOtpView()
        
        self.btnResend.isHidden = true
        self.viewTimer.isHidden = false
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        // Request location authorization
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    
    }
    
    func getCountryPhonceCode (_ country : String) -> String
        {
            var countryDictionary  = ["AF":"93",
                                      "AL":"355",
                                      "DZ":"213",
                                      "AS":"1",
                                      "AD":"376",
                                      "AO":"244",
                                      "AI":"1",
                                      "AG":"1",
                                      "AR":"54",
                                      "AM":"374",
                                      "AW":"297",
                                      "AU":"61",
                                      "AT":"43",
                                      "AZ":"994",
                                      "BS":"1",
                                      "BH":"973",
                                      "BD":"880",
                                      "BB":"1",
                                      "BY":"375",
                                      "BE":"32",
                                      "BZ":"501",
                                      "BJ":"229",
                                      "BM":"1",
                                      "BT":"975",
                                      "BA":"387",
                                      "BW":"267",
                                      "BR":"55",
                                      "IO":"246",
                                      "BG":"359",
                                      "BF":"226",
                                      "BI":"257",
                                      "KH":"855",
                                      "CM":"237",
                                      "CA":"1",
                                      "CV":"238",
                                      "KY":"345",
                                      "CF":"236",
                                      "TD":"235",
                                      "CL":"56",
                                      "CN":"86",
                                      "CX":"61",
                                      "CO":"57",
                                      "KM":"269",
                                      "CG":"242",
                                      "CK":"682",
                                      "CR":"506",
                                      "HR":"385",
                                      "CU":"53",
                                      "CY":"537",
                                      "CZ":"420",
                                      "DK":"45",
                                      "DJ":"253",
                                      "DM":"1",
                                      "DO":"1",
                                      "EC":"593",
                                      "EG":"20",
                                      "SV":"503",
                                      "GQ":"240",
                                      "ER":"291",
                                      "EE":"372",
                                      "ET":"251",
                                      "FO":"298",
                                      "FJ":"679",
                                      "FI":"358",
                                      "FR":"33",
                                      "GF":"594",
                                      "PF":"689",
                                      "GA":"241",
                                      "GM":"220",
                                      "GE":"995",
                                      "DE":"49",
                                      "GH":"233",
                                      "GI":"350",
                                      "GR":"30",
                                      "GL":"299",
                                      "GD":"1",
                                      "GP":"590",
                                      "GU":"1",
                                      "GT":"502",
                                      "GN":"224",
                                      "GW":"245",
                                      "GY":"595",
                                      "HT":"509",
                                      "HN":"504",
                                      "HU":"36",
                                      "IS":"354",
                                      "IN":"91",
                                      "ID":"62",
                                      "IQ":"964",
                                      "IE":"353",
                                      "IL":"972",
                                      "IT":"39",
                                      "JM":"1",
                                      "JP":"81",
                                      "JO":"962",
                                      "KZ":"77",
                                      "KE":"254",
                                      "KI":"686",
                                      "KW":"965",
                                      "KG":"996",
                                      "LV":"371",
                                      "LB":"961",
                                      "LS":"266",
                                      "LR":"231",
                                      "LI":"423",
                                      "LT":"370",
                                      "LU":"352",
                                      "MG":"261",
                                      "MW":"265",
                                      "MY":"60",
                                      "MV":"960",
                                      "ML":"223",
                                      "MT":"356",
                                      "MH":"692",
                                      "MQ":"596",
                                      "MR":"222",
                                      "MU":"230",
                                      "YT":"262",
                                      "MX":"52",
                                      "MC":"377",
                                      "MN":"976",
                                      "ME":"382",
                                      "MS":"1",
                                      "MA":"212",
                                      "MM":"95",
                                      "NA":"264",
                                      "NR":"674",
                                      "NP":"977",
                                      "NL":"31",
                                      "AN":"599",
                                      "NC":"687",
                                      "NZ":"64",
                                      "NI":"505",
                                      "NE":"227",
                                      "NG":"234",
                                      "NU":"683",
                                      "NF":"672",
                                      "MP":"1",
                                      "NO":"47",
                                      "OM":"968",
                                      "PK":"92",
                                      "PW":"680",
                                      "PA":"507",
                                      "PG":"675",
                                      "PY":"595",
                                      "PE":"51",
                                      "PH":"63",
                                      "PL":"48",
                                      "PT":"351",
                                      "PR":"1",
                                      "QA":"974",
                                      "RO":"40",
                                      "RW":"250",
                                      "WS":"685",
                                      "SM":"378",
                                      "SA":"966",
                                      "SN":"221",
                                      "RS":"381",
                                      "SC":"248",
                                      "SL":"232",
                                      "SG":"65",
                                      "SK":"421",
                                      "SI":"386",
                                      "SB":"677",
                                      "ZA":"27",
                                      "GS":"500",
                                      "ES":"34",
                                      "LK":"94",
                                      "SD":"249",
                                      "SR":"597",
                                      "SZ":"268",
                                      "SE":"46",
                                      "CH":"41",
                                      "TJ":"992",
                                      "TH":"66",
                                      "TG":"228",
                                      "TK":"690",
                                      "TO":"676",
                                      "TT":"1",
                                      "TN":"216",
                                      "TR":"90",
                                      "TM":"993",
                                      "TC":"1",
                                      "TV":"688",
                                      "UG":"256",
                                      "UA":"380",
                                      "AE":"971",
                                      "GB":"44",
                                      "US":"1",
                                      "UY":"598",
                                      "UZ":"998",
                                      "VU":"678",
                                      "WF":"681",
                                      "YE":"967",
                                      "ZM":"260",
                                      "ZW":"263",
                                      "BO":"591",
                                      "BN":"673",
                                      "CC":"61",
                                      "CD":"243",
                                      "CI":"225",
                                      "FK":"500",
                                      "GG":"44",
                                      "VA":"379",
                                      "HK":"852",
                                      "IR":"98",
                                      "IM":"44",
                                      "JE":"44",
                                      "KP":"850",
                                      "KR":"82",
                                      "LA":"856",
                                      "LY":"218",
                                      "MO":"853",
                                      "MK":"389",
                                      "FM":"691",
                                      "MD":"373",
                                      "MZ":"258",
                                      "PS":"970",
                                      "PN":"872",
                                      "RE":"262",
                                      "RU":"7",
                                      "BL":"590",
                                      "SH":"290",
                                      "KN":"1",
                                      "LC":"1",
                                      "MF":"590",
                                      "PM":"508",
                                      "VC":"1",
                                      "ST":"239",
                                      "SO":"252",
                                      "SJ":"47",
                                      "SY":"963",
                                      "TW":"886",
                                      "TZ":"255",
                                      "TL":"670",
                                      "VE":"58",
                                      "VN":"84",
                                      "VG":"284",
                                      "VI":"340"]
            if countryDictionary[country] != nil {
                return countryDictionary[country]!
            }

            else {
                return ""
            }

        }
    
    // Call the function

    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        
        // Reverse geocode the location to get the country information
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            if let error = error {
                print("Reverse geocoding failed with error: \(error.localizedDescription)")
                return
            }
            
            if let country = placemarks?.first?.country,
               let countryCode = placemarks?.first?.isoCountryCode {
                print("Country: \(country), Country Code: \(countryCode)")
//
//                if let phoneCode = self.getPhoneCodeForCountry(countryCode) {
//                    print("Phone Code: +\(phoneCode)")
//                } else {
//                    print("Unable to determine phone code.")
//                }
                
                // Stop updating location after getting the information
                self.locationManager.stopUpdatingLocation()
            } else {
                print("Unable to determine country and country code.")
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager failed with error: \(error.localizedDescription)")
    }
    
    
    // MARK: - setupOtpView
    
    func setupOtpView(){
        self.viewOTP.fieldsCount = 6
        self.viewOTP.fieldBorderWidth = 2
        self.viewOTP.defaultBorderColor = UIColor(red: 103/255, green: 114/255, blue: 148/255, alpha: 0.16)
        self.viewOTP.filledBorderColor = UIColor(red: 103/255, green: 114/255, blue: 148/255, alpha: 0.16)
        self.viewOTP.cursorColor = UIColor.black
        self.viewOTP.displayType = .roundedCorner
        self.viewOTP.fieldSize = 40
        self.viewOTP.separatorSpace = 8
        self.viewOTP.shouldAllowIntermediateEditing = false
        self.viewOTP.delegate = self
        self.viewOTP.initializeUI()
    }
    
    
    func setDropDownSelectCountry() {
        
        let arrCountry = NSMutableArray()
        
        for obj in self.arrCountryList
        {
            let country_name = (obj as? NSDictionary)?.value(forKey: "country_name") as? String
            
            arrCountry.add(country_name!)
        }
        
        dropDownCountry.dataSource = arrCountry as! [String]
        dropDownCountry.anchorView = btnSelectCountry
        dropDownCountry.direction = .any
        
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtSelectCountry.text = item
            
            for obj in self.arrCountryList
            {
                let country_name = (obj as? NSDictionary)?.value(forKey: "country_name") as? String
                let country_id = (obj as? NSDictionary)?.value(forKey: "country_id") as? String
                
                self.country_id = country_id ?? ""
                if item == country_name
                {
                    self.callGetStateAPI(ID: country_id ?? "")
                }
            }
            
            
        }
        
        dropDownCountry.bottomOffset = CGPoint(x: 0, y: btnSelectCountry.bounds.height)
        dropDownCountry.topOffset = CGPoint(x: 0, y: -btnSelectCountry.bounds.height)
        dropDownCountry.dismissMode = .onTap
        dropDownCountry.textColor = UIColor.darkGray
        dropDownCountry.backgroundColor = UIColor.white
        dropDownCountry.selectionBackgroundColor = UIColor.clear
        dropDownCountry.selectedTextColor = .black
        dropDownCountry.reloadAllComponents()
    }
    
    func setDropDownSelectState() {
        
        let arrState = NSMutableArray()
        
        for obj in self.arrStateList
        {
            let state_name = (obj as? NSDictionary)?.value(forKey: "state_name") as? String
            
            arrState.add(state_name!)
        }
        
        dropDownState.dataSource = arrState as! [String]
        dropDownState.anchorView = btnSelectState
        dropDownState.direction = .any
        
        dropDownState.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtSelectState.text = item
            
            for obj in self.arrStateList
            {
                let state_name = (obj as? NSDictionary)?.value(forKey: "state_name") as? String
                let state_id = (obj as? NSDictionary)?.value(forKey: "state_id") as? String
                
                self.state_id = state_id ?? ""
                if item == state_name
                {
                    self.callGetCityAPI(ID: state_id ?? "")
                }
            }
            
        }
        
        dropDownState.bottomOffset = CGPoint(x: 0, y: btnSelectState.bounds.height)
        dropDownState.topOffset = CGPoint(x: 0, y: -btnSelectState.bounds.height)
        dropDownState.dismissMode = .onTap
        dropDownState.textColor = UIColor.darkGray
        dropDownState.backgroundColor = UIColor.white
        dropDownState.selectionBackgroundColor = UIColor.clear
        dropDownState.selectedTextColor = .black

        dropDownState.reloadAllComponents()
    }
    
    func setDropDownSelectCity() {
        
        let arrCity = NSMutableArray()
        
        for obj in self.arrCityList
        {
            let city_name = (obj as? NSDictionary)?.value(forKey: "city_name") as? String
            
            arrCity.add(city_name!)
        }
        
        dropDownCity.dataSource = arrCity as! [String]
        dropDownCity.anchorView = self.btnSelectCity
        dropDownCity.direction = .any
        
        dropDownCity.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtSelecteCity.text = item
            
            for obj in self.arrCityList
            {
                let city_id = (obj as? NSDictionary)?.value(forKey: "city_id") as? String
                
                self.city_id = city_id ?? ""
               
            }
            
        }
        
        dropDownCity.bottomOffset = CGPoint(x: 0, y: btnSelectCity.bounds.height)
        dropDownCity.topOffset = CGPoint(x: 0, y: -btnSelectCity.bounds.height)
        dropDownCity.dismissMode = .onTap
        dropDownCity.textColor = UIColor.darkGray
        dropDownCity.backgroundColor = UIColor.white
        dropDownCity.selectionBackgroundColor = UIColor.clear
        dropDownCity.selectedTextColor = .black

        dropDownCity.reloadAllComponents()
    }
    
   
    @IBAction func clickedPP(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        vc.isSignUP = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func clickedOpenDropDown(_ sender: Any) {
  
        let controller = DialCountriesController(locale: Locale(identifier: "ae"))
        controller.delegate = self
        controller.show(vc: self)
     }
   
    @IBAction func clickedDropCountry(_ sender: Any) {
        self.view.endEditing(true)
        let picker = ADCountryPicker()
        // delegate
        picker.delegate = self
        
        // Display calling codes
        picker.showCallingCodes = true
        
        // or closure
        picker.didSelectCountryClosure = { name, code in
            _ = picker.navigationController?.popToRootViewController(animated: true)
            print(code)
        }
        
        // Use this below code to present the picker
        
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)

    }
    @IBAction func clickedDropState(_ sender: Any) {
        
        self.view.endEditing(true)
        
        dropDownState.show()
        
    }
    @IBAction func clickedDropCity(_ sender: Any) {
        
        self.view.endEditing(true)
        
        dropDownCity.show()
    }
    
    
    @IBAction func btnAgreementClicked(_ sender: Any) {
        if btnAgreementCheckBox.isSelected == true
        {
            btnAgreementCheckBox.isSelected = false
            imgCheckUncheck.image = UIImage(named: "uncheckbox")
        }
        else
        {
            btnAgreementCheckBox.isSelected = true
            imgCheckUncheck.image = UIImage(named: "checkbox")
        }
    }
    
    @IBAction func clickedShowHidePass(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            txtPass.isSecureTextEntry = false
            
            imgPass.image = UIImage(named: "ic_HidePass")
        }else{
            sender.isSelected = true
            txtPass.isSecureTextEntry = true
            
            imgPass.image = UIImage(named: "ic_ShowPass")
        }
    }
    
    @IBAction func clickedSignup(_ sender: Any) {
        
        self.view.hideAllToasts()
        
        let window = UIApplication.shared.windows
        window.last?.hideAllToasts()
        
        if txtName.text == ""
        {
            self.view.makeToast("Enter name".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter name".localizeString(string: Language.shared.currentAppLang))
        }
        else if txtEmailPhone.text == ""{
            self.view.makeToast("Enter email".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter email".localizeString(string: Language.shared.currentAppLang))
        }
        else if !AppUtilites.isValidEmail(testStr: txtEmailPhone.text ?? "") {
            self.view.makeToast("Enter valid email address".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter valid email address".localizeString(string: Language.shared.currentAppLang))
        }
        else if txtPhone.text == ""{
            self.view.makeToast("Enter Phone Number".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Phone Number".localizeString(string: Language.shared.currentAppLang))
        }
        else if (self.txtPhone.text?.count ?? 0) < 8
        {
            self.view.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))

            return
        }
        else if txtSelectCountry.text == "Select Country".localizeString(string: Language.shared.currentAppLang){
            self.view.makeToast("Select Country".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Select Country".localizeString(string: Language.shared.currentAppLang))
        }
        else if txtSelectCountry.text == "India"
        {
            if txtSelectState.text == "Select State".localizeString(string: Language.shared.currentAppLang)
            {
                self.view.makeToast("Select State".localizeString(string: Language.shared.currentAppLang))
                let window = UIApplication.shared.windows
                window.last?.makeToast("Select State".localizeString(string: Language.shared.currentAppLang))
            }
            else if txtSelecteCity.text == "Select City".localizeString(string: Language.shared.currentAppLang)
            {
                self.view.makeToast("Select City".localizeString(string: Language.shared.currentAppLang))
                let window = UIApplication.shared.windows
                window.last?.makeToast("Select City".localizeString(string: Language.shared.currentAppLang))
            }
            else if txtPass.text == ""
            {
                self.view.makeToast("Enter Password".localizeString(string: Language.shared.currentAppLang))
                let window = UIApplication.shared.windows
                window.last?.makeToast("Enter Password".localizeString(string: Language.shared.currentAppLang))

            }
            else if btnAgreementCheckBox.isSelected == false
            {
                self.view.makeToast("Agree with term & condition".localizeString(string: Language.shared.currentAppLang))
                let window = UIApplication.shared.windows
                window.last?.makeToast("Agree with term & condition".localizeString(string: Language.shared.currentAppLang))

            }
            else
            {
                self.callSendOTPAPI()
            }
        }
        else if txtSelecteCity.text == "Select City".localizeString(string: Language.shared.currentAppLang)
        {
            self.view.makeToast("Select City".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Select City".localizeString(string: Language.shared.currentAppLang))
        }
        else if txtPass.text == ""
        {
            self.view.makeToast("Enter Password".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Password".localizeString(string: Language.shared.currentAppLang))

        }
        else if btnAgreementCheckBox.isSelected == false
        {
            self.view.makeToast("Agree with term & condition".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Agree with term & condition".localizeString(string: Language.shared.currentAppLang))

        }
        else
        {
            self.callSendOTPAPI()
        }
        
    }
    @IBAction func clickedVerify(_ sender: Any) {
        if enterOTP == ""
        {
            self.view.makeToast("Enter OTP".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter OTP".localizeString(string: Language.shared.currentAppLang))
        }
        else
        {
            callVerifyAPI()
        }
    }
    
    func validateMobileNumber(_ mobileNumber: String) -> Bool {
        // Define a regular expression pattern for a valid mobile number
        let mobileNumberPattern = #"^\d{8,12}$"#

        // Create a regular expression object
        guard let regex = try? NSRegularExpression(pattern: mobileNumberPattern) else {
            return false
        }

        // Create a range for the entire string
        let range = NSRange(location: 0, length: mobileNumber.utf16.count)

        // Use the regular expression to check if the mobile number matches the pattern
        return regex.firstMatch(in: mobileNumber, options: [], range: range) != nil
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func clickedGoogl(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func clickedResendOTP(_ sender: Any) {
        setupOtpView()
        self.counter = 59
        startTimer()
        
        self.btnResend.isHidden = true
        self.viewTimer.isHidden = false
     
       callSendOTPAPI()
    }
    
    func startTimer() {
        
        self.lblTimer.text = "\(counter) sec"
        
        myTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)
    }
    
    
    @objc func timerFired() {
        
        if counter != 1 {
           // print("\(counter) seconds to the end of the world")
            counter -= 1
            
            self.lblTimer.text = "\(counter) sec"
        }
        else
        {
            stopTimer()
        }
        
        // Code to execute when the timer fires
    }
    
    func stopTimer() {
        
        counter = 59
        self.viewTimer.isHidden = true
        self.btnResend.isHidden = false

        myTimer?.invalidate()
    }
    
    
    
    @IBAction func clickedApple(_ sender: Any) {
        self.startSignInWithAppleFlow()
    }
    
    @IBAction func clickedHideBG(_ sender: Any) {
        btnBG.isHidden = true
        viewBGG.isHidden = true
        mainViewOTP.isHidden = true
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
  //      request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
//    @available(iOS 13, *)
//    private func sha256(_ input: String) -> String {
//        let inputData = Data(input.utf8)
//        let hashedData = SHA256.hash(data: inputData)
//        let hashString = hashedData.compactMap {
//            return String(format: "%02x", $0)
//        }.joined()
//
//        return hashString
//    }
    
    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
        Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        // let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile?.name
        let givenName = user.profile?.givenName
        let familyName = user.profile?.familyName
        let email = user.profile?.email
        
        callSocialLoginAPI(social_id: userId ?? "", social_type: "2", email: email ?? "", username: fullName?.replacingOccurrences(of: " ", with: "") ?? "")
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    //MARK: - API Calling
    
    func callSendOTPAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["mobile_number":self.txtPhone.text ?? "","dial_code":self.txtCountryCode.text?.replacingOccurrences(of: "+", with: "") ?? ""] as [String : Any]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(SEND_OTP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                APIClient.sharedInstance.hideIndicator()
                
                if IsSuccess != "error" {
                    if let responseUser = response {
                        self.setupOtpView()
                        self.counter = 59
                        self.startTimer()
                        self.btnBG.isHidden = false
                        self.mainViewOTP.isHidden = false
                        self.viewBGG.isHidden = false
                        self.view.makeToast(message)
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                
                print("error \(String(describing: error))")
            }
        })
    }
    
    func callVerifyAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["mobile_number":"\(self.txtPhone.text ?? "")","otp":self.enterOTP,"dial_code":self.txtCountryCode.text?.replacingOccurrences(of: "+", with: "") ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(VERIFY_OTP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                
                
                if IsSuccess != "error" {
                    if let responseUser = response {
                        self.callSignUpAPI()
                    } else {
                        APIClient.sharedInstance.hideIndicator()
                        self.view.makeToast(message)
                    }
                    
                } else {
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                
                print("error \(String(describing: error))")
            }
        })
    }
    
    
    func callSignUpAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["name": txtName.text ?? "",
                     "email": txtEmailPhone.text ?? "",
                     "password": txtPass.text ?? "",
                     "mobileNumber": txtPhone.text?.replacingOccurrences(of: "+", with: "") ?? "",
                     "signuptype":"1",
                     "latitude":self.objLatitude,
                     "longitude":self.objLongitude,
                     "fcmToken":"kasjdglsa","country_id":country_id,"state_id":state_id,"city_id":city_id,"country_code":"\(self.txtCountryCode.text ?? "")"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(SIGNUP_API, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                APIClient.sharedInstance.hideIndicator()
                
                if IsSuccess != "error" {
                    
                    if let responseUser = response {
                        
                        let dicData = response?.value(forKey: "data") as? NSDictionary
                        
                        let objData = GFCLoginData(fromDictionary: dicData!)
                        
                        appDelegate?.dicUserLoginData = objData
                        appDelegate?.saveCurrentUserData(dic: objData)
                        
                        UserDefaults.standard.setValue(true, forKey: "UserLogin")
                        UserDefaults.standard.synchronize()
                        
                        self.authModule.signUp(fullName: objData.name ?? "", login: objData.email.before(first: "@"))
 
                        self.view.makeToast("Registered successfully")

                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                
                print("error \(String(describing: error))")
            }
        })
    }
    
    
    func callGetCountryAPI() {
        
       // APIClient.sharedInstance.showIndicator()
        
        let param = ["":""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_COUNTRY, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                APIClient.sharedInstance.hideIndicator()
                
                if IsSuccess != "error" {
                    
                    
                    if let responseUser = response {
                        
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        self.arrCountryList = arrData?.mutableCopy() as! NSMutableArray
                       
                        var strCountryName = ""
                        
                        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
                            print(countryCode)
                            
                            let countryName = Locale.current.localizedString(forRegionCode: countryCode)
                            print(countryName ?? "No name")
                            self.txtSelectCountry.text = countryName ?? ""
                            strCountryName = countryName ?? ""
                            
                            if countryCode != ""
                            {
                                let getCode = self.getCountryPhonceCode(countryCode)
                                print(getCode)
                                
                                self.txtCountryCode.text = "+\(getCode ?? "")"
                            }
                        }
                        
                        for objCountry in self.arrCountryList
                        {
                            let country_name = (objCountry as? NSDictionary)?.value(forKey: "country_name") as? String
                            let country_id = (objCountry as? NSDictionary)?.value(forKey: "country_id") as? String
                            
                            if strCountryName == country_name
                            {
                                self.country_id = country_id ?? ""
                                self.callGetStateAPI(ID: country_id ?? "")
                            }
                        }
                        
                        self.setDropDownSelectCountry()
                        
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                
                print("error \(String(describing: error))")
            }
        })
    }
    
    
    func callGetStateAPI(ID: String) {
        
     //   APIClient.sharedInstance.showIndicator()
        
        let param = ["":""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_STATE + ID, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                APIClient.sharedInstance.hideIndicator()
                
                if IsSuccess != "error" {
                    
                    
                    if let responseUser = response {
                        
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        self.arrStateList = arrData?.mutableCopy() as! NSMutableArray
                        
                        self.setDropDownSelectState()
                        
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                
                print("error \(String(describing: error))")
            }
        })
    }
    
    
    func callGetCityAPI(ID: String) {
        
       // APIClient.sharedInstance.showIndicator()
        
        let param = ["":""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_CITY + ID, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                APIClient.sharedInstance.hideIndicator()
                
                if IsSuccess != "error" {
                    
                    
                    if let responseUser = response {
                        
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        self.arrCityList = arrData?.mutableCopy() as! NSMutableArray
                        
                        self.setDropDownSelectCity()
                        
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                
                print("error \(String(describing: error))")
            }
        })
    }
    
    //MARK: - API Calling
    func callSocialLoginAPI(social_id: String,social_type: String,email: String,username: String)
    {
        let windows = UIApplication.shared.windows
        windows.last?.hideAllToasts()
        
        //APIClient.sharedInstance.showIndicator()
        
        let FirebaseToken = UserDefaults.standard.value(forKey: "FirebaseToken") as? String
        
        var param = ["":""]
        
        if social_type == "2"
        {
            param = ["email": email,"name": username,"signuptype":social_type,"g_id":social_id,"latitude":self.objLatitude,
                         "longitude":self.objLongitude,"fcmToken":FirebaseToken ?? "123"]
        }
        else
        {
            param = ["email": email,"name": username,"signuptype":social_type,"fb_id":social_id,"latitude":self.objLatitude,
                         "longitude":self.objLongitude,"fcmToken":FirebaseToken ?? "123"]
        }
       

        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(SIGNUP_API, parameters: param) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let message = response?["message"] as? String ?? ""
            let IsSuccess = response?["status"] as? String ?? ""

            if error == nil
            {
                APIClient.sharedInstance.hideIndicator()
                
                if IsSuccess != "error" {
                    
                    if let responseUser = response {
                        
                        let dicData = response?.value(forKey: "data") as? NSDictionary
                        
                        let objData = GFCLoginData(fromDictionary: dicData!)
                        
                        appDelegate?.dicUserLoginData = objData
                        appDelegate?.saveCurrentUserData(dic: objData)
                        
                        UserDefaults.standard.setValue(true, forKey: "UserLogin")
                        UserDefaults.standard.synchronize()
                                                                        
                        appDelegate?.setUpSideMenu()
                        
                    } else {
                        self.view.makeToast(message)

                    }
                    
                } else {
                    self.view.makeToast(message)

                    
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
                let windows = UIApplication.shared.windows
                windows.last?.makeToast(message)
            }
        }
        
    }
    
}

extension SignupViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        
        self.enterOTP = otpString ?? ""
    }
}

extension SignupViewController : DialCountriesControllerDelegate {
    func didSelected(with country: Country) {
//        JSN.log("selected country ===>%@", country.dialCode)
//        JSN.log("selected country flag ===>%@", country.flag)
        self.txtCountryCode.text = " + \((country.dialCode ?? "+91"))"
        
        self.txtCountryCode.text = country.dialCode ?? "+91"
    }
    
}



@available(iOS 13.0, *)
extension SignupViewController: ASAuthorizationControllerDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                fatalError("Invalid state: A login callback was received, but no login request was sent.\(appleIDCredential)")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token:- ")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data:\(appleIDToken.debugDescription)")
                return
            }
            
            let fullName = appleIDCredential.fullName
            
            let firstName = fullName?.givenName ?? fullName?.familyName
            var LastName  = fullName?.familyName ?? fullName?.givenName
            
            // Initialize a Firebase credential.
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            
            let userIdentifier = appleIDCredential.user

            // Sign in with Firebase.
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if (error != nil) {
                    print(error!.localizedDescription)
                    return
                }
                else{
                    let additionalInfo = authResult?.additionalUserInfo?.profile
                    
                    if let email = additionalInfo!["email"] as? String
                    {
                        print(email)
                        self.callSocialLoginAPI(social_id: userIdentifier, social_type: "3", email: email, username: firstName?.replacingOccurrences(of: " ", with: "") ?? "")
                    }
                    else
                    {
                      self.callSocialLoginAPI(social_id: userIdentifier, social_type: "3", email: "\(userIdentifier)@gmail.com", username: firstName?.replacingOccurrences(of: " ", with: "") ?? "")
                    }
                    
                }
                
            }
        }
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            // Handle error.
            print("Sign in with Apple errored: \(error)")
        }
        
    }
    
}

extension SignupViewController: ADCountryPickerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String)
    {
        _ = picker.navigationController?.popToRootViewController(animated: true)
        
        self.txtSelectCountry.text = name
        for obj in self.arrCountryList
        {
            let country_name = (obj as? NSDictionary)?.value(forKey: "country_name") as? String
            let country_id = (obj as? NSDictionary)?.value(forKey: "country_id") as? String
            
            self.country_id = country_id ?? ""
            if name == country_name
            {
                self.callGetStateAPI(ID: country_id ?? "")
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension String {
    func before(first delimiter: Character) -> String {
        if let index = firstIndex(of: delimiter) {
            let before = prefix(upTo: index)
            return String(before)
        }
        return ""
    }
    
    func after(first delimiter: Character) -> String {
        if let index = firstIndex(of: delimiter) {
            let after = suffix(from: index).dropFirst()
            return String(after)
        }
        return ""
    }
}


// MARK: - AuthModuleDelegate
extension SignupViewController: AuthModuleDelegate {
    func authModule(_ authModule: AuthModule, didSignUpUser user: QBUUser) {
        guard let fullName = user.fullName, let login = user.login else { return }
        Profile.synchronize(withUser: user)
        authModule.login(fullName: fullName, login: login)
    }
    
    func authModule(_ authModule: AuthModule, didLoginUser user: QBUUser) {
 
        let fullName = Profile().fullName
        if user.fullName != fullName {

            authModule.updateFullName(fullName: appDelegate?.dicUserLoginData.name ?? "")
            return
        }
        Profile.synchronize(withUser: user)
        connection.establish()
    }
    
    func authModule(_ authModule: AuthModule, didUpdateUpdateFullNameUser user: QBUUser) {
        Profile.synchronize(withUser: user)
        connection.establish()
    }
    
    func authModule(_ authModule: AuthModule, didReceivedError error: ErrorInfo) {
       // handleError(error)
    }
}

// MARK: - ConnectionModuleDelegate
extension SignupViewController: ConnectionModuleDelegate {
    func connectionModuleDidConnect(_ connectionModule: ConnectionModule) {
        //onCompleteAuth?()
    }
    
    func connectionModuleDidNotConnect(_ connectionModule: ConnectionModule, error: Error) {
        let error = ErrorInfo(info: error.localizedDescription, statusCode: error._code)
       // handleError(error)
    }
}

//MARK: - InputContainerDelegate
extension SignupViewController: InputContainerDelegate {
    func inputContainer(_ container: InputContainer, didChangeValidState isValid: Bool) {
        
    }
}
