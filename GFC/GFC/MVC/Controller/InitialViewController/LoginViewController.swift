//
//  LoginViewController.swift
//  GFC
//
//  Created by Parth Anghan on 18/10/21.
//

import UIKit
import OTPFieldView
import LGSideMenuController
import DropDown
import DialCountries
import GoogleSignIn
import AuthenticationServices
import FirebaseAuth
import CoreLocation
import Quickblox
import QuickbloxWebRTC

class LoginViewController: UIViewController, UITextFieldDelegate,GIDSignInDelegate, ASAuthorizationControllerPresentationContextProviding, CLLocationManagerDelegate {
    
    //MARK:- @IBOutlet
    @IBOutlet weak var btnSEnd: UIButton!
    @IBOutlet weak var btnResendOtp: UIButton!
    @IBOutlet weak var viewTimer: UIView!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var viewGoogle: UIView!
    @IBOutlet weak var viewFacebook: UIView!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var imgPass: UIImageView!
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewPopupForgotPass: UIView!
    @IBOutlet weak var txtPhoneForgot: UITextField!
    
    @IBOutlet weak var lblCodeForGot: UITextField!
    
    @IBOutlet weak var lblForgotPassTitle: UILabel!
    @IBOutlet weak var lblForgotPassSubTitle: UILabel!
    @IBOutlet weak var viewTxtPhone: UIView!
    @IBOutlet weak var viewOTP: OTPFieldView!
    
    @IBOutlet weak var viewResetPassPopup: UIView!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var txtRePass: UITextField!
    @IBOutlet weak var imgNewPass: UIImageView!
    @IBOutlet weak var imgRePass: UIImageView!
    
    @IBOutlet weak var viewMainOtp: UIView!
    
    @IBOutlet weak var lblTResetPassword: UILabel!
    @IBOutlet weak var lblTPasswordDes: UILabel!
    
    @IBOutlet weak var btnTUpdatePass: UIButton!
    
    @IBOutlet weak var lblTLogin: UILabel!
    
    @IBOutlet weak var lblTLoginWithGoogle: UILabel!
    @IBOutlet weak var lblTLoginWithApple: UILabel!
    
    @IBOutlet weak var btnTLogin: UIButton!
    @IBOutlet weak var btnTForgotPass: UIButton!
    @IBOutlet weak var btnTDontHaveAccp: UIButton!
    
    @IBOutlet weak var lblLanguage: UILabel!
    
    @IBOutlet weak var lblTOtpCode: UILabel!
    @IBOutlet weak var lblTOtpDes: UILabel!
    @IBOutlet weak var lblTResendIn: UILabel!
    
    @IBOutlet weak var btnVerifyOtp: UIButton!
    
    var str_User_OTP = ""
    var isOpenForgotPassPhone = true
    
    var str_patientId = ""
    
    var enteredOtp = ""
    
    let dropDown = DropDown()
    let dropDownLan = DropDown()

    var isAppleLogin:Bool = false
    fileprivate var currentNonce: String?
    
    var locationManager = CLLocationManager()
    var objLatitude = "21.00"
    var objLongitude = "72.00"
   
    var myTimer: Timer?

    var counter = 59
    
    var arrLan = ["English","Arabic","Kurdish"]
    
    private let authModule = AuthModule()
    private var connection = ConnectionModule()
 
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        authModule.delegate = self
        connection.delegate = self

        
//        DispatchQueue.main.async {
//            if CLLocationManager.locationServicesEnabled(){
//                self.locationManager.delegate = self
//                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
//                self.locationManager.distanceFilter = 10
//                self.locationManager.requestWhenInUseAuthorization()
//                self.locationManager.requestAlwaysAuthorization()
//                self.locationManager.startUpdatingLocation()
//            }
//        }
        
        if Language.shared.isArabic == false{
            txtPhone.textAlignment = .left
            txtPassword.textAlignment = .left
            txtRePass.textAlignment = .left
            txtNewPass.textAlignment = .left
            txtPhoneForgot.textAlignment = .left
        }else{
            txtPhone.textAlignment = .right
            txtPassword.textAlignment = .right
            txtRePass.textAlignment = .right
            txtNewPass.textAlignment = .right
            txtPhoneForgot.textAlignment = .right
        }
        
        lblTLogin.text = "Join us to start searching".localizeString(string: Language.shared.currentAppLang)
        lblTLoginWithGoogle.text = "Login With Google".localizeString(string: Language.shared.currentAppLang)
        lblTLoginWithApple.text = "Login With Apple".localizeString(string: Language.shared.currentAppLang)
        lblForgotPassTitle.text = "Forgot Password".localizeString(string: Language.shared.currentAppLang)
        lblForgotPassSubTitle.text = "Enter mobile number which\nyou have used at the time of signup".localizeString(string: Language.shared.currentAppLang)
        lblTOtpCode.text = "Enter 6 Digits Code".localizeString(string: Language.shared.currentAppLang)
        lblTOtpDes.text = "Enter the 6 digits code that you received on your Registered\nmobile number.".localizeString(string: Language.shared.currentAppLang)
        lblTResendIn.text = "Resend OTP In".localizeString(string: Language.shared.currentAppLang)
        lblTResetPassword.text = "Reset Password".localizeString(string: Language.shared.currentAppLang)
        lblTPasswordDes.text = "Set the new password for your account so you can login\nand access all the features.".localizeString(string: Language.shared.currentAppLang)

        txtPhone.placeholder = "Phone Number".localizeString(string: Language.shared.currentAppLang)
        txtPassword.placeholder = "Password".localizeString(string: Language.shared.currentAppLang)
        txtPhoneForgot.placeholder = "Phone Number".localizeString(string: Language.shared.currentAppLang)
        txtNewPass.placeholder = "New Password".localizeString(string: Language.shared.currentAppLang)
        txtRePass.placeholder = "Confirm Password".localizeString(string: Language.shared.currentAppLang)

        btnTLogin.setTitle("Login".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnTForgotPass.setTitle("Forgot Password".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnTDontHaveAccp.setTitle("Don’t have an account? Join us".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnSEnd.setTitle("Send OTP".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnVerifyOtp.setTitle("Verify OTP".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnTUpdatePass.setTitle("Update Password".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        txtCountryCode.delegate = self
        
        // The view to which the drop down will appear on
        dropDown.anchorView = txtCountryCode // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["+1","+91","+268","+2","+56"]
    
        setUp()
        
        self.btnResendOtp.isHidden = true
        self.viewTimer.isHidden = false

        imgPass.image = UIImage(named: "ic_HidePass")
        txtPassword.isSecureTextEntry = true
        
        txtNewPass.isSecureTextEntry = true
        imgNewPass.image = UIImage(named: "ic_HidePass")
        
        txtRePass.isSecureTextEntry = true
        imgRePass.image = UIImage(named: "ic_HidePass")
        
        setDropDownLan()
        
        // Do any additional setup after loading the view.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        objLatitude = "\(location?.coordinate.latitude ?? 0.0)"
        objLongitude = "\(location?.coordinate.longitude ?? 0.0)"
                
        self.locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.btnResendOtp.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setDropDownLan()
    {
        dropDownLan.dataSource = arrLan
        dropDownLan.anchorView = lblLanguage
        dropDownLan.direction = .bottom
        
        dropDownLan.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblLanguage.text = item
            
            if index == 0{
                Language.shared.isArabic = false
                txtPhone.textAlignment = .left
                txtPassword.textAlignment = .left
                txtPhoneForgot.textAlignment = .left
                lblForgotPassSubTitle.textAlignment = .left
            }else{
                Language.shared.isArabic = true
                txtPhone.textAlignment = .right
                txtPassword.textAlignment = .right
                txtPhoneForgot.textAlignment = .right
                lblForgotPassSubTitle.textAlignment = .right
            }
            viewDidLoad()
            DispatchQueue.main.async {
                setSemantricFlow()
            }
            
        }
        dropDownLan.bottomOffset = CGPoint(x: 0, y: lblLanguage.bounds.height)
        dropDownLan.topOffset = CGPoint(x: 0, y: -lblLanguage.bounds.height)
        dropDownLan.dismissMode = .onTap
        dropDownLan.textColor = UIColor.darkGray
        dropDownLan.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownLan.selectionBackgroundColor = UIColor.clear
        dropDownLan.selectedTextColor = .black

        dropDownLan.reloadAllComponents()
    }
    
    @IBAction func DropDownOpenClicked(_ sender: Any) {
        
        let controller = DialCountriesController(locale: Locale(identifier: "ae"))
            controller.delegate = self
            controller.show(vc: self)        
    }
    
    func setUp()
    {
        lblForgotPassTitle.text = "Forgot Password".localizeString(string: Language.shared.currentAppLang)
        lblForgotPassSubTitle.text = "Enter mobile number which\nyou have used at the time of signup".localizeString(string: Language.shared.currentAppLang)
        btnSEnd.setTitle("Send OTP".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        viewBg.isHidden = true
        viewPopupForgotPass.isHidden = true
        viewResetPassPopup.isHidden = true
        viewMainOtp.isHidden = true
        
        viewMainOtp.clipsToBounds = true
        viewMainOtp.layer.cornerRadius = 30
        viewMainOtp.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner

        
        viewPopupForgotPass.clipsToBounds = true
        viewPopupForgotPass.layer.cornerRadius = 30
        viewPopupForgotPass.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        viewResetPassPopup.clipsToBounds = true
        viewResetPassPopup.layer.cornerRadius = 30
        viewResetPassPopup.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewBg.addGestureRecognizer(tap)
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
        // Close
        self.viewPopupForgotPass.slideOut(to: kFTAnimationBottom, in: self.viewPopupForgotPass.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        self.viewResetPassPopup.slideOut(to: kFTAnimationBottom, in: self.viewResetPassPopup.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewBg.isHidden = true
        }
    }
    
    func setupOtpView(){
        self.viewOTP.fieldsCount = 6
        self.viewOTP.filledBackgroundColor = .clear
        self.viewOTP.defaultBackgroundColor = .clear
        self.viewOTP.cursorColor = UIColor.init(hexString: "677294", alpha: 1)
        self.viewOTP.filledBorderColor = UIColor.init(hexString: "677294", alpha: 0.16)
        self.viewOTP.defaultBorderColor = UIColor.init(hexString: "677294", alpha: 0.16)
        self.viewOTP.tintColor = .red
        self.viewOTP.borderColor = .yellow
        self.viewOTP.displayType = .roundedCorner
        self.viewOTP.fieldBorderWidth = 0.8
        self.viewOTP.fieldSize = 40
        self.viewOTP.separatorSpace = 8
        self.viewOTP.fieldFont = UIFont(name: "PTSans-Bold", size: 26.0)!
        self.viewOTP.shouldAllowIntermediateEditing = false
        self.viewOTP.delegate = self
        self.viewOTP.initializeUI()
    }
    
    //MARK: - Action Method
    
    @IBAction func clickedLan(_ sender: Any) {
        dropDownLan.show()
        
    }
    
    @IBAction func clickedSelectCodeForGot(_ sender: Any) {
        let controller = DialCountriesController(locale: Locale(identifier: "ae"))
            controller.delegate = self
            controller.show(vc: self)
    }
    
    @IBAction func clickedShowHidePass(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            imgPass.image = UIImage(named: "ic_ShowPass")
            txtPassword.isSecureTextEntry = false
        }else{
            sender.isSelected = true
            imgPass.image = UIImage(named: "ic_HidePass")
            txtPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func clickedShowNewPass(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            txtNewPass.isSecureTextEntry = false
            imgNewPass.image = UIImage(named: "ic_ShowPass")
        }else{
            sender.isSelected = true
            txtNewPass.isSecureTextEntry = true
            imgNewPass.image = UIImage(named: "ic_HidePass")
        }

    }
    
    @IBAction func clickedShowRePass(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            txtRePass.isSecureTextEntry = false
            imgRePass.image = UIImage(named: "ic_ShowPass")
        }else{
            sender.isSelected = true
            txtRePass.isSecureTextEntry = true
            imgRePass.image = UIImage(named: "ic_HidePass")
        }

    }
    
    @IBAction func clickedGoogle(_ sender: Any)
    {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().signIn()
    }
   
    @IBAction func clickedAppleLogin(_ sender: Any)
    {
        self.startSignInWithAppleFlow()
    }
    
    @IBAction func clickedVerifyOtp(_ sender: Any) {
        if str_User_OTP == enteredOtp
        {
            
            self.viewPopupForgotPass.slideOut(to: kFTAnimationBottom, in: self.viewPopupForgotPass.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
            self.viewMainOtp.slideOut(to: kFTAnimationBottom, in: self.viewPopupForgotPass.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))

            
            self.viewResetPassPopup.slideIn(from: kFTAnimationBottom, in: self.viewResetPassPopup.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        }
        else
        {
            self.view.makeToast("Enter valid OTP".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter valid OTP".localizeString(string: Language.shared.currentAppLang))

        }
    }
    
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
  //      request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
//    @available(iOS 13, *)
//    private func sha256(_ input: String) -> String {
//        let inputData = Data(input.utf8)
//        let hashedData = SHA256.hash(data: inputData)
//        let hashString = hashedData.compactMap {
//            return String(format: "%02x", $0)
//        }.joined()
//
//        return hashString
//    }
    
    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
        Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        // let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile?.name
        let givenName = user.profile?.givenName
        let familyName = user.profile?.familyName
        let email = user.profile?.email
        
       callSocialLoginAPI(social_id: userId ?? "", social_type: "2", email: email ?? "", username: fullName?.replacingOccurrences(of: " ", with: "") ?? "")
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }

    
    @IBAction func clickedLogin(_ sender: Any) {
     
        if self.txtPhone.text == ""
        {
            self.view.makeToast("Enter Phone Number".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Phone Number".localizeString(string: Language.shared.currentAppLang))

            return
        }
        else if (self.txtPhone.text?.count ?? 0) < 8
        {
            self.view.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))

            return
        }
        else if self.txtPassword.text == ""
        {
            self.view.makeToast("Enter Password".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Password".localizeString(string: Language.shared.currentAppLang))

            return
        }
        else{
            callLoginAPI()
        }
    }
    
     
    @IBAction func clickedForgotPass(_ sender: Any) {
        

        isOpenForgotPassPhone = true
        viewResetPassPopup.isHidden = true
        viewMainOtp.isHidden = true
        viewPopupForgotPass.isHidden = false
        
        // Open
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.viewBg.isHidden = false
        }
        
        self.viewPopupForgotPass.slideIn(from: kFTAnimationBottom, in: self.viewPopupForgotPass.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
    }
    
    @IBAction func clickedJoinUs(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedContinueForgotPass(_ sender: Any) {
        
        if txtPhoneForgot.text == "" {
            self.view.makeToast("Enter Phone Number".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Phone Number".localizeString(string: Language.shared.currentAppLang))

            return
        }
        else if (self.txtPhoneForgot.text?.count ?? 0) < 8
        {
            self.view.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))

            return
        }
        
        if isOpenForgotPassPhone == true
        {
            callForgetPassAPI()
        }
        else
        {
            
            // VERYFY OTP
            
            if str_User_OTP == enteredOtp
            {
                self.viewPopupForgotPass.slideOut(to: kFTAnimationBottom, in: self.viewPopupForgotPass.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
                
                self.viewResetPassPopup.slideIn(from: kFTAnimationBottom, in: self.viewResetPassPopup.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
            }
            else
            {
                self.view.makeToast("Enter valid OTP".localizeString(string: Language.shared.currentAppLang))
                let window = UIApplication.shared.windows
                window.last?.makeToast("Enter valid OTP".localizeString(string: Language.shared.currentAppLang))

            }
            
            // Close
           
        }
    }
    
    @IBAction func clickedResendOtp(_ sender: Any) {
       callForgetPassAPI()
    }
    
    func startTimer() {
        
        self.lblTime.text = "00:\(counter)"
        
        myTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)
    }
    
    
    @objc func timerFired() {
        
        if counter != 1 {
           // print("\(counter) seconds to the end of the world")
            counter -= 1
            
            self.lblTime.text = "00:\(counter)"
        }
        else
        {
            stopTimer()
        }
        
        // Code to execute when the timer fires
    }
    
    func stopTimer() {
        
        counter = 59
        self.viewTimer.isHidden = true
        self.btnResendOtp.isHidden = false
        
        myTimer?.invalidate()
    }

    
    @IBAction func clickedContimuResetPass(_ sender: Any) {
        if txtNewPass.text == ""
        {
            self.view.makeToast("Please enter new password".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Please enter new password".localizeString(string: Language.shared.currentAppLang))
        }
        else if txtRePass.text == ""
        {
            self.view.makeToast("Please enter confirm password".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Please enter confirm password".localizeString(string: Language.shared.currentAppLang))
        }
        else if txtNewPass.text != txtRePass.text
        {
            self.view.makeToast("Confirm Password does not match".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Confirm Password does not match".localizeString(string: Language.shared.currentAppLang))
        }
        else
        {
            callResetPasswordPassAPI()
        }
    }
    
    
    func hexStringToUIColor (hex:String, is_Alpha:CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(is_Alpha)
        )
    }
    
    func callLoginAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["username": "\(txtPhone.text ?? "")","password": txtPassword.text!,"signuptype":"1","dial_code":"\(self.txtCountryCode.text?.replacingOccurrences(of: "+", with: "") ?? "")"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(LOGIN_API, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                
                APIClient.sharedInstance.hideIndicator()

                if IsSuccess != "error" {
                    
                    if let responseUser = response {
                        
                        let dicData = response?.value(forKey: "data") as? NSDictionary
                        
                        let objData = GFCLoginData(fromDictionary: dicData!)
                        
                        
                        appDelegate?.dicUserLoginData = objData
                        appDelegate?.saveCurrentUserData(dic: objData)
                        
                        UserDefaults.standard.setValue(true, forKey: "UserLogin")
                        UserDefaults.standard.synchronize()
                        
                       self.authModule.login(fullName: objData.name ?? "", login: objData.email.before(first: "@"))
 
                    } else {
                        self.view.makeToast(message)
                        let window = UIApplication.shared.windows
                        window.last?.makeToast(message)
                     }
                    
                } else {
                    self.view.makeToast(message)
                    let window = UIApplication.shared.windows
                    window.last?.makeToast(message)                    
                }
                
            } else {
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                print("error \(String(describing: error))")
            }
        })
    }
 
    func signUp(fullName: String, login: String) {
        let newUser = QBUUser()
        newUser.login = login
        newUser.fullName = fullName
        newUser.password = "12345678"
        QBRequest.signUp(newUser, successBlock: { [weak self] response, user in
           
            print("Register successful! \(response) \(user)")

            appDelegate?.setUpSideMenu()
            
        }, errorBlock: { [weak self] response in
            
            print("Register Fail! \(response)")
            
            QBRequest.logIn(withUserLogin: fullName, password: login, successBlock: { (response, user) in
                print("Login successful! \(response) \(user)")
                
                appDelegate?.setUpSideMenu()
                
            }, errorBlock: { (response) in
                print("Login error: \(response)")
                
                appDelegate?.setUpSideMenu()
            })
        })
    }
    
    
    //MARK: - API Calling
    func callSocialLoginAPI(social_id: String,social_type: String,email: String,username: String)
    {
        let windows = UIApplication.shared.windows
        windows.last?.hideAllToasts()
        
        //APIClient.sharedInstance.showIndicator()
        
        let FirebaseToken = UserDefaults.standard.value(forKey: "FirebaseToken") as? String
        
        var param = ["":""]
        
        if social_type == "2"
        {
            param = ["email": email,"name": username,"signuptype":social_type,"g_id":social_id,"latitude":self.objLatitude,
                     "longitude":self.objLongitude,"fcmToken":FirebaseToken ?? "123"]
        }
        else
        {
            param = ["email": email,"name": username,"signuptype":social_type,"fb_id":social_id,"latitude":self.objLatitude,
                         "longitude":self.objLongitude,"fcmToken":FirebaseToken ?? "123"]
        }
       

        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(LOGIN_API, parameters: param) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let message = response?["message"] as? String ?? ""
            let IsSuccess = response?["status"] as? String ?? ""

            if error == nil
            {
                APIClient.sharedInstance.hideIndicator()
                
                if IsSuccess != "error" {
                    
                    if let responseUser = response {
                        
                        let dicData = response?.value(forKey: "data") as? NSDictionary
                        
                        let objData = GFCLoginData(fromDictionary: dicData!)
                        
                        appDelegate?.dicUserLoginData = objData
                        appDelegate?.saveCurrentUserData(dic: objData)
              
                        UserDefaults.standard.setValue(true, forKey: "UserLogin")
                        UserDefaults.standard.synchronize()
                                                                        
                        appDelegate?.setUpSideMenu()
                        
                    } else {
                        self.view.makeToast(message)

                    }
                    
                } else {
                    self.view.makeToast(message)

                    
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
                let windows = UIApplication.shared.windows
                windows.last?.makeToast(message)
            }
        }
        
    }
    
    
    func callForgetPassAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["username": txtPhoneForgot.text!,"dial_code":self.lblCodeForGot.text ?? ""]
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(FORGET_PASSWORD, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
               
                APIClient.sharedInstance.hideIndicator()

                if IsSuccess != "error" {
                    
                    if let responseUser = response {
                        
                        self.setupOtpView()
                        self.isOpenForgotPassPhone = false
                        
                        self.viewPopupForgotPass.isHidden = true
                        self.viewMainOtp.isHidden = false
                        
                        self.counter = 59
                        self.startTimer()
                        
                        self.btnResendOtp.isHidden = true
                        self.viewTimer.isHidden = false

                        self.btnSEnd.setTitle("Verify OTP".localizeString(string: Language.shared.currentAppLang), for: .normal)
                        
                        let dicData = response?.value(forKey: "data") as? NSDictionary
                        let patientId = dicData?.value(forKey: "patientId") as? String
                        let otp = dicData?.value(forKey: "otp") as? Int
                        self.str_patientId = "\(patientId ?? "")"
                        self.str_User_OTP = "\(otp ?? 0)"
                        
                        appDelegate?.window?.makeToast(message)

                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                print("error \(String(describing: error))")
            }
        })
    }

    
    func callResetPasswordPassAPI() {
        if txtNewPass.text == ""
        {
            self.view.makeToast("Please enter new password".localizeString(string: Language.shared.currentAppLang))
            return
        }
        else if txtRePass.text == ""
        {
            self.view.makeToast("Please enter confirm password".localizeString(string: Language.shared.currentAppLang))
            return
        }
        else if txtNewPass.text != txtRePass.text
        {
            self.view.makeToast("Confirm Password does not match".localizeString(string: Language.shared.currentAppLang))
            return
        }
        
        APIClient.sharedInstance.showIndicator()
        
        let param = ["patientId":  self.str_patientId,"password": txtNewPass.text ?? "","confirm_password": txtRePass.text ?? ""]
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(RESET_PASSWORD, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
               
                APIClient.sharedInstance.hideIndicator()

                self.txtPhoneForgot.text = ""
                self.txtNewPass.text = ""
                self.txtRePass.text = ""

                
                if IsSuccess != "error" {
                    
                    if let responseUser = response {
                        
                        let dicData = response?.value(forKey: "data") as? NSDictionary
                        
                        appDelegate?.window?.makeToast(message)

                        
                        // Close
                        self.viewResetPassPopup.slideOut(to: kFTAnimationBottom, in: self.viewResetPassPopup.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.viewBg.isHidden = true
                        }
                        
                    } else {
                        self.view.makeToast(message)
                        
                        
                        // Close
                        self.viewResetPassPopup.slideOut(to: kFTAnimationBottom, in: self.viewResetPassPopup.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.viewBg.isHidden = true
                        }

                    }
                    
                } else {
                    self.view.makeToast(message)

                    
                    // Close
                    self.viewResetPassPopup.slideOut(to: kFTAnimationBottom, in: self.viewResetPassPopup.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.viewBg.isHidden = true
                    }
                    
                }
                
            } else {
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                print("error \(String(describing: error))")
            }
        })
    }
    
}

extension LoginViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        self.enteredOtp = otpString
        
        if self.enteredOtp == str_User_OTP
        {
            
        }
        
    }
}

extension LoginViewController : DialCountriesControllerDelegate {
    func didSelected(with country: Country) {
//        JSN.log("selected country ===>%@", country.dialCode)
//        JSN.log("selected country flag ===>%@", country.flag)
      //  self.txtCountryCode.text =  "+\(country.dialCode ?? "+91")"
        
        if self.viewBg.isHidden == true
        {
            self.txtCountryCode.text = country.dialCode ?? "+91"
        }
        else
        {
            self.lblCodeForGot.text = "\(country.dialCode ?? "+91")"
        }
        
    }
    
}

@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                fatalError("Invalid state: A login callback was received, but no login request was sent.\(appleIDCredential)")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token:- ")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data:\(appleIDToken.debugDescription)")
                return
            }
            
            let fullName = appleIDCredential.fullName
            
            let firstName = fullName?.givenName ?? fullName?.familyName
            var LastName  = fullName?.familyName ?? fullName?.givenName
            
            // Initialize a Firebase credential.
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            
            let userIdentifier = appleIDCredential.user

            // Sign in with Firebase.
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if (error != nil) {
                    print(error!.localizedDescription)
                    return
                }
                else{
                    let additionalInfo = authResult?.additionalUserInfo?.profile
                    
                    if let email = additionalInfo!["email"] as? String
                    {
                        print(email)
                        
                        if firstName?.replacingOccurrences(of: " ", with: "") == nil
                        {
                            
                            self.callSocialLoginAPI(social_id: userIdentifier, social_type: "3", email: email, username: userIdentifier)
                        }
                        else
                        {
                            
                            self.callSocialLoginAPI(social_id: userIdentifier, social_type: "3", email: email, username: firstName?.replacingOccurrences(of: " ", with: "") ?? "")
                        }
                    }
                    else
                    {
                        
                        if firstName?.replacingOccurrences(of: " ", with: "") == nil
                        {
                            
                            self.callSocialLoginAPI(social_id: userIdentifier, social_type: "3", email: "\(userIdentifier)@gmail.com", username: userIdentifier)
                        }
                        else
                        {
                            
                            self.callSocialLoginAPI(social_id: userIdentifier, social_type: "3", email: "\(userIdentifier)@gmail.com", username: firstName?.replacingOccurrences(of: " ", with: "") ?? "")
                        }
                        
                    }
                    
                }
                
            }
        }
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            // Handle error.
            print("Sign in with Apple errored: \(error)")
        }
        
    }
    
}

// MARK: - AuthModuleDelegate
extension LoginViewController: AuthModuleDelegate {
    func authModule(_ authModule: AuthModule, didSignUpUser user: QBUUser) {
        guard let fullName = user.fullName, let login = user.login else { return }
        Profile.synchronize(withUser: user)
        authModule.login(fullName: fullName, login: login)
    }
    
    func authModule(_ authModule: AuthModule, didLoginUser user: QBUUser) {
 
        let fullName = Profile().fullName
        if user.fullName != fullName {

            authModule.updateFullName(fullName: appDelegate?.dicUserLoginData.name ?? "")
            return
        }
        Profile.synchronize(withUser: user)
        connection.establish()
    }
    
    func authModule(_ authModule: AuthModule, didUpdateUpdateFullNameUser user: QBUUser) {
        Profile.synchronize(withUser: user)
        connection.establish()
    }
    
    func authModule(_ authModule: AuthModule, didReceivedError error: ErrorInfo) {
       // handleError(error)
    }
}

// MARK: - ConnectionModuleDelegate
extension LoginViewController: ConnectionModuleDelegate {
    func connectionModuleDidConnect(_ connectionModule: ConnectionModule) {
        //onCompleteAuth?()
    }
    
    func connectionModuleDidNotConnect(_ connectionModule: ConnectionModule, error: Error) {
        let error = ErrorInfo(info: error.localizedDescription, statusCode: error._code)
       // handleError(error)
    }
}

//MARK: - InputContainerDelegate
extension LoginViewController: InputContainerDelegate {
    func inputContainer(_ container: InputContainer, didChangeValidState isValid: Bool) {
        
    }
}
