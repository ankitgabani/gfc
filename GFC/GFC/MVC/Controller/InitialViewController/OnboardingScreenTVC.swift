//
//  OnboardingScreenTVC.swift
//  GFC
//
//  Created by Parth Anghan on 18/10/21.
//

import UIKit
import Quickblox
class OnboardingScreenTVC: UIViewController {
    
    @IBOutlet weak var lblTEasyAppointment: UILabel!
    
    @IBOutlet weak var btnGetStarted: UIButton!
    @IBOutlet weak var btnSkiop: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedStarted(_ sender: Any) {
        UserDefaults.standard.setValue(true, forKey: "isIntroFinished")
        UserDefaults.standard.synchronize()

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func clickedSkio(_ sender: Any) {
        UserDefaults.standard.setValue(true, forKey: "isIntroFinished")
        UserDefaults.standard.synchronize()

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }    
}
