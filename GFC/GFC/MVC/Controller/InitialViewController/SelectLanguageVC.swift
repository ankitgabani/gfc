//
//  SelectLanguageVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 19/12/23.
//

import UIKit
import Quickblox
class SelectLanguageVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSelect: UIButton!
    
    var arrLanImg = ["Ellipse 197","Ellipse 195","Ellipse 196"]
    
    var arrName = ["English","Arabic","Kurdish"]
    
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self

        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedSelect(_ sender: Any) {
        if selectedIndex == -1{
            self.view.makeToast("Please select any language")
            let window = UIApplication.shared.windows
            window.last?.makeToast("Please select any language")
        }else{
            UserDefaults.standard.setValue(true, forKey: "isSelectLanguage")
            UserDefaults.standard.synchronize()
            
            appDelegate?.setUpLoginData()
        }
        
    }
    

}

extension SelectLanguageVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLanImg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "SelectLagCell") as! SelectLagCell
        
        cell.imgPic.image = UIImage(named: arrLanImg[indexPath.row])
        
        cell.lblName.text = arrName[indexPath.row]
        
        if selectedIndex == indexPath.row{
            cell.viewMain.backgroundColor = UIColor(hexString: "08364B", alpha: 1)
            cell.lblName.textColor = .white
        }else{
            cell.viewMain.backgroundColor = .white
            cell.lblName.textColor = .black
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            Language.shared.isArabic = false
        }
        else
        {
            Language.shared.isArabic = true
        }
        
        selectedIndex = indexPath.row
        
        btnSelect.setTitle("Select".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        setSemantricFlow()
        tblView.reloadData()
    }
}

class SelectLagCell: UITableViewCell
{
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgPic: UIImageView!
    
}
