//
//  OnboardingScreenSVC.swift
//  GFC
//
//  Created by Parth Anghan on 18/10/21.
//

import UIKit
import Quickblox
class OnboardingScreenSVC: UIViewController {
    
    @IBOutlet weak var lblTChooseBestDoc: UILabel!
    
    @IBOutlet weak var btnGetStarted: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickedStarted(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "OnboardingScreenTVC") as! OnboardingScreenTVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func clickedSkio(_ sender: Any) {
        UserDefaults.standard.setValue(true, forKey: "isIntroFinished")
        UserDefaults.standard.synchronize()

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

}
