//
//  NewRatingVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 28/11/23.
//

import UIKit
import Quickblox
class NewRatingVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var viewThree: UIView!
    @IBOutlet weak var viewFour: UIView!
    @IBOutlet weak var viewFive: UIView!
    
    @IBOutlet weak var lblOne: UILabel!
    @IBOutlet weak var lblTwo: UILabel!
    @IBOutlet weak var lblThree: UILabel!
    @IBOutlet weak var lblFour: UILabel!
    @IBOutlet weak var lblFive: UILabel!
    
    @IBOutlet weak var imgOne: UIImageView!
    @IBOutlet weak var imgTwo: UIImageView!
    @IBOutlet weak var imgThree: UIImageView!
    @IBOutlet weak var imgFOur: UIImageView!
    @IBOutlet weak var imgFive: UIImageView!
    
    @IBOutlet weak var txtViewComment: UITextView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRateyourex: UILabel!
    @IBOutlet weak var lblTComment: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!

    @IBOutlet weak var btnBack: UIButton!
    var selectedRate = 0
    
    var arrAllInfo = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTitle.text = "Ratings".localizeString(string: Language.shared.currentAppLang)
        lblRateyourex.text = "Rate your experience".localizeString(string: Language.shared.currentAppLang)
        lblTComment.text = "Comment".localizeString(string: Language.shared.currentAppLang)
        
        txtViewComment.delegate = self
        txtViewComment.text = "Share your experience..".localizeString(string: Language.shared.currentAppLang)

        
//        lblOne.text = "".localizeString(string: Language.shared.currentAppLang)
//        lblTwo.text = "".localizeString(string: Language.shared.currentAppLang)
//        lblThree.text = "".localizeString(string: Language.shared.currentAppLang)
//        lblFour.text = "".localizeString(string: Language.shared.currentAppLang)
//        lblFive.text = "".localizeString(string: Language.shared.currentAppLang)
        
        btnSubmit.setTitle("Submit".localizeString(string: Language.shared.currentAppLang), for: .normal)

        if Language.shared.isArabic == false{
            txtViewComment.textAlignment = .left
        }else{
            txtViewComment.textAlignment = .right
        }
        
        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtViewComment.text == "Share your experience..".localizeString(string: Language.shared.currentAppLang) {
            txtViewComment.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtViewComment.text.isEmpty {
            txtViewComment.text = "Share your experience..".localizeString(string: Language.shared.currentAppLang)
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedOne(_ sender: Any) {
        selectedRate = 1
        viewOne.backgroundColor = UIColor(hexString: "#0A374C")
        viewTwo.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewThree.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewFour.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewFive.backgroundColor = UIColor(hexString: "#D8D8D8")
        
        lblOne.textColor = .white
        lblTwo.textColor = .black
        lblThree.textColor = .black
        lblFour.textColor = .black
        lblFive.textColor = .black
        
        imgOne.image = UIImage(named: "S_Rate")
        imgTwo.image = UIImage(named: "Un_Rate")
        imgThree.image = UIImage(named: "Un_Rate")
        imgFOur.image = UIImage(named: "Un_Rate")
        imgFive.image = UIImage(named: "Un_Rate")
    }
    @IBAction func clickedTwo(_ sender: Any) {
        selectedRate = 2
        viewTwo.backgroundColor = UIColor(hexString: "#0A374C")
        viewOne.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewThree.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewFour.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewFive.backgroundColor = UIColor(hexString: "#D8D8D8")
        
        lblTwo.textColor = .white
        lblOne.textColor = .black
        lblThree.textColor = .black
        lblFour.textColor = .black
        lblFive.textColor = .black
        
        imgTwo.image = UIImage(named: "S_Rate")
        imgOne.image = UIImage(named: "Un_Rate")
        imgThree.image = UIImage(named: "Un_Rate")
        imgFOur.image = UIImage(named: "Un_Rate")
        imgFive.image = UIImage(named: "Un_Rate")
    }
    @IBAction func clickedThree(_ sender: Any) {
        selectedRate = 3
        viewThree.backgroundColor = UIColor(hexString: "#0A374C")
        viewOne.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewTwo.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewFour.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewFive.backgroundColor = UIColor(hexString: "#D8D8D8")
        
        lblThree.textColor = .white
        lblOne.textColor = .black
        lblTwo.textColor = .black
        lblFour.textColor = .black
        lblFive.textColor = .black
        
        imgThree.image = UIImage(named: "S_Rate")
        imgOne.image = UIImage(named: "Un_Rate")
        imgTwo.image = UIImage(named: "Un_Rate")
        imgFOur.image = UIImage(named: "Un_Rate")
        imgFive.image = UIImage(named: "Un_Rate")
    }
    @IBAction func clickedFour(_ sender: Any) {
        selectedRate = 4
        viewFour.backgroundColor = UIColor(hexString: "#0A374C")
        viewOne.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewTwo.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewThree.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewFive.backgroundColor = UIColor(hexString: "#D8D8D8")
        
        lblFour.textColor = .white
        lblOne.textColor = .black
        lblTwo.textColor = .black
        lblThree.textColor = .black
        lblFive.textColor = .black
        
        imgFOur.image = UIImage(named: "S_Rate")
        imgOne.image = UIImage(named: "Un_Rate")
        imgTwo.image = UIImage(named: "Un_Rate")
        imgThree.image = UIImage(named: "Un_Rate")
        imgFive.image = UIImage(named: "Un_Rate")
    }
    @IBAction func clickedFiv(_ sender: Any) {
        selectedRate = 5
        viewFive.backgroundColor = UIColor(hexString: "#0A374C")
        viewOne.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewTwo.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewThree.backgroundColor = UIColor(hexString: "#D8D8D8")
        viewFour.backgroundColor = UIColor(hexString: "#D8D8D8")
        
        lblFive.textColor = .white
        lblOne.textColor = .black
        lblTwo.textColor = .black
        lblThree.textColor = .black
        lblFour.textColor = .black
        
        imgFive.image = UIImage(named: "S_Rate")
        imgOne.image = UIImage(named: "Un_Rate")
        imgTwo.image = UIImage(named: "Un_Rate")
        imgThree.image = UIImage(named: "Un_Rate")
        imgFOur.image = UIImage(named: "Un_Rate")
    }
    
    @IBAction func clickedubmit(_ sender: Any) {
        if selectedRate == 0
        {
            self.view.makeToast("Please select rate".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Please select rate".localizeString(string: Language.shared.currentAppLang))
        }
        else if txtViewComment.text == ""
        {
            self.view.makeToast("Please enter message".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Please enter message".localizeString(string: Language.shared.currentAppLang))
        }
        else
        {
            callRatingAPI()
        }
        
    }
    
    // MARK: - calling API
    
    func callRatingAPI()
    {
        APIClient.sharedInstance.showIndicator()
        
        let dicData = arrAllInfo[0] as? NSMutableDictionary
        
        let name = dicData?.value(forKey: "name") as? String
        let email = dicData?.value(forKey: "email") as? String
        let appointment_type = dicData?.value(forKey: "appointment_type") as? String
        let userid = dicData?.value(forKey: "userid") as? String
        let appointment_id = dicData?.value(forKey: "appointment_id") as? String
        let id = dicData?.value(forKey: "id") as? String
        
        let para = ["name":name ?? "","email":email ?? "","appointment_type":appointment_type ?? "","userid":userid ?? "","appointment_id":appointment_id ?? "","id":id ?? "","rating":"\(self.selectedRate)","message":self.txtViewComment.text ?? ""] as [String : Any]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(ADD_RATING, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        self.view.makeToast(message ?? "")
                        let window = UIApplication.shared.windows
                        window.last?.makeToast(message ?? "")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.clickedBack(self)
                        }
                    }
                    else
                    {
                        self.view.makeToast(message ?? "")
                        let window = UIApplication.shared.windows
                        window.last?.makeToast(message ?? "")
                    }
                }
                else
                {
                    APIClient.sharedInstance.hideIndicator()
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    

}
