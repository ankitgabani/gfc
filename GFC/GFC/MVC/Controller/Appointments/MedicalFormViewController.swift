//
//  MedicalFormViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 30/03/22.
//

import UIKit
import Quickblox
class MedicalFormViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtDiagnosisName: UITextField!
    @IBOutlet weak var txtTimeOfDiagnosis: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var txtWeight: UITextField!
    @IBOutlet weak var txtHypertension: UITextField!
    @IBOutlet weak var txtDiabetes: UITextField!
    @IBOutlet weak var txtThyroid: UITextField!
    @IBOutlet weak var txtAsthma: UITextField!
    @IBOutlet weak var txtLiver: UITextField!
    @IBOutlet weak var txtRenal: UITextField!
    @IBOutlet weak var txtCardio: UITextField!
    @IBOutlet weak var txtBllod: UITextField!
    @IBOutlet weak var txtAlcoholism: UITextField!
    @IBOutlet weak var txtSmoking: UITextField!
    @IBOutlet weak var txtDrugs: UITextField!
    @IBOutlet weak var txtAllergy: UITextField!
    @IBOutlet weak var txtFamily: UITextView!
    @IBOutlet weak var txtAnyOther: UITextView!
    @IBOutlet weak var txtPatient: UITextView!
    
  
    @IBOutlet weak var viewDN: UIView!
    @IBOutlet weak var viewTD: UIView!
    @IBOutlet weak var viewAP: UIView!
    @IBOutlet weak var viewWeight: UIView!
    @IBOutlet weak var viewHypre: UIView!
    @IBOutlet weak var viewDiabetes: UIView!
    @IBOutlet weak var viewThyroid: UIView!
    @IBOutlet weak var viewAsthma: UIView!
    @IBOutlet weak var viewLiver: UIView!
    @IBOutlet weak var viewRenal: UIView!
    @IBOutlet weak var viewCardio: UIView!
    @IBOutlet weak var viewBlood: UIView!
    @IBOutlet weak var viewAlcoholism: UIView!
    @IBOutlet weak var viewSmoking: UIView!
    @IBOutlet weak var viewDrugs: UIView!
    @IBOutlet weak var viewAllergy: UIView!
    @IBOutlet weak var viewFamily: UIView!
    @IBOutlet weak var viewAny: UIView!
    @IBOutlet weak var viewSuffering: UIView!
    
    // title
    
    @IBOutlet weak var lblTMedicalForm: UILabel!
    @IBOutlet weak var lblTDignosisName: UILabel!
    @IBOutlet weak var lblTSince: UILabel!
    @IBOutlet weak var lblTAge: UILabel!
    @IBOutlet weak var lblTWeight: UILabel!
    @IBOutlet weak var lblTHypertension: UILabel!
    @IBOutlet weak var lblTDiabetes: UILabel!
    @IBOutlet weak var lblTThyroid: UILabel!
    @IBOutlet weak var lblTAsthma: UILabel!
    @IBOutlet weak var lblTLiverDiseases: UILabel!
    @IBOutlet weak var lblTRenalFailure: UILabel!
    @IBOutlet weak var lblTCardio: UILabel!
    @IBOutlet weak var lblTBloodTransfusion: UILabel!
    @IBOutlet weak var lblTAlcoholism: UILabel!
    @IBOutlet weak var lblTSmoking: UILabel!
    @IBOutlet weak var lblTDrugs: UILabel!
    @IBOutlet weak var lblTAllergy: UILabel!
    @IBOutlet weak var lblTFamilyHistory: UILabel!
    @IBOutlet weak var lblTAnyOtherHistory: UILabel!
    @IBOutlet weak var lblPatientSuffering: UILabel!
    @IBOutlet weak var lblTDocument: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    // dicMedicalList
    var dicMedicalList =  GFCMedicalFormData()
    
    var strAppointmetnID = ""
    
    var arrImg = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTMedicalForm.text = "Medical Form".localizeString(string: Language.shared.currentAppLang)
        lblTDignosisName.text = "Diagnosis Name".localizeString(string: Language.shared.currentAppLang)
        lblTSince.text = "Since, Time of diagnosis (In Months)".localizeString(string: Language.shared.currentAppLang)
        lblTAge.text = "Age of Patient".localizeString(string: Language.shared.currentAppLang)
        lblTWeight.text = "Weight (KG)".localizeString(string: Language.shared.currentAppLang)
        lblTDiabetes.text = "Diabetes".localizeString(string: Language.shared.currentAppLang)
        lblTHypertension.text = "Hypertension".localizeString(string: Language.shared.currentAppLang)
        lblTAsthma.text = "Asthma".localizeString(string: Language.shared.currentAppLang)
        lblTThyroid.text = "Thyroid".localizeString(string: Language.shared.currentAppLang)
        lblTRenalFailure.text = "Renal Failure".localizeString(string: Language.shared.currentAppLang)
        lblTLiverDiseases.text = "Liver Diseases".localizeString(string: Language.shared.currentAppLang)
        lblTBloodTransfusion.text = "Blood Transfusion".localizeString(string: Language.shared.currentAppLang)
        lblTCardio.text = "Cardio Vascular System".localizeString(string: Language.shared.currentAppLang)
        lblTSmoking.text = "Smoking".localizeString(string: Language.shared.currentAppLang)
        lblTAlcoholism.text = "Alcoholism".localizeString(string: Language.shared.currentAppLang)
        lblTAllergy.text = "Allergy to any medicine".localizeString(string: Language.shared.currentAppLang)
        lblTDrugs.text = "Drugs".localizeString(string: Language.shared.currentAppLang)
        lblTFamilyHistory.text = "Family History".localizeString(string: Language.shared.currentAppLang)
        lblTAnyOtherHistory.text = "Any other history".localizeString(string: Language.shared.currentAppLang)
        lblPatientSuffering.text = "Patient Suffering From".localizeString(string: Language.shared.currentAppLang)
        lblTDocument.text = "Documents".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            self.txtDiagnosisName.textAlignment = .left
            self.txtTimeOfDiagnosis.textAlignment = .left
            self.txtAge.textAlignment = .left
            self.txtWeight.textAlignment = .left
            self.txtHypertension.textAlignment = .left
            self.txtDiabetes.textAlignment = .left
            self.txtThyroid.textAlignment = .left
            self.txtAsthma.textAlignment = .left
            self.txtLiver.textAlignment = .left
            self.txtRenal.textAlignment = .left
            self.txtCardio.textAlignment = .left
            self.txtBllod.textAlignment = .left
            self.txtAlcoholism.textAlignment = .left
            self.txtSmoking.textAlignment = .left
            self.txtDrugs.textAlignment = .left
            self.txtAllergy.textAlignment = .left
            self.txtFamily.textAlignment = .left
            self.txtAnyOther.textAlignment = .left
            self.txtPatient.textAlignment = .left
        }else{
            self.txtDiagnosisName.textAlignment = .right
            self.txtTimeOfDiagnosis.textAlignment = .right
            self.txtAge.textAlignment = .right
            self.txtWeight.textAlignment = .right
            self.txtHypertension.textAlignment = .right
            self.txtDiabetes.textAlignment = .right
            self.txtThyroid.textAlignment = .right
            self.txtAsthma.textAlignment = .right
            self.txtLiver.textAlignment = .right
            self.txtRenal.textAlignment = .right
            self.txtCardio.textAlignment = .right
            self.txtBllod.textAlignment = .right
            self.txtAlcoholism.textAlignment = .right
            self.txtSmoking.textAlignment = .right
            self.txtDrugs.textAlignment = .right
            self.txtAllergy.textAlignment = .right
            self.txtFamily.textAlignment = .right
            self.txtAnyOther.textAlignment = .right
            self.txtPatient.textAlignment = .right
        }
        
        tblView.delegate = self
        tblView.dataSource = self
     
        viewDN.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewTD.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewAP.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewWeight.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewHypre.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewDiabetes.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewThyroid.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewAsthma.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewLiver.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewRenal.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewCardio.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewBlood.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewAlcoholism.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewSmoking.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewDrugs.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewAllergy.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewFamily.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewAny.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        viewSuffering.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)

    }

    override func viewWillAppear(_ animated: Bool) {
        callMedicalAPI()
    }
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callMedicalAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["appointment_id":self.strAppointmetnID]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(MEDICAL_FORM_DETAIL, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        
                        let dicData = GFCMedicalFormData(fromDictionary: dic!)
                        self.dicMedicalList = dicData
                        
                        let arrImages = dic?.value(forKey: "images") as? NSArray
                        
                        self.arrImg = (arrImages?.mutableCopy() as? NSMutableArray)!
                        self.tblView.reloadData()
                        
                        self.txtDiagnosisName.text = self.dicMedicalList.diagnosisName ?? ""
                        self.txtTimeOfDiagnosis.text = self.dicMedicalList.diagnosisTime ?? ""
                        self.txtAge.text = self.dicMedicalList.age ?? ""
                        self.txtWeight.text = self.dicMedicalList.weight ?? ""
                        self.txtHypertension.text = self.dicMedicalList.hypertension ?? ""
                        self.txtDiabetes.text = self.dicMedicalList.diabetes ?? ""
                        self.txtThyroid.text = self.dicMedicalList.thyroid ?? ""
                        self.txtAsthma.text = self.dicMedicalList.asthma ?? ""
                        self.txtLiver.text = self.dicMedicalList.liverDiseases ?? ""
                        self.txtRenal.text = self.dicMedicalList.renalFailure ?? ""
                        self.txtCardio.text = self.dicMedicalList.cardio ?? ""
                        self.txtBllod.text = self.dicMedicalList.bloodTansfusion ?? ""
                        self.txtAlcoholism.text = self.dicMedicalList.alcoholism ?? ""
                        self.txtSmoking.text = self.dicMedicalList.smoking ?? ""
                        self.txtDrugs.text = self.dicMedicalList.drugs ?? ""
                        self.txtAllergy.text = self.dicMedicalList.allergyMedicine ?? ""
                        self.txtFamily.text = self.dicMedicalList.familyHistory ?? ""
                        self.txtAnyOther.text = self.dicMedicalList.otherHistory ?? ""
                        self.txtPatient.text = self.dicMedicalList.patinetSuffering ?? ""
                       
                    }
                }
            }
        }
    }
    
}
extension MedicalFormViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "UploadedDocumentTblView") as! UploadedDocumentTblView
        
        let str = arrImg[indexPath.row] as! String
        
        cell.lblName.text = str
        
        if str.contains(".pdf") == true
        {
            cell.lblType.text = "PDF"
        }
        else
        {
            cell.lblType.text = "JPG"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let str = arrImg[indexPath.row] as! String
        
        if str.contains(".pdf") == true
        {
            if let pdfURL = URL(string: str) {
                openPDFInSafari(pdfURL: pdfURL)
            }
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
            vc.modalPresentationStyle = .overFullScreen
            vc.strImg = arrImg[indexPath.row] as! String
            self.present(vc, animated: false, completion: nil)
        }
               
    }
    
    
    func openPDFInSafari(pdfURL: URL) {
           if UIApplication.shared.canOpenURL(pdfURL) {
               UIApplication.shared.open(pdfURL, options: [:], completionHandler: nil)
           } else {
               // Handle the case where the URL can't be opened, e.g., display an alert.
               print("Unable to open PDF in Safari.")
           }
       }
    
}

class UploadedDocumentTblView: UITableViewCell{
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnClose: UIButton!
}
