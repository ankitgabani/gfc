//
//  PaymentDetailViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 30/03/22.
//

import UIKit
import Quickblox
class PaymentDetailViewController: UIViewController {

    @IBOutlet weak var paymentHistoryTableView: UITableView!
    
    @IBOutlet weak var lblTPaymentHistory: UILabel!
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var arrPayment: [GFCPaymentHistoryData] = [GFCPaymentHistoryData]()
    
    var strAppointmentID = ""
    @IBOutlet weak var viewNoData: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTPaymentHistory.text = "Payment History".localizeString(string: Language.shared.currentAppLang)
        lblTNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)


        paymentHistoryTableView.dataSource = self
        paymentHistoryTableView.delegate = self
        
        callPaymentAPI()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnPayClicked(_ sender: UIButton){
        
    }
    
    // MARK: - call API
    
    func callPaymentAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["appointment_id":self.strAppointmentID]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(PAYMENT_HISTORY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        for obj in arrData!
                        {
                            let dicData = GFCPaymentHistoryData(fromDictionary: obj as! NSDictionary)
                            self.arrPayment.append(dicData)
                        }
                        if self.arrPayment.count > 0
                        {
                            self.paymentHistoryTableView.isHidden = false
                            self.viewNoData.isHidden = true
                        }
                        else
                        {
                            self.viewNoData.isHidden = false
                            self.paymentHistoryTableView.isHidden = true
                        }
                        self.paymentHistoryTableView.reloadData()
                    }
                }
            }
        }
    }
  
}



extension PaymentDetailViewController: UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPayment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentHistoryTableViewCell") as! PaymentHistoryTableViewCell
        cell.btnPay.addTarget(self, action: #selector(btnPayClicked(_:)), for: .touchDragInside)
        cell.btnPay.tag = indexPath.row
        
        cell.lblStatus.text = "Status".localizeString(string: Language.shared.currentAppLang)

        
        let dicData = arrPayment[indexPath.row]
        
        if dicData.image.contains(".pdf") == true
        {
            cell.imgPic.image = UIImage(named: "pdf")
        }
        else
        {
            var media_link_url = dicData.image ?? ""
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        }
        
        cell.lblPrice.text = "₹ \(dicData.amount ?? "")"
        cell.lblNote.text = "Note : \(dicData.note ?? "")"
        cell.lblDate.text = dicData.date ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrPayment[indexPath.row]
        
        let img = dicData.image ?? ""
        
        if img.contains(".pdf")
        {
            if let pdfURL = URL(string: img) {
                openPDFInSafari(pdfURL: pdfURL)
            }
            
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
            vc.modalPresentationStyle = .overFullScreen
            vc.strImg = dicData.image ?? ""
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    func openPDFInSafari(pdfURL: URL) {
        if UIApplication.shared.canOpenURL(pdfURL) {
            UIApplication.shared.open(pdfURL, options: [:], completionHandler: nil)
        } else {
            // Handle the case where the URL can't be opened, e.g., display an alert.
            print("Unable to open PDF in Safari.")
        }
    }
}


class PaymentHistoryTableViewCell: UITableViewCell{
    
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
}

