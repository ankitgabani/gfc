//
//  LabPopUpVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 02/03/23.
//

import UIKit
import NYTPhotoViewer
import PDFKit

class LabPopUpVC: UIViewController {

    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var lblLabName: UILabel!
    @IBOutlet weak var lblTName: UILabel!
    @IBOutlet weak var lblBD: UILabel!
    @IBOutlet weak var lblBT: UILabel!
    @IBOutlet weak var lblNP: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPM: UILabel!
    @IBOutlet weak var lblAddres: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    
    @IBOutlet weak var btnTClose: UIButton!
    
    @IBOutlet weak var lblTTestName: UILabel!
    @IBOutlet weak var lblTBookingDate: UILabel!
    @IBOutlet weak var lblTBookingSlot: UILabel!
    @IBOutlet weak var lblTNoOfPerson: UILabel!
    @IBOutlet weak var lblTAmount: UILabel!
    @IBOutlet weak var lblTPaymentMode: UILabel!
    @IBOutlet weak var lblTAddress: UILabel!
    @IBOutlet weak var lblTCity: UILabel!
    @IBOutlet weak var lblTState: UILabel!
    @IBOutlet weak var lblTLabTReport: UILabel!
    
    @IBOutlet weak var collectionViewImg: UICollectionView!
    
    // dicLabDetail
    
    var dicLabDetail = GFCAppointmentLabAppointment()
    
    let SocityNoticesectionInsetsCat = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    let SocityNoticeitemsPerRowCat: CGFloat = 1
    
    var SocityNoticeflowLayout: UICollectionViewFlowLayout {
        let _SocityNoticeflowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.SocityNoticesectionInsetsCat.left * (self.SocityNoticeitemsPerRowCat + 1)
            let availableWidth = self.collectionViewImg.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.SocityNoticeitemsPerRowCat
            
            _SocityNoticeflowLayout.itemSize = CGSize(width: 50, height: 50)
            
            _SocityNoticeflowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            _SocityNoticeflowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _SocityNoticeflowLayout.minimumInteritemSpacing = 10
            _SocityNoticeflowLayout.minimumLineSpacing = 10
        }
        
        // edit properties here
        return _SocityNoticeflowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTTestName.text = "Test Name".localizeString(string: Language.shared.currentAppLang)
        lblTBookingDate.text = "Booking Date".localizeString(string: Language.shared.currentAppLang)
        lblTBookingSlot.text = "Booking Slot".localizeString(string: Language.shared.currentAppLang)
        lblTNoOfPerson.text = "No Of Person".localizeString(string: Language.shared.currentAppLang)
        lblTAmount.text = "Amount".localizeString(string: Language.shared.currentAppLang)
        lblTPaymentMode.text = "Payment Mode".localizeString(string: Language.shared.currentAppLang)
        lblTAddress.text = "Address".localizeString(string: Language.shared.currentAppLang)
        lblTCity.text = "City".localizeString(string: Language.shared.currentAppLang)
        lblTState.text = "State".localizeString(string: Language.shared.currentAppLang)
        lblTLabTReport.text = "Lab Report Images/PDF".localizeString(string: Language.shared.currentAppLang)
        
        btnTClose.setTitle("Close".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        collectionViewImg.delegate = self
        collectionViewImg.dataSource = self
        collectionViewImg.collectionViewLayout = SocityNoticeflowLayout
        
        if self.dicLabDetail.labReport.count > 0
        {
            self.lblNoData.isHidden = true
            self.collectionViewImg.isHidden = false
        }
        else
        {
            self.lblNoData.isHidden = false
            self.collectionViewImg.isHidden = true
        }

        self.lblLabName.text = dicLabDetail.labname ?? ""
        self.lblTName.text = dicLabDetail.testname ?? ""
        self.lblBT.text = "\(dicLabDetail.slotStartTime ?? "") - \(dicLabDetail.slotEndTime ?? "")"
        self.lblNP.text = "\(dicLabDetail.noOfPerson ?? "") Person"
        
        let totalAmount = (Double(dicLabDetail.noOfPerson ?? "") ?? 0.0) * (Double(dicLabDetail.amount ?? "") ?? 0.0)
        
        self.lblAmount.text = "₹ \(totalAmount ?? 0.0)"
        
        self.lblPM.text = dicLabDetail.paymentMode ?? ""
        self.lblAddres.text = dicLabDetail.address ?? ""
        self.lblCity.text = dicLabDetail.city ?? ""
        self.lblState.text = dicLabDetail.state ?? ""
        
        if let bidAccepted = dicLabDetail.bookingDate
        {
            if bidAccepted != ""
            {
                if bidAccepted != "0000-00-00"
                {
                    let formatter = DateFormatter()
                    formatter.locale = Locale(identifier: "en_US")
                    formatter.dateFormat = "yyyy-MM-dd"
                    let date1 = formatter.date(from: bidAccepted)
                    let Dform = DateFormatter()
                    Dform.dateFormat = "MMM dd, yyyy"
                    let strDate = Dform.string(from: date1!)
                    self.lblBD.text  = strDate
                }
                else
                {
                    self.lblBD.text  = ""
                }
            }
            else
            {
                self.lblBD.text  = ""
            }
        }
      
        // Do any additional setup after loading the view.
    }
    @IBAction func clickedClose(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    

   

}

extension LabPopUpVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.dicLabDetail.labReport.count > 0
        {
            return self.dicLabDetail.labReport.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionViewImg.dequeueReusableCell(withReuseIdentifier: "LabReportShowImgCell", for: indexPath) as! LabReportShowImgCell
        
        let dicData = self.dicLabDetail.labReport[indexPath.row]
        
        if dicData.contains(".pdf") == true
        {
            cell.imgPic.image = UIImage(named: "pdf")
        }
        else
        {
            var media_link_url = dicData ?? ""
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "user"), options: [], completed: nil)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionViewImg.cellForItem(at: indexPath) as! LabReportShowImgCell
                
        
        let dicData = self.dicLabDetail.labReport[indexPath.row]
        
        if dicData.contains(".pdf") == true
        {
            if let pdfURL = URL(string: dicData) {
                openPDFInSafari(pdfURL: pdfURL)
            }
            
        }
        else
        {
            if let img = cell.imgPic.image{
                let photo = ChatImage()
                photo.image = img
                
                let photosViewController: NYTPhotosViewController = NYTPhotosViewController(photos: [photo])
                present(photosViewController, animated: true, completion: nil)
            }
        }
        
        
    }
    
    func openPDFInSafari(pdfURL: URL) {
           if UIApplication.shared.canOpenURL(pdfURL) {
               UIApplication.shared.open(pdfURL, options: [:], completionHandler: nil)
           } else {
               // Handle the case where the URL can't be opened, e.g., display an alert.
               print("Unable to open PDF in Safari.")
           }
       }
    
}

class LabReportShowImgCell: UICollectionViewCell
{
    @IBOutlet weak var imgPic: UIImageView!
    
}
