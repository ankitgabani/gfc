//
//  PatientDetailViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 30/03/22.
//

import UIKit
import DropDown

class PatientDetailViewController: UIViewController {

    @IBOutlet weak var viewImag: UIView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtSelectCountry: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    
    @IBOutlet weak var maleMainView: UIView!
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var viewFN: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewMN: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewStreet: UIView!
    @IBOutlet weak var viewSC: UIView!
    @IBOutlet weak var viewState: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var imgPic: UIImageView!
    
    @IBOutlet weak var lblTPatientInfo: UILabel!
    @IBOutlet weak var lblTGender: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    var dicHospital = GFCAppointmentHospitalAppointment()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTPatientInfo.text = "Patient Info".localizeString(string: Language.shared.currentAppLang)
        lblTGender.text = "GENDER".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            txtFirstName.textAlignment = .left
            txtEmail.textAlignment = .left
            txtMobileNo.textAlignment = .left
            txtAddress.textAlignment = .left
            txtStreet.textAlignment = .left
            txtDate.textAlignment = .left
            txtSelectCountry.textAlignment = .left
            txtState.textAlignment = .left
            txtCity.textAlignment = .left

        }else{
            txtFirstName.textAlignment = .right
            txtEmail.textAlignment = .right
            txtMobileNo.textAlignment = .right
            txtAddress.textAlignment = .right
            txtStreet.textAlignment = .right
            txtDate.textAlignment = .right
            txtSelectCountry.textAlignment = .right
            txtState.textAlignment = .right
            txtCity.textAlignment = .right
        }

        viewImag.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1), alpha: 1, x: 0, y: 0, blur: 15, spread: 0)
        
        viewFN.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        
        viewEmail.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        
        viewMN.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        
        viewAddress.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        
        viewStreet.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        
        viewSC.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        
        viewState.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        
        viewCity.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
       
        viewDate.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        
        txtFirstName.text = dicHospital.patientName ?? ""
        txtEmail.text = dicHospital.patientEmail ?? ""
        txtMobileNo.text = dicHospital.phone ?? ""
        txtAddress.text = dicHospital.address ?? ""
        txtStreet.text = dicHospital.patientStreet ?? ""
        txtDate.text = dicHospital.patientPassport ?? ""
        lblGender.text = dicHospital.gender ?? ""
        
        if dicHospital.profileImage == ""
        {
            self.imgPic.contentMode = .scaleAspectFit
            self.imgPic.image = UIImage(named: "user")
        }
        else
        {
            var media_link_url = dicHospital.profileImage ?? ""
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            self.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: nil, options: [], completed: nil)
        }
        
        txtFirstName.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtMobileNo.isUserInteractionEnabled = false
        txtAddress.isUserInteractionEnabled = false
        txtStreet.isUserInteractionEnabled = false
        txtSelectCountry.isUserInteractionEnabled = false
        txtState.isUserInteractionEnabled = false
        txtCity.isUserInteractionEnabled = false
        txtDate.isUserInteractionEnabled = false
     
        self.callCountryListAPI()
        callStateListAPI()
        callCityListAPI()
    }

    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - call API
    
    func callCountryListAPI()
    {
       // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if dicData.countryId == self.dicHospital.patientCountry
                            {
                                self.txtSelectCountry.text = dicData.countryName ?? ""
                            }
                        }
                    }
                }
            }
        }
    }
    
    func callStateListAPI()
    {
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_STATE + "\(self.dicHospital.patientCountry ?? "")", parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray

                        for obj in arrData!
                        {
                            let dicData = GFCStateListData(fromDictionary: obj as! NSDictionary)

                            if dicData.stateId == self.dicHospital.patientState
                            {
                                self.txtState.text = dicData.stateName ?? ""
                            }
                        }

                    }
                }
            }
        }
    }
    
    func callCityListAPI()
    {
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_CITY + "\(self.dicHospital.patientState ?? "")", parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray

                        for obj in arrData!
                        {
                            let dicData = GFCCityListData(fromDictionary: obj as! NSDictionary)

                            if dicData.cityId == self.dicHospital.patientCity
                            {
                                self.txtCity.text = dicData.cityName ?? ""
                            }
                        }
                    }
                }
            }
        }
    }
    
}
