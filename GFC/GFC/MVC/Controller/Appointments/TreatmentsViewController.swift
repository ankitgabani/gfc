//
//  TreatmentsViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 30/03/22.
//

import UIKit
import Quickblox
class TreatmentsViewController: UIViewController {
    
    @IBOutlet weak var viewNoDate: UIView!
    @IBOutlet weak var lblTreatmentName: UILabel!
    @IBOutlet weak var lblDoctorName: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var lblTTreatment: UILabel!
    
    @IBOutlet weak var lblTTreatmetnamr: UILabel!
    @IBOutlet weak var lblTDoctorName: UILabel!
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var lblTTitle: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var dicTreatmentList = GFCTreatmentListData()

    var strAppointmetnID = ""
    
    var arrImg = NSMutableArray()
    
    let SocityNoticesectionInsetsCat = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
    let SocityNoticeitemsPerRowCat: CGFloat = 1
    
    var SocityNoticeflowLayout: UICollectionViewFlowLayout {
        let _SocityNoticeflowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.SocityNoticesectionInsetsCat.left * (self.SocityNoticeitemsPerRowCat + 1)
            let availableWidth = self.collectionView.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.SocityNoticeitemsPerRowCat
            
            _SocityNoticeflowLayout.itemSize = CGSize(width: 100, height: 100)
            
            _SocityNoticeflowLayout.sectionInset = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
            _SocityNoticeflowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _SocityNoticeflowLayout.minimumInteritemSpacing = 10
            _SocityNoticeflowLayout.minimumLineSpacing = 10
        }
        
        // edit properties here
        return _SocityNoticeflowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTTitle.text = "Treatments".localizeString(string: Language.shared.currentAppLang)
        lblTNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        lblTTreatmetnamr.text = "Treatment Name".localizeString(string: Language.shared.currentAppLang)
        lblTDoctorName.text = "Doctor Name".localizeString(string: Language.shared.currentAppLang)
        lblTTreatment.text = "Treatments".localizeString(string: Language.shared.currentAppLang)

        callTreatmentAPI()
        viewNoDate.isHidden = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout = SocityNoticeflowLayout
   
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func callTreatmentAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["appointment_id":self.strAppointmetnID]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TREATMENET_LIST, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        
                        let arrImages = dic?.value(forKey: "images") as? NSArray
                        
                        self.arrImg = (arrImages?.mutableCopy() as? NSMutableArray)!
                                                
                        if (arrImages?.count ?? 0) > 0
                        {
                            self.collectionView.isHidden = false
                            self.viewNoDate.isHidden = true
                            self.scrollView.isHidden = false
                        }
                        else
                        {
                            self.collectionView.isHidden = true
                            self.viewNoDate.isHidden = false
                            self.scrollView.isHidden = true
                        }
                        
                        let dicData = GFCTreatmentListData(fromDictionary: dic!)
                        self.dicTreatmentList = dicData
                        
                        self.lblTreatmentName.text = self.dicTreatmentList.treatmentName ?? ""
                        self.lblDoctorName.text = self.dicTreatmentList.doctorName ?? ""
                        
                        self.collectionView.reloadData()
                        
                    }
                }
            }
        }
    }
    
}

extension TreatmentsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "TreatmentCollectionCell", for: indexPath) as! TreatmentCollectionCell
        
        let dicData = arrImg[indexPath.row] as! String
        
        if dicData.contains(".pdf") == true
        {
            cell.imgPic.contentMode = .scaleAspectFit
            cell.imgPic.image = UIImage(named: "pdf")
        }
        else
        {
            cell.imgPic.contentMode = .scaleAspectFill
            var media_link_url = dicData
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "App Logo"), options: [], completed: nil)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dicData = arrImg[indexPath.row] as! String
        
        if dicData.contains(".pdf") == true
        {
            if let pdfURL = URL(string: dicData) {
                openPDFInSafari(pdfURL: pdfURL)
            }
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
            vc.modalPresentationStyle = .overFullScreen
            vc.strImg = dicData
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    func openPDFInSafari(pdfURL: URL) {
        if UIApplication.shared.canOpenURL(pdfURL) {
            UIApplication.shared.open(pdfURL, options: [:], completionHandler: nil)
        } else {
            // Handle the case where the URL can't be opened, e.g., display an alert.
            print("Unable to open PDF in Safari.")
        }
    }
}

class TreatmentCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var imgPic: UIImageView!
    
}
