//
//  MedicalEditVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 14/03/23.
//

import UIKit
import DropDown
import MobileCoreServices
import WebKit

class MedicalEditVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIDocumentPickerDelegate, UITextViewDelegate {
  
    @IBOutlet weak var txtDiagnosisName: UITextField!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewPopUp: UIView!
    
    @IBOutlet weak var lblAge: UITextField!
    @IBOutlet weak var lblWeight: UITextField!
    @IBOutlet weak var txtHypertension: UITextField!
    @IBOutlet weak var lblDiabetis: UITextField!
    @IBOutlet weak var txtThyroid: UITextField!
    @IBOutlet weak var txtAsthma: UITextField!
    @IBOutlet weak var txtLiver: UITextField!
    @IBOutlet weak var txtRenal: UITextField!
    @IBOutlet weak var txtCardio: UITextField!
    @IBOutlet weak var txtBlood: UITextField!
    @IBOutlet weak var txtAlcoholism: UITextField!
    @IBOutlet weak var txtSmoking: UITextField!
    @IBOutlet weak var txtDrugs: UITextField!
    @IBOutlet weak var txtAllergy: UITextField!
    @IBOutlet weak var txtFmaily: UITextView!
    @IBOutlet weak var txtAntHistory: UITextView!
    @IBOutlet weak var txtPaientFrom: UITextView!
    @IBOutlet weak var tblView: UITableView!
    
    // title
    
    @IBOutlet weak var lblTSelectDocument: UILabel!
    @IBOutlet weak var lblTJPG: UILabel!
    @IBOutlet weak var lblTPDF: UILabel!
    
    @IBOutlet weak var lblTMedicalForm: UILabel!
    @IBOutlet weak var lblTDignosisName: UILabel!
    @IBOutlet weak var lblTSince: UILabel!
    @IBOutlet weak var lblTAge: UILabel!
    @IBOutlet weak var lblTWeight: UILabel!
    @IBOutlet weak var lblTHypertension: UILabel!
    @IBOutlet weak var lblTDiabetes: UILabel!
    @IBOutlet weak var lblTThyroid: UILabel!
    @IBOutlet weak var lblTAsthma: UILabel!
    @IBOutlet weak var lblTLiverDiseases: UILabel!
    @IBOutlet weak var lblTRenalFailure: UILabel!
    @IBOutlet weak var lblTCardio: UILabel!
    @IBOutlet weak var lblTBloodTransfusion: UILabel!
    @IBOutlet weak var lblTAlcoholism: UILabel!
    @IBOutlet weak var lblTSmoking: UILabel!
    @IBOutlet weak var lblTDrugs: UILabel!
    @IBOutlet weak var lblTAllergy: UILabel!
    @IBOutlet weak var lblTFamilyHistory: UILabel!
    @IBOutlet weak var lblTAnyOtherHistory: UILabel!
    @IBOutlet weak var lblPatientSuffering: UILabel!
    @IBOutlet weak var lblTDocument: UILabel!
    
    @IBOutlet weak var btnTAddDocument: UIButton!
    @IBOutlet weak var btnUpdateDetail: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    // dropDownHypertension
    let dropDownHypertension = DropDown()
    
    let dropDownDiabetis = DropDown()

    let dropDownThyroid = DropDown()
    
    let dropDownAsthma = DropDown()
    
    let dropDownLiver = DropDown()
    
    let dropDownRenal = DropDown()
    
    let dropDownCardio = DropDown()
    
    let dropDownBlood = DropDown()
    
    let dropDownAlcoholism = DropDown()
    
    let dropDownSmoking = DropDown()

    let dropDownDrugs = DropDown()

    let dropDownAllergy = DropDown()

    var arrHypertension = ["Select","Yes","No"]
    
    var strAppointmentID = ""
    
    var imagePicker: ImagePicker!
    var selectedImg: String?
    
    var imagePickerNew = UIImagePickerController()
    
    var arrUploadDocBOL = NSMutableArray()
    var arrUploadDocInvoice = NSMutableArray()

    var arrAllSelectedPdfInvoice = NSMutableArray()
    
    var isFromHome = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTMedicalForm.text = "Medical Form".localizeString(string: Language.shared.currentAppLang)
        lblTDignosisName.text = "Diagnosis Name".localizeString(string: Language.shared.currentAppLang)
        lblTSince.text = "Since, Time of diagnosis (In Months)".localizeString(string: Language.shared.currentAppLang)
        lblTAge.text = "Age of Patient".localizeString(string: Language.shared.currentAppLang)
        lblTWeight.text = "Weight (KG)".localizeString(string: Language.shared.currentAppLang)
        lblTDiabetes.text = "Diabetes".localizeString(string: Language.shared.currentAppLang)
        lblTHypertension.text = "Hypertension".localizeString(string: Language.shared.currentAppLang)
        lblTAsthma.text = "Asthma".localizeString(string: Language.shared.currentAppLang)
        lblTThyroid.text = "Thyroid".localizeString(string: Language.shared.currentAppLang)
        lblTRenalFailure.text = "Renal Failure".localizeString(string: Language.shared.currentAppLang)
        lblTLiverDiseases.text = "Liver Diseases".localizeString(string: Language.shared.currentAppLang)
        lblTBloodTransfusion.text = "Blood Transfusion".localizeString(string: Language.shared.currentAppLang)
        lblTCardio.text = "Cardio Vascular System".localizeString(string: Language.shared.currentAppLang)
        lblTSmoking.text = "Smoking".localizeString(string: Language.shared.currentAppLang)
        lblTAlcoholism.text = "Alcoholism".localizeString(string: Language.shared.currentAppLang)
        lblTAllergy.text = "Allergy to any medicine".localizeString(string: Language.shared.currentAppLang)
        lblTDrugs.text = "Drugs".localizeString(string: Language.shared.currentAppLang)
        lblTFamilyHistory.text = "Family History".localizeString(string: Language.shared.currentAppLang)
        lblTAnyOtherHistory.text = "Any other history".localizeString(string: Language.shared.currentAppLang)
        lblPatientSuffering.text = "Patient Suffering From".localizeString(string: Language.shared.currentAppLang)
        lblTDocument.text = "Documents".localizeString(string: Language.shared.currentAppLang)
        lblTSelectDocument.text = "Select Document Type".localizeString(string: Language.shared.currentAppLang)
//        lblTJPG.text = "JPG".localizeString(string: Language.shared.currentAppLang)
//        lblTPDF.text = "PDF".localizeString(string: Language.shared.currentAppLang)
        
        txtDiagnosisName.placeholder = "Diagnosis Name"
        txtTime.placeholder = "Since, Time of diagnosis (In Months)".localizeString(string: Language.shared.currentAppLang)
        lblAge.placeholder = "Age of Patient".localizeString(string: Language.shared.currentAppLang)
        lblWeight.placeholder = "Weight (KG)".localizeString(string: Language.shared.currentAppLang)
        lblDiabetis.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtHypertension.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtAsthma.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtThyroid.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtRenal.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtLiver.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtBlood.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtCardio.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtSmoking.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtAlcoholism.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtAllergy.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        txtDrugs.placeholder = "Select".localizeString(string: Language.shared.currentAppLang)
        
        txtFmaily.delegate = self
        txtFmaily.text = "Family History".localizeString(string: Language.shared.currentAppLang)
        
        txtAntHistory.delegate = self
        txtAntHistory.text = "Any other history".localizeString(string: Language.shared.currentAppLang)

        txtPaientFrom.delegate = self
        txtPaientFrom.text = "Patient Suffering From".localizeString(string: Language.shared.currentAppLang)
        
        btnTAddDocument.setTitle("Add Document".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnUpdateDetail.setTitle("Update Details".localizeString(string: Language.shared.currentAppLang), for: .normal)

        if Language.shared.isArabic == false{
            txtDiagnosisName.textAlignment = .left
            txtTime.textAlignment = .left
            lblAge.textAlignment = .left
            lblWeight.textAlignment = .left
            lblDiabetis.textAlignment = .left
            txtHypertension.textAlignment = .left
            txtAsthma.textAlignment = .left
            txtThyroid.textAlignment = .left
            txtRenal.textAlignment = .left
            txtLiver.textAlignment = .left
            txtBlood.textAlignment = .left
            txtCardio.textAlignment = .left
            txtSmoking.textAlignment = .left
            txtAlcoholism.textAlignment = .left
            txtAllergy.textAlignment = .left
            txtDrugs.textAlignment = .left
            txtFmaily.textAlignment = .left
            txtAntHistory.textAlignment = .left
            txtPaientFrom.textAlignment = .left
        }else{
            txtDiagnosisName.textAlignment = .right
            txtTime.textAlignment = .right
            lblAge.textAlignment = .right
            lblWeight.textAlignment = .right
            lblDiabetis.textAlignment = .right
            txtHypertension.textAlignment = .right
            txtAsthma.textAlignment = .right
            txtThyroid.textAlignment = .right
            txtRenal.textAlignment = .right
            txtLiver.textAlignment = .right
            txtBlood.textAlignment = .right
            txtCardio.textAlignment = .right
            txtSmoking.textAlignment = .right
            txtAlcoholism.textAlignment = .right
            txtAllergy.textAlignment = .right
            txtDrugs.textAlignment = .right
            txtFmaily.textAlignment = .right
            txtAntHistory.textAlignment = .right
            txtPaientFrom.textAlignment = .right
        }

        tblView.delegate = self
        tblView.dataSource = self
        
        viewBG.isHidden = true
        viewPopUp.isHidden = true
        
        setUpDropDown()
        setUpTxt()
        
        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtFmaily{
            if txtFmaily.text == "Family History".localizeString(string: Language.shared.currentAppLang) {
                txtFmaily.text = ""
            }
        }else if textView == txtAntHistory{
            if txtAntHistory.text == "Any other history".localizeString(string: Language.shared.currentAppLang) {
                txtAntHistory.text = ""
            }
        }else if textView == txtPaientFrom{
            if txtPaientFrom.text == "Patient Suffering From".localizeString(string: Language.shared.currentAppLang) {
                txtPaientFrom.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtFmaily{
            if txtFmaily.text.isEmpty {
                txtFmaily.text = "Family History".localizeString(string: Language.shared.currentAppLang)
            }
        }else if textView == txtAntHistory{
            if txtAntHistory.text.isEmpty {
                txtAntHistory.text = "Any other history".localizeString(string: Language.shared.currentAppLang)
            }
        }else{
            if txtPaientFrom.text.isEmpty {
                txtPaientFrom.text = "Patient Suffering From".localizeString(string: Language.shared.currentAppLang)
            }
        }
    }
    
    func setUpTxt()
    {
        txtHypertension.text = "Select".localizeString(string: Language.shared.currentAppLang)
        lblDiabetis.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtThyroid.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtAsthma.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtLiver.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtRenal.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtCardio.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtBlood.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtAlcoholism.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtSmoking.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtDrugs.text = "Select".localizeString(string: Language.shared.currentAppLang)
        txtAllergy.text = "Select".localizeString(string: Language.shared.currentAppLang)

    }
    
    func setUpDropDown()
    {
        setDropDownHypertension()
        setDropDownDiabetis()
        setDropDownThyroid()
        setDropDownAsthma()
        setDropDownLiver()
        setDropDownRenal()
        setDropDownCardio()
        setDropDownBlood()
        setDropDownAlcoholism()
        setDropDownSmoking()
        setDropDownDrugs()
        setDropDownAllergy()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        if isFromHome == true
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let home = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentsListViewController") as! AppointmentsListViewController
            self.navigationController?.pushViewController(home, animated: false)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func clickedHypertention(_ sender: Any) {
        dropDownHypertension.show()
    }
    @IBAction func clickedDiabetes(_ sender: Any) {
        dropDownDiabetis.show()
    }
    @IBAction func clickedThyroid(_ sender: Any) {
        dropDownThyroid.show()
    }
    @IBAction func clickedAdthma(_ sender: Any) {
        dropDownAsthma.show()
    }
    @IBAction func clickedLiver(_ sender: Any) {
        dropDownLiver.show()
    }
    @IBAction func clickedRenal(_ sender: Any) {
        dropDownRenal.show()
    }
    @IBAction func clickedCardio(_ sender: Any) {
        dropDownCardio.show()
    }
    @IBAction func clickedBlood(_ sender: Any) {
        dropDownBlood.show()
    }
    @IBAction func clickedAlcoholism(_ sender: Any) {
        dropDownAlcoholism.show()
    }
    @IBAction func clickedSmooking(_ sender: Any) {
        dropDownSmoking.show()
    }
    @IBAction func clickedDrugs(_ sender: Any) {
        dropDownDrugs.show()
    }
    @IBAction func clickedAllergy(_ sender: Any) {
        dropDownAllergy.show()
    }
    @IBAction func clickedAddDocumnet(_ sender: Any) {
        viewBG.isHidden = false
        viewPopUp.isHidden = false
    }
    @IBAction func clickedUpdateDetails(_ sender: Any) {
        if self.txtDiagnosisName.text == ""
        {
            self.view.makeToast("Enter diagnosis name".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter diagnosis name".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtTime.text == ""
        {
            self.view.makeToast("Enter time of daignosis".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter time of daignosis".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.lblAge.text == ""
        {
            self.view.makeToast("Enter age of patient".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter age of patient".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.lblWeight.text == ""
        {
            self.view.makeToast("Enter weight of patient".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter weight of patient".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtHypertension.text == ""
        {
            self.view.makeToast("Select hypertention".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select hypertention".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.lblDiabetis.text == ""
        {
            self.view.makeToast("Select Diabetes".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select Diabetes".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtThyroid.text == ""
        {
            self.view.makeToast("Select Thyroid".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select Thyroid".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtAsthma.text == ""
        {
            self.view.makeToast("Select Asthma".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select Asthma".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtLiver.text == ""
        {
            self.view.makeToast("Select liver diseases".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select liver diseases".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtRenal.text == ""
        {
            self.view.makeToast("Select renal failure".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select renal failure".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtCardio.text == ""
        {
            self.view.makeToast("Select cardio vascular system".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select cardio vascular system".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtBlood.text == ""
        {
            self.view.makeToast("Select blood transfusion".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select blood transfusion".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtAlcoholism.text == ""
        {
            self.view.makeToast("Select alcoholism".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select alcoholism".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtSmoking.text == ""
        {
            self.view.makeToast("Select smoking".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select smoking".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtDrugs.text == ""
        {
            self.view.makeToast("Select drugs".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select drugs".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtAllergy.text == ""
        {
            self.view.makeToast("Select allergy to medicine".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Select allergy to medicine".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtFmaily.text == ""
        {
            self.view.makeToast("Enter family history".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter family history".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtAntHistory.text == ""
        {
            self.view.makeToast("Enter any other history".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter any other history".localizeString(string: Language.shared.currentAppLang))
        }
        else if self.txtPaientFrom.text == ""
        {
            self.view.makeToast("Enter patient suffering from".localizeString(string: Language.shared.currentAppLang))
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("Enter patient suffering from".localizeString(string: Language.shared.currentAppLang))
        }
        else
        {
            if self.arrUploadDocBOL.count + self.arrAllSelectedPdfInvoice.count != 0
            {
                myImageUploadRequest(imgKey: "image")
            }
            else
            {
                callAddMedicalAPI()
            }
        }
    }
    
    // Mark :- PopUp button action
    @IBAction func clickedPopUpCancel(_ sender: Any) {
        viewBG.isHidden = true
        viewPopUp.isHidden = true
    }
    
    @IBAction func clickedJPG(_ sender: Any) {
        let alert1 = UIAlertController(title: "Choose Photo".localizeString(string: Language.shared.currentAppLang), message: nil, preferredStyle: .actionSheet)
        
        alert1.addAction(UIAlertAction(title: "Camera".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert1.addAction(UIAlertAction(title: "Gallery".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert1.addAction(UIAlertAction.init(title: "Cancel".localizeString(string: Language.shared.currentAppLang), style: .cancel, handler: nil))
        
        self.present(alert1, animated: true, completion: nil)
    }
    
    @IBAction func clickedPDF(_ sender: Any) {
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.pdf", "public.data"], in: .import)
        documentPicker.delegate = self
        documentPicker.allowsMultipleSelection = true
        present(documentPicker, animated: true, completion: nil)
    }
    
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            
            ctx.cgContext.drawPDFPage(page)
        }
        
        return img
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {

        for url in urls {
            
            let cico = url as URL
            print(cico)
            print(cico)

            print(cico.lastPathComponent)

            print(cico.pathExtension)

            let url_New = URL(string: cico.relativeString)

        //    let new_img = drawPDFfromURL(url: url_New!)
            
            self.arrUploadDocInvoice.add(url_New!)

            let pdfData = try! Data(contentsOf: url_New!)
            let data7777 : Data = pdfData
            self.arrAllSelectedPdfInvoice.add(data7777)

            self.viewBG.isHidden = true
            
            self.tblView.reloadData()
            
        }
        
    }
    
    // MARK: - call API
    
    func callAddMedicalAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let patientId = appDelegate?.dicUserLoginData.id ?? ""
        
        let appointment_id = self.strAppointmentID
        
        let diagnosis_name = self.txtDiagnosisName.text ?? ""
        
        let diagnosis_time = self.txtTime.text ?? ""
        
        let age = self.lblAge.text ?? ""
        
        let weight = self.lblWeight.text ?? ""
        
        let hypertension = self.txtHypertension.text ?? ""
        
        let thyroid = self.txtThyroid.text ?? ""
        
        let asthma = self.txtAsthma.text ?? ""
        
        let liver_diseases = self.txtLiver.text ?? ""
        
        let renal_failure = self.txtRenal.text ?? ""
        
        let cardio = self.txtCardio.text ?? ""
        
        let alcoholism = self.txtAlcoholism.text ?? ""
        
        let smoking = self.txtSmoking.text ?? ""
        
        let drugs = self.txtDrugs.text ?? ""

        let allergy_medicine = self.txtAllergy.text ?? ""
        
        let family_history = self.txtFmaily.text ?? ""
        
        let diabetes = self.lblDiabetis .text ?? ""

        let blood_tansfusion = self.txtBlood.text ?? ""

        let patinet_suffering = self.txtPaientFrom.text ?? ""
        
        let other_history = self.txtAntHistory.text ?? ""
        
        let para = ["patientId":patientId,"diagnosis_name":diagnosis_name,"diagnosis_time":diagnosis_time,"age":age,"weight":weight,"hypertension":hypertension,"thyroid":thyroid,"asthma":asthma,"liver_diseases":liver_diseases,"renal_failure":renal_failure,"cardio":cardio,"alcoholism":alcoholism,"smoking":smoking,"drugs":drugs,"allergy_medicine":allergy_medicine,"family_history":family_history,"diabetes":diabetes,"blood_tansfusion":blood_tansfusion,"patinet_suffering":patinet_suffering,"appointment_id":appointment_id,"other_history":other_history]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(MEDICAL_FORM, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
                        vc.isMedical = true
                        let homeNavigation = UINavigationController(rootViewController: vc)
                        homeNavigation.modalPresentationStyle = .overFullScreen
                        homeNavigation.navigationBar.isHidden = true
                        vc.strTitle = "Medical History Uploaded Successfully"
                        self.present(homeNavigation, animated: false)
                    }
                    else
                    {
                        self.view.makeToast(message)
                    }
                }
            }
        }
    }
   
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePickerNew.delegate = self
            imagePickerNew.sourceType = UIImagePickerController.SourceType.camera
            imagePickerNew.allowsEditing = true
            imagePickerNew.mediaTypes = [kUTTypeImage as String]
            self.present(imagePickerNew, animated: true, completion: nil)
        }
        else
        {
            
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("You don't have camera")
        }
    }
    
    func openGallary()
    {
        
        imagePickerNew.delegate = self
        imagePickerNew.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePickerNew.allowsEditing = true
        imagePickerNew.mediaTypes = ["public.image"]
        self.present(imagePickerNew, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        {
            let imageurl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            selectedImg = imageurl?.lastPathComponent
            self.viewBG.isHidden = true
            self.arrUploadDocBOL.add(image)
            
            self.tblView.reloadData()

        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            let imageurl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            
            selectedImg = imageurl?.lastPathComponent
            self.viewBG.isHidden = true
            
            self.arrUploadDocBOL.add(image)
            
            self.tblView.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func myImageUploadRequest(imgKey: String) {
        
        APIClient.sharedInstance.showIndicator()
        
        let myUrl = NSURL(string: BASE_URL + MEDICAL_FORM);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let patientId = appDelegate?.dicUserLoginData.id ?? ""
        
        let appointment_id = self.strAppointmentID
        
        let diagnosis_name = self.txtDiagnosisName.text ?? ""
        
        let diagnosis_time = self.txtTime.text ?? ""
        
        let age = self.lblAge.text ?? ""
        
        let weight = self.lblWeight.text ?? ""
        
        let hypertension = self.txtHypertension.text ?? ""
        
        let thyroid = self.txtThyroid.text ?? ""
        
        let asthma = self.txtAsthma.text ?? ""
        
        let liver_diseases = self.txtLiver.text ?? ""
        
        let renal_failure = self.txtRenal.text ?? ""
        
        let cardio = self.txtCardio.text ?? ""
        
        let alcoholism = self.txtAlcoholism.text ?? ""
        
        let smoking = self.txtSmoking.text ?? ""
        
        let drugs = self.txtDrugs.text ?? ""

        let allergy_medicine = self.txtAllergy.text ?? ""
        
        let family_history = self.txtFmaily.text ?? ""
        
        let diabetes = self.lblDiabetis.text ?? ""

        let blood_tansfusion = self.txtBlood.text ?? ""

        let patinet_suffering = self.txtPaientFrom.text ?? ""
        
        let other_history = self.txtAntHistory.text ?? ""
        
        let params = ["patientId":patientId,"diagnosis_name":diagnosis_name,"diagnosis_time":diagnosis_time,"age":age,"weight":weight,"hypertension":hypertension,"thyroid":thyroid,"asthma":asthma,"liver_diseases":liver_diseases,"renal_failure":renal_failure,"cardio":cardio,"alcoholism":alcoholism,"smoking":smoking,"drugs":drugs,"allergy_medicine":allergy_medicine,"family_history":family_history,"diabetes":diabetes,"blood_tansfusion":blood_tansfusion,"patinet_suffering":patinet_suffering,"appointment_id":appointment_id,"other_history":other_history]
        
        print(params)

        let boundary = generateBoundaryString()
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = createBodyWithParameters(parameters: params, filePathKey: imgKey, boundary: boundary, imgKey: imgKey) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            APIClient.sharedInstance.hideIndicator()
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            
            // print reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("response data = \(responseString!)")
            
            do{
                
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                    
                    // try to read out a string array
                    if let message = json["message"] as? String
                    {
                        DispatchQueue.main.async {
                           
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
                            let homeNavigation = UINavigationController(rootViewController: vc)
                            homeNavigation.modalPresentationStyle = .overFullScreen
                            homeNavigation.navigationBar.isHidden = true
                            vc.strTitle = "Medical History Uploaded Successfully".localizeString(string: Language.shared.currentAppLang)
                            vc.isMedical = true
                            self.present(homeNavigation, animated: false)
                        }
                    }
                }
                
            }catch{
                
            }
            
        }
        
        task.resume()
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, boundary: String, imgKey: String) -> NSData {
        
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "\(imgKey).jpg"
        let mimetype = "image/jpeg"
        
        var objTotalCount = 0
        
        for (index,objImg) in arrUploadDocBOL.enumerated()
        {
            let imageData = (objImg as? UIImage)?.jpegData(compressionQuality: 1)
            
            objTotalCount = index
            
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"image[\(index)]\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(imageData! as Data)
            body.appendString(string: "\r\n")
            body.appendString(string: "--\(boundary)--\r\n")
        }
        
        let filenamePdfBOL = "application.pdf"
        let mimetypePdfBOL = "application/pdf"
        
        for (index,objPDf) in arrAllSelectedPdfInvoice.enumerated()
        {
            var objNewIndex = 0
            
            if objTotalCount == 0 && index == 0
            {
                objNewIndex = objTotalCount
            }
            else
            {
                objNewIndex = objTotalCount + 1
            }
            
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"image[\(objNewIndex)]\"; filename=\"\(filenamePdfBOL)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetypePdfBOL)\r\n\r\n")
            body.append(objPDf as! Data)
            body.appendString(string: "\r\n")
            body.appendString(string: "--\(boundary)--\r\n")
        }
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func randomString(of length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var s = ""
        for _ in 0 ..< length {
            s.append(letters.randomElement()!)
        }
        return s
    }
    
}


extension MedicalEditVC: UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return arrUploadDocBOL.count
        }
        else
        {
            return arrAllSelectedPdfInvoice.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "UploadDocumentCell") as! UploadDocumentCell
                
        if indexPath.section == 0
        {
            
            cell.lblName.textAlignment = .center
            cell.lblName.text = "/data/user/gfc\(random(digits: 150)).jpg"
            
            cell.lblType.text = "JPG"
            
            cell.btnClose.tag = indexPath.row
            cell.btnClose.removeTarget(self, action: #selector(clickedRemovePDF(_ :)), for: .touchUpInside)
            cell.btnClose.addTarget(self, action: #selector(clickedRemove(_ :)), for: .touchUpInside)

        }
        else
        {
            cell.lblType.text = "PDF"

            cell.lblName.textAlignment = .center
            cell.lblName.text = "/data/user/gfc\(random(digits: 150)).pdf"

            cell.btnClose.tag = indexPath.row
            cell.btnClose.removeTarget(self, action: #selector(clickedRemove(_ :)), for: .touchUpInside)
            cell.btnClose.addTarget(self, action: #selector(clickedRemovePDF(_ :)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.section == 0
        {
            let dicData = arrUploadDocBOL[indexPath.row] as? UIImage
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
            vc.modalPresentationStyle = .overFullScreen
            vc.isShowingImage = true
            vc.strImage = dicData!
            self.present(vc, animated: false, completion: nil)
        }
        else
        {
            let dicData = arrUploadDocInvoice[indexPath.row] as? URL
            
            if let pdfURL = dicData {
                openPDFInSafari(pdfURL: pdfURL)
            }
        }
       
    }
    
    func openPDFInSafari(pdfURL: URL) {
           if UIApplication.shared.canOpenURL(pdfURL) {
               UIApplication.shared.open(pdfURL, options: [:], completionHandler: nil)
           } else {
               // Handle the case where the URL can't be opened, e.g., display an alert.
               print("Unable to open PDF in Safari.")
           }
       }
    
    @objc func clickedRemove(_ sender: UIButton)
    {
        let dicData = arrUploadDocBOL[sender.tag] as? UIImage
        self.arrUploadDocBOL.remove(dicData!)
        
        self.tblView.reloadData()
    }
    
    
    @objc func clickedRemovePDF(_ sender: UIButton)
    {
        let dicData = arrAllSelectedPdfInvoice[sender.tag]
        self.arrAllSelectedPdfInvoice.remove(dicData)

        let dicData1 = arrUploadDocInvoice[sender.tag]
        self.arrUploadDocInvoice.remove(dicData1)
        
        self.tblView.reloadData()
    }
    
    func random(digits:Int) -> String {
        var number = String()
        for _ in 1...digits {
           number += "\(Int.random(in: 1...9))"
        }
        return number
    }

    
}

extension MedicalEditVC {
    
    func setDropDownHypertension()
    {
        dropDownHypertension.dataSource = arrHypertension
        dropDownHypertension.anchorView = txtHypertension
        dropDownHypertension.direction = .bottom
        
        dropDownHypertension.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtHypertension.text = item
        }
        dropDownHypertension.bottomOffset = CGPoint(x: 0, y: txtHypertension.bounds.height)
        dropDownHypertension.topOffset = CGPoint(x: 0, y: -txtHypertension.bounds.height)
        dropDownHypertension.dismissMode = .onTap
        dropDownHypertension.textColor = UIColor.darkGray
        dropDownHypertension.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownHypertension.selectionBackgroundColor = UIColor.clear
        dropDownHypertension.selectedTextColor = .black

        dropDownHypertension.reloadAllComponents()
    }
    
    func setDropDownDiabetis()
    {
        dropDownDiabetis.dataSource = arrHypertension
        dropDownDiabetis.anchorView = lblDiabetis
        dropDownDiabetis.direction = .bottom
        
        dropDownDiabetis.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblDiabetis.text = item
        }
        dropDownDiabetis.bottomOffset = CGPoint(x: 0, y: lblDiabetis.bounds.height)
        dropDownDiabetis.topOffset = CGPoint(x: 0, y: -lblDiabetis.bounds.height)
        dropDownDiabetis.dismissMode = .onTap
        dropDownDiabetis.textColor = UIColor.darkGray
        dropDownDiabetis.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownDiabetis.selectionBackgroundColor = UIColor.clear
        dropDownDiabetis.selectedTextColor = .black

        dropDownDiabetis.reloadAllComponents()
    }
    
    func setDropDownThyroid()
    {
        dropDownThyroid.dataSource = arrHypertension
        dropDownThyroid.anchorView = txtThyroid
        dropDownThyroid.direction = .bottom
        
        dropDownThyroid.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtThyroid.text = item
        }
        dropDownThyroid.bottomOffset = CGPoint(x: 0, y: txtThyroid.bounds.height)
        dropDownThyroid.topOffset = CGPoint(x: 0, y: -txtThyroid.bounds.height)
        dropDownThyroid.dismissMode = .onTap
        dropDownThyroid.textColor = UIColor.darkGray
        dropDownThyroid.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownThyroid.selectionBackgroundColor = UIColor.clear
        dropDownThyroid.selectedTextColor = .black

        dropDownThyroid.reloadAllComponents()
    }
    
    func setDropDownAsthma()
    {
        dropDownAsthma.dataSource = arrHypertension
        dropDownAsthma.anchorView = txtAsthma
        dropDownAsthma.direction = .bottom
        
        dropDownAsthma.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtAsthma.text = item
        }
        dropDownAsthma.bottomOffset = CGPoint(x: 0, y: txtAsthma.bounds.height)
        dropDownAsthma.topOffset = CGPoint(x: 0, y: -txtAsthma.bounds.height)
        dropDownAsthma.dismissMode = .onTap
        dropDownAsthma.textColor = UIColor.darkGray
        dropDownAsthma.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownAsthma.selectionBackgroundColor = UIColor.clear
        dropDownAsthma.selectedTextColor = .black
        dropDownAsthma.reloadAllComponents()
    }
    
    func setDropDownLiver()
    {
        dropDownLiver.dataSource = arrHypertension
        dropDownLiver.anchorView = txtLiver
        dropDownLiver.direction = .bottom
        
        dropDownLiver.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtLiver.text = item
        }
        dropDownLiver.bottomOffset = CGPoint(x: 0, y: txtLiver.bounds.height)
        dropDownLiver.topOffset = CGPoint(x: 0, y: -txtLiver.bounds.height)
        dropDownLiver.dismissMode = .onTap
        dropDownLiver.textColor = UIColor.darkGray
        dropDownLiver.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownLiver.selectionBackgroundColor = UIColor.clear
        dropDownLiver.selectedTextColor = .black
        dropDownLiver.reloadAllComponents()
    }
    
    func setDropDownRenal()
    {
        dropDownRenal.dataSource = arrHypertension
        dropDownRenal.anchorView = txtRenal
        dropDownRenal.direction = .bottom
        
        dropDownRenal.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtRenal.text = item
        }
        dropDownRenal.bottomOffset = CGPoint(x: 0, y: txtRenal.bounds.height)
        dropDownRenal.topOffset = CGPoint(x: 0, y: -txtRenal.bounds.height)
        dropDownRenal.dismissMode = .onTap
        dropDownRenal.textColor = UIColor.darkGray
        dropDownRenal.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownRenal.selectionBackgroundColor = UIColor.clear
        dropDownRenal.selectedTextColor = .black

        dropDownRenal.reloadAllComponents()
    }
    
    func setDropDownCardio()
    {
        dropDownCardio.dataSource = arrHypertension
        dropDownCardio.anchorView = txtCardio
        dropDownCardio.direction = .bottom
        
        dropDownCardio.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCardio.text = item
        }
        dropDownCardio.bottomOffset = CGPoint(x: 0, y: txtCardio.bounds.height)
        dropDownCardio.topOffset = CGPoint(x: 0, y: -txtCardio.bounds.height)
        dropDownCardio.dismissMode = .onTap
        dropDownCardio.textColor = UIColor.darkGray
        dropDownCardio.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCardio.selectionBackgroundColor = UIColor.clear
        dropDownCardio.selectedTextColor = .black

        dropDownCardio.reloadAllComponents()
    }
    
    func setDropDownBlood()
    {
        dropDownBlood.dataSource = arrHypertension
        dropDownBlood.anchorView = txtBlood
        dropDownBlood.direction = .bottom
        
        dropDownBlood.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtBlood.text = item
        }
        dropDownBlood.bottomOffset = CGPoint(x: 0, y: txtBlood.bounds.height)
        dropDownBlood.topOffset = CGPoint(x: 0, y: -txtBlood.bounds.height)
        dropDownBlood.dismissMode = .onTap
        dropDownBlood.textColor = UIColor.darkGray
        dropDownBlood.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownBlood.selectionBackgroundColor = UIColor.clear
        dropDownBlood.selectedTextColor = .black
        dropDownBlood.reloadAllComponents()
    }
    
    func setDropDownAlcoholism()
    {
        dropDownAlcoholism.dataSource = arrHypertension
        dropDownAlcoholism.anchorView = txtAlcoholism
        dropDownAlcoholism.direction = .bottom
        
        dropDownAlcoholism.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtAlcoholism.text = item
        }
        dropDownAlcoholism.bottomOffset = CGPoint(x: 0, y: txtAlcoholism.bounds.height)
        dropDownAlcoholism.topOffset = CGPoint(x: 0, y: -txtAlcoholism.bounds.height)
        dropDownAlcoholism.dismissMode = .onTap
        dropDownAlcoholism.textColor = UIColor.darkGray
        dropDownAlcoholism.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownAlcoholism.selectionBackgroundColor = UIColor.clear
        dropDownAlcoholism.selectedTextColor = .black
        dropDownAlcoholism.reloadAllComponents()
    }
    
    func setDropDownSmoking()
    {
        dropDownSmoking.dataSource = arrHypertension
        dropDownSmoking.anchorView = txtSmoking
        dropDownSmoking.direction = .bottom
        
        dropDownSmoking.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtSmoking.text = item
        }
        dropDownSmoking.bottomOffset = CGPoint(x: 0, y: txtSmoking.bounds.height)
        dropDownSmoking.topOffset = CGPoint(x: 0, y: -txtSmoking.bounds.height)
        dropDownSmoking.dismissMode = .onTap
        dropDownSmoking.textColor = UIColor.darkGray
        dropDownSmoking.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownSmoking.selectionBackgroundColor = UIColor.clear
        dropDownSmoking.selectedTextColor = .black

        dropDownSmoking.reloadAllComponents()
    }
    
    func setDropDownDrugs()
    {
        dropDownDrugs.dataSource = arrHypertension
        dropDownDrugs.anchorView = txtDrugs
        dropDownDrugs.direction = .bottom
        
        dropDownDrugs.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtDrugs.text = item
        }
        dropDownDrugs.bottomOffset = CGPoint(x: 0, y: txtDrugs.bounds.height)
        dropDownDrugs.topOffset = CGPoint(x: 0, y: -txtDrugs.bounds.height)
        dropDownDrugs.dismissMode = .onTap
        dropDownDrugs.textColor = UIColor.darkGray
        dropDownDrugs.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownDrugs.selectionBackgroundColor = UIColor.clear
        dropDownDrugs.selectedTextColor = .black

        dropDownDrugs.reloadAllComponents()
    }
    
    func setDropDownAllergy()
    {
        dropDownAllergy.dataSource = arrHypertension
        dropDownAllergy.anchorView = txtAllergy
        dropDownAllergy.direction = .bottom
        
        dropDownAllergy.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtAllergy.text = item
        }
        dropDownAllergy.bottomOffset = CGPoint(x: 0, y: txtAllergy.bounds.height)
        dropDownAllergy.topOffset = CGPoint(x: 0, y: -txtAllergy.bounds.height)
        dropDownAllergy.dismissMode = .onTap
        dropDownAllergy.textColor = UIColor.darkGray
        dropDownAllergy.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownAllergy.selectionBackgroundColor = UIColor.clear
        dropDownAllergy.selectedTextColor = .black

        dropDownAllergy.reloadAllComponents()
    }
    
}

class UploadDocumentCell: UITableViewCell
{
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
}
