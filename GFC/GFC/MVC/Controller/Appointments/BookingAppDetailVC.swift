//
//  BookingAppDetailVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 13/09/24.
//

import UIKit
import Quickblox
import QuickbloxWebRTC
import PushKit

class BookingAppDetailVC: UIViewController {

    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewPrice: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDegree: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblOnlineModeFee: UILabel!
    @IBOutlet weak var lblTotalFee: UILabel!
    @IBOutlet weak var imgPhoto: UIImageView!
    
    @IBOutlet weak var lblTConsultationFee: UILabel!
    @IBOutlet weak var lblTDate: UILabel!
    @IBOutlet weak var lblTTime: UILabel!
    @IBOutlet weak var lblTConsultationMode: UILabel!
    @IBOutlet weak var lblTOnlineModeFee: UILabel!
    @IBOutlet weak var lblTTotal: UILabel!
    @IBOutlet weak var lblTAddress: UILabel!
    @IBOutlet weak var lblTCallBtn: UILabel!
    @IBOutlet weak var lblTChatBtn: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    
    @IBOutlet weak var viewButtton: UIView!
    @IBOutlet weak var imgOnlineModeFee: UIImageView!
    @IBOutlet weak var lblModeType: UILabel!
    @IBOutlet weak var viewOnlineModeFee: UIView!

    
    @IBOutlet weak var viewOnlineModeHeightConst: NSLayoutConstraint!
    
    var dicDocotrDetail = GFCAppointmentDoctorsAppointment()
    
    private var users = Users()
    
    private let authModule = AuthModule()
    private let connection = ConnectionModule()
    private var splashVC: SplashScreenViewController? = nil
    private let callHelper = CallHelper()
    
    private var callViewController: CallViewController? = nil

     private let chatManager = ChatManager.instance

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callDoctorDetailsDataApi()
        
        connection.delegate = self
        
        connection.activateAutomaticMode()
        authModule.delegate = self
        
        callHelper.delegate = self

        viewDetail.layer.applySketchShadow(color: .black, alpha: 0.2, x: 0, y: 0, blur: 17, spread: 0)
        viewDate.layer.applySketchShadow(color: .black, alpha: 0.2, x: 0, y: 0, blur: 17, spread: 0)
        viewPrice.layer.applySketchShadow(color: .black, alpha: 0.2, x: 0, y: 0, blur: 17, spread: 0)
        
        lblTTime.text = "Time Slot".localizeString(string: Language.shared.currentAppLang)
        lblTDate.text = "Date".localizeString(string: Language.shared.currentAppLang)
        lblTConsultationFee.text = "Consultation Fee".localizeString(string: Language.shared.currentAppLang)
        lblTAddress.text = "Address".localizeString(string: Language.shared.currentAppLang)
        lblTConsultationMode.text = "Consultation Mode".localizeString(string: Language.shared.currentAppLang)
        lblTCallBtn.text = "Call".localizeString(string: Language.shared.currentAppLang)
        lblTChatBtn.text = "Chat".localizeString(string: Language.shared.currentAppLang)
        
        var media_link_url = "\(dicDocotrDetail.image_url ?? "")\(self.dicDocotrDetail.image ?? "")"
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        self.imgPhoto.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        self.lblName.text = dicDocotrDetail.doctorName ?? ""
        self.lblTime.text = dicDocotrDetail.time ?? ""
        self.lblFee.text = "$\(dicDocotrDetail.consultantFee ?? "")"
        self.lblAddress.text = dicDocotrDetail.doctorAddress ?? ""
//        self.lblType.text = dicDrQueryDetail.specializationName ?? ""
//        self.lblDegree.text = dicDrQueryDetail.qualification ?? ""
        
        if dicDocotrDetail.rating == "0"
        {
            lblRate.isUserInteractionEnabled = true
//            lblRate.setTitle("Rate Us".localizeString(string: Language.shared.currentAppLang), for: .normal)
        }
        else
        {
            let rating = Double(dicDocotrDetail.rating ?? "")?.round(to: 1)

            lblRate.isUserInteractionEnabled = false
//            lblRate.setTitle("\(rating ?? 0.0)", for: .normal)
        }
        
        if let bidAccepted = dicDocotrDetail.date
        {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US")
            formatter.dateFormat = "yyyy-MM-dd"
            if let date1 = formatter.date(from: bidAccepted)
            {
                let Dform = DateFormatter()
                Dform.dateFormat = "MMM dd, yyyy"
                let strDate = Dform.string(from: date1)
                self.lblDate.text = strDate
            }
        }
        
        if dicDocotrDetail.appointment_type == "Online" {
            
            viewButtton.isHidden = false
            
        } else if dicDocotrDetail.appointment_type == "Appointment" {
            
            viewButtton.isHidden = true
            imgOnlineModeFee.isHidden = true
            lblTOnlineModeFee.isHidden = true
            lblOnlineModeFee.isHidden = true
            lblModeType.text = "Appointment"
            viewOnlineModeFee.isHidden = true
            viewOnlineModeHeightConst.constant = 0
            
        } else {
            
            viewButtton.isHidden = true
            lblTOnlineModeFee.isHidden = true
            lblOnlineModeFee.isHidden = true
            lblModeType.text = ""
            viewOnlineModeFee.isHidden = true
            viewOnlineModeHeightConst.constant = 0
            
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CallPermissions.check(with: .video, presentingViewController: self) { granted in
        }
    }
    
    // MARK: - callDoctorDetailsDataApi()
    func callDoctorDetailsDataApi()
    {
        
        let para = ["doctor_id": dicDocotrDetail.doctorId ?? ""]
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(DOCTORS_DETAILS, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
             
            if error == nil{
                APIClient.sharedInstance.hideIndicator()
                
                if let dicResponse = response as? NSDictionary{
                    if let arraData = dicResponse.value(forKey: "data") as? NSArray{
                        
                        let dicData = arraData[0] as! NSDictionary
                        let dicDetails = GFCDoctorDetailsData(fromDictionary: dicData)
                        
                        appDelegate?.objOtherUserId = Int(dicDetails.quickbloxId ?? "") ?? 0
                        appDelegate?.objOtherUserName = dicDetails.doctorName ?? ""
                        appDelegate?.objOtherUserLogin = dicDetails.email.before(first: "@")
                    }
                }
            }
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedVideoCall(_ sender: Any) {
        call(with: QBRTCConferenceType.video)
     }
    
    @IBAction func clickedChat(_ sender: Any) {
     
        let objUser = QBUUser()
        objUser.id = UInt(appDelegate?.objOtherUserId ?? 0)
        objUser.fullName = appDelegate?.objOtherUserName ?? ""
        chatManager.storage.update(users: [objUser])
        
        chatManager.createPrivateDialog(withOpponent: objUser, completion: { [weak self] (error, dialog) in
            guard let self = self else {return}
            guard let dialog = dialog,
                  let dialogID = dialog.id else {
                return
            }
            
            guard let chatController = Screen.chatViewController() else {
                return
            }
            chatController.dialogID = dialogID
            self.navigationController?.pushViewController(chatController, animated: true)
        })
        
    }
    
    
    private func call(with conferenceType: QBRTCConferenceType) {
        if connection.established == false {
          //  showNoInternetAlert { [weak self] (action) in
                self.connection.establish()
          //  }
            return
        }
        
        guard callHelper.registeredCallId?.isEmpty == true else {
            return
        }
        
        CallPermissions.check(with: conferenceType, presentingViewController: self) { [weak self] granted in
            guard let self = self, granted == true else {
                return
            }
            self.connection.activateCallMode()
            var callMembers: [NSNumber: String] = [:]
            
            callMembers[NSNumber(value: Int(dicDocotrDetail.quickblox_id ?? "") ?? 0)] = dicDocotrDetail.doctorName ?? ""
            
            let hasVideo = conferenceType == .video
            self.callHelper.registerCall(withMembers: callMembers, hasVideo: hasVideo)
        }
    }

}

// MARK: - AuthModuleDelegate
extension BookingAppDetailVC: AuthModuleDelegate {
    func authModule(_ authModule: AuthModule, didLoginUser user: QBUUser) {
        Profile.synchronize(withUser: user)
        connection.establish()
        
    }
    
    func authModuleDidLogout(_ authModule: AuthModule) {
        connection.deactivateAutomaticMode()
        
    }
    
    func authModule(_ authModule: AuthModule, didReceivedError error: ErrorInfo) {
        showUnAuthorizeAlert(message: "\(error.statusCode) \(error.info)" , logoutAction: { [weak self] logoutAction in
            self?.logout()
        }, tryAgainAction: { [weak self] tryAgainAction in
            let profile = Profile()
            self?.authModule.login(fullName: profile.fullName, login: profile.login)
        })
    }
    
    private func logout() {
        if connection.established == false {
          //  showNoInternetAlert { [weak self] (action) in
            self.connection.establish()
          //  }
            return
        }
        
    }
}


// MARK: - ConnectionModuleDelegate
extension BookingAppDetailVC: ConnectionModuleDelegate {
    func connectionModuleWillConnect(_ connectionModule: ConnectionModule) {
        showAnimatedAlertView(nil, message: ConnectionConstant.connectingState)
    }
    
    func connectionModuleDidConnect(_ connectionModule: ConnectionModule) {
        hideAlertView()
    }
    
    func connectionModuleDidNotConnect(_ connectionModule: ConnectionModule, error: Error) {
        if error._code.isNetworkError {
            //showNoInternetAlert { [weak self] (action) in
            self.connection.establish()
           // }
            return
        }
        showAlertView(nil, message: error.localizedDescription, handler: nil)
    }
    
    func connectionModuleWillReconnect(_ connectionModule: ConnectionModule) {
        showAnimatedAlertView(nil, message: ConnectionConstant.reconnectingState)
    }
    
    func connectionModuleDidReconnect(_ connectionModule: ConnectionModule) {
        hideAlertView()
    }
    
    func connectionModuleTokenHasExpired(_ connectionModule: ConnectionModule) {
        let profile = Profile()
        authModule.login(fullName: profile.fullName, login: profile.login)
    }
}


// MARK: - CallHelperDelegate
extension BookingAppDetailVC: CallHelperDelegate {
    func helper(_ helper: CallHelper, didAcceptCall callId: String) {
        self.helper(helper, showCall: callId)
    }
    
    func helper(_ helper: CallHelper, didRegisterCall callId: String, mediaListener: MediaListener, mediaController: MediaController, direction: CallDirection, members: [NSNumber : String], hasVideo: Bool) {
        
        connection.activateCallMode()
        
        callViewController = hasVideo == true ? Screen.videoCallViewController() : Screen.audioCallViewController()
        
        callViewController?.setupWithCallId(callId, members: members, mediaListener: mediaListener, mediaController: mediaController, direction: direction)
        
        if direction == .outgoing {
            self.helper(helper, showCall: callId)
            return
        }
        let usersIDs = Array(members.keys)
        users.users(usersIDs) { [weak self] (users, error) in
            guard let self = self, let users = users else {
                return
            }
            
            let profile = Profile()
            
            var callMembers: [NSNumber : String] = [:]
            for user in users {
                if profile.ID != user.id
                {
                    callMembers[NSNumber(value: user.id)] = user.fullName ?? "User"
                }
            }
            self.callViewController?.callInfo.updateWithMembers(callMembers)
            let title = Array(callMembers.values).joined(separator: ", ")
            helper.updateCall(callId, title: title)
        }
    }
    
    func helper(_ helper: CallHelper, didUnregisterCall callId: String) {
        connection.deactivateCallMode()
        guard let callViewController = callViewController else {
            return
        }
        
        callViewController.dismiss(animated: false) {
            self.callViewController = nil
        }
    }
    
    //Internal Method
    private func helper(_ helper: CallHelper, showCall callId: String) {
        guard let callViewController = self.callViewController,
              let callViewControllerCallId = callViewController.callInfo.callId,
              callViewControllerCallId == callId else {
            return
        }
        hideAlertView()
        
        let navVC = UINavigationController(rootViewController: callViewController)
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: false)
        
        users.selected.removeAll()
        callViewController.hangUp = { (callId) in
            helper.unregisterCall(callId, userInfo: ["hangup": "hang up"])
        }
    }
    
}
