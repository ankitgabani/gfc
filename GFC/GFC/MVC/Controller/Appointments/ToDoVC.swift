//
//  ToDoVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 06/03/23.
//

import UIKit
import Quickblox
class ToDoVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var lblTToDoNote: UILabel!
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var arrNote: [GFCToDoNoteListData] = [GFCToDoNoteListData]()
    
    var strAppointmetnID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTToDoNote.text = "To Do Note".localizeString(string: Language.shared.currentAppLang)
        lblTNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)

        tblView.delegate = self
        tblView.dataSource = self
        callNoteAPI()
        
        viewNoData.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    func callNoteAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["appointment_id":self.strAppointmetnID]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TO_DO_NOTE, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        for obj in arrData!
                        {
                            let dicData = GFCToDoNoteListData(fromDictionary: obj as! NSDictionary)
                            self.arrNote.append(dicData)
                        }
                        self.tblView.reloadData()
                    }
                    else
                    {
                        self.viewNoData.isHidden = false
                    }
                }
               
            }
        }
    }
    
    // MARK: - tblView Delegate Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNote.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "ToDoTblViewCell") as! ToDoTblViewCell
        
        cell.viewMAin.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)

        let dicData = arrNote[indexPath.row]
        
        if dicData.image.contains(".pdf") == true
        {
            cell.imgPic.contentMode = .scaleAspectFit
            cell.imgPic.image = UIImage(named: "pdf")
        }
        else
        {
            cell.imgPic.contentMode = .scaleAspectFill
            var media_link_url = dicData.image ?? ""
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        }
        
        cell.lblName.text = dicData.descriptionField ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrNote[indexPath.row]
        
        if dicData.image.contains(".pdf") == true
        {
            if let pdfURL = URL(string: dicData.image ?? "") {
                openPDFInSafari(pdfURL: pdfURL)
            }
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
            vc.modalPresentationStyle = .overFullScreen
            vc.strImg = dicData.image ?? ""
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    func openPDFInSafari(pdfURL: URL) {
        if UIApplication.shared.canOpenURL(pdfURL) {
            UIApplication.shared.open(pdfURL, options: [:], completionHandler: nil)
        } else {
            // Handle the case where the URL can't be opened, e.g., display an alert.
            print("Unable to open PDF in Safari.")
        }
    }

}

class ToDoTblViewCell : UITableViewCell
{
    @IBOutlet weak var viewMAin: UIView!
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}
