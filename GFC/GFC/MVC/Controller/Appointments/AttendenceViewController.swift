//
//  AttendenceViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 30/03/22.
//

import UIKit
import Quickblox
class AttendenceViewController: UIViewController {
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var txtRelative: UITextField!
    
    @IBOutlet weak var viewFN: UIView!
    @IBOutlet weak var viewMN: UIView!
    @IBOutlet weak var viewRelative: UIView!
    @IBOutlet weak var viewAddress: UIView!
    
    @IBOutlet weak var lblTAttendantInfo: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var dicHospital = GFCAppointmentHospitalAppointment()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTAttendantInfo.text = "Attendant Info".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            txtFirstName.textAlignment = .left
            txtPhoneNumber.textAlignment = .left
            txtRelative.textAlignment = .left
            txtAddress.textAlignment = .left
        }else{
            txtFirstName.textAlignment = .right
            txtPhoneNumber.textAlignment = .right
            txtRelative.textAlignment = .right
            txtAddress.textAlignment = .right
        }
    
        viewFN.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        
        viewMN.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        
        viewRelative.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        
        viewAddress.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 10, spread: 0)
        
        txtFirstName.text = dicHospital.attendantName ?? ""
        txtPhoneNumber.text = dicHospital.attendantPhone ?? ""
        txtRelative.text = dicHospital.relation ?? ""
        txtAddress.text = dicHospital.attendantAddress ?? ""
        
    }
  
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
