//
//  ImageVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 13/03/23.
//

import UIKit
import Quickblox
class ImageVC: UIViewController {

    @IBOutlet weak var imgPic: UIImageView!
    
    var strImg = ""
    
    var isShowingImage = false
    var strImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isShowingImage == true{
            self.imgPic.image = strImage
        }
        else
        {
            if strImg == ""
            {
                self.imgPic.contentMode = .scaleAspectFit
                self.imgPic.image = UIImage(named: "app_logo")
            }
            else
            {
                self.imgPic.contentMode = .scaleAspectFit
                var media_link_url = strImg
                media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
                self.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
            }
        }
      
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
}
