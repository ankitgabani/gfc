//
//  AppoitmentDetailViewController.swift
//  GFC
//
//  Created by iMac on 28/03/22.
//

import UIKit
import Quickblox
class AppoitmentDetailViewController: UIViewController {

    @IBOutlet weak var appointmentDetailTblView: UITableView!
    
    @IBOutlet weak var lblTAppointmentDetail: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var arrHospitalList: [GFCAppointmentHospitalAppointment] = [GFCAppointmentHospitalAppointment]()
 
    var arrAppointmentDetail = ["Patient Detail","Attendant","Medical Form","Treatments","Plan","Flight Detail","To Do Note","Payment History"]
 
    var dicHospital = GFCAppointmentHospitalAppointment()
    
    var dicMedicalDetail = GFCMedicalDetailData()
    
    var isOpenHospital = false
    
    var appointment_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTAppointmentDetail.text = "Appointment Detail".localizeString(string: Language.shared.currentAppLang)

        appointmentDetailTblView.delegate = self
        appointmentDetailTblView.dataSource = self
      
        callMedicalDetailAPI()
        
        if isOpenHospital == true
        {
            callLabCateListAPI()
        }
    }
   
    @IBAction func backBtnClicked(_ sender: Any) {
        if isOpenHospital == false
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            appDelegate?.setUpSideMenu()
        }
    }
    
    func callMedicalDetailAPI()
    {
       // APIClient.sharedInstance.showIndicator()
        
        var para = ["":""]
        
        if isOpenHospital == true
        {
            para = ["appointment_id":"\(self.appointment_id)"]
        }
        else
        {
            para = ["appointment_id":dicHospital.appointId ?? ""]
        }
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(MEDICAL_FORM_DETAIL, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        
                        let dicData = GFCMedicalDetailData(fromDictionary: dic!)
                        self.dicMedicalDetail = dicData
                    }
                }
            }
        }
    }
    
    func callLabCateListAPI()
    {
        APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        let _url = "?patientId=\(appDelegate?.dicUserLoginData.id ?? "")"
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_APPOINTMENT + _url, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        self.arrHospitalList.removeAll()
                        if let arrHospital = dic?.value(forKey: "hospital_appointment") as? NSArray
                        {
                            for objHospital in arrHospital
                            {
                                let dicDataHospital = GFCAppointmentHospitalAppointment(fromDictionary: objHospital as! NSDictionary)
                                self.arrHospitalList.append(dicDataHospital)
                            }
                            self.arrHospitalList = self.arrHospitalList.reversed()
                        }
                    }
                }
            }
        }
    }
    
}

extension AppoitmentDetailViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAppointmentDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentCellTableViewCell") as! AppointmentCellTableViewCell
        
        cell.lblTitle.text = arrAppointmentDetail[indexPath.row].localizeString(string: Language.shared.currentAppLang)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            if isOpenHospital == true
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
                controller.dicHospital = arrHospitalList[0]
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "PatientDetailViewController") as! PatientDetailViewController
                controller.dicHospital = dicHospital
                self.navigationController?.pushViewController(controller, animated: true)
            }
            
        }
        if indexPath.row == 1{
            if isOpenHospital == true
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "AttendenceViewController") as! AttendenceViewController
                controller.dicHospital = arrHospitalList[0]
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "AttendenceViewController") as! AttendenceViewController
                controller.dicHospital = dicHospital
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        if indexPath.row == 2{
            if isOpenHospital == true
            {
                if dicMedicalDetail.id == ""
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MedicalEditVC") as! MedicalEditVC
                    vc.strAppointmentID = "\(appointment_id)"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "MedicalFormViewController") as! MedicalFormViewController
                    controller.strAppointmetnID = "\(appointment_id)"
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
            else
            {
                if dicMedicalDetail.id == ""
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MedicalEditVC") as! MedicalEditVC
                    vc.strAppointmentID = dicHospital.appointId ?? ""
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "MedicalFormViewController") as! MedicalFormViewController
                    controller.strAppointmetnID = dicHospital.appointId ?? ""
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
        if indexPath.row == 3{
            if isOpenHospital == true
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "TreatmentsViewController") as! TreatmentsViewController
                controller.strAppointmetnID = "\(appointment_id)"
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "TreatmentsViewController") as! TreatmentsViewController
                controller.strAppointmetnID = dicHospital.appointId ?? ""
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        if indexPath.row == 4{
            if isOpenHospital == true
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlanVC") as! PlanVC
                controller.strAppointmetnID = "\(appointment_id)"
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlanVC") as! PlanVC
                controller.strAppointmetnID = dicHospital.appointId ?? ""
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        if indexPath.row == 5{
            if isOpenHospital == true
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "FlightDetailViewController") as! FlightDetailViewController
                controller.strAppointmentID = "\(appointment_id)"
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "FlightDetailViewController") as! FlightDetailViewController
                controller.strAppointmentID = dicHospital.appointId ?? ""
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        if indexPath.row == 6{
            if isOpenHospital == true
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ToDoVC") as! ToDoVC
                controller.strAppointmetnID = "\(appointment_id)"
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ToDoVC") as! ToDoVC
                controller.strAppointmetnID = dicHospital.appointId ?? ""
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        if indexPath.row == 7{
            if isOpenHospital == true
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentDetailViewController") as! PaymentDetailViewController
                controller.strAppointmentID = "\(appointment_id)"
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentDetailViewController") as! PaymentDetailViewController
                controller.strAppointmentID = dicHospital.appointId ?? ""
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
}


class AppointmentCellTableViewCell: UITableViewCell{
    
    @IBOutlet weak var lblTitle: UILabel!
    
}
