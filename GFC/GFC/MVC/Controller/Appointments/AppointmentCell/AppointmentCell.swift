//
//  AppointmentCell.swift
//  GFC Globalhealth
//
//  Created by iMac on 12/09/24.
//

import UIKit
import Quickblox
class AppointmentCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgPhoto: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewMain.layer.applySketchShadow(color: .black, alpha: 0.1, x: 0, y: 0, blur: 17, spread: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
