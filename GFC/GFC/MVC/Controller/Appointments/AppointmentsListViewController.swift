
//
//  AppointmentsListViewController.swift
//  GFC
//
//  Created by iMac on 28/03/22.
//

import UIKit
import Quickblox
class AppointmentsListViewController: UIViewController {

    @IBOutlet weak var btnHospital: UIButton!
    @IBOutlet weak var btnDoctor: UIButton!
    @IBOutlet weak var btnLab: UIButton!
    
    @IBOutlet weak var viewHospital: UIView!
    @IBOutlet weak var viewDoctor: UIView!
    @IBOutlet weak var viewLab: UIView!
 
    @IBOutlet weak var viewLabMain: UIView!
    @IBOutlet weak var tblViewLab: UITableView!
    
    @IBOutlet weak var viewDoctorMain: UIView!
    @IBOutlet weak var tblViewDoctor: UITableView!
    
    @IBOutlet weak var viewHospitalMain: UIView!
    @IBOutlet weak var tblViewHospital: UITableView!
    
    @IBOutlet weak var lblTAppointment: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var arrHospitalList: [GFCAppointmentHospitalAppointment] = [GFCAppointmentHospitalAppointment]()
    
    var arrDoctorList: [GFCAppointmentDoctorsAppointment] = [GFCAppointmentDoctorsAppointment]()
    
    var arrLabList: [GFCAppointmentLabAppointment] = [GFCAppointmentLabAppointment]()
    
    
    var strType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        
        lblTAppointment.text = "My Appointment".localizeString(string: Language.shared.currentAppLang)
        
        btnHospital.setTitle("Hospital".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnDoctor.setTitle("Doctor".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnLab.setTitle("Lab".localizeString(string: Language.shared.currentAppLang), for: .normal)

        strType = "doctor"
        viewDoctor.backgroundColor = UIColor(hexString: "08364B")
        btnDoctor.setTitleColor(.white, for: .normal)
        viewDoctorMain.isHidden = false
        viewHospital.backgroundColor = .clear
        btnHospital.setTitleColor(.gray, for: .normal)
        viewHospitalMain.isHidden = true
        viewLab.backgroundColor = .clear
        btnLab.setTitleColor(.gray, for: .normal)
        viewLabMain.isHidden = true
        
        viewHospitalMain.backgroundColor = .clear
        viewDoctorMain.backgroundColor = .clear
        viewLabMain.backgroundColor = .clear
        
        tblViewHospital.delegate = self
        tblViewHospital.dataSource = self
        
        tblViewDoctor.delegate = self
        tblViewDoctor.dataSource = self
        
        tblViewLab.delegate = self
        tblViewLab.dataSource = self
        
        let nib = UINib(nibName: "AppointmentCell", bundle: nil)
        tblViewDoctor.register(nib, forCellReuseIdentifier: "AppointmentCell")
        tblViewLab.register(UINib(nibName: String(describing: AppointmentLabCell.self), bundle: nil  ), forCellReuseIdentifier: "AppointmentLabCell")
        tblViewHospital.register(UINib(nibName: String(describing: AppointmentLabCell.self), bundle: nil  ), forCellReuseIdentifier: "AppointmentLabCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callLabCateListAPI()
    }
    
    @IBAction func clickedHospital(_ sender: Any) {
        strType = "hospital"
        
        viewHospital.backgroundColor = UIColor(hexString: "08364B")
        btnHospital.setTitleColor(.white, for: .normal)
        viewHospitalMain.isHidden = false
        viewDoctor.backgroundColor = .clear
        btnDoctor.setTitleColor(.gray, for: .normal)
        viewDoctorMain.isHidden = true
        viewLab.backgroundColor = .clear
        btnLab.setTitleColor(.gray, for: .normal)
        viewLabMain.isHidden = true
        callLabCateListAPI()
        tblViewHospital.reloadData()
    }
    
    @IBAction func clickedDoctor(_ sender: Any) {
        strType = "doctor"
        
        viewDoctor.backgroundColor = UIColor(hexString: "08364B")
        btnDoctor.setTitleColor(.white, for: .normal)
        viewDoctorMain.isHidden = false
        viewHospital.backgroundColor = .clear
        btnHospital.setTitleColor(.gray, for: .normal)
        viewHospitalMain.isHidden = true
        viewLab.backgroundColor = .clear
        btnLab.setTitleColor(.gray, for: .normal)
        viewLabMain.isHidden = true
        callLabCateListAPI()
        tblViewDoctor.reloadData()
    }
    
    @IBAction func clickedLAb(_ sender: Any) {
        strType = "lab"
        viewLab.backgroundColor = UIColor(hexString: "08364B")
        btnLab.setTitleColor(.white, for: .normal)
        viewLabMain.isHidden = false
        viewDoctor.backgroundColor = .clear
        btnDoctor.setTitleColor(.gray, for: .normal)
        viewDoctorMain.isHidden = true
        viewHospital.backgroundColor = .clear
        btnHospital.setTitleColor(.gray, for: .normal)
        viewHospitalMain.isHidden = true
        callLabCateListAPI()
        tblViewLab.reloadData()
    }
   
    @IBAction func backBtnClicked(_ sender: Any) {
        appDelegate?.setUpSideMenu()
    }
    
    func callLabCateListAPI()
    {
         APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        let _url = "?patientId=\(appDelegate?.dicUserLoginData.id ?? "")"
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_APPOINTMENT + _url, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        self.arrHospitalList.removeAll()
                        self.arrDoctorList.removeAll()
                        self.arrLabList.removeAll()
                        if let arrHospital = dic?.value(forKey: "hospital_appointment") as? NSArray
                        {
                            for objHospital in arrHospital
                            {
                                let dicDataHospital = GFCAppointmentHospitalAppointment(fromDictionary: objHospital as! NSDictionary)
                                self.arrHospitalList.append(dicDataHospital)
                            }
                            self.arrHospitalList = self.arrHospitalList.reversed()
                            self.tblViewHospital.reloadData()
                        }
                        
                        if let arrDoctors = dic?.value(forKey: "doctors_appointment") as? NSArray
                        {
                            for objDoctor in arrDoctors
                            {
                                let dicDataDoctor = GFCAppointmentDoctorsAppointment(fromDictionary: objDoctor as! NSDictionary)
                                self.arrDoctorList.append(dicDataDoctor)
                            }
                          //  self.arrDoctorList = self.arrDoctorList.reversed()
                            self.tblViewDoctor.reloadData()
                        }
                        
                        if let arrLab = dic?.value(forKey: "lab_appointment") as? NSArray
                        {
                            for objLab in arrLab
                            {
                                let dicDataLab = GFCAppointmentLabAppointment(fromDictionary: objLab as! NSDictionary)
                                self.arrLabList.append(dicDataLab)
                            }
                            self.arrLabList = self.arrLabList.reversed()
                            self.tblViewLab.reloadData()
                        }
                    }
                }
            }
        }
    }
    
}

extension AppointmentsListViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblViewHospital
        {
            return arrHospitalList.count
        }
        else if tableView == tblViewDoctor
        {
            return arrDoctorList.count
        }
        else
        {
            return arrLabList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblViewHospital
        {
            let cell = self.tblViewHospital.dequeueReusableCell(withIdentifier: "AppointmentLabCell") as! AppointmentLabCell
            
            let dicData = arrHospitalList[indexPath.row]
            
            cell.lblAddress.text = dicData.address ?? ""
            
            var media_link_url = "\(dicData.profileImage ?? "")"
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
            
            cell.lblId.text = "\("Booking Id".localizeString(string: Language.shared.currentAppLang)) : \(dicData.appointId ?? "")"
            
            cell.lblName.text = dicData.hospital_name ?? ""
            
            let bidAccepted = dicData.createdAt ?? ""
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US")
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date1 = formatter.date(from: bidAccepted)
            let Dform = DateFormatter()
            Dform.dateFormat = "MMM dd, yyyy"
            let strDate = Dform.string(from: date1!)
            cell.lblDate.text = strDate
            
            return cell
        }
        else if tableView == tblViewDoctor
        {
         let cell = self.tblViewDoctor.dequeueReusableCell(withIdentifier: "AppointmentCell") as! AppointmentCell
            
            let dicData = arrDoctorList[indexPath.row]

//            cell.lblBooking.text = "\("Booking Id".localizeString(string: Language.shared.currentAppLang)) : \(dicData.appointmentId ?? "")"
            cell.lblName.text = dicData.doctorName ?? ""
            cell.lblDate.text = "Date: \(dicData.date ?? "")"
            cell.lblTime.text = "Time: \(dicData.time ?? "")"
            
            var media_link_url = "\(dicData.image_url ?? "")\(dicData.image ?? "")"
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPhoto.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)


            if dicData.appointment_type == "" {
                cell.lblType.text = "Type: null"
            } else {
                cell.lblType.text = "Type: \(dicData.appointment_type ?? "")"
            }
            
//            cell.lblTPatientId.text = "Patient UHID".localizeString(string: Language.shared.currentAppLang)
//            cell.lblTDoctorName.text = "Doctor Name".localizeString(string: Language.shared.currentAppLang)
//            cell.lblTAddress.text = "Address".localizeString(string: Language.shared.currentAppLang)
//
//            if dicData.rating == "0"
//            {
//                cell.lblRate.isUserInteractionEnabled = true
//                cell.lblRate.setTitle("Rate Us".localizeString(string: Language.shared.currentAppLang), for: .normal)
//            }
//            else
//            {
//                let rating = Double(dicData.rating ?? "")?.round(to: 1)
//
//                cell.lblRate.isUserInteractionEnabled = false
//                cell.lblRate.setTitle("\(rating ?? 0.0)", for: .normal)
//            }
//
//            if dicData.doctorAddress == ""
//            {
//                cell.lblAddress.text = "N/A"
//            }
//            else
//            {
//                cell.lblAddress.text = dicData.doctorAddress ?? ""
//            }
//
//            if let bidAccepted = dicData.date
//            {
//                if bidAccepted != ""
//                {
//                    let formatter = DateFormatter()
//                    formatter.locale = Locale(identifier: "en_US")
//                    formatter.dateFormat = "yyyy-MM-dd"
//                    if let date1 = formatter.date(from: bidAccepted)
//                    {
//                        let Dform = DateFormatter()
//                        Dform.dateFormat = "MMM dd, yyyy"
//                        let strDate = Dform.string(from: date1)
//                        cell.lblDate.text = strDate
//                    }
//
//                }
//
//            }
//
//            cell.lblRate.tag = indexPath.row
//            cell.lblRate.addTarget(self, action: #selector(clickedRateDoctor(_:)), for: .touchUpInside)
            
            
//
           return cell
      }
        else
        {
            let cell = self.tblViewLab.dequeueReusableCell(withIdentifier: "AppointmentLabCell") as! AppointmentLabCell
            
            let dicData = arrLabList[indexPath.row]

            cell.lblId.text = "\("\("Booking Id".localizeString(string: Language.shared.currentAppLang)) : \(dicData.id ?? "")")\n\n\(dicData.testname ?? "")"
            
            cell.lblName.text = dicData.labname ?? ""
            
            cell.lblAddress.text = dicData.address ?? ""
                        
            if let bidAccepted = dicData.bookingDate
            {
                if bidAccepted != ""
                {
                    if bidAccepted != "0000-00-00"
                    {
                        let formatter = DateFormatter()
                        formatter.locale = Locale(identifier: "en_US")
                        formatter.dateFormat = "yyyy-MM-dd"
                        let date1 = formatter.date(from: bidAccepted)
                        let Dform = DateFormatter()
                        Dform.dateFormat = "MMM dd, yyyy"
                        let strDate = Dform.string(from: date1!)
                        cell.lblDate.text = strDate
                    }
                    else
                    {
                        cell.lblDate.text = ""
                    }
                }
                else
                {
                    cell.lblDate.text = ""
                }
            }
           
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblViewLab
        {
            return UITableView.automaticDimension
        }
        else
        {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblViewHospital
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AppoitmentDetailViewController") as! AppoitmentDetailViewController
            vc.dicHospital = arrHospitalList[indexPath.row]
            vc.isOpenHospital = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if tableView == tblViewDoctor
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingAppDetailVC") as! BookingAppDetailVC
            vc.modalPresentationStyle = .overFullScreen
            vc.dicDocotrDetail = arrDoctorList[indexPath.row]
            
//            self.present(vc, animated: false, completion: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LabPopUpVC") as! LabPopUpVC
            vc.modalPresentationStyle = .overFullScreen
            vc.dicLabDetail = arrLabList[indexPath.row]
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    @objc func clickedRateHospital(_ sender: UIButton)
    {
        let dicData = arrHospitalList[sender.tag]
        
        let arrInfo = NSMutableArray()
        
        let dicInfo = NSMutableDictionary()
        dicInfo.setValue(appDelegate?.dicUserLoginData.name ?? "", forKey: "name")
        dicInfo.setValue(appDelegate?.dicUserLoginData.email ?? "", forKey: "email")
        dicInfo.setValue(self.strType, forKey: "appointment_type")
        dicInfo.setValue(appDelegate?.dicUserLoginData.id ?? "", forKey: "userid")
        dicInfo.setValue(dicData.appointId ?? "", forKey: "appointment_id")
        dicInfo.setValue(dicData.hospitalId ?? "", forKey: "id")
        
        arrInfo.add(dicInfo)
        
        let mainS = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainS.instantiateViewController(withIdentifier: "NewRatingVC") as! NewRatingVC
        vc.arrAllInfo = arrInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func clickedRateDoctor(_ sender: UIButton)
    {
        let dicData = arrDoctorList[sender.tag]
        
        let arrInfo = NSMutableArray()
        
        let dicInfo = NSMutableDictionary()
        dicInfo.setValue(appDelegate?.dicUserLoginData.name ?? "", forKey: "name")
        dicInfo.setValue(appDelegate?.dicUserLoginData.email ?? "", forKey: "email")
        dicInfo.setValue(self.strType, forKey: "appointment_type")
        dicInfo.setValue(appDelegate?.dicUserLoginData.id ?? "", forKey: "userid")
        dicInfo.setValue(dicData.id ?? "", forKey: "appointment_id")
        dicInfo.setValue(dicData.doctorId ?? "", forKey: "id")
        
        arrInfo.add(dicInfo)
        
        let mainS = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainS.instantiateViewController(withIdentifier: "NewRatingVC") as! NewRatingVC
        vc.arrAllInfo = arrInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func clickedRateLab(_ sender: UIButton)
    {
        let dicData = arrLabList[sender.tag]
        
        let arrInfo = NSMutableArray()
        
        let dicInfo = NSMutableDictionary()
        dicInfo.setValue(appDelegate?.dicUserLoginData.name ?? "", forKey: "name")
        dicInfo.setValue(appDelegate?.dicUserLoginData.email ?? "", forKey: "email")
        dicInfo.setValue(self.strType, forKey: "appointment_type")
        dicInfo.setValue(appDelegate?.dicUserLoginData.id ?? "", forKey: "userid")
        dicInfo.setValue(dicData.id ?? "", forKey: "appointment_id")
        dicInfo.setValue(dicData.testId ?? "", forKey: "id")
        
        arrInfo.add(dicInfo)
        
        let mainS = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainS.instantiateViewController(withIdentifier: "NewRatingVC") as! NewRatingVC
        vc.arrAllInfo = arrInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

class HospitalListCell: UITableViewCell
{
    @IBOutlet weak var lblBokkingID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPName: UILabel!
    @IBOutlet weak var lblPEMail: UILabel!
    @IBOutlet weak var lblPPhone: UILabel!
    
    @IBOutlet weak var lblPatientName: UILabel!
    @IBOutlet weak var lblTPatientEmail: UILabel!
    @IBOutlet weak var lblTPatientPhoen: UILabel!
    
    @IBOutlet weak var btnRate: UIButton!
    
    @IBOutlet weak var lblTHospitalNam: UILabel!
    @IBOutlet weak var lblHospitalName: UILabel!
    
    
}

class DoctorListCell: UITableViewCell
{
    @IBOutlet weak var lblBooking: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPUID: UILabel!
    @IBOutlet weak var lblDName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblTPatientId: UILabel!
    @IBOutlet weak var lblTDoctorName: UILabel!
    @IBOutlet weak var lblTAddress: UILabel!
    
    @IBOutlet weak var lblRate: UIButton!
    @IBOutlet weak var lblApp: UILabel!
    
}

class LabListCell: UITableViewCell
{
    @IBOutlet weak var lblBookingID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLName: UILabel!
    @IBOutlet weak var lblTSlot: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblTLabName: UILabel!
    @IBOutlet weak var lblTTimeSlot: UILabel!
    @IBOutlet weak var lblTAddress: UILabel!
    
    @IBOutlet weak var btnRate: UIButton!
    
}

