//
//  DoctorPopUpVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 02/03/23.
//

import UIKit
import NYTPhotoViewer
import PDFKit

class DoctorPopUpVC: UIViewController {

    @IBOutlet weak var lblNoData1: UILabel!
    @IBOutlet weak var lblNoData2: UILabel!
    
    @IBOutlet weak var btnPdf2: UIButton!
    
    @IBOutlet weak var pdf2: UIImageView!
    
    @IBOutlet weak var lblBD: UILabel!
    @IBOutlet weak var lblBT: UILabel!
    @IBOutlet weak var lblCF: UILabel!
    @IBOutlet weak var lblPID: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblTBookingDate: UILabel!
    @IBOutlet weak var lblTBookingTime: UILabel!
    @IBOutlet weak var lblTConsultationFe: UILabel!
    @IBOutlet weak var lblTPatientId: UILabel!
    
    @IBOutlet weak var btnTClose: UIButton!
    
    @IBOutlet weak var collcetionVIewImg: UICollectionView!
       
    @IBOutlet weak var lblP1: UILabel!
    @IBOutlet weak var lblP2: UILabel!
    
    @IBOutlet weak var imgPic: UIImageView!
    
    var dicDocotrDetail = GFCAppointmentDoctorsAppointment()
    
    let SocityNoticesectionInsetsCat = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    let SocityNoticeitemsPerRowCat: CGFloat = 1
    
    var SocityNoticeflowLayout: UICollectionViewFlowLayout {
        let _SocityNoticeflowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.SocityNoticesectionInsetsCat.left * (self.SocityNoticeitemsPerRowCat + 1)
            let availableWidth = self.collcetionVIewImg.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.SocityNoticeitemsPerRowCat
            
            _SocityNoticeflowLayout.itemSize = CGSize(width: 50, height: 50)
            
            _SocityNoticeflowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            _SocityNoticeflowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _SocityNoticeflowLayout.minimumInteritemSpacing = 10
            _SocityNoticeflowLayout.minimumLineSpacing = 10
        }
        
        // edit properties here
        return _SocityNoticeflowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collcetionVIewImg.delegate = self
        collcetionVIewImg.dataSource = self
        collcetionVIewImg.collectionViewLayout = SocityNoticeflowLayout
        
        lblTBookingTime.text = "Booking Time".localizeString(string: Language.shared.currentAppLang)
        lblTBookingDate.text = "Booking Date".localizeString(string: Language.shared.currentAppLang)
        lblTPatientId.text = "Patient UHID".localizeString(string: Language.shared.currentAppLang)
        lblTConsultationFe.text = "Consultation Fee".localizeString(string: Language.shared.currentAppLang)
        lblP1.text = "patient medical reports".localizeString(string: Language.shared.currentAppLang)
        lblP2.text = "doctor prescription.".localizeString(string: Language.shared.currentAppLang)
        lblNoData1.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        lblNoData1.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        
        btnTClose.setTitle("Close".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        var media_link_url = "\(dicDocotrDetail.image_url ?? "")\(self.dicDocotrDetail.image ?? "")"
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        self.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        if self.dicDocotrDetail.prescriptionDocument1.count > 0
        {
            self.lblNoData1.isHidden = true
            self.collcetionVIewImg.isHidden = false
        }
        else
        {
            self.lblNoData1.isHidden = false
            self.collcetionVIewImg.isHidden = true
        }
        
        if self.dicDocotrDetail.prescriptionDocument2 != ""
        {
            self.lblNoData2.isHidden = true
            self.btnPdf2.isHidden = false
            self.pdf2.isHidden = false
        }
        else
        {
            self.lblNoData2.isHidden = false
            self.btnPdf2.isHidden = true
            self.pdf2.isHidden = true
        }
        
        self.lblName.text = dicDocotrDetail.doctorName ?? ""
        self.lblBT.text = dicDocotrDetail.time ?? ""
        self.lblCF.text = "₹ \(dicDocotrDetail.consultantFee ?? "")"
        self.lblPID.text = dicDocotrDetail.uhid ?? ""
        self.lblAddress.text = dicDocotrDetail.doctorAddress ?? ""
        
        if let bidAccepted = dicDocotrDetail.date
        {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US")
            formatter.dateFormat = "yyyy-MM-dd"
            if let date1 = formatter.date(from: bidAccepted)
            {
                let Dform = DateFormatter()
                Dform.dateFormat = "MMM dd, yyyy"
                let strDate = Dform.string(from: date1)
                self.lblBD.text = strDate
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        
    }
    
    
    @IBAction func clickedOpenPdf2(_ sender: Any) {
        
        if let pdfURL = URL(string: "\(dicDocotrDetail.prescriptionDocument2 ?? "")") {
            openPDFInSafari(pdfURL: pdfURL)
        }
        
    }
    
}

extension DoctorPopUpVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if dicDocotrDetail.prescriptionDocument1.count > 0
        {
            return dicDocotrDetail.prescriptionDocument1.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collcetionVIewImg.dequeueReusableCell(withReuseIdentifier: "LabReportShowImgCell", for: indexPath) as! LabReportShowImgCell
        
        let dicData = dicDocotrDetail.prescriptionDocument1[indexPath.row].prescriptionFiles ?? ""
        if dicData.contains(".pdf") == true
        {
            cell.imgPic.image = UIImage(named: "pdf")
        }
        else
        {
            var media_link_url = "\(dicDocotrDetail.prescriptionDocument1Url ?? "")\(dicData ?? "")"
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "user"), options: [], completed: nil)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collcetionVIewImg.cellForItem(at: indexPath) as! LabReportShowImgCell
        
        let dicData = dicDocotrDetail.prescriptionDocument1[indexPath.row].prescriptionFiles ?? ""

        if dicData.contains(".pdf") == true
        {
            if let pdfURL = URL(string: "\(dicDocotrDetail.prescriptionDocument1Url ?? "")\(dicData ?? "")") {
                openPDFInSafari(pdfURL: pdfURL)
            }
        }
        else
        {
            if let img = cell.imgPic.image{
                let photo = ChatImage()
                photo.image = img
                
                let photosViewController: NYTPhotosViewController = NYTPhotosViewController(photos: [photo])
                present(photosViewController, animated: true, completion: nil)
            }
        }
       
    }
    
    func openPDFInSafari(pdfURL: URL) {
           if UIApplication.shared.canOpenURL(pdfURL) {
               UIApplication.shared.open(pdfURL, options: [:], completionHandler: nil)
           } else {
               // Handle the case where the URL can't be opened, e.g., display an alert.
               print("Unable to open PDF in Safari.")
               self.view.makeToast("Unable to open PDF")
           }
       }
    
}

class ChatImage: NSObject, NYTPhoto {
    
    var image: UIImage?
    var imageData: Data?
    var placeholderImage: UIImage?
    var attributedCaptionTitle: NSAttributedString?
    var attributedCaptionSummary: NSAttributedString?
    var attributedCaptionCredit: NSAttributedString?
    
    init(image: UIImage? = nil, imageData: NSData? = nil) {
        super.init()
        
        self.image = image
        self.imageData = imageData as Data?
    }
    
}
