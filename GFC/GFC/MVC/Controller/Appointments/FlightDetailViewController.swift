//
//  FlightDetailViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 30/03/22.
//

import UIKit
import Quickblox
class FlightDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var imgNull: UIImageView!
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewNoData: UIView!
    
    @IBOutlet weak var lblDes: UILabel!
    
    @IBOutlet weak var lblTFlightDetail: UILabel!
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    //dicFlightDetail
    var dicFlightDetail = GFCFlightDetailData()
    
    var arrImg =  NSMutableArray()
    
    var strAppointmentID = ""
    
    var selectedImage = 0
    
    var strImgUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTFlightDetail.text = "Flight Detail".localizeString(string: Language.shared.currentAppLang)
        lblTNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        
        viewNoData.isHidden = true
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        callFlightDetailAPI()
    }
    
    @IBAction func clickedShowimgs(_ sender: Any) {
        if strImgUrl.contains(".pdf") == true
        {
            if let pdfURL = URL(string: strImgUrl) {
                openPDFInSafari(pdfURL: pdfURL)
            }
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
            vc.modalPresentationStyle = .overFullScreen
            vc.strImg = strImgUrl
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "FlightDetailCollCell", for: indexPath) as! FlightDetailCollCell
        
        let dicData = arrImg[indexPath.row] as! String
        
        if dicData.contains(".pdf")
        {
            cell.imgPic.image = UIImage(named: "pdf")
        }
        else
        {
            var media_link_url = dicData
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        }
        
        cell.lblName.text = ""
        
        if selectedImage == 0
        {
            cell.imgPic.layer.borderWidth = 1.5
        }
        else
        {
            cell.imgPic.layer.borderWidth = 0
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedImage = indexPath.row
        let dicData = arrImg[indexPath.row] as! String
        
        if dicData.contains(".pdf") == true
        {
            if let pdfURL = URL(string: dicData) {
                openPDFInSafari(pdfURL: pdfURL)
            }
        }
        else
        {
            if dicData.contains(".pdf") == true
            {
                self.imgPic.image = UIImage(named: "pdf")
            }
            else
            {
                var media_link_url = dicData
                media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
                self.strImgUrl = media_link_url
                self.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
            }
        }
    }
    
    func openPDFInSafari(pdfURL: URL) {
        if UIApplication.shared.canOpenURL(pdfURL) {
            UIApplication.shared.open(pdfURL, options: [:], completionHandler: nil)
        } else {
            // Handle the case where the URL can't be opened, e.g., display an alert.
            print("Unable to open PDF in Safari.")
        }
    }
    
    // MARK: - call API
    
    func callFlightDetailAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["appointment_id":self.strAppointmentID]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(FLIGHT_DETEIL, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        
                        let arrImage = dic?.value(forKey: "images") as? NSArray
                        
                        self.arrImg = (arrImage?.mutableCopy() as? NSMutableArray)!
                        
                        self.collectionView.reloadData()
                        
                        let dicData = GFCFlightDetailData(fromDictionary: dic!)
                        self.dicFlightDetail = dicData
                        
                        self.lblDes.text = dicData.descriptionField ?? ""
                        
                        if (self.arrImg.count ?? 0)  > 0
                        {
                            
                            self.imgNull.isHidden = true
                            self.viewNoData.isHidden = true
                            
                            let dicImg = self.arrImg[0] as! String
                            
                            if dicImg.contains(".pdf") == true
                            {
                                self.imgPic.contentMode = .scaleAspectFit
                                self.imgPic.image = UIImage(named: "pdf")
                            }
                            else
                            {
                                self.imgPic.contentMode = .scaleAspectFill
                                var media_link_url = self.arrImg[0] as! String
                                media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
                                self.strImgUrl = media_link_url
                                self.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
                            }
                            
                        }
                        else
                        {
                            self.imgNull.isHidden = false
                            self.viewNoData.isHidden = false
                            self.imgPic.isHidden = true
                            self.lblDes.text = ""
                        }
                    }
                    else
                    {
                        self.viewNoData.isHidden = false
                        self.imgPic.isHidden = true
                        self.lblDes.text = ""
                    }
                        
                }
                else
                {
                    self.viewNoData.isHidden = false
                    self.imgPic.isHidden = true
                    self.lblDes.text = ""
                }
            }
        }
    }
}


class FlightDetailCollCell: UICollectionViewCell{
    
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}

