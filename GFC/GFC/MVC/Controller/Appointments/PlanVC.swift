//
//  PlanVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 06/03/23.
//

import UIKit
import Quickblox
class PlanVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var viewNoDate: UIView!
    @IBOutlet weak var collectionImage: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var lblTPlan: UILabel!
    @IBOutlet weak var lblTImage: UILabel!
    
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    let SocityNoticesectionInsetsCat = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
    let SocityNoticeitemsPerRowCat: CGFloat = 1
    
    var SocityNoticeflowLayout: UICollectionViewFlowLayout {
        let _SocityNoticeflowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.SocityNoticesectionInsetsCat.left * (self.SocityNoticeitemsPerRowCat + 1)
            let availableWidth = self.collectionImage.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.SocityNoticeitemsPerRowCat
            
            _SocityNoticeflowLayout.itemSize = CGSize(width: 100, height: 100)
            
            _SocityNoticeflowLayout.sectionInset = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
            _SocityNoticeflowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _SocityNoticeflowLayout.minimumInteritemSpacing = 10
            _SocityNoticeflowLayout.minimumLineSpacing = 10
        }
        
        // edit properties here
        return _SocityNoticeflowLayout
    }
    
    var dicPlanDetail = GFCPlanDetailData()
    
    var arrImg = NSMutableArray()
    
    var strAppointmetnID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        lblTPlan.text = "Plan".localizeString(string: Language.shared.currentAppLang)
        lblTImage.text = "Images".localizeString(string: Language.shared.currentAppLang)
        
        viewNoDate.isHidden = true
        collectionImage.delegate = self
        collectionImage.dataSource = self
        collectionImage.collectionViewLayout = SocityNoticeflowLayout
        
        callPlanAPI()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Action
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - call API
    
    func callPlanAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["appointment_id":self.strAppointmetnID]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(PLAN_DETEIL, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        
                        let arrImage = dic?.value(forKey: "images") as? NSArray
                        
                        self.arrImg = (arrImage?.mutableCopy() as? NSMutableArray)!
                        
                        if (arrImage?.count ?? 0) > 0
                        {
                            self.scrollView.isHidden = false
                            self.viewNoDate.isHidden = true
                        }
                        else
                        {
                            self.scrollView.isHidden = true
                            self.viewNoDate.isHidden = false
                        }
                        
                        let dicData = GFCPlanDetailData(fromDictionary: dic!)
                        self.dicPlanDetail = dicData
                     
                        self.tblView.reloadData()
                        self.collectionImage.reloadData()
                    }
                }
            }
        }
    }
    
    // MARK: - collectioView Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionImage.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
        
        let dicData = arrImg[indexPath.row] as! String
        
        if dicData.contains(".pdf") == true
        {
            cell.imgPic.contentMode = .scaleAspectFit
            cell.imgPic.image = UIImage(named: "pdf")
        }
        else
        {
            cell.imgPic.contentMode = .scaleAspectFill
            var media_link_url = arrImg[indexPath.row] as! String
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dicData = arrImg[indexPath.row] as! String

        if dicData.contains(".pdf") == true
        {
            if let pdfURL = URL(string: dicData) {
                openPDFInSafari(pdfURL: pdfURL)
            }
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
            vc.modalPresentationStyle = .overFullScreen
            vc.strImg = arrImg[indexPath.row] as! String
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    func openPDFInSafari(pdfURL: URL) {
        if UIApplication.shared.canOpenURL(pdfURL) {
            UIApplication.shared.open(pdfURL, options: [:], completionHandler: nil)
        } else {
            // Handle the case where the URL can't be opened, e.g., display an alert.
            print("Unable to open PDF in Safari.")
        }
    }
  
}

extension PlanVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "PlanDetailCell") as! PlanDetailCell
        
        cell.lblHospitalName.text = self.dicPlanDetail.hospitalName ?? ""
        cell.lblTreatmentName.text = self.dicPlanDetail.treatmentName ?? ""
        cell.lblDoctorNAme.text = self.dicPlanDetail.doctorName ?? ""
        cell.lblDaysToStay.text = self.dicPlanDetail.dayToStay ?? ""
        cell.lblDaysINHospital.text = self.dicPlanDetail.daysInHospital ?? ""
        cell.lblCostOfTreatment.text = "₹ \(self.dicPlanDetail.costOfTreatment ?? "")"
        cell.lblCostOfStaying.text = "₹ \(self.dicPlanDetail.costOfStaying ?? "")"
        cell.lblOtherNotes.text = self.dicPlanDetail.otherNote ?? ""
        
        cell.lblTHospitalName.text = "Hospital Name".localizeString(string: Language.shared.currentAppLang)
        cell.lblTTeatmentName.text = "Treatment Name".localizeString(string: Language.shared.currentAppLang)
        cell.lblTDoctorName.text = "Doctor Name".localizeString(string: Language.shared.currentAppLang)
        cell.lblTDayToStay.text = "Days to Stay".localizeString(string: Language.shared.currentAppLang)
        cell.lblTDaysHospital.text = "Days In Hospital".localizeString(string: Language.shared.currentAppLang)
        cell.lblTCostTreatment.text = "Cost Of Treatment".localizeString(string: Language.shared.currentAppLang)
        cell.lblTCostStaying.text = "Cost Of Staying".localizeString(string: Language.shared.currentAppLang)
        cell.lblTOtherName.text = "Other Notes".localizeString(string: Language.shared.currentAppLang)


        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

class ImageCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var imgPic: UIImageView!
}

class PlanDetailCell: UITableViewCell
{
    @IBOutlet weak var lblHospitalName: UILabel!
    @IBOutlet weak var lblTreatmentName: UILabel!
    @IBOutlet weak var lblDoctorNAme: UILabel!
    @IBOutlet weak var lblDaysToStay: UILabel!
    @IBOutlet weak var lblDaysINHospital: UILabel!
    @IBOutlet weak var lblCostOfTreatment: UILabel!
    @IBOutlet weak var lblCostOfStaying: UILabel!
    @IBOutlet weak var lblOtherNotes: UILabel!

    @IBOutlet weak var lblTHospitalName: UILabel!
    @IBOutlet weak var lblTTeatmentName: UILabel!
    @IBOutlet weak var lblTDoctorName: UILabel!
    @IBOutlet weak var lblTDayToStay: UILabel!
    @IBOutlet weak var lblTDaysHospital: UILabel!
    @IBOutlet weak var lblTCostTreatment: UILabel!
    @IBOutlet weak var lblTCostStaying: UILabel!
    @IBOutlet weak var lblTOtherName: UILabel!
    
    
}
