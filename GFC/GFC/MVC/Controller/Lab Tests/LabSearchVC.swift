//
//  LabSearchVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 02/03/23.
//

import UIKit
import DropDown

class LabSearchVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var ViewNoData: UIView!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var txtCountry: UITextField!
    
    @IBOutlet weak var lblTSearchLabTest: UILabel!
    @IBOutlet weak var lblTLocation: UILabel!
    
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    var dropDown = DropDown()
    
    var arrCountryList: [GFCCountryListData] = [GFCCountryListData]()
    
    var arrLabCateList: [GFCLabCateListList] = [GFCLabCateListList]()
    
    var strCountryID = ""
    
    var strImgurl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTSearchLabTest.text = "Search Lab Test".localizeString(string: Language.shared.currentAppLang)
        txtSearch.placeholder = "Search.....".localizeString(string: Language.shared.currentAppLang)
        lblNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            txtSearch.textAlignment = .left
        }else{
            txtSearch.textAlignment = .right
        }
        
        tblView.delegate = self
        tblView.dataSource = self
        
        callCountryListAPI()
        
        self.callLabCateListAPI(country: appDelegate?.currentCountry.countryId ?? "", name: self.txtSearch.text ?? "")
        
        self.txtSearch.delegate = self
        self.txtSearch.addTarget(self, action: #selector(SearchLabTest(_ :)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickedCountry(_ sender: Any) {
        dropDown.show()
    }
    
    @objc func SearchLabTest(_ textfield:UITextField)
    {
        if textfield.text?.count == 0
        {
        //    callLabCateListAPI(country: self.strCountryID, name: textfield.text ?? "")
            
            self.callLabCateListAPI(country: appDelegate?.currentCountry.countryId ?? "", name: textfield.text ?? "")

            self.arrLabCateList.removeAll()
            tblView.reloadData()
        }
        else
        {
            //callLabCateListAPI(country: self.strCountryID, name: textfield.text ?? "")
            
            self.callLabCateListAPI(country: appDelegate?.currentCountry.countryId ?? "", name: textfield.text ?? "")
        }
       
    }
    
    func setDropDownCountry()
    {
        let Country = NSMutableArray()
        
        for obj in arrCountryList
        {
            Country.add(obj.countryName ?? "")
        }
        
        dropDown.dataSource = Country as! [String]
        dropDown.anchorView = btnDrop
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            
            self.txtCountry.text = item
            
            for obj in arrCountryList
            {
                if obj.countryName == item
                {
                    self.strCountryID = obj.countryId ?? ""
                    appDelegate?.currentCountry = obj
                }
                callLabCateListAPI(country: strCountryID, name: self.txtSearch.text ?? "")
                
//                self.callLabCateListAPI(country: appDelegate?.currentCountry.countryId ?? "", name: self.txtSearch.text ?? "")
            }
            
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: btnDrop.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: -btnDrop.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.selectedTextColor = .black
        dropDown.reloadAllComponents()
    }
 
    func callCountryListAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCountryList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if dicData.status == "1"
                            {
                                self.arrCountryList.append(dicData)
                            }
                        }
                        
                        if self.arrCountryList.count > 0
                        {
                            if appDelegate?.currentCountry.countryName == nil
                            {
                                self.strCountryID = self.arrCountryList[0].countryId ?? ""
                                self.txtCountry.text = self.arrCountryList[0].countryName ?? ""
                                appDelegate?.currentCountry = self.arrCountryList[0]
                            }
                            else
                            {
                                self.txtCountry.text = appDelegate?.currentCountry.countryName ?? ""
                                self.strCountryID = appDelegate?.currentCountry.countryId ?? ""
                             }
                        }
                        
                        self.setDropDownCountry()
                        
//                        self.callLabCateListAPI(country: appDelegate?.currentCountry.countryId ?? "", name: self.txtSearch.text ?? "")

                        self.callLabCateListAPI(country: self.strCountryID, name: self.txtSearch.text ?? "")
                    }
                }
            }
        }
    }
    
    func callLabCateListAPI(country:String, name:String)
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["token":"123456","categoryId":"","country":country,"name":name]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_CATE_LIST, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        self.arrLabCateList.removeAll()
                        
                        let img = dic?.value(forKey: "imageUrl") as? String
                        
                        self.strImgurl = img ?? ""
                        
                        if let arrList = dic?.value(forKey: "lists") as? NSArray
                        {
                            for obj in arrList
                            {
                                let dicData = GFCLabCateListList(fromDictionary: obj as! NSDictionary)
                                self.arrLabCateList.append(dicData)
                            }
                        }
                        if self.arrLabCateList.count > 0
                        {
                            self.tblView.isHidden = false
                            self.ViewNoData.isHidden = true

                        }
                        else
                        {
                            self.tblView.isHidden = true
                            self.ViewNoData.isHidden = false
                        }
                        self.tblView.reloadData()
                    }
                    else
                    {
                        self.tblView.isHidden = false
                    }
                }
            }
        }
    }
    
}

extension LabSearchVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLabCateList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "SearchLabCell")  as! SearchLabCell
        
        let dicData = arrLabCateList[indexPath.row]
        
        cell.lblName.text = dicData.name ?? ""
        cell.lblPrice.text = "\(appDelegate?.currentCountry.currency ?? "") \(dicData.discountedPrice ?? "")"
        cell.lblLabName.text = "\("Lab Name".localizeString(string: Language.shared.currentAppLang)):\(dicData.labName ?? "")"
        

        
        let attributedText = NSAttributedString(
            string: "\(appDelegate?.currentCountry.currency ?? "") \(dicData.price ?? "")",
            attributes: [.strikethroughStyle: NSUnderlineStyle.single.rawValue]
        )
        cell.lblLoPrice.attributedText = attributedText
        
        let rating = Double(dicData.rating ?? "")?.round(to: 1)
        cell.lblRating.text = "\(rating ?? 0.0)"
        
        var media_link_url = self.strImgurl + dicData.image ?? ""
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "TestDetailViewController") as! TestDetailViewController
        controller.dicTestData = arrLabCateList[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
}

class SearchLabCell: UITableViewCell
{
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLabName: UILabel!
    @IBOutlet weak var lblTreatment: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblLoPrice: UILabel!
    
    @IBOutlet weak var lblRating: UILabel!
}
