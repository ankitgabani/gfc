//
//  TestDetailViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 16/04/22.
//

import UIKit
import DropDown

class TestDetailViewController: UIViewController {
    
    @IBOutlet weak var lblCheckupPlan: UILabel!
    @IBOutlet weak var lblIncludedTests: UILabel!
    
    @IBOutlet weak var lblNewTitle: UILabel!
    @IBOutlet weak var lblLabName: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblNewPrice: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPre: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var lblNOA: UILabel!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewBGMain: UIView!
    
    @IBOutlet weak var viewSlots: NSLayoutConstraint!
    
    @IBOutlet weak var CollectionViewDate: UICollectionView!
    @IBOutlet weak var collectionViewTime: UICollectionView!
    
    @IBOutlet weak var txtPerson: UITextField!
    @IBOutlet weak var viewPerson: UIView!
    
    @IBOutlet weak var lblTSuccessful: UILabel!
    @IBOutlet weak var lblTTestBokk: UILabel!
    @IBOutlet weak var lblTTestDetail: UILabel!
    @IBOutlet weak var lblTAddress: UILabel!
    @IBOutlet weak var lblTBestSuitablDate: UILabel!
    @IBOutlet weak var lblTBestSuitableTimeSlot: UILabel!
    @IBOutlet weak var lblTPrecations: UILabel!
    @IBOutlet weak var lblTDescription: UILabel!
    
    @IBOutlet weak var btnTOk: UIButton!
    @IBOutlet weak var btnBookTest: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    var arrPerson = ["1 Person","2 Person","3 Person","4 Person"]
    var arrFixTimeSlotPerson = ["09:00 AM - 12:00 PM","12:00 PM - 03:00 PM","03:00 PM - 06:00 PM","06:00 PM - 09:00 PM"]

    var dropDown = DropDown()
    
    var dicTestData = GFCLabCateListList()
    
    var strSelectedDay = 0
    
    var strSelectDate = ""
    
    var strTestId = ""
    var strDoctor_Name = ""
    
    var strSelectedTimeSlot = -1

    var strStartSelectTime = ""
    
    var strEndSelectTime = ""
    
    var arrTimeSlots: [GFCTimeSlotData] = [GFCTimeSlotData]()
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    let sitemsPerRow : CGFloat = 2
    
    let sectionInsetsCat = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    let itemsPerRowCat: CGFloat = 2
    
    var flowLayoutLiveSlot: UICollectionViewFlowLayout {
        let _SlotflowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.sectionInsetsCat.left * (self.itemsPerRowCat + 1)
            let availableWidth = self.collectionViewTime.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.itemsPerRowCat
            
            _SlotflowLayout.itemSize = CGSize(width: widthPerItem, height: 40)
            
            _SlotflowLayout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
            _SlotflowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
            _SlotflowLayout.minimumInteritemSpacing = 8
            _SlotflowLayout.minimumLineSpacing = 8
        }
        
        // edit properties here
        return _SlotflowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTTestDetail.text = "Test Detail".localizeString(string: Language.shared.currentAppLang)
        lblIncludedTests.text = "Tests Includes".localizeString(string: Language.shared.currentAppLang)
        lblTAddress.text = "Address".localizeString(string: Language.shared.currentAppLang)
        lblTBestSuitablDate.text = "Best Suitable Date".localizeString(string: Language.shared.currentAppLang)
        lblTBestSuitableTimeSlot.text = "Best Suitable Time Slot".localizeString(string: Language.shared.currentAppLang)
        lblTPrecations.text = "Precautions".localizeString(string: Language.shared.currentAppLang)
        lblTDescription.text = "Description".localizeString(string: Language.shared.currentAppLang)
        
        btnBookTest.setTitle("Book Test".localizeString(string: Language.shared.currentAppLang), for: .normal)

        self.lblNewTitle.text = dicTestData.name ?? ""
        self.lblLabName.text = "\("Lab Name".localizeString(string: Language.shared.currentAppLang)) : \(dicTestData.labName ?? "")"
        self.lblSubTitle.text = dicTestData.includedTests ?? ""
        
        let attributedText = NSAttributedString(
            string: "\(appDelegate?.currentCountry.currency ?? "") \(dicTestData.price ?? "")",
            attributes: [.strikethroughStyle: NSUnderlineStyle.single.rawValue]
        )
        self.lblOldPrice.attributedText = attributedText
        
        self.lblNewPrice.text = "\("Price :".localizeString(string: Language.shared.currentAppLang)) \(appDelegate?.currentCountry.currency ?? "") \(dicTestData.discountedPrice ?? "")"
        
        self.lblPre.text = dicTestData.precautions ?? ""
        
        self.lblDes.text = dicTestData.descriptionField ?? ""
        
        self.lblAddress.text = dicTestData.address ?? ""
        
        CollectionViewDate.dataSource = self
        CollectionViewDate.delegate = self
        
        collectionViewTime.delegate  = self
        collectionViewTime.dataSource = self
        collectionViewTime.collectionViewLayout = flowLayoutLiveSlot
        
        self.txtPerson.text = "No Of Person".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            txtPerson.textAlignment = .left
        }else{
            txtPerson.textAlignment = .right
        }
        
        setDropDown()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let date1 = formatter.date(from: Date.getCurrentDate())
        
        let Dform = DateFormatter()
        Dform.dateFormat = "EEEE"
        let strDate = Dform.string(from: date1!)
        
        let formatterAPI = DateFormatter()
        formatterAPI.dateFormat = "-MM-dd"
        let dateAPI = formatterAPI.string(from: date1!)
        
        
        let Dform789 = DateFormatter()
        Dform789.dateFormat = "yyyy-MM-dd"
        let strDateNew = Dform789.string(from: date1!)

        self.strSelectDate = strDateNew
        
        let totalHeight = 2 * 40
        
        self.viewSlots.constant = CGFloat(totalHeight) + 5.0
        self.collectionViewTime.isHidden = false
        self.lblNOA.isHidden = true
        
        viewBG.isHidden = true
        viewBGMain.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.myNotificationKeyPopUp, object: nil)

    }
    
    @objc func onNotification(notification:Notification)
    {
        viewBG.isHidden = false
        viewBGMain.isHidden = false
    }

    
    func setDropDown() {
        
        dropDown.dataSource = arrPerson
        dropDown.anchorView = viewPerson
        dropDown.direction = .any
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtPerson.text = item
            
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: viewPerson.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: -viewPerson.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.selectedTextColor = .black
        dropDown.reloadAllComponents()
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBookClicked(_ sender: Any) {
        
        if appDelegate?.currentCountry.countryId == "101"
        {
            if strSelectDate == ""
            {
                self.view.makeToast("Select date slot".localizeString(string: Language.shared.currentAppLang))
            }
            else if strSelectedTimeSlot == -1
            {
                self.view.makeToast("Select time slot".localizeString(string: Language.shared.currentAppLang))
            }
            else if txtPerson.text == "No Of Person".localizeString(string: Language.shared.currentAppLang)
            {
                self.view.makeToast("Select person".localizeString(string: Language.shared.currentAppLang))
            }
            else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "BookTestPopupViewController") as! BookTestPopupViewController
                controller.strTopTitle = "Book Test".localizeString(string: Language.shared.currentAppLang)
                controller.strBtnTitle = "Book".localizeString(string: Language.shared.currentAppLang)
                controller.modalPresentationStyle = .overFullScreen
                controller.parentVC = self
                controller.isOpenFromLab = true
                controller.dicBookTestData = dicTestData
                controller.strNoPerson = self.txtPerson.text ?? ""
                controller.strBookingDate = strSelectDate
                controller.strStartTime = strStartSelectTime
                controller.strEndTime = strEndSelectTime
                let navController = UINavigationController(rootViewController: controller) //Add navigation controller
                navController.navigationBar.isHidden = true
                navController.modalPresentationStyle = .overFullScreen
                navController.modalTransitionStyle = .crossDissolve
                
                self.present(navController, animated: true, completion: nil)
                
            }
        }
        else
        {
            self.view.makeToast("This service is not available in other country".localizeString(string: Language.shared.currentAppLang))
        }
        
        
    }
    
    @IBAction func clickedOpenDrop(_ sender: Any) {
        
        dropDown.show()
    }
   
    @IBAction func clickedOk(_ sender: Any) {
        viewBG.isHidden = true
        viewBGMain.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}

extension TestDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CollectionViewDate{
            return Date.getDates(forLastNDays: 5).count
        }else{
            return arrFixTimeSlotPerson.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == CollectionViewDate
        {
            let cell = self.CollectionViewDate.dequeueReusableCell(withReuseIdentifier: "BookTimeCollectionViewCell", for: indexPath) as! BookTimeCollectionViewCell
            
            if indexPath.row == 0
            {
                var objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                cell.lblName.text = objDate

            }
            else if indexPath.row == 1
            {
                var objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                cell.lblName.text = "\(objDate.dropLast(5))"

            }
            else
            {
                var objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                cell.lblName.text = "\(objDate.dropLast(5))"
            }
            
            if strSelectedDay == indexPath.row
            {
                cell.lblName.textColor = .white
                cell.mainView.backgroundColor = UIColor(red: 8/255, green: 54/255, blue: 75/255, alpha: 1)
            }
            else
            {
                cell.lblName.textColor = UIColor(red: 8/255, green: 54/255, blue: 75/255, alpha: 1)
                cell.mainView.backgroundColor = UIColor.white
            }
            
            return cell
        }
        else
        {
            let cell = collectionViewTime.dequeueReusableCell(withReuseIdentifier: "AvailableSlotCollectionViewCell", for: indexPath) as! AvailableSlotCollectionViewCell
            
            let dicData = arrFixTimeSlotPerson[indexPath.row]
            
            cell.lblName.text = dicData
            
            let dateCurrents = Date()
            let formatters = DateFormatter()
            formatters.dateFormat = "HH"
            let resultDate = formatters.string(from: dateCurrents)
            
            if strSelectedDay == 0
            {
                if indexPath.row == 0
                {
                    if 12 > Int(resultDate ?? "") ?? 0
                    {
                        print("Different Date Past Time Enabled")
                        cell.mainView.alpha = 1
                        cell.isUserInteractionEnabled = true
                    }
                    else
                    {
                        cell.mainView.alpha = 0.5
                        cell.isUserInteractionEnabled = false
                    }
                }
                else if indexPath.row == 1
                {
                    if 15 > Int(resultDate ?? "") ?? 0
                    {
                        print("Different Date Past Time Enabled")
                        cell.mainView.alpha = 1
                        cell.isUserInteractionEnabled = true
                    }
                    else
                    {
                        cell.mainView.alpha = 0.5
                        cell.isUserInteractionEnabled = false
                    }
                }
                else if indexPath.row == 2
                {
                    if 18 > Int(resultDate ?? "") ?? 0
                    {
                        print("Different Date Past Time Enabled")
                        cell.mainView.alpha = 1
                        cell.isUserInteractionEnabled = true

                    }
                    else
                    {
                        cell.mainView.alpha = 0.5
                        cell.isUserInteractionEnabled = false
                    }
                }
                else if indexPath.row == 3
                {
                    if 21 > Int(resultDate ?? "") ?? 0
                    {
                        print("Different Date Past Time Enabled")
                        cell.mainView.alpha = 1
                        cell.isUserInteractionEnabled = true

                    }
                    else
                    {
                        cell.mainView.alpha = 0.5
                        cell.isUserInteractionEnabled = false
                    }
                }

            }
            else
            {
                cell.mainView.alpha = 1
                cell.isUserInteractionEnabled = true
            }
            
            if strSelectedTimeSlot == indexPath.row
            {
                cell.lblName.textColor = .white
                cell.mainView.backgroundColor = UIColor(red: 8/255, green: 54/255, blue: 75/255, alpha: 1)
            }
            else
            {
                cell.lblName.textColor = UIColor(red: 8/255, green: 54/255, blue: 75/255, alpha: 1)
                cell.mainView.backgroundColor = UIColor(red: 14/255, green: 190/255, blue: 126/255, alpha: 0.08)
             }

            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == CollectionViewDate
        {
            strSelectedDay = indexPath.row
            strSelectedTimeSlot = -1
 

            if indexPath.row == 0
            {
                var objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                let selectDating = objDate

                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
                let date1 = formatter.date(from: Date.getCurrentDate())
                
                let Dform = DateFormatter()
                Dform.dateFormat = "EEEE"
                let strDate = Dform.string(from: date1!)
                
                let formatterAPI = DateFormatter()
                formatterAPI.dateFormat = "-MM-dd"
                let dateAPI = formatterAPI.string(from: date1!)
                
                let Dform789 = DateFormatter()
                Dform789.dateFormat = "yyyy-MM-dd"
                let strDateNew = Dform789.string(from: date1!)

                self.strSelectDate = strDateNew
                
                self.collectionViewTime.isHidden = false
                self.lblNOA.isHidden = true
 
            }
            else if indexPath.row == 1
            {
                var objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                let selectDating = objDate

                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "en_US")
                formatter.dateFormat = "dd MMM yyyy"
                let date1 = formatter.date(from: selectDating.replacingOccurrences(of: "Tomorrow, ", with: ""))
                
                let Dform = DateFormatter()
                Dform.dateFormat = "EEEE"
                let strDate = Dform.string(from: date1!)
                
                let formatterAPI = DateFormatter()
                formatterAPI.dateFormat = "-MM-dd"
                let dateAPI = formatterAPI.string(from: date1!)
                
                let Dform789 = DateFormatter()
                Dform789.dateFormat = "yyyy-MM-dd"
                let strDateNew = Dform789.string(from: date1!)

                self.strSelectDate = strDateNew
                
                self.collectionViewTime.isHidden = false
                self.lblNOA.isHidden = true
 
            }
            else
            {
                let objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                let selectDating = objDate
 
                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "en_US")
                formatter.dateFormat = "EEE, dd MMM yyyy"
                let date1 = formatter.date(from: selectDating)
                
                let Dform = DateFormatter()
                Dform.dateFormat = "EEEE"
                let strDate = Dform.string(from: date1!)
                
                let formatterAPI = DateFormatter()
                formatterAPI.dateFormat = "-MM-dd"
                let dateAPI = formatterAPI.string(from: date1!)
                
                
                let Dform789 = DateFormatter()
                Dform789.dateFormat = "yyyy-MM-dd"
                let strDateNew = Dform789.string(from: date1!)

                self.strSelectDate = strDateNew
                
                self.collectionViewTime.isHidden = false
                self.lblNOA.isHidden = true
 
            }

        }
        else
        {
            strSelectedTimeSlot = indexPath.row
            let dicData = arrFixTimeSlotPerson[indexPath.row]

            if indexPath.row == 0
            {
                strStartSelectTime = "09:00 AM"
                strEndSelectTime = "12:00 PM"
            }
            else if indexPath.row == 1
            {
                strStartSelectTime = "12:00 PM"
                strEndSelectTime = "03:00 PM"

            }
            else if indexPath.row == 2
            {
                strStartSelectTime = "03:00 PM"
                strEndSelectTime = "06:00 PM"

            }
            else if indexPath.row == 3
            {
                strStartSelectTime = "06:00 PM"
                strEndSelectTime = "09:00 PM"
            }
            
        }
        self.collectionViewTime.reloadData()
        self.CollectionViewDate.reloadData()
    }
}


class DateCollectionViewCell: UICollectionViewCell{
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAvailability: UILabel!
}

class TimeCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblTIme: UILabel!
}
 
extension String {
    
    //MARK:- Convert UTC To Local Date by passing date formats value
    func UTCToLocal(incomingFormat: String, outGoingFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = outGoingFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    //MARK:- Convert Local To UTC Date by passing date formats value
    func localToUTC(incomingFormat: String, outGoingFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = outGoingFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
}
