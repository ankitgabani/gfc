//
//  LabTestVC.swift
//  GFC Globalhealth
//
//  Created by Ankit Gabani on 28/02/23.
//

import UIKit
import DropDown
import SDWebImage

class LabTestVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource
{
    
    @IBOutlet weak var lblCountryName: UITextField!
    @IBOutlet weak var viewTarget: UIView!
    @IBOutlet weak var collectionviewTop: UICollectionView!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var txtCountry: UITextField!
    
    @IBOutlet weak var viewNoData: UIView!
    
    @IBOutlet weak var lblTNoData: UILabel!
    @IBOutlet weak var lblTLabTest: UILabel!
    @IBOutlet weak var lblTLocation: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var dropDown = DropDown()
    
    var arrCountryList: [GFCCountryListData] = [GFCCountryListData]()
    
    var arrLabCate: [GFCLabCateData] = [GFCLabCateData]()
    
    var arrLabCateList: [GFCLabCateListList] = [GFCLabCateListList]()
    
    var selectesdIndex = 0
    
    var strCateID = ""
    
    var strName = ""
    
    var strCountryID = ""
    
    var strImgurl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTLocation.text = "Location".localizeString(string: Language.shared.currentAppLang)
        lblTLabTest.text = "Lab Test".localizeString(string: Language.shared.currentAppLang)
        
        viewNoData.isHidden = true
        
        tblView.delegate = self
        tblView.dataSource = self
        
        collectionviewTop.delegate = self
        collectionviewTop.dataSource = self
        
        
        self.txtCountry.text = "India"
        self.strCountryID = "101"
        
        setDropDownCountry()
        
        callCountryListAPI()
        
        callLabCateAPI()
        
        // Do any additional setup after loading the view.
    }
    
    
    func setDropDownCountry()
    {
        let Country = NSMutableArray()
        
        for obj in arrCountryList
        {
            Country.add(obj.countryName ?? "")
        }
        
        dropDown.dataSource = Country as! [String]
        dropDown.anchorView = btnDrop
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            
            self.txtCountry.text = item
            
            for obj in arrCountryList
            {
                if obj.countryName == item
                {
                    self.strCountryID = obj.countryId ?? ""
                    appDelegate?.currentCountry = obj
                    appDelegate?.strCurrentSelectCountry
                }
            }
            
            self.callLabCateListAPI(categoryId: self.strCateID, country: self.strCountryID, name: self.strName)

        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: btnDrop.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: -btnDrop.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.selectedTextColor = .black
        dropDown.reloadAllComponents()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelegate?.setUpSideMenu()
    }
    
    @IBAction func clickedChooseCountry(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func clickedSearch(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LabSearchVC") as! LabSearchVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLabCateList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "LabTestTableCell") as! LabTestTableCell
        
        let dicData = arrLabCateList[indexPath.row]
        
        cell.lblName.text = dicData.name ?? ""
        cell.lblsubName.text = dicData.includedTests ?? ""
        cell.lblPrice.text = "\(appDelegate?.currentCountry.currency ?? "") \(dicData.discountedPrice ?? "")"
        
        cell.lblLab.text = "\("Lab Name".localizeString(string: Language.shared.currentAppLang)) : \(dicData.labName ?? "")"
        
        let rating = Double(dicData.rating ?? "")?.round(to: 1)
        cell.lblRating.text = "\(rating ?? 0.0)"
        
        var media_link_url = self.strImgurl + dicData.image ?? ""
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        cell.imgPro.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        let attributedText = NSAttributedString(
            string: "\(appDelegate?.currentCountry.currency ?? "") \(dicData.price ?? "")",
            attributes: [.strikethroughStyle: NSUnderlineStyle.single.rawValue]
        )
        cell.lblOldPrice.attributedText = attributedText
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrLabCateList[indexPath.row]
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "TestDetailViewController") as! TestDetailViewController
        controller.strTestId  = dicData.testId ?? ""
        controller.dicTestData = arrLabCateList[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
//        return 146
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrLabCate.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionviewTop.dequeueReusableCell(withReuseIdentifier: "TestLabCollectionCell", for: indexPath) as! TestLabCollectionCell
        
        let dicData = arrLabCate[indexPath.row]
        
        cell.lblName.text = dicData.testCateName ?? ""
        
        if selectesdIndex == indexPath.row
        {
            cell.mainView.backgroundColor = UIColor(hexString: "08364B")
            cell.lblName.textColor = .white
            self.strCateID = dicData.testCateId ?? ""
            self.strName = dicData.testCateName ?? ""
        }
        else
        {
            cell.mainView.backgroundColor = .clear
            cell.lblName.textColor = .darkGray
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectesdIndex = indexPath.row
        let dicData = arrLabCate[indexPath.row]
        
        self.callLabCateListAPI(categoryId: dicData.testCateId ?? "", country: self.strCountryID, name: self.strName)
        
        self.collectionviewTop.reloadData()
        self.tblView.reloadData()
    }
    
    // MARK: - call API
    
    
    
    func callCountryListAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCountryList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if dicData.status == "1"
                            {
                                self.arrCountryList.append(dicData)
                            }
                             
                        }
                        
                        if self.arrCountryList.count > 0
                        {
                            if appDelegate?.currentCountry.countryName == nil
                            {
                                self.strCountryID = self.arrCountryList[0].countryId ?? ""
                                self.txtCountry.text = self.arrCountryList[0].countryName ?? ""
                                appDelegate?.currentCountry = self.arrCountryList[0]
                            }
                            else
                            {
                                self.txtCountry.text = appDelegate?.currentCountry.countryName ?? ""
                                self.strCountryID = appDelegate?.currentCountry.countryId ?? ""
                             }
                        }
                        
                        self.setDropDownCountry()
                    }
                }
            }
        }
    }
    
    func callLabCateAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["token":"123456"]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_LAB_TEST_CATEGORY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrLabCate.removeAll()
                        
                        for obj in arrData!
                        {
                            let dicData = GFCLabCateData(fromDictionary: obj as! NSDictionary)
                            self.arrLabCate.append(dicData)
                        }
                        
                        
                        if self.arrLabCate.count > 0
                        {
                            self.strCateID = self.arrLabCate[0].testCateId ?? ""
                            self.callLabCateListAPI(categoryId: self.strCateID, country: self.strCountryID, name: self.strName)
                        }
                        
                        self.collectionviewTop.reloadData()
                    }
                }
            }
        }
    }
    
    func callLabCateListAPI(categoryId:String, country:String, name:String)
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["token":"123456","categoryId":categoryId,"country":country,"name":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_CATE_LIST, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        self.arrLabCateList.removeAll()
                        let img = dic?.value(forKey: "imageUrl") as? String
                        
                        self.strImgurl = img ?? ""
                        
                        if let arrList = dic?.value(forKey: "lists") as? NSArray
                        {
                            for obj in arrList
                            {
                                let dicData = GFCLabCateListList(fromDictionary: obj as! NSDictionary)
                                self.arrLabCateList.append(dicData)
                            }
                        }
                        
                        if self.arrLabCateList.count > 0
                        {
                            self.viewNoData.isHidden = true
                            self.tblView.isHidden = false
                        }
                        else
                        {
                            self.viewNoData.isHidden = false
                            self.tblView.isHidden = true
                        }
                        
                        self.tblView.reloadData()
                    }
                }
            }
        }
    }
    
}

class TestLabCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var mainView: UIView!
    
}

class LabTestTableCell: UITableViewCell{
    
    override func awakeFromNib() {
        super.awakeFromNib()

        viewRating.layer.cornerRadius = 9
        viewRating.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblsubName: UILabel!
    @IBOutlet weak var lblLab: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgPro: UIImageView!
    @IBOutlet weak var lblOldPrice: UILabel!
    
    @IBOutlet weak var viewRating: UIView!
    @IBOutlet weak var lblRating: UILabel!
}

