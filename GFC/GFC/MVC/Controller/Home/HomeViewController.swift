//
//  HomeViewController.swift
//  GFC
//
//  Created by Parth Anghan on 21/10/21.
//

import UIKit
import SDWebImage
import LGSideMenuController
import CoreLocation

struct PopularDoctorData{
    let imgUrl : String?
    let DrName : String?
    let DrCharge: String?
}

struct SpecialityDataModel{
    let name : String?
}

struct FeaturedDataModel{
    let imgUrl : String?
    let DrName : String?
    let DrCharge: String?
    let DrRating : String?
}

class HomeViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblNameTitl: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var imgCancel: UIImageView!
    @IBOutlet weak var txtSearch: UITextField!
    //  @IBOutlet weak var collectionViewLiveDoctor: UICollectionView!
    @IBOutlet weak var collectionViewBodyPart: UICollectionView!
    @IBOutlet weak var collectionViewPopular: UICollectionView!
    
    @IBOutlet weak var collectionViewFeature: UICollectionView!
    
    @IBOutlet weak var viewNoDataTop: UIView!
    @IBOutlet weak var viewNoDataDoc: UIView!
    
    @IBOutlet weak var lblTPopularDoc: UILabel!
    @IBOutlet weak var lblTNoDataPopular: UILabel!
    @IBOutlet weak var lblTDoctors: UILabel!
    @IBOutlet weak var lblTNoDataDoctors: UILabel!
    
    @IBOutlet weak var TblViewItem: UITableView!
    
    
    
    // var dicHomeScreenData = GFCHomeScreenData()
    
    var arrLiveDoctor = NSMutableArray()
    var arrBodyPart = NSMutableArray()
    var arrPopular = NSMutableArray()
    var arrFeature = NSMutableArray()
    
    var arrPopularDoctor = NSMutableArray()
    var arrSpecialityData = NSMutableArray()
    var arrFeatureData = NSMutableArray()
    
    var arrHospitalList: [GFCHospitalListData] = [GFCHospitalListData]()
    
    var isAPICall = false
    
    var arrBodyPart11 = ["blue_shape","heart_shape","eye_shape","bodyshape"]
    
    var arrBodyPart22 = ["teeth","heart","eye","body"]
    
    var flowLayoutLiveDoctor: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 116, height: 168)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 15
        return _flowLayout
    }
    
    var flowLayoutLiveBodyPart: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 95, height: 65)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 12
        return _flowLayout
    }
    
    var flowLayoutPopular: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 120, height: 190)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 10
        return _flowLayout
    }
    
    var flowLayoutFeature: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        _flowLayout.itemSize = CGSize(width: 120, height: 150)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        return _flowLayout
    }
    
    var countryCodeShort = ""
    
    var locationManager = CLLocationManager()
    
    var objLatitude = ""
    var objLongitude = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print()
        
        DispatchQueue.main.async {
            if CLLocationManager.locationServicesEnabled(){
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.distanceFilter = 10
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.requestAlwaysAuthorization()
                self.locationManager.startUpdatingLocation()
            }
        }
        
//        lblTPopularDoc.text = "Popular Doctors".localizeString(string: Language.shared.currentAppLang)
//        lblTDoctors.text = "Doctors".localizeString(string: Language.shared.currentAppLang)
//        lblTNoDataDoctors.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
//        lblTNoDataPopular.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)

        txtSearch.placeholder = "Search doctors".localizeString(string: Language.shared.currentAppLang )
        
        if Language.shared.isArabic == false{
            txtSearch.textAlignment = .left
        }else{
            txtSearch.textAlignment = .right
        }
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            
            countryCodeShort = countryCode ?? ""
        }
        
        callFeatureApi()
        callCountryListAPI(code: countryCodeShort)
        
        collectionViewBodyPart.delegate = self
        collectionViewBodyPart.dataSource = self
        
        collectionViewPopular.delegate = self
        collectionViewPopular.dataSource = self
        self.collectionViewPopular.collectionViewLayout = flowLayoutPopular
        
//        collectionViewFeature.delegate = self
//        collectionViewFeature.dataSource = self
//        self.collectionViewFeature.collectionViewLayout = flowLayoutFeature
        
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 20
        topView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
        
        if txtSearch.text == ""
        {
            imgCancel.isHidden = true
        }
        else
        {
            imgCancel.isHidden = false
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.Name.myNotificationKeyProfile, object: nil)
        
        callHospitalListAPI()
        TblViewItem.dataSource = self
        TblViewItem.delegate = self
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        objLatitude = "\(location?.coordinate.latitude ?? 0.0)"
        objLongitude = "\(location?.coordinate.longitude ?? 0.0)"
        
        callFeatureApi()
        
        self.locationManager.startUpdatingLocation()
    }
    
    //MARK:- Notification
    @objc func onNotification(notification:Notification)
    {
        lblNameTitl.text = "Hi, \(appDelegate?.dicUserLoginData.name ?? "")!"
        
        let url = URL(string: appDelegate?.dicUserLoginData.profileImage ?? "")
        imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "user"))
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        lblNameTitl.text = "Hi, \(appDelegate?.dicUserLoginData.name ?? "")!"
        
        let url = URL(string: appDelegate?.dicUserLoginData.profileImage ?? "")
        imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "user"))
    }
    
    @IBAction func clickedMenu(_ sender: Any) {
        if Language.shared.isArabic == true{
            self.sideMenuController?.showRightViewAnimated(sender: self)
        }else{
            self.sideMenuController?.showLeftViewAnimated(sender: self)
        }
    }
    
    @IBAction func btnFindDoctorClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FindDoctorVC") as! FindDoctorVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnProfileClicked(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("tabChange"), object: nil, userInfo: ["index": 3])
    }
    
    @IBAction func clickedClear(_ sender: Any) {
        txtSearch.text = ""
        imgCancel.isHidden = true
        btnCancel.isHidden = true
    }
    
    // MARK: - callAPI
    func callCountryListAPI(code: String)
    {
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if dicData.status == "1"
                            {
                                if dicData.sortname == self.countryCodeShort
                                {
                                    appDelegate?.currentCountry = dicData
                                    appDelegate?.currentCountry.countryName = dicData.countryName ?? ""
                                    appDelegate?.currentCountry.countryId = dicData.countryId ?? ""
                                }
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    //MARK: - API callDoctorFeatureApi
    func callFeatureApi(){
        APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        let _url = "?latitude=\(self.objLatitude)&longitude=\(self.objLongitude)"
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(GET_SPECIALITY + _url, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            if error == nil{
                APIClient.sharedInstance.hideIndicator()
                
                if let dic = response as? NSDictionary
                {
                    if let arrData = dic.value(forKey: "data") as? NSDictionary{
                        
                        if let featureddoctor = arrData.value(forKey: "featured_doctor") as? NSArray{
                            self.arrFeatureData = ((featureddoctor.mutableCopy() as? NSMutableArray)!)
                            DispatchQueue.main.async {
//                                self.collectionViewFeature.reloadData()
                            }
                        }
                        
                        if self.arrFeatureData.count > 0
                        {
//                            self.collectionViewFeature.isHidden = false
                            self.viewNoDataTop.isHidden = true
                        }
                        else
                        {
                            self.viewNoDataTop.isHidden = false
                            self.collectionViewFeature.isHidden = true
                        }
                        
                        if let featureddoctor77 = arrData.value(forKey: "popular_doctor") as? NSArray{
                            self.arrPopularDoctor = ((featureddoctor77.mutableCopy() as? NSMutableArray)!)
                            DispatchQueue.main.async
                            {
                                self.collectionViewPopular.reloadData()
                            }
                        }
                        
                        if self.arrPopularDoctor.count > 0
                        {
                            self.collectionViewPopular.isHidden = false
//                            self.viewNoDataDoc.isHidden = true
                        }
                        else
                        {
                            self.viewNoDataDoc.isHidden = false
                            self.collectionViewPopular.isHidden = true
                        }
                        
                        if let SpecialityData = arrData.value(forKey: "specialityData") as? NSArray
                        {
                            self.arrSpecialityData = (SpecialityData.mutableCopy() as? NSMutableArray)!
                            DispatchQueue.main.async
                            {
                               self.collectionViewBodyPart.reloadData()
                            }
                        }
                    }
                }
                else
                {
                    
                }
            }
        }
    }
    
    func callHospitalListAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        let _url = "?country_id=101"
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_HOSPITAL_C_ID + _url, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    self.arrHospitalList.removeAll()
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        for obj in arrData!
                        {
                            let dicData = GFCHospitalListData(fromDictionary: obj as! NSDictionary)
                            self.arrHospitalList.append(dicData)
                        }
                    }
                    self.isAPICall = true
                    self.TblViewItem.reloadData()
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewBodyPart
        {
            return arrSpecialityData.count // arrBodyPart.count
        }
        else if collectionView == collectionViewPopular
        {
            return arrPopularDoctor.count //arrPopular.count
        }
        else if collectionView == collectionViewFeature
        {
            return arrFeatureData.count// arrFeature.count
        }
        else
        {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewBodyPart
        {
            let cell = collectionViewBodyPart.dequeueReusableCell(withReuseIdentifier: "BodyPartCollectionCell", for: indexPath) as! BodyPartCollectionCell
            if let dicData = arrSpecialityData[indexPath.row] as? NSDictionary{
                let Name = dicData.value(forKey: "specialization") as? String ?? ""
                
                cell.specializationName.text = Name
            }
            
            
            return cell
        }
        else if collectionView == collectionViewPopular
        {
            let cell = collectionViewPopular.dequeueReusableCell(withReuseIdentifier: "PopularDoctorCollectionCell", for: indexPath) as! PopularDoctorCollectionCell
            
            if let dicData = arrPopularDoctor[indexPath.row] as? NSDictionary{
                let docName = dicData.value(forKey: "doctor_name") as? String ?? ""
                let docimg = dicData.value(forKey: "image_url") as? String ?? ""
                let docCharge = dicData.value(forKey: "consultant_charge") as? String ?? ""
                let specilization_id = dicData.value(forKey: "specilization_id") as? String ?? ""
                let rating7 = dicData.value(forKey: "rating") as? String ?? ""
                
                let rating = Double(rating7 ?? "")?.round(to: 1)
                
                cell.lblRating.text = "\(rating ?? 0.0)"
                
                let url = URL(string: docimg)
                cell.lblDrName.text = docName
//                cell.lblWork.text = "Rs. \(docCharge)"
                cell.profileImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.profileImg.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
                
                for obj in arrSpecialityData
                {
                    let id = (obj as? NSDictionary)?.value(forKey: "id") as? String
                    let specialization = (obj as? NSDictionary)?.value(forKey: "specialization") as? String
                    if specilization_id == id
                    {
//                        cell.lblSp.text = specialization ?? ""
                    }
                }
                
            }
            
//            cell.mainView.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5), alpha: 1, x: 0, y: 0, blur: 5, spread: 0)
            
            return cell
        }
        else
        {
            let cell = collectionViewFeature.dequeueReusableCell(withReuseIdentifier: "FeatureDoctorCollectionCell", for: indexPath) as! FeatureDoctorCollectionCell
            cell.viewMian.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5), alpha: 1, x: 0, y: 0, blur: 5, spread: 0)
            
            if let dicData = arrFeatureData[indexPath.row] as? NSDictionary
            {
                let Drname = dicData.value(forKey: "doctor_name") as? String ?? ""
                let DrImg = dicData.value(forKey: "image_url") as? String ?? ""
                let DrCharge = dicData.value(forKey: "consultant_charge") as? String ?? ""
                let Drrate = dicData.value(forKey: "rate_plan_id") as? String ?? ""
                
                let rating7 = dicData.value(forKey: "rating") as? String ?? ""
                
                let rating = Double(rating7 ?? "")?.round(to: 1)
                
                cell.rateLbl.text = "\(rating ?? 0.0)"
                
                let url = URL(string: DrImg)
                cell.drName.text = Drname
                cell.drCharge.text = "Rs. \(DrCharge)"

                cell.drImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.drImg.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
            }
            return cell
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewBodyPart
        {
            
            let dicData = arrSpecialityData[indexPath.row] as? NSDictionary
            let id = dicData?.value(forKey: "id") as? String ?? ""
            let docName = dicData?.value(forKey: "specialization") as? String ?? ""
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorsAppoViewController") as! DoctorsAppoViewController
            vc.strSpecialityID = id
            vc.strSpecialization = docName
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if collectionView == collectionViewPopular{
            
            let dicData = arrPopularDoctor[indexPath.row] as? NSDictionary
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorDetailsVC") as! DoctorDetailsVC
            vc.strDoctor_id = "\(dicData?.value(forKey: "id") as! String)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if collectionView == collectionViewFeature{
            
            let dicData = arrFeatureData[indexPath.row] as? NSDictionary
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorDetailsVC") as! DoctorDetailsVC
            vc.strDoctor_id = "\(dicData?.value(forKey: "id") as! String)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}


extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHospitalList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TblViewItem.dequeueReusableCell(withIdentifier: "TblMainCell") as! TblMainCell
        
        let dicData = arrHospitalList[indexPath.row]
        
        cell.lblHName.text = dicData.name ?? ""
        cell.lblHLocation.text = dicData.address ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

class TblMainCell: UITableViewCell {
    
    @IBOutlet weak var lblHName: UILabel!
    @IBOutlet weak var lblHLocation: UILabel!
    @IBOutlet weak var lblHRating: UILabel!
    
}
