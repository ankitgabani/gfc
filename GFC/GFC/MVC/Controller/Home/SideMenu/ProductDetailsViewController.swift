//
//  ProductDetailsViewController.swift
//  GFC
//
//  Created by Parth Anghan on 25/10/21.
//

import UIKit
import GMStepper

class ProductDetailsViewController: UIViewController {

    @IBOutlet weak var stepper: GMStepper!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func onBakcBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func stepperValueChanged(_ sender: GMStepper) {
        print(stepper.value, terminator: "")
    }
    @IBAction func onBtnCart(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: - API
    func callPharmacyProductDetailAPI() {
        let param = ["token": "12345678","country":appDelegate?.currentCountry.countryId ?? ""]
        APIClient.sharedInstance.requestWith(method: .post, endPoint: PHARMACY_PRODUCTS, loader: true, params: param, model: PharmacyProductsModel.self, success: { (result) in
            
            print(result)
            if let model = result as? PharmacyProductsModel{
               
            }
        }, failure: { (error) in
            self.view.makeToast("Something went wrong!")
        })
    }
}
