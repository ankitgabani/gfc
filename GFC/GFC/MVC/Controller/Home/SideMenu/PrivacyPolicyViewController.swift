//
//  PrivacyPolicyViewController.swift
//  GFC
//
//  Created by Parth Anghan on 25/10/21.
//

import UIKit
import Quickblox
class PrivacyPolicyViewController: UIViewController {

    @IBOutlet weak var imgBG: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imgBG.isUserInteractionEnabled = true
        imgBG.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
            
            let viewcontrollers = self.navigationController?.viewControllers
            var isExist = false
            for viewcontroller in viewcontrollers! {
                if viewcontroller.isKind(of: HomeViewController.self) {
                    isExist = true
                    break
                }
            }
            self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
            self.navigationController?.popToViewController(controller, animated: true)
        }
    }

}
