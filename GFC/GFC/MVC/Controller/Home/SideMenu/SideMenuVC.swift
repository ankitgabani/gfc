//
//  SideMenuVC.swift
//  GFC
//
//  Created by Parth Anghan on 25/10/21.
//

import UIKit
import LGSideMenuController
import StoreKit
import SDWebImage
import PushKit
import Quickblox
import QuickbloxWebRTC

struct UsersConstant {
    static let perPage:UInt = 100
    static let searchPerPage:UInt = 10
    static let noUsers = "No user with that name"
    static let subscriptionID = "last_voip_subscription_id"
    static let token = "last_voip_token"
    static let needUpdateToken = "last_voip_token_need_update"
    static let okAction = "Ok"
}

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPhoneNumbere: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblTLogout: UILabel!
    
    
    var arrMenu = ["Pharmacy","Lab","Treatments","Services","My Appointments","My Orders","Privacy & Policy","Share App","Rate App"]
        
    var arrMenuImages = ["ic_menu_my_doctors","Groupmedical_records","Group (2)","Group (2)","Group-1","Group (1)-1","Group-2","Group (1)-2","Group (2)-1","social-media"]
    
    var selectedIndex = 0
    
    private let authModule = AuthModule()
    private let connection = ConnectionModule()
    
    var onSignOut: (() -> Void)?
    private var users = Users()
    private var navigationTitleView = TitleViewBack()
    private var voipRegistry: PKPushRegistry = PKPushRegistry(queue: DispatchQueue.main)
    private let callHelper = CallHelper()
    private var callViewController: CallViewController? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        connection.delegate = self
         
        connection.activateAutomaticMode()
        
        authModule.delegate = self
        
        callHelper.delegate = self
        
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = Set<PKPushType>([.voIP])

        
        lblTLogout.text = "Logout".localizeString(string: Language.shared.currentAppLang)
        
        lblUserName.text = appDelegate?.dicUserLoginData.name
        lblPhoneNumbere.text = "+\(appDelegate?.dicUserLoginData.country_code ?? "") \(appDelegate?.dicUserLoginData.mobileNumber ?? "")"
        
        //        let url = URL(string: appDelegate?.dicUserLoginData.profileImage ?? "")
        //        imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        //        imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))

        
        tblView.delegate = self
        tblView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.Name.myNotificationKeyProfile, object: nil)

        // Do any additional setup after loading the view.
    }
    
    //MARK:- Notification
    @objc func onNotification(notification:Notification)
    {
        self.navigationController?.navigationBar.isHidden = true
        let url = URL(string: appDelegate?.dicUserLoginData.profileImage ?? "")
        imgUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "user"))
        
        lblUserName.text = appDelegate?.dicUserLoginData.name
        lblPhoneNumbere.text = appDelegate?.dicUserLoginData.mobileNumber
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        let url = URL(string: appDelegate?.dicUserLoginData.profileImage ?? "")
        imgUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "user"))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell

        cell.lblName.text = arrMenu[indexPath.row].localizeString(string: Language.shared.currentAppLang)
        
        cell.imgSide.image = UIImage(named: arrMenuImages[indexPath.row])
        
        if selectedIndex == indexPath.row
        {
            cell.viewBg.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.10)
        }
        else
        {
            cell.viewBg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tblView.reloadData()
        
        if indexPath.row == 0
        {
            if Language.shared.isArabic == true{
                NotificationCenter.default.post(name: Notification.Name("tabChange"), object: nil, userInfo: ["index": 2])
                self.sideMenuController?.hideRightView(animated: true, completion: nil)
            }else{
                NotificationCenter.default.post(name: Notification.Name("tabChange"), object: nil, userInfo: ["index": 2])
                self.sideMenuController?.hideLeftView(animated: true, completion: nil)
            }
            
        }
        else if indexPath.row == 1
        {
            if Language.shared.isArabic == true{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate

                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController

                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "LabTestVC") as! LabTestVC
                navigation.viewControllers = [homeViewController]
                self.sideMenuController?.hideRightView(animated: true, completion: nil)
            }else{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate

                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController

                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "LabTestVC") as! LabTestVC
                navigation.viewControllers = [homeViewController]
                self.sideMenuController?.hideLeftView(animated: true, completion: nil)
            }
            
        }
        else if indexPath.row == 2
        {
            if Language.shared.isArabic == true{
                NotificationCenter.default.post(name: Notification.Name("tabChange"), object: nil, userInfo: ["index": 1])
                
                self.sideMenuController?.hideRightView(animated: true, completion: nil)
            }else{
                NotificationCenter.default.post(name: Notification.Name("tabChange"), object: nil, userInfo: ["index": 1])
                
                self.sideMenuController?.hideLeftView(animated: true, completion: nil)
            }
            
        }
        else if indexPath.row == 3
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate

            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController

            let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "ServicesVC") as! ServicesVC
            navigation.viewControllers = [homeViewController]
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
        else if indexPath.row == 4
        {
            if Language.shared.isArabic == true{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate

                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController

                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentsListViewController") as! AppointmentsListViewController
                navigation.viewControllers = [homeViewController]
                self.sideMenuController?.hideRightView(animated: true, completion: nil)
            }else{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate

                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController

                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentsListViewController") as! AppointmentsListViewController
                navigation.viewControllers = [homeViewController]
                self.sideMenuController?.hideLeftView(animated: true, completion: nil)
            }
            
        }
        else if indexPath.row == 5
        {
            if Language.shared.isArabic == true{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate

                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController

                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
                navigation.viewControllers = [homeViewController]
                self.sideMenuController?.hideRightView(animated: true, completion: nil)
            }else{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate

                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController

                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
                navigation.viewControllers = [homeViewController]
                self.sideMenuController?.hideLeftView(animated: true, completion: nil)
            }
        }
        else if indexPath.row == 6
        {
            if Language.shared.isArabic == true{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate

                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController

                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
                navigation.viewControllers = [homeViewController]
                homeViewController.isSignUP = false
                self.sideMenuController?.hideRightView(animated: true, completion: nil)
            }else{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate

                let controller = appdelegate.window?.rootViewController as! LGSideMenuController
                let navigation = controller.rootViewController as! UINavigationController

                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
                navigation.viewControllers = [homeViewController]
                homeViewController.isSignUP = false
                self.sideMenuController?.hideLeftView(animated: true, completion: nil)
            }
            
        }
        else if indexPath.row == 7
        {
            if let logo = UIImage(named: "app_logo"), let websiteURL = URL(string: "https://itunes.apple.com/app/id6448429453") {
               let objectsToShare = ["GFC Globalhealth", websiteURL, logo] as [Any]
               let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: [])
               if let popoverController = activityVC.popoverPresentationController {
                  popoverController.sourceView = self.view
                  popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
               }
               present(activityVC, animated: true)
            }
        }
        else if indexPath.row == 8
        {
            
            rateApp()
        }
//        else
//        {
//            AppUtilites.showAlert(title: "Are you sure want to Delete Account?".localizeString(string: Language.shared.currentAppLang), message: "", actionButtonTitle: "Yes".localizeString(string: Language.shared.currentAppLang), cancelButtonTitle: "No".localizeString(string: Language.shared.currentAppLang))
//            {
//
//                UserDefaults.standard.setValue(false, forKey: "UserLogin")
//                UserDefaults.standard.synchronize()
//                appDelegate?.dicUserLoginData = GFCLoginData()
//                appDelegate?.saveCurrentUserData(dic:  GFCLoginData())
//
//                appDelegate?.setUpLanguageData()
//            }
//        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    @IBAction func clickedCancelMenu(_ sender: Any) {
        if Language.shared.isArabic == true{
            self.sideMenuController?.hideRightViewAnimated(sender: self)

        }else{
            self.sideMenuController?.hideLeftViewAnimated(sender: self)
        }
    }
    @IBAction func clickedLogout(_ sender: Any) {
     
        AppUtilites.showAlert(title: "Are you sure want to Logout?".localizeString(string: Language.shared.currentAppLang), message: "", actionButtonTitle: "Yes".localizeString(string: Language.shared.currentAppLang), cancelButtonTitle: "No".localizeString(string: Language.shared.currentAppLang))
        {
           
            UserDefaults.standard.setValue(false, forKey: "UserLogin")
            UserDefaults.standard.synchronize()
            appDelegate?.dicUserLoginData = GFCLoginData()
            appDelegate?.saveCurrentUserData(dic:  GFCLoginData())
 
//            appDelegate?.setUpLanguageData()
//            
            self.logout()
 
        }
        
        
    }
    
    @IBAction func clickedProfile(_ sender: Any) {
        if Language.shared.isArabic == true{
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
    //        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "PRofileVC") as! PRofileVC
    //        navigation.viewControllers = [homeViewController]
            NotificationCenter.default.post(name: Notification.Name("tabChange"), object: nil, userInfo: ["index": 3])
            self.sideMenuController?.hideRightView(animated: true, completion: nil)
        }else{
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
            let controller = appdelegate.window?.rootViewController as! LGSideMenuController
            let navigation = controller.rootViewController as! UINavigationController
            
    //        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "PRofileVC") as! PRofileVC
    //        navigation.viewControllers = [homeViewController]
            NotificationCenter.default.post(name: Notification.Name("tabChange"), object: nil, userInfo: ["index": 3])
            self.sideMenuController?.hideLeftView(animated: true, completion: nil)
        }
       
    }
    
    func rateApp() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()

        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "6448429453") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)

            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
// MARK: - PKPushRegistryDelegate
extension SideMenuVC: PKPushRegistryDelegate {
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        guard let token = registry.pushToken(for: .voIP) else {
            return
        }
        let userDefaults = UserDefaults.standard
        if let lastToken = userDefaults.object(forKey: UsersConstant.token) as? Data,
           token == lastToken {
            return
        }
        userDefaults.setValue(token, forKey: UsersConstant.token)
        userDefaults.set(true, forKey: UsersConstant.needUpdateToken)
        if connection.tokenHasExpired {
            return
        }
        deleteLastSubscription { [weak self] in
            self?.createSubscription(withToken: token)
        }
    }
    
    private func deleteLastSubscription(withCompletion completion:@escaping () -> Void) {
        let userDefaults = UserDefaults.standard
        guard let lastSubscriptionId = userDefaults.object(forKey: UsersConstant.subscriptionID) as? NSNumber  else {
            completion()
            return
        }
        
        QBRequest.deleteSubscription(withID: lastSubscriptionId.uintValue) { (response) in
            userDefaults.removeObject(forKey: UsersConstant.subscriptionID)
            completion()
        } errorBlock: { (response) in
            completion()
        }
    }
    
    private func createSubscription(withToken token: Data) {
        guard let deviceUUID = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        let userDefaults = UserDefaults.standard
        let subscription = QBMSubscription()
        subscription.notificationChannel = .APNSVOIP
        subscription.deviceUDID = deviceUUID
        subscription.deviceToken = token
        
        QBRequest.createSubscription(subscription, successBlock: { response, objects in
            guard let subscriptions = objects, subscriptions.isEmpty == false else {
                return
            }
            var newSubscription: QBMSubscription? = nil
            for subscription in subscriptions {
                if subscription.notificationChannel == .APNSVOIP,
                   let subscriptionsUIUD = subscription.deviceUDID,
                   subscriptionsUIUD == deviceUUID {
                    newSubscription = subscription
                }
            }
            guard let newSubscriptionID = newSubscription?.id else {
                return
            }
            userDefaults.setValue(NSNumber(value: newSubscriptionID), forKey: UsersConstant.subscriptionID)
        }, errorBlock: { response in
        })
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        deleteLastSubscription {
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry,
                      didReceiveIncomingPushWith payload: PKPushPayload,
                      for type: PKPushType,
                      completion: @escaping () -> Void) {
        
        defer {
            completion()
        }
        
        guard (type == .voIP) else {
            return
        }
        
        guard let opponentsIDs = payload.dictionaryPayload["opponentsIDs"] as? String,
              let contactIdentifier = payload.dictionaryPayload["contactIdentifier"] as? String,
              let sessionID = payload.dictionaryPayload["sessionID"] as? String,
              let conferenceType = payload.dictionaryPayload["conferenceType"] as? String,
              let timestamp = payload.dictionaryPayload["timestamp"] as? String else {
            return
        }
        
        let dictionaryPayload = ["opponentsIDs": opponentsIDs,
                                 "contactIdentifier": contactIdentifier,
                                 "sessionID": sessionID,
                                 "conferenceType": conferenceType,
                                 "timestamp": timestamp
        ]
        
        if callHelper.callReceived(sessionID) == true {
            // when a voip push is received with the same session
            // that has an active call at that moment
            debugPrint("\(#function) Received a voip push with the same session that has an active call at that moment")
            return
        }
        
        connection.activateCallMode()
        callHelper.registerCall(withPayload: dictionaryPayload, completion: completion)
        connection.activateAutomaticMode()
    }
}

// MARK: - CallHelperDelegate
extension SideMenuVC: CallHelperDelegate {
    func helper(_ helper: CallHelper, didAcceptCall callId: String) {
        self.helper(helper, showCall: callId)
    }
    
    func helper(_ helper: CallHelper, didRegisterCall callId: String, mediaListener: MediaListener, mediaController: MediaController, direction: CallDirection, members: [NSNumber : String], hasVideo: Bool) {
        
        connection.activateCallMode()
        
        callViewController = hasVideo == true ? Screen.videoCallViewController() : Screen.audioCallViewController()
        
        callViewController?.setupWithCallId(callId, members: members, mediaListener: mediaListener, mediaController: mediaController, direction: direction)
        
        if direction == .outgoing {
            self.helper(helper, showCall: callId)
            return
        }
        let usersIDs = Array(members.keys)
        users.users(usersIDs) { [weak self] (users, error) in
            guard let self = self, let users = users else {
                return
            }
            var callMembers: [NSNumber : String] = [:]
            for user in users {
                callMembers[NSNumber(value: user.id)] = user.fullName ?? "User"
            }
            self.callViewController?.callInfo.updateWithMembers(callMembers)
            let title = Array(callMembers.values).joined(separator: ", ")
            helper.updateCall(callId, title: title)
        }
    }
    
    func helper(_ helper: CallHelper, didUnregisterCall callId: String) {
        connection.deactivateCallMode()
        guard let callViewController = callViewController else {
            return
        }
        
        callViewController.dismiss(animated: false) {
            self.callViewController = nil
        }
    }
    
    //Internal Method
    private func helper(_ helper: CallHelper, showCall callId: String) {
        guard let callViewController = self.callViewController,
              let callViewControllerCallId = callViewController.callInfo.callId,
              callViewControllerCallId == callId else {
            return
        }
        hideAlertView()
        
        let navVC = UINavigationController(rootViewController: callViewController)
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: false)
        
         callViewController.hangUp = { (callId) in
            helper.unregisterCall(callId, userInfo: ["hangup": "hang up"])
        }
    }
}


// MARK: - AuthModuleDelegate
extension SideMenuVC: AuthModuleDelegate {
    func authModule(_ authModule: AuthModule, didLoginUser user: QBUUser) {
        Profile.synchronize(withUser: user)
        connection.establish()
        let userDefaults = UserDefaults.standard
        guard userDefaults.bool(forKey: UsersConstant.needUpdateToken) != false,
              let token = userDefaults.object(forKey: UsersConstant.token) as? Data else {
            return
        }
        deleteLastSubscription { [weak self] in
            self?.createSubscription(withToken: token)
        }
    }
    
    func authModuleDidLogout(_ authModule: AuthModule) {
        connection.deactivateAutomaticMode()
        onSignOut?()
        Profile.clear()
    }
    
    func authModule(_ authModule: AuthModule, didReceivedError error: ErrorInfo) {
        showUnAuthorizeAlert(message: "\(error.statusCode) \(error.info)" , logoutAction: { [weak self] logoutAction in
            self?.logout()
        }, tryAgainAction: { [weak self] tryAgainAction in
            let profile = Profile()
            self?.authModule.login(fullName: profile.fullName, login: profile.login)
        })
    }
    
    private func logout() {
        if connection.established == false {
            showNoInternetAlert { [weak self] (action) in
                self?.connection.establish()
                self?.authModule.logout()
            }
            return
        }
        deleteLastSubscription { [weak self] in
            self?.connection.breakConnection {
                self?.authModule.logout()
            }
        }
    }
}

// MARK: - ConnectionModuleDelegate
extension SideMenuVC: ConnectionModuleDelegate {
    func connectionModuleWillConnect(_ connectionModule: ConnectionModule) {
        showAnimatedAlertView(nil, message: ConnectionConstant.connectingState)
    }
    
    func connectionModuleDidConnect(_ connectionModule: ConnectionModule) {
        hideAlertView()
    }
    
    func connectionModuleDidNotConnect(_ connectionModule: ConnectionModule, error: Error) {
        if error._code.isNetworkError {
            showNoInternetAlert { [weak self] (action) in
                self?.connection.establish()
            }
            return
        }
        showAlertView(nil, message: error.localizedDescription, handler: nil)
    }
    
    func connectionModuleWillReconnect(_ connectionModule: ConnectionModule) {
        showAnimatedAlertView(nil, message: ConnectionConstant.reconnectingState)
    }
    
    func connectionModuleDidReconnect(_ connectionModule: ConnectionModule) {
        navigationTitleView.textColor = .white
        hideAlertView()
    }
    
    func connectionModuleTokenHasExpired(_ connectionModule: ConnectionModule) {
        let profile = Profile()
        authModule.login(fullName: profile.fullName, login: profile.login)
    }
}
