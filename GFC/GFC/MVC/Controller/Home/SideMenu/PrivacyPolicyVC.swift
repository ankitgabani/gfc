//
//  PrivacyPolicyVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 01/05/23.
//

import UIKit
import WebKit

class PrivacyPolicyVC: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var txtViw: UITextView!
    @IBOutlet weak var viewWeb: UIView!
    
    @IBOutlet weak var lblTPrivacyPolicy: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var isSignUP = false
    
    var webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTPrivacyPolicy.text = "Privacy Policy".localizeString(string: Language.shared.currentAppLang)
        
        DispatchQueue.main.async {
            self.webView.frame = CGRect.init(x: 0, y: 0, width: self.viewWeb.frame.size.width, height: self.viewWeb.frame.size.height)
            
            self.webView.load(NSURLRequest(url: NSURL(string: "https://gfcglobalhealth.com/privacy-policy")! as URL) as URLRequest)
            
            self.webView.allowsBackForwardNavigationGestures = true
            
            self.webView.navigationDelegate = self
            
            self.webView.scrollView.bounces = false
            
            self.viewWeb.addSubview(self.webView)
        }
       
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        if isSignUP == true
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            appDelegate?.setUpSideMenu()
        }
        
    }
    

}
