//
//  ProductDetailVC.swift
//  GFC Globalhealth
//
//  Created by Code on 03/03/23.
//

import UIKit
import Quickblox
class ProductDetailVC: UIViewController {

    @IBOutlet weak var lblQtn: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var collViewSelectImage: UICollectionView!
    @IBOutlet weak var collViewPackageSize: UICollectionView!
    
    @IBOutlet weak var viewAddtoCart: UIView!
    @IBOutlet weak var viewStapper: UIView!
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var lblDiscountedPrice: UILabel!
    @IBOutlet weak var lblOriginalPrice: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var lblSellerName: UILabel!
    @IBOutlet weak var lblRatingAndReviews: UILabel!
    @IBOutlet weak var lblCartCount: UILabel!
    
    @IBOutlet weak var lblTPackageSize: UILabel!
    @IBOutlet weak var lblTProductDetail: UILabel!
    
    @IBOutlet weak var lblTseller: UILabel!
    
    @IBOutlet weak var lblTAddtoCart: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var viewRating: UIView!
    @IBOutlet weak var lblRating: UILabel!
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 17)
    let itemsPerRow: CGFloat = 8
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.sectionInsets.left * (self.itemsPerRow + 1)
            let availableWidth = self.collViewSelectImage.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.itemsPerRow
            
            _flowLayout.itemSize = CGSize(width: 60, height: 60)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 17)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _flowLayout.minimumInteritemSpacing = 15
            _flowLayout.minimumLineSpacing = 15
        }
        
        // edit properties here
        return _flowLayout
    }
    
    let SizesectionInsets = UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 17)
    let SizeitemsPerRow: CGFloat = 4
    
    var SizelowLayout: UICollectionViewFlowLayout {
        let _SizeflowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.SizesectionInsets.left * (self.SizeitemsPerRow + 1)
            let availableWidth = self.collViewPackageSize.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.SizeitemsPerRow
            
            _SizeflowLayout.itemSize = CGSize(width: 75, height: 75)
            
            _SizeflowLayout.sectionInset = UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 17)
            _SizeflowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _SizeflowLayout.minimumInteritemSpacing = 12
            _SizeflowLayout.minimumLineSpacing = 12
        }
        
        // edit properties here
        return _SizeflowLayout
    }
    
    var strSelectImg = 0
    
    var strSelectSize = 0
    
    var pID = ""
    
    var arrProductImg: [GFCProductDetailImage] = [GFCProductDetailImage]()
    
    var dicProductDetail = GFCProductDetailProductDetail()

    var imgBase_url = ""
    
    var objQty = 1
    
    var strCartID = ""
    
    var arrVarient = NSMutableArray()
    
    var objCheckSize = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTAddtoCart.text = "Add to cart".localizeString(string: Language.shared.currentAppLang)
        lblTPackageSize.text = "Package Size".localizeString(string: Language.shared.currentAppLang)
        lblTProductDetail.text = "Product details".localizeString(string: Language.shared.currentAppLang)
        lblTseller.text = "Seller".localizeString(string: Language.shared.currentAppLang)
        
        collViewSelectImage.delegate = self
        collViewSelectImage.dataSource = self
        collViewSelectImage.collectionViewLayout = flowLayout
        
        collViewPackageSize.delegate = self
        collViewPackageSize.dataSource = self
        collViewPackageSize.collectionViewLayout = SizelowLayout
        
        viewAddtoCart.isHidden = false
        viewStapper.isHidden = true

        callProductDetailAPI()

        lblProductDescription.text?.maxLength(length: 35)
        
        tblView.delegate = self
        tblView.dataSource = self
        
        
        viewRating.layer.cornerRadius = 9
        viewRating.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callCartListAPI(isRemove: false)
    }
    
        
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedCart(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedAddToCart(_ sender: Any) {
        self.view.hideAllToasts()
        
        if appDelegate?.currentCountry.countryId == "101"
        {
            self.view.makeToast("Item added to cart".localizeString(string: Language.shared.currentAppLang))
            
            viewStapper.isHidden = false
            viewAddtoCart.isHidden = true
            objQty = 1
            self.lblQtn.text = "\(objQty)"
     
            callUpdateCartAPI()
        }
        else
        {
            self.view.makeToast("This service is not available in other country".localizeString(string: Language.shared.currentAppLang))
        }
        
        
    }
    
    @IBAction func clickedAddQty(_ sender: Any) {
        self.view.hideAllToasts()
        
        self.view.makeToast("Item added to cart".localizeString(string: Language.shared.currentAppLang))
        
        objQty = objQty + 1
        self.lblQtn.text = "\(objQty)"
 
        callUpdateCartAPI()
    }
    
    @IBAction func clickdSubQty(_ sender: Any)
    {
        self.view.hideAllToasts()
        
        self.view.makeToast("Item removed from cart".localizeString(string: Language.shared.currentAppLang))
        if objQty != 1
        {
            objQty = objQty - 1
            self.lblQtn.text = "\(objQty)"
            self.callUpdateCartAPI()
        }
        else
        {
            viewStapper.isHidden = true
            viewAddtoCart.isHidden = false
            
            callDeleteCartAPI(cart_id: self.strCartID)
        }
       
    }
    
    func callDeleteCartAPI(cart_id:String)
    {
        let para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? "","cart_id":cart_id]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(DELETE_CART, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String

                    if status == "success"
                    {
                        self.view.makeToast(message)
                        self.callCartListAPI(isRemove: true)
                    }
                    else
                    {
                        
                    }
                }
            }
        }
    }
    
    func callUpdateCartAPI()
    {
        
        
        let dicV = self.arrVarient[strSelectSize] as? NSDictionary
        
        var discountedPrice = ""
        var size = ""
        var productStock = ""
        var price = ""
        var objPassSize = ""
        
        var para = ["":""]
        
        if strSelectSize == 0
        {
            discountedPrice = dicV?.value(forKey: "discountedPrice") as? String ?? ""
            size = dicV?.value(forKey: "size") as? String ?? ""
            productStock = dicV?.value(forKey: "productStock") as? String ?? ""
            price = dicV?.value(forKey: "price") as? String ?? ""
            
            para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? "","productId":self.pID,"quantity":"\(objQty)","varient":"size"]
            
        }
        else if strSelectSize == 1
        {
            discountedPrice = dicV?.value(forKey: "discountedPrice1") as? String ?? ""
            size = dicV?.value(forKey: "size1") as? String ?? ""
            productStock = dicV?.value(forKey: "productStock1") as? String ?? ""
            price = dicV?.value(forKey: "price1") as? String ?? ""
            
            para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? "","productId":self.pID,"quantity":"\(objQty)","varient":"size1"]
        }
        else if strSelectSize == 2
        {
            discountedPrice = dicV?.value(forKey: "discountedPrice2") as? String ?? ""
            size = dicV?.value(forKey: "size2") as? String ?? ""
            productStock = dicV?.value(forKey: "productStock2") as? String ?? ""
            price = dicV?.value(forKey: "price2") as? String ?? ""
            
            para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? "","productId":self.pID,"quantity":"\(objQty)","varient":"size2"]
        }
        else
        {
            discountedPrice = dicV?.value(forKey: "discountedPrice3") as? String ?? ""
            size = dicV?.value(forKey: "size3") as? String ?? ""
            productStock = dicV?.value(forKey: "productStock3") as? String ?? ""
            price = dicV?.value(forKey: "price3") as? String ?? ""
            
            para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? "","productId":self.pID,"quantity":"\(objQty)","varient":"size3"]
        }
        
      
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(UPDATR_CART_ITEM, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    self.callCartListAPI(isRemove: false)
                 }
            }
        }
    }
    
    func callCartListAPI(isRemove: Bool)
    {
        
        let para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? ""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CART_ITEM, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let cartProductList = response?.value(forKey: "cartProductList") as? NSArray
                    
                    self.lblCartCount.text = "\(cartProductList?.count ?? 0)"
                    
                    if (cartProductList?.count ?? 0) > 0
                    {
                        for obj in cartProductList!
                        {
                            let dicPro = obj as? NSDictionary
                            let pId = dicPro?.value(forKey: "pId") as? String
                            let quantity = dicPro?.value(forKey: "quantity") as? String
                            let cart_id = dicPro?.value(forKey: "cart_id") as? String
                            
                            let size = dicPro?.value(forKey: "size") as? String
                            let size1 = dicPro?.value(forKey: "size1") as? String
                            let size2 = dicPro?.value(forKey: "size2") as? String
                            let size3 = dicPro?.value(forKey: "size3") as? String

                            if self.pID == pId
                            {
                                if self.strSelectSize == 0
                                {
                                    if size == ""
                                    {
                                        self.strCartID = cart_id ?? ""
                                        self.lblQtn.text = "1"
                                        self.objQty = 1
                                        
                                        self.viewStapper.isHidden = true
                                        self.viewAddtoCart.isHidden = false
                                    }
                                    else
                                    {
                                        
                                        if self.objCheckSize == size
                                        {
                                            self.strCartID = cart_id ?? ""
                                            self.lblQtn.text = quantity ?? ""
                                            self.objQty = Int(quantity ?? "") ?? 0
                                            
                                            if isRemove == false
                                            {
                                                self.viewStapper.isHidden = false
                                                self.viewAddtoCart.isHidden = true
                                            }
                                            
                                            return
                                            
                                        }
                                        else
                                        {
                                            self.viewStapper.isHidden = true
                                            self.viewAddtoCart.isHidden = false
                                        }
                                    }
                                    
                                }
                                else if self.strSelectSize == 1
                                {
                                    if size1 == ""
                                    {
                                        self.strCartID = cart_id ?? ""
                                        self.lblQtn.text = "1"
                                        self.objQty = 1
                                        
                                        self.viewStapper.isHidden = true
                                        self.viewAddtoCart.isHidden = false
                                    }
                                    else
                                    {
                                        
                                        if self.objCheckSize == size1
                                        {
                                            self.strCartID = cart_id ?? ""
                                            self.lblQtn.text = quantity ?? ""
                                            self.objQty = Int(quantity ?? "") ?? 0
                                            
                                            if isRemove == false
                                            {
                                                self.viewStapper.isHidden = false
                                                self.viewAddtoCart.isHidden = true
                                            }
                                            
                                            return
                                        }
                                        else
                                        {
                                            self.viewStapper.isHidden = true
                                            self.viewAddtoCart.isHidden = false
                                        }
                                    }
                                    
                                }
                                else if self.strSelectSize == 2
                                {
                                    if size2 == ""
                                    {
                                        self.strCartID = cart_id ?? ""
                                        self.lblQtn.text = "1"
                                        self.objQty = 1
                                        
                                        self.viewStapper.isHidden = true
                                        self.viewAddtoCart.isHidden = false
                                    }
                                    else
                                    {
                                        if self.objCheckSize == size2
                                        {
                                            self.strCartID = cart_id ?? ""
                                            self.lblQtn.text = quantity ?? ""
                                            self.objQty = Int(quantity ?? "") ?? 0
                                            
                                            if isRemove == false
                                            {
                                                self.viewStapper.isHidden = false
                                                self.viewAddtoCart.isHidden = true
                                            }
                                            
                                            return
                                        }
                                        else
                                        {
                                            self.viewStapper.isHidden = true
                                            self.viewAddtoCart.isHidden = false
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    if size3 == ""
                                    {
                                        self.strCartID = cart_id ?? ""
                                        self.lblQtn.text = "1"
                                        self.objQty = 1
                                        
                                        self.viewStapper.isHidden = true
                                        self.viewAddtoCart.isHidden = false
                                    }
                                    else
                                    {
                                        if self.objCheckSize == size3
                                        {
                                            self.strCartID = cart_id ?? ""
                                            self.lblQtn.text = quantity ?? ""
                                            self.objQty = Int(quantity ?? "") ?? 0
                                            
                                            if isRemove == false
                                            {
                                                self.viewStapper.isHidden = false
                                                self.viewAddtoCart.isHidden = true
                                            }
                                            
                                            return
                                        }
                                        else
                                        {
                                            self.viewStapper.isHidden = true
                                            self.viewAddtoCart.isHidden = false
                                        }
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                    else
                    {
                        self.viewStapper.isHidden = true
                        self.viewAddtoCart.isHidden = false
                    }
                    
                    
                }
            }
        }
    }
    
    func callProductDetailAPI()
    {
        let para = ["token":"54545","pid":self.pID]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(PHARMACY_P_DETAIL, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String

                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        
                        let b_url = dic?.value(forKey: "imgURL") as? String
                        
                        self.imgBase_url = b_url!
                        
                        let dicP_Detail = dic?.value(forKey: "product_details") as? NSDictionary
                        
                        let dicPData = GFCProductDetailProductDetail(fromDictionary: dicP_Detail!)
                        self.dicProductDetail = dicPData

                        if  self.dicProductDetail.images.count > 0
                        {
                            let dicData = self.dicProductDetail.images[0]
                            
                            var media_link_url = self.imgBase_url + dicData.imgName ?? ""
                            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
                            self.imgMain.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
                        }
                        self.lblProductName.text = self.dicProductDetail.name ?? ""
                        self.lblProductDescription.text = self.dicProductDetail.brandName ?? ""
                        self.lblRating.text = "\(self.dicProductDetail.rating ?? 0)"
                        self.lblDiscountedPrice.text = "\(appDelegate?.currentCountry.currency ?? "") \(self.dicProductDetail.discountedPrice ?? "")"
                        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "\(appDelegate?.currentCountry.currency ?? "") \(self.dicProductDetail.price ?? "")")
                            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
                        self.lblOriginalPrice.attributedText = attributeString
                        self.lblSize.text = "Size: \(self.dicProductDetail.size ?? "")"
                        self.lblSellerName.text = self.dicProductDetail.pharmacy ?? ""
                        
                        let dicObj = NSMutableDictionary()
                        let discountedPrice = self.dicProductDetail.discountedPrice ?? ""
                        let size = self.dicProductDetail.size ?? ""
                        let productStock = self.dicProductDetail.productStock ?? ""
                        let price = self.dicProductDetail.price ?? ""
                        dicObj.setValue(price, forKey: "price")
                        dicObj.setValue(discountedPrice, forKey: "discountedPrice")
                        dicObj.setValue(size, forKey: "size")
                        dicObj.setValue(productStock, forKey: "productStock")

                        self.arrVarient.removeAllObjects()
                        
                        if discountedPrice != "0"
                        {
                            self.objCheckSize = size
                            self.arrVarient.add(dicObj)
                        }

                        let dicObj1 = NSMutableDictionary()
                        let discountedPrice1 = self.dicProductDetail.discountedPrice1 ?? ""
                        let size1 = self.dicProductDetail.size1 ?? ""
                        let productStock1 = self.dicProductDetail.productStock1 ?? ""
                        let price1 = self.dicProductDetail.price1 ?? ""
                        dicObj1.setValue(price1, forKey: "price1")
                        dicObj1.setValue(discountedPrice1, forKey: "discountedPrice1")
                        dicObj1.setValue(size1, forKey: "size1")
                        dicObj1.setValue(productStock1, forKey: "productStock1")
                        
                        if discountedPrice1 != "0"
                        {
                            self.arrVarient.add(dicObj1)
                        }
                        
                        let dicObj2 = NSMutableDictionary()
                        let discountedPrice2 = self.dicProductDetail.discountedPrice2 ?? ""
                        let size2 = self.dicProductDetail.size2 ?? ""
                        let productStock2 = self.dicProductDetail.productStock2 ?? ""
                        let price2 = self.dicProductDetail.price2 ?? ""
                        dicObj2.setValue(price2, forKey: "price2")
                        dicObj2.setValue(discountedPrice2, forKey: "discountedPrice2")
                        dicObj2.setValue(size2, forKey: "size2")
                        dicObj2.setValue(productStock2, forKey: "productStock2")

                        if discountedPrice2 != "0"
                        {
                            self.arrVarient.add(dicObj2)
                        }
                        
                        let dicObj3 = NSMutableDictionary()
                        let discountedPrice3 = self.dicProductDetail.discountedPrice3 ?? ""
                        let size3 = self.dicProductDetail.size3 ?? ""
                        let productStock3 = self.dicProductDetail.productStock3 ?? ""
                        let price3 = self.dicProductDetail.price3 ?? ""
                        dicObj3.setValue(price3, forKey: "price3")
                        dicObj3.setValue(discountedPrice3, forKey: "discountedPrice3")
                        dicObj3.setValue(size3, forKey: "size3")
                        dicObj3.setValue(productStock3, forKey: "productStock3")

                        if discountedPrice3 != "0"
                        {
                            self.arrVarient.add(dicObj3)
                        }
                       
                        self.collViewPackageSize.reloadData()
                        self.collViewSelectImage.reloadData()
                        
                        self.callCartListAPI(isRemove: false)
                        self.tblView.reloadData()
                    }
                }
            }
        }
    }
    
}
extension ProductDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collViewSelectImage
        {
            if dicProductDetail.images != nil
            {
                return dicProductDetail.images.count
            }
            else
            {
                return 0
            }
        }
        else
        {
            return arrVarient.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collViewSelectImage
        {
            let cell = self.collViewSelectImage.dequeueReusableCell(withReuseIdentifier: "SelectImageCollViewCell", for: indexPath) as! SelectImageCollViewCell
            
            let dicData = dicProductDetail.images[indexPath.row]
            
            var media_link_url = imgBase_url + dicData.imgName ?? ""
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgProduct.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)

            if strSelectImg == indexPath.row
            {
                cell.imgProduct.layer.borderWidth = 2
                cell.imgProduct.layer.borderColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1).cgColor
            }
            else
            {
                
                cell.imgProduct.layer.borderWidth = 0
                cell.imgProduct.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1).cgColor
            }
            
            return cell
        }
        else
        {
            let cell = self.collViewPackageSize.dequeueReusableCell(withReuseIdentifier: "PackageSizeCollViewCell", for: indexPath) as! PackageSizeCollViewCell
           
       
            let dicData = arrVarient[indexPath.row] as? NSDictionary
            
            var discountedPrice = ""
            var size = ""

            if indexPath.row == 0
            {
                discountedPrice = (dicData?.value(forKey: "discountedPrice") as? String)!
                size = (dicData?.value(forKey: "productStock") as? String)!
            }
            else
            {
                discountedPrice = ((dicData?.value(forKey: "discountedPrice\(indexPath.row)") as? String)?.replacingOccurrences(of: "discountedPrice0", with: "discountedPrice"))!
                size = ((dicData?.value(forKey: "productStock\(indexPath.row)") as? String)?.replacingOccurrences(of: "productStock0", with: "productStock"))!
            }
            
            
            if indexPath.row == 0
            {
                let size77 = (dicData?.value(forKey: "size") as? String)!
                
                cell.lblSize.text = "Size: \(size77 ?? "")"
            }
            else if indexPath.row == 1
            {
                let size77 = (dicData?.value(forKey: "size1") as? String)!
                
                cell.lblSize.text = "Size: \(size77 ?? "")"
            }
            else if indexPath.row == 2
            {
                let size77 = (dicData?.value(forKey: "size2") as? String)!
                
                cell.lblSize.text = "Size: \(size77 ?? "")"
            }
            else
            {
                let size77 = (dicData?.value(forKey: "size3") as? String)!
                
                cell.lblSize.text = "Size: \(size77 ?? "")"
            }
            
            cell.lblPrice.text = "\(appDelegate?.currentCountry.currency ?? "") \(discountedPrice ?? "")"

 
            if strSelectSize == indexPath.row
            {
                cell.viewMain.layer.borderWidth = 1
                cell.viewMain.layer.borderColor = UIColor(red: 255/255, green: 155/255, blue: 55/255, alpha: 1).cgColor
                cell.viewMain.backgroundColor = UIColor(red: 255/255, green: 249/255, blue: 243/255, alpha: 1)
            }
            else
            {
                cell.viewMain.layer.borderWidth = 1
                cell.viewMain.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
                cell.viewMain.layer.borderColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1).cgColor
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collViewSelectImage
        {
            let dicData = dicProductDetail.images[indexPath.row]
            
            strSelectImg = indexPath.row
            let img = imgBase_url + dicData.imgName ?? ""
            var media_link_url = img
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            self.imgMain.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
            collViewSelectImage.reloadData()
        }
        else
        {
            strSelectSize = indexPath.row
            
            let dicV = self.arrVarient[indexPath.row] as? NSDictionary
            
            var discountedPrice = ""
            var size = ""
            var productStock = ""
            var price = ""
            
            if indexPath.row == 0
            {
                discountedPrice = dicV?.value(forKey: "discountedPrice") as? String ?? ""
                size = dicV?.value(forKey: "size") as? String ?? ""
                productStock = dicV?.value(forKey: "productStock") as? String ?? ""
                price = dicV?.value(forKey: "price") as? String ?? ""
                
                objCheckSize = size

            }
            else if indexPath.row == 1
            {
                discountedPrice = dicV?.value(forKey: "discountedPrice1") as? String ?? ""
                size = dicV?.value(forKey: "size1") as? String ?? ""
                productStock = dicV?.value(forKey: "productStock1") as? String ?? ""
                price = dicV?.value(forKey: "price1") as? String ?? ""
                
                objCheckSize = size

            }
            else if indexPath.row == 2
            {
                discountedPrice = dicV?.value(forKey: "discountedPrice2") as? String ?? ""
                size = dicV?.value(forKey: "size2") as? String ?? ""
                productStock = dicV?.value(forKey: "productStock2") as? String ?? ""
                price = dicV?.value(forKey: "price2") as? String ?? ""

                objCheckSize = size

            }
            else
            {
                discountedPrice = dicV?.value(forKey: "discountedPrice3") as? String ?? ""
                size = dicV?.value(forKey: "size3") as? String ?? ""
                productStock = dicV?.value(forKey: "productStock3") as? String ?? ""
                price = dicV?.value(forKey: "price3") as? String ?? ""
                
                objCheckSize = size

            }
 
            self.lblDiscountedPrice.text = "\(appDelegate?.currentCountry.currency ?? "") \(discountedPrice ?? "")"
            let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "\(appDelegate?.currentCountry.currency ?? "") \(price ?? "")")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
            self.lblOriginalPrice.attributedText = attributeString
            self.lblSize.text = "Size: \(size ?? "")"
            
            collViewPackageSize.reloadData()
            
            self.callCartListAPI(isRemove: false)
        }
    }
    
}

extension ProductDetailVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "ProductDetailCell") as! ProductDetailCell
        
        cell.lblDetaiol.text = self.dicProductDetail.descriptionField ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

class SelectImageCollViewCell: UICollectionViewCell{
    @IBOutlet weak var imgProduct: UIImageView!
}

class PackageSizeCollViewCell: UICollectionViewCell{
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblSize: UILabel!
}

class ProductDetailCell: UITableViewCell
{
    @IBOutlet weak var lblDetaiol: UILabel!
}

extension String {
   func maxLength(length: Int) -> String {
       var str = self
       let nsString = str as NSString
       if nsString.length >= length {
           str = nsString.substring(with:
               NSRange(
                location: 0,
                length: nsString.length > length ? length : nsString.length)
           )
       }
       return  str
   }
}

