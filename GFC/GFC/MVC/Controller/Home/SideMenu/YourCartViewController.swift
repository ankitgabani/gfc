//
//  YourCartViewController.swift
//  GFC
//
//  Created by Parth Anghan on 25/10/21.
//

import UIKit
import Quickblox
class YourCartViewController: UIViewController {

    @IBOutlet weak var imgBG: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imgBG.isUserInteractionEnabled = true
        imgBG.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "checkoutViewController") as! checkoutViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
