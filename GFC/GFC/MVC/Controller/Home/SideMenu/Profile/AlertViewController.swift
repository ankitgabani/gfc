//
//  AlertViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 17/05/22.
//

import UIKit
import Quickblox
class AlertViewController: UIViewController {

    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var lblTSuccessful: UILabel!
    
    @IBOutlet weak var btnTOk: UIButton!
    
    var strMessage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblMessage.text = strMessage

        self.navigationController?.navigationBar.isHidden = true
        
        if strMessage == "Pateint Details Save Successfully"
        {
            appDelegate?.setUpSideMenu()
        }
        
    }
    
    
    @IBAction func btnOkClicked(_ sender: Any) {
        if strMessage == "Test has been booked successfully."
        {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LabTestVC") as? LabTestVC {
                
                let viewcontrollers = self.navigationController?.viewControllers
                var isExist = false
                for viewcontroller in viewcontrollers! {
                    if viewcontroller.isKind(of: LabTestVC.self) {
                        isExist = true
                        break
                    }
                }
                if isExist == true {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
                    self.navigationController?.popToViewController(controller, animated: true)
                }
            }
        }
        else if strMessage == "Send Query Uploaded Successfully!"
        {
            appDelegate?.setUpSideMenu()
        }
        else
        {
            self.dismiss(animated: true)
        }
        
    }
    
}
