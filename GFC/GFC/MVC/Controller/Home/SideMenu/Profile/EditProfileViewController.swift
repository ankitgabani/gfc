//
//  EditProfileViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 17/05/22.
//

import UIKit
import ADCountryPicker

class EditProfileViewController: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    @IBOutlet weak var lblCode: UITextField!
    
    @IBOutlet weak var lblTEdiProfile: UILabel!
    
    @IBOutlet weak var btnTSubmit: UIButton!
    @IBOutlet weak var btnTCancel: UIButton!
    
    var strName = ""
    
    var strEmail = ""
    
    var strNumber = ""
    
    var delegate:PizzaDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTEdiProfile.text = "Edit Profile".localizeString(string: Language.shared.currentAppLang)
        
        btnTSubmit.setTitle("Submit".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnTCancel.setTitle("Cancel".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        if Language.shared.isArabic == false{
            txtName.textAlignment = .left
            txtEmailAddress.textAlignment = .left
            txtPhoneNumber.textAlignment = .left
        }else{
            txtName.textAlignment = .right
            txtEmailAddress.textAlignment = .right
            txtPhoneNumber.textAlignment = .right
        }
        
        lblCode.text = "+\(appDelegate?.dicUserLoginData.country_code ?? "")"
        txtName.text = strName
        txtEmailAddress.text = strEmail
        txtPhoneNumber.text = appDelegate?.dicUserLoginData.mobileNumber ?? ""
    }
    @IBAction func clickedPicker(_ sender: Any) {
        self.view.endEditing(true)
        let picker = ADCountryPicker()
        // delegate
        picker.delegate = self
        
        // Display calling codes
        picker.showCallingCodes = true
        
        // or closure
        picker.didSelectCountryClosure = { name, code in
            _ = picker.navigationController?.popToRootViewController(animated: true)
            print(code)
        }
        
        // Use this below code to present the picker
        
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
    }
    
    
    @IBAction func btnCancelClickdd(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        editProfileAPI()
    }
    
    func editProfileAPI()
    {
        APIClient.sharedInstance.showIndicator()
        
        let coding = lblCode.text?.replacingOccurrences(of: "+", with: "")
        
        let param = ["patientId":appDelegate?.dicUserLoginData.id ?? "" , "name":self.txtName.text ?? "", "mobileNumber":self.txtPhoneNumber.text ?? "", "email":self.txtEmailAddress.text ?? "","country_code":lblCode.text ?? ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(EDIT_PROFILE, parameters: param as [String : Any]) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        
                        appDelegate?.dicUserLoginData.name = self.txtName.text ?? ""
                        appDelegate?.dicUserLoginData.email = self.txtEmailAddress.text ?? ""
                        appDelegate?.dicUserLoginData.mobileNumber = self.txtPhoneNumber.text ?? ""

                        let pro = appDelegate?.getCurrentUserData()
                        pro?.name = self.txtName.text ?? ""
                        pro?.email = self.txtEmailAddress.text ?? ""
                        pro?.mobileNumber = self.txtPhoneNumber.text ?? ""
                        
                        appDelegate?.saveCurrentUserData(dic: pro!)
                        
                        self.delegate?.onPizzaReady(type: "") // Delegate
                        
                        NotificationCenter.default.post(name: Notification.Name.myNotificationKeyProfile, object: nil, userInfo:["text": ""]) // Notification
                        
                        let alert = UIAlertController(title: "successful!".localizeString(string: Language.shared.currentAppLang), message: "Profile updated successfully.".localizeString(string: Language.shared.currentAppLang), preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: "OK".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { acrtion in
                            self.dismiss(animated: true)
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                     
                    }
                    else
                    {
                        let message = response?.value(forKey: "message") as? String

                        self.view.makeToast(message)
                    }
                }
                else
                {
                    let message = response?.value(forKey: "message") as? String
                    
                    self.view.makeToast(message)
                }
            }
        }
    }
}

extension EditProfileViewController: ADCountryPickerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String)
    {
        _ = picker.navigationController?.popToRootViewController(animated: true)
        
        self.lblCode.text = dialCode
        
        self.dismiss(animated: true, completion: nil)
    }
}
