//
//  AddressListViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 17/05/22.
//

import UIKit
import Quickblox
class AddressListViewController: UIViewController {
    
    @IBOutlet weak var addressTblView: UITableView!
    
    @IBOutlet weak var viewNoData: UIView!
    
    @IBOutlet weak var lblTAddressList: UILabel!
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var btnTAddNewAddress: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var arrAddressList: [GFCAddressListData] = [GFCAddressListData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTAddressList.text = "Address List".localizeString(string: Language.shared.currentAppLang)
        lblTNoData.text = "No Address Found".localizeString(string: Language.shared.currentAppLang)
        
        btnTAddNewAddress.setTitle("Add New Address".localizeString(string: Language.shared.currentAppLang), for: .normal)

        viewNoData.isHidden = true
        
        addressTblView.delegate = self
        addressTblView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callGetADDressListAPI()
    }
    
    @IBAction func btnAddAddressClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        vc.isBookLab = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func clickedEdit(_ sender : UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewAddressViewController") as! AddNewAddressViewController
        vc.strTitle = "Update Address".localizeString(string: Language.shared.currentAppLang)
        vc.dicData = arrAddressList[sender.tag]
        vc.isUpdate = true
        vc.isBookLab = false
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func clickedDelete(_ sender : UIButton)
    {
        let alert = UIAlertController(title: "WARNING!".localizeString(string: Language.shared.currentAppLang), message: "Are you sure you want to delete this address?".localizeString(string: Language.shared.currentAppLang), preferredStyle: .alert)
        let no = UIAlertAction(title: "No".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { action in
            
        })
        let yes = UIAlertAction(title: "Yes".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { action in
            
            self.callDeleteAddressListAPI(addressId: self.arrAddressList[sender.tag].id ?? "")
            
        })
        alert.addAction(no)
        alert.addAction(yes)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
    }
    
    
    func callDeleteAddressListAPI(addressId: String){
        APIClient.sharedInstance.showIndicator()
        
        let param = ["address_id":addressId,"token":"12345"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(DALETE_ADDRESS, parameters: param){ response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    self.callGetADDressListAPI()
                }
            }
            
        }
    }
    
    func callGetADDressListAPI(){
        APIClient.sharedInstance.showIndicator()
        
        let param = ["":""]
        
        print(param)
        
        let _url = "patientId=\(appDelegate?.dicUserLoginData.id ?? "")"
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_ADDRESS_LIST + _url, parameters: param){ response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                APIClient.sharedInstance.hideIndicator()
                
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    self.arrAddressList.removeAll()
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as! NSArray
                        
                        for obj in arrData
                        {
                            let dicData = GFCAddressListData(fromDictionary: obj as! NSDictionary)
                            self.arrAddressList.append(dicData)
                        }
                    }
                    
                    self.arrAddressList = self.arrAddressList.reversed()
                    
                    if self.arrAddressList.count > 0
                    {
                        self.viewNoData.isHidden = true
                        self.addressTblView.isHidden = false
                    }
                    else
                    {
                        self.viewNoData.isHidden = false
                        self.addressTblView.isHidden = true
                    }
                    
                    self.addressTblView.reloadData()
                }
            }
            
        }
    }
}

class AddressTblViewCell: UITableViewCell{
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblTPhone: UILabel!
    @IBOutlet weak var lblTAddress: UILabel!
    
    
}

extension AddressListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.addressTblView.dequeueReusableCell(withIdentifier: "AddressTblViewCell") as! AddressTblViewCell
        
        let dicData = arrAddressList[indexPath.row]
        
        cell.lblTPhone.text = "Phone".localizeString(string: Language.shared.currentAppLang)
        cell.lblTAddress.text = "ADDRESS".localizeString(string: Language.shared.currentAppLang)
        
        cell.btnEdit.setTitle("Edit".localizeString(string: Language.shared.currentAppLang), for: .normal)
        cell.btnDelete.setTitle("Delete".localizeString(string: Language.shared.currentAppLang), for: .normal)

        if dicData.title == "home"
        {
            cell.lblType.text = "Home".localizeString(string: Language.shared.currentAppLang)
        }
        else
        {
            cell.lblType.text = "Office".localizeString(string: Language.shared.currentAppLang)
        }

        cell.lblPhoneNumber.text = dicData.mobile ?? ""
        cell.lblName.text = dicData.name ?? ""
        
        cell.lblAddress.text = "\(dicData.address ?? "") \(dicData.city ?? "") \(dicData.state ?? "") \(dicData.pincode ?? ""), \(dicData.locality ?? ""), \(dicData.country ?? "")"
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(clickedEdit(_:)), for: .touchUpInside)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(clickedDelete(_:)), for: .touchUpInside)
              
        return cell
    }
   
    
}
