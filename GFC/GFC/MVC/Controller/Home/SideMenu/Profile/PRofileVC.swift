//
//  GFC
//
//  Created by Parth Anghan on 25/10/21.
//

import UIKit
import MobileCoreServices
import SDWebImage

protocol PizzaDelegate
{
    func onPizzaReady(type: String)
}

extension Notification.Name {
    
    public static let myNotificationKeyProfile = Notification.Name(rawValue: "myNotificationKeyProfile")
}


class PRofileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,PizzaDelegate {
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtContactNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblTSetUpProfile: UILabel!
    @IBOutlet weak var lblTProfileDes: UILabel!
    @IBOutlet weak var lblTPersonalInformation: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    var imagePicker: ImagePicker!
    var selectedImg: String?
    
    var imagePickerNew = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTSetUpProfile.text = "Set up your profile".localizeString(string: Language.shared.currentAppLang)
        lblTProfileDes.text = "Update your profile to connect your doctor with\nbetter impression.".localizeString(string: Language.shared.currentAppLang)
        lblTPersonalInformation.text = "Personal information".localizeString(string: Language.shared.currentAppLang)
        lblName.text = "Name".localizeString(string: Language.shared.currentAppLang)
        lblContactNumber.text = "Contact Number".localizeString(string: Language.shared.currentAppLang)
        lblEmail.text = "Email".localizeString(string: Language.shared.currentAppLang)
        
        txtLocation.text = "Manage Address".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            txtFirstName.textAlignment = .left
            txtContactNumber.textAlignment = .left
            txtEmail.textAlignment = .left
            txtLocation.textAlignment = .left
        }else{
            txtFirstName.textAlignment = .right
            txtContactNumber.textAlignment = .right
            txtEmail.textAlignment = .right
            txtLocation.textAlignment = .right
        }
        
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 30
        topView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        txtFirstName.text = appDelegate?.dicUserLoginData.name ?? ""
        txtContactNumber.text = "+\(appDelegate?.dicUserLoginData.country_code ?? "") \(appDelegate?.dicUserLoginData.mobileNumber ?? "")"
        txtEmail.text = appDelegate?.dicUserLoginData.email ?? ""
        
        let url = URL(string: appDelegate?.dicUserLoginData.profileImage ?? "")
        imgUser.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "user"))

    }
    
    func onPizzaReady(type: String)
    {
        txtFirstName.text = appDelegate?.dicUserLoginData.name ?? ""
        txtContactNumber.text = "+\(appDelegate?.dicUserLoginData.country_code ?? "") \(appDelegate?.dicUserLoginData.mobileNumber ?? "")"
        txtEmail.text = appDelegate?.dicUserLoginData.email ?? ""
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePickerNew.delegate = self
            imagePickerNew.sourceType = UIImagePickerController.SourceType.camera
            imagePickerNew.allowsEditing = true
            imagePickerNew.mediaTypes = [kUTTypeImage as String]
            self.present(imagePickerNew, animated: true, completion: nil)
        }
        else
        {
            
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("You don't have camera")
        }
    }
    
    func openGallary()
    {
        
        imagePickerNew.delegate = self
        imagePickerNew.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePickerNew.allowsEditing = true
        imagePickerNew.mediaTypes = ["public.image"]
        self.present(imagePickerNew, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        {
            let imageurl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            selectedImg = imageurl?.lastPathComponent
            DispatchQueue.main.async {
                self.imgUser.image = image
                self.myImageUploadRequest(imageToUpload: image, imgKey: "profileImage")
            }
            
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            let imageurl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            
            selectedImg = imageurl?.lastPathComponent
            
            DispatchQueue.main.async {
                self.imgUser.image = image
                self.myImageUploadRequest(imageToUpload: image, imgKey: "profileImage")
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedChageImage(_ sender: Any) {
        
        let alert1 = UIAlertController(title: "Choose Photo".localizeString(string: Language.shared.currentAppLang), message: nil, preferredStyle: .actionSheet)
        
        alert1.addAction(UIAlertAction(title: "Camera".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert1.addAction(UIAlertAction(title: "Gallery".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert1.addAction(UIAlertAction.init(title: "Cancel".localizeString(string: Language.shared.currentAppLang), style: .cancel, handler: nil))
        
        self.present(alert1, animated: true, completion: nil)
        
        
    }
    
    @IBAction func onBtnEditName(_ sender: UIButton){
        
    }
    
    @IBAction func onBtnEditContactNumber(_ sender: UIButton){
        
    }
    
    @IBAction func onBtnEditEmail(_ sender: UIButton){
        
    }
    
    @IBAction func btnEditProfileClicked(_ sender: Any) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        controller.strName = txtFirstName.text ?? ""
        controller.strEmail = txtEmail.text ?? ""
        controller.strNumber = txtContactNumber.text ?? ""
        controller.delegate = self
        self.present(controller, animated: true)
        
    }
    
    @IBAction func onBtnEditLocation(_ sender: UIButton){
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    func myImageUploadRequest(imageToUpload: UIImage, imgKey: String) {
        
        APIClient.sharedInstance.showIndicator()
        
        let myUrl = NSURL(string: BASE_URL + EDIT_PROFILE_IMAGE);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let params = ["patientId": "\(appDelegate?.dicUserLoginData.id ?? "")","token":"123456"]
        
        let boundary = generateBoundaryString()
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let imageData = imageToUpload.jpegData(compressionQuality: 1)
        if imageData == nil  {
            return
        }
        
        request.httpBody = createBodyWithParameters(parameters: params, filePathKey: imgKey, imageDataKey: imageData! as NSData, boundary: boundary, imgKey: imgKey) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            APIClient.sharedInstance.hideIndicator()
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            
            // print reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("response data = \(responseString!)")
            
            do{
                
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                    
                    // try to read out a string array
                    if let dicData = json["data"] as? NSDictionary
                    {
                        DispatchQueue.main.async {
                            
                            let profileImage = dicData.value(forKey: "profileImage") as? String
                            
                            appDelegate?.dicUserLoginData.profileImage = profileImage ?? ""

                            let pro = appDelegate?.getCurrentUserData()
                            pro?.profileImage = profileImage ?? ""
                            appDelegate?.saveCurrentUserData(dic: pro!)
                            
                            NotificationCenter.default.post(name: Notification.Name.myNotificationKeyProfile, object: nil, userInfo:["text": ""]) // Notification

                            let alert = UIAlertController(title: "Successful!", message: "Profile Photo Updated Successfully", preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { acrtion in
                                self.dismiss(animated: true)
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }
                
            }catch{
                
            }
            
        }
        
        task.resume()
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String, imgKey: String) -> NSData {
        
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "\(imgKey).jpg"
        let mimetype = "image/jpeg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\("profileImage")\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func randomString(of length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var s = ""
        for _ in 0 ..< length {
            s.append(letters.randomElement()!)
        }
        return s
    }
}


extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
