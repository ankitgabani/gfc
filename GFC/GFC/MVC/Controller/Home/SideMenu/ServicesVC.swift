//
//  ServicesVC.swift
//  GFC Globalhealth
//
//  Created by Code on 01/03/23.
//

import UIKit
import SDWebImage

class ServicesVC: UIViewController {

    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var servicesTblView: UITableView!
    @IBOutlet weak var viewPopUpMain: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgprofile: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var btnTClose: UIButton!
    
    @IBOutlet weak var lblTService: UILabel!
    
    var arrService : [GFCServicesData] = [GFCServicesData]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        servicesTblView.delegate = self
        servicesTblView.dataSource = self
        
        viewBG.isHidden = true
        viewPopUp.isHidden = true
        viewPopUpMain.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 18, spread: 0)
        
        servicesListAPI()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelegate?.setUpSideMenu()
        
    }
    @IBAction func clickedClose(_ sender: Any) {
        viewBG.isHidden = true
        viewPopUp.isHidden = true
    }
    
    func servicesListAPI(){
        APIClient.sharedInstance.showIndicator()
        
        let param = ["":""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_SERVIES, parameters: param) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let arrData = response?.value(forKey: "data") as? NSArray
                    
                    for obj in arrData!
                    {
                        let dicData = GFCServicesData(fromDictionary: obj as! NSDictionary)
                        self.arrService.append(dicData)
                    }
                    self.servicesTblView.reloadData()
                }
            }
            
        }
    }
    
    

}
extension ServicesVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrService.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.servicesTblView.dequeueReusableCell(withIdentifier: "ServicesTblViewCell") as! ServicesTblViewCell
        cell.viewMain.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 18, spread: 0)
        
        let dicData = arrService[indexPath.row]
        
        cell.lbltitle.text = dicData.serviceName ?? ""
        cell.lblDescription.text = dicData.serviceDescription ?? ""
        
        let url = URL(string: dicData.image ?? "")
        cell.imgService.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgService.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewBG.isHidden = false
        viewPopUp.isHidden = false
      
        let dicData = arrService[indexPath.row]
        self.lblTitle.text = dicData.serviceName ?? ""
        self.lblDescription.text = dicData.serviceDescription ?? ""
        
        let url = URL(string: dicData.image ?? "")
        self.imgprofile.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgprofile.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}

class ServicesTblViewCell: UITableViewCell{
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
}
