//
//  SuccessPopUpVC.swift
//  GFC Globalhealth
//
//  Created by macOS on 01/03/23.
//

import UIKit
import Quickblox
class SuccessPopUpVC: UIViewController {

    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnTOk: UIButton!
    
    var strTitle = ""
    
    var isMedical = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = "Successful!".localizeString(string: Language.shared.currentAppLang)
        
        btnTOk.setTitle("OK".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        lblSubTitle.text = strTitle.localizeString(string: Language.shared.currentAppLang)
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedOK(_ sender: Any)
    {
        if isMedical == true
        {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let home = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentsListViewController") as! AppointmentsListViewController
            let homeNavigation = UINavigationController(rootViewController: home)
            homeNavigation.isNavigationBarHidden = true
            appDelegate?.window?.rootViewController = homeNavigation
            appDelegate?.window?.makeKeyAndVisible()
        }
        else
        {
            appDelegate?.setUpSideMenu()
        }
    }
}
