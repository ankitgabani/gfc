//
//  FeatureDoctorCollectionCell.swift
//  GFC
//
//  Created by Parth Anghan on 21/10/21.
//

import UIKit
import Quickblox
class FeatureDoctorCollectionCell: UICollectionViewCell {
    @IBOutlet weak var viewMian: UIView!
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var drImg: UIImageView!
    @IBOutlet weak var drName: UILabel!
    
    @IBOutlet weak var drCharge: UILabel!
}
