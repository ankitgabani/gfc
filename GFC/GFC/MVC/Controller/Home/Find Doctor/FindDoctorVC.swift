//
//  FindDoctorVC.swift
//  GFC
//
//  Created by Gabani M1 on 22/10/21.
//

import UIKit
import DropDown
import SDWebImage

class FindDoctorVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgCancel: UIImageView!
    @IBOutlet weak var btnCancel: UIButton!

    @IBOutlet weak var txtSpecial: UITextField!
    @IBOutlet weak var viewBG: UIView!
    
    @IBOutlet weak var lblTSpecial: UILabel!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var lblTFindDoc: UILabel!
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var txtCity: UITextField!
    
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var llCity: UILabel!
    
     var strCity = ""
     var strCityId = ""
    
    var dropDown = DropDown()
    var dropDownCountry = DropDown()
    var dropDownCity = DropDown()
    
    var arrCountryList: [GFCCountruListData] = [GFCCountruListData]()
    var arrGetCityList: [GFCGetCityListData] = [GFCGetCityListData]()
    
    var arrFindDoc: [GFCDoctorDetailsData] = [GFCDoctorDetailsData]()
    
    var arrSpecialityByDoctor: [GFCSpecialityByDoctorData] = [GFCSpecialityByDoctorData]()
    
    var arrFindDoctor: [GFCDoctorSearchDoctorList] = [GFCDoctorSearchDoctorList]()
    
    var strCountry = ""
    
    var isOpenFilter = false
    
    var strSpeId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTFindDoc.text = "Find Doctors".localizeString(string: Language.shared.currentAppLang)
        lblTSpecial.text = "Country".localizeString(string: Language.shared.currentAppLang)
        llCity.text = "City".localizeString(string: Language.shared.currentAppLang)
        
        lblNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)

        txtSearch.placeholder = "Search.....".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            txtSearch.textAlignment = .left
        }else{
            txtSearch.textAlignment = .right
        }
        
        tblView.delegate = self
        tblView.dataSource = self
        
        if txtSearch.text == ""
        {
            imgCancel.isHidden = true
        }
        else
        {
            imgCancel.isHidden = false
        }

        self.txtSearch.delegate = self
        self.txtSearch.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
        
        viewBG.isHidden = true
        
        txtSpecial.text = "Select"
         
        callCountryAPI()
    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField)
    {
        isOpenFilter = false
        if textfield.text?.count == 0
        {
            tblView.reloadData()
        }
        else
        {
            callSearchAPI(country: appDelegate?.currentCountry.countryId ?? "", keyword: textfield.text ?? "")
        }
       
    }
    
    @IBAction func clickedClear(_ sender: Any) {
        txtSearch.text = ""
        imgCancel.isHidden = true
        btnCancel.isHidden = true
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedOpenFilter(_ sender: Any) {
        viewBG.isHidden = false
        isOpenFilter = true
    }
    
    @IBAction func clickedS(_ sender: Any) {
        dropDownCountry.show()
    }
    @IBAction func clickeddCity(_ sender: Any) {
        dropDownCity.show()
    }
    
    
    @IBAction func clickedAppyFilter(_ sender: Any) {
        viewBG.isHidden = true
        
        callFindDoctorAPI(city: self.strCity)
    }
    @IBAction func clickedClose(_ sender: Any) {
        viewBG.isHidden = true
    }
    
    func setDropDownCounty()
    {
        let arrCountry = NSMutableArray()
        
        for obj in arrCountryList
        {
            arrCountry.add(obj.country ?? "")
        }
        
        dropDownCountry.dataSource = arrCountry as! [String]
        dropDownCountry.anchorView = txtSpecial
        dropDownCountry.direction = .bottom
        
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtSpecial.text = item
            
            for obj in arrCountryList
            {
                if item == obj.country ?? ""
                {
                    self.strSpeId = obj.id ?? ""
                    
                    callCityAPI(country_id: obj.id ?? "")
                }
            }
           
        }
        
        dropDownCountry.bottomOffset = CGPoint(x: 0, y: txtSpecial.bounds.height)
        dropDownCountry.topOffset = CGPoint(x: 0, y: -txtSpecial.bounds.height)
        dropDownCountry.dismissMode = .onTap
        dropDownCountry.textColor = UIColor.darkGray
        dropDownCountry.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCountry.selectionBackgroundColor = UIColor.clear
        dropDownCountry.selectedTextColor = .black
        dropDownCountry.reloadAllComponents()
    }
    
    func setDropDownCity()
    {
        let arrCity = NSMutableArray()
        arrCity.add("Select city")
        for obj in arrGetCityList
        {
            arrCity.add(obj.city ?? "")
        }
        
        dropDownCity.dataSource = arrCity as! [String]
        dropDownCity.anchorView = txtCity
        dropDownCity.direction = .bottom
        
        dropDownCity.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCity.text = item
            
            for obj in arrGetCityList
            {
                if item == obj.city ?? ""
                {
                    self.strCity = obj.city ?? ""
                    self.strCityId = obj.id ?? ""
                }
                else if item == "Select city"
                {
                    self.strCity = ""
                    self.strCityId = ""
                }
                
            }
           
        }
        
        dropDownCity.bottomOffset = CGPoint(x: 0, y: txtCity.bounds.height)
        dropDownCity.topOffset = CGPoint(x: 0, y: -txtCity.bounds.height)
        dropDownCity.dismissMode = .onTap
        dropDownCity.textColor = UIColor.darkGray
        dropDownCity.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCity.selectionBackgroundColor = UIColor.clear
        dropDownCity.selectedTextColor = .black
        dropDownCity.reloadAllComponents()
    }
    
    func callSpecialityByDoctor(specilization_id:String)
    {
        APIClient.sharedInstance.showIndicator()
        
        let para = ["specilization_id":specilization_id]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_SPECIALITYBY_DOC, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    self.arrSpecialityByDoctor.removeAll()
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                       
                        for objSpecialityDoc in arrData!
                        {
                            let dicSpecialityByDoctor = GFCSpecialityByDoctorData(fromDictionary: objSpecialityDoc as! NSDictionary)
                            self.arrSpecialityByDoctor.append(dicSpecialityByDoctor)
                        }
                        
                        self.tblView.reloadData()
                    }
                }
            }
        }
    }
    
    func callCountryAPI()
    {
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY_LIST, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCountryList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCountruListData(fromDictionary: obj as! NSDictionary)
                            self.arrCountryList.append(dicData)
                        }
                        
                        if self.arrCountryList.count > 0
                        {
                            self.txtSpecial.text = self.arrCountryList[0].country ?? ""

                            self.callCityAPI(country_id: self.arrCountryList[0].id ?? "")
                            
                            self.strSpeId = self.arrCountryList[0].id ?? ""
                            
                            self.callFindDoctorAPI(city: "")
                        }
                        
                        
                        self.setDropDownCounty()
                        self.setDropDownCity()
                    }
                    else
                    {
                        
                    }
                }
            }
        }
    }
    
    func callCityAPI(country_id: String)
    {
        let para = ["country_id":country_id]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_CITY_LIST, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        self.arrGetCityList.removeAll()
                        
                        for obj in arrData!
                        {
                            let dicData = GFCGetCityListData(fromDictionary: obj as! NSDictionary)
                            
                            if self.strSpeId == dicData.countryId
                            {
                                self.arrGetCityList.append(dicData)
                            }
                        }
                       
                        self.setDropDownCity()
                    }
                    else
                    {
                        
                    }
                }
            }
        }
    }
    
    func callFindDoctorAPI(city: String)
    {
        APIClient.sharedInstance.showIndicator()
        
        let para = ["city":city]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_DOCTOR_LIST, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    self.arrFindDoctor.removeAll()
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        for obj in arrData!
                        {
                            let dicSpeciality = GFCDoctorSearchDoctorList(fromDictionary: obj as! NSDictionary)
                            
                            if city == ""
                            {
                                
                                if self.strSpeId == dicSpeciality.countryId
                                {
                                    self.arrFindDoctor.append(dicSpeciality)
                                }
                            }
                            else
                            {
                                if self.strCityId == dicSpeciality.cityId
                                {
                                    self.arrFindDoctor.append(dicSpeciality)
                                }

                            }
                        }
                        
                        self.viewNoData.isHidden = true
                        self.tblView.isHidden = false
                        self.tblView.reloadData()
                    }
                    else
                    {
                        self.viewNoData.isHidden = false
                        self.tblView.isHidden = true
                        self.tblView.reloadData()
                    }
                }
            }
        }
    }
    
    func callSearchAPI(country:String,keyword: String)
    {
   //     APIClient.sharedInstance.showIndicator()
        
//        let para = ["keyword":keyword,"country":country]
        
        let para = ["doctor_name":keyword]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_DOCTOR_LIST, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrFindDoctor.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCDoctorSearchDoctorList(fromDictionary: obj as! NSDictionary)
                            self.arrFindDoctor.append(dicData)
                        }
                        self.viewNoData.isHidden = true
                        self.tblView.reloadData()
                    }
                    else
                    {
                        self.viewNoData.isHidden = false
                        self.tblView.isHidden = true
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isOpenFilter == true
        {
            return arrFindDoctor.count
        }
        else
        {
            return arrFindDoctor.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "FindDoctorsCell") as! FindDoctorsCell
        
        let dicData = arrFindDoctor[indexPath.row]
        
        cell.lblName.text = dicData.doctorName ?? ""
        cell.lblType.text = dicData.specializationName ?? ""
        cell.lblPrice.text = "Rs.\(dicData.consultantCharge ?? "")"
        if dicData.qualification == ""{
            cell.lblegree.text = "N/A"
        }else{
            cell.lblegree.text = dicData.qualification ?? ""
        }
        
        let url = URL(string: "https://gfcglobalhealth.com/software/assets/uploads/doctor_profile/\(dicData.image ?? "")")
        cell.imgPic.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPic.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
        
        cell.btnBook.tag = indexPath.row
        cell.btnBook.addTarget(self, action: #selector(clickedBook(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func clickedBook(_ sender: UIButton)
    {
        let dicData = arrFindDoctor[sender.tag]
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorDetailsVC") as! DoctorDetailsVC
        vc.strDoctor_id = dicData.id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
