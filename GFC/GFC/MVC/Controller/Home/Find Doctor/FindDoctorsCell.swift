//
//  FindDoctorsCell.swift
//  GFC
//
//  Created by Gabani M1 on 22/10/21.
//

import UIKit
import Quickblox
class FindDoctorsCell: UITableViewCell {

    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblegree: UILabel!
    @IBOutlet weak var btnBook: UIButton!
    
    @IBOutlet weak var lblTConsultationFee: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
