//
//  DoctorDetailsVC.swift
//  GFC
//
//  Created by Gabani M1 on 22/10/21.
//

import UIKit
import HCSStarRatingView
import SDWebImage

class DoctorDetailsVC: UIViewController {
    
    @IBOutlet weak var lblqualification: UILabel!
    @IBOutlet weak var imgDr: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblspecialization_name: UILabel!
    @IBOutlet weak var lblconsultant_charge: UILabel!
    
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblDrPhone: UILabel!
    
    @IBOutlet weak var lblHospitalnmae: UILabel!
    
    @IBOutlet weak var viewASlots: NSLayoutConstraint!
    
    @IBOutlet weak var lblDes: UILabel!
    
    @IBOutlet weak var tblViewMain: UIView! // 540
    
    @IBOutlet weak var collectioViewTime: UICollectionView!
    @IBOutlet weak var collectionViewSlots: UICollectionView!
    @IBOutlet weak var lblNoA: UILabel!
    
    @IBOutlet weak var lblTConsultationFe: UILabel!
    @IBOutlet weak var lblTAddress: UILabel!
    @IBOutlet weak var lblTDoctorDetail: UILabel!
    @IBOutlet weak var lblTAvailableSlots: UILabel!
    @IBOutlet weak var lblTDescriptiom: UILabel!
    
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var btnTBookAppointment: UIButton!
    @IBOutlet weak var btnTSentQuery: UIButton!
   
    @IBOutlet weak var lblDoctorNumber: UILabel!
    @IBOutlet weak var lblHospitalName: UILabel!
    
    @IBOutlet weak var scrollMainView: UIScrollView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    @IBOutlet weak var lblTSelectDate: UILabel!
    @IBOutlet weak var viewMainSelectDate: UIView!
    @IBOutlet weak var viewMainSlots: UIView!
    
    @IBOutlet weak var btnProceed: UIButton!
    
    
    var flowLayoutTime: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: 130, height: 54)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _flowLayout.minimumInteritemSpacing = 0.0
            _flowLayout.minimumLineSpacing = 15
        }
        
        return _flowLayout
    }
    
    let sectionInsetsCat = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    let itemsPerRowCat: CGFloat = 2
    
    var flowLayoutLiveSlot: UICollectionViewFlowLayout {
        let _SlotflowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.sectionInsetsCat.left * (self.itemsPerRowCat + 1)
            let availableWidth = self.collectionViewSlots.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.itemsPerRowCat
            
            _SlotflowLayout.itemSize = CGSize(width: widthPerItem, height: 40)
            
            _SlotflowLayout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
            _SlotflowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
            _SlotflowLayout.minimumInteritemSpacing = 8
            _SlotflowLayout.minimumLineSpacing = 8
        }
        
        // edit properties here
        return _SlotflowLayout
    }
    
    var dicDrDetail = GFCDoctorDetailsData()
    
    var strDoctor_id = ""
    var strDoctor_Name = ""

    var strSelectedDay = 0
    var strSelectedTimeSlot = -1
    
    var strSelectDate = ""
    var strSelectTime = ""
    
    var arrTimeSlots: [GFCTimeSlotData] = [GFCTimeSlotData]()
    
    var objSEHieght = 0
    
    var strDate12 = ""
    
    var strClickedType = 0
    
     
    override func viewDidLoad() {
        super.viewDidLoad()
        
        strClickedType = 1
        
        view1.layer.borderColor = CGColor(red: 155/255, green: 192/255, blue: 60/255, alpha: 1)
        view2.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        view3.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        
        lblTSelectDate.isHidden = false
        viewMainSelectDate.isHidden = false
        lblTAvailableSlots.isHidden = false
        viewMainSlots.isHidden = false
        lblTDescriptiom.isHidden = false
        lblDes.isHidden = false
        
//        view1.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.19), alpha: 1, x: 1, y: 1, blur: 5, spread: 0)
//        view2.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.19), alpha: 1, x: 1, y: 1, blur: 5, spread: 0)
//        view3.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.19), alpha: 1, x: 1, y: 1, blur: 5, spread: 0)
      
        
        lblTDoctorDetail.text = "Doctor Details".localizeString(string: Language.shared.currentAppLang)
        lblTAvailableSlots.text = "Available Slots".localizeString(string: Language.shared.currentAppLang)
        lblTDescriptiom.text = "Description".localizeString(string: Language.shared.currentAppLang)
        lblTAddress.text = "Address".localizeString(string: Language.shared.currentAppLang)
        lblTConsultationFe.text = "Consultation Fee".localizeString(string: Language.shared.currentAppLang)
        lblNoA.text = "No Slots Available".localizeString(string: Language.shared.currentAppLang)
        
        btnTBookAppointment.setTitle("".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnTSentQuery.setTitle("".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        self.tblViewMain.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
        
        collectioViewTime.delegate = self
        collectioViewTime.dataSource = self

        DispatchQueue.main.async {
            
            if Language.shared.isArabic == false{
                self.btnBack.setImage(UIImage(named: "Group"), for: .normal)
               // self.collectioViewTime.semanticContentAttribute = .forceRightToLeft
            }else{
                self.btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
             //   self.collectioViewTime.semanticContentAttribute = .forceLeftToRight
            }
            
            self.collectioViewTime.reloadData()
        }
       
        // collectioViewTime.collectionViewLayout = flowLayoutTime
        
        collectionViewSlots.delegate = self
        collectionViewSlots.dataSource = self
        collectionViewSlots.collectionViewLayout = flowLayoutLiveSlot
        
        APIClient.sharedInstance.showIndicator()
        let seconds = 1.5
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            APIClient.sharedInstance.hideIndicator()
            // Put your code which should be executed with a delay here
        }
        
        callDoctorDetailsDataApi()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let date1 = formatter.date(from: Date.getCurrentDate())
        
        let Dform = DateFormatter()
        Dform.dateFormat = "EEEE"
        let strDate = Dform.string(from: date1!)
        
        let formatterAPI = DateFormatter()
        formatterAPI.dateFormat = "-MM-dd"
        let dateAPI = formatterAPI.string(from: date1!)
        
        let formatterAPI12 = DateFormatter()
        formatterAPI12.dateFormat = "dd-MM-yyyy"
        let dateAPI12 = formatterAPI12.string(from: date1!)
        
        self.strDate12 = dateAPI12 ?? ""
        self.strSelectDate = dateAPI
        
        callTimeSlotApi(strDate: strDate)
    }
   
    
    @IBAction func clickedProced(_ sender: Any) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetailsVC") as! BookingDetailsVC
//        vc.dicDrDetail = dicDrDetail
//        vc.strSelectDate = strDate12
//        vc.strSelectTime = strSelectTime
//        self.navigationController?.pushViewController(vc, animated: true)
        
        if strClickedType == 1 {
            // Book Appointment
            if strSelectTime == ""
            {
                self.view.makeToast("Select time slot".localizeString(string: Language.shared.currentAppLang))
            }
            else
            {
                self.callBookNowAPI()
            }
            
        } else if strClickedType == 2 {
            // Online
            if strSelectTime == ""
            {
                self.view.makeToast("Select time slot".localizeString(string: Language.shared.currentAppLang))
            }
            else
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetailsVC") as! BookingDetailsVC
                vc.dicDrDetail = dicDrDetail
                vc.strSelectDate = strDate12
                vc.strSelectTime = strSelectTime
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        } else {
            // Sent
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SentQueryVC") as! SentQueryVC
//            vc.dicDrQueryDetail = dicDrDetail
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedBookApp(_ sender: Any)
    {
        strClickedType = 1
        
        view1.layer.borderColor = CGColor(red: 155/255, green: 192/255, blue: 60/255, alpha: 1)
        view2.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        view3.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        
        lblTSelectDate.isHidden = false
        viewMainSelectDate.isHidden = false
        lblTAvailableSlots.isHidden = false
        viewMainSlots.isHidden = false
        lblTDescriptiom.isHidden = false
        lblDes.isHidden = false
    }
    
    @IBAction func clickedBookOnlineCons(_ sender: Any) {
        strClickedType = 2
        view3.layer.borderColor = CGColor(red: 155/255, green: 192/255, blue: 60/255, alpha: 1)
        view1.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        view2.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        
        lblTSelectDate.isHidden = false
        viewMainSelectDate.isHidden = false
        lblTAvailableSlots.isHidden = false
        viewMainSlots.isHidden = false
        lblTDescriptiom.isHidden = false
        lblDes.isHidden = false
    }
    
    @IBAction func clickedSentQuery(_ sender: Any) {
        strClickedType = 3
        view2.layer.borderColor = CGColor(red: 155/255, green: 192/255, blue: 60/255, alpha: 1)
        view1.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        view3.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
        
        lblTSelectDate.isHidden = true
        viewMainSelectDate.isHidden = true
        lblTAvailableSlots.isHidden = true
        viewMainSlots.isHidden = true
        lblTDescriptiom.isHidden = true
        lblDes.isHidden = true
    }
    
}

extension DoctorDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectioViewTime
        {
            return Date.getDates(forLastNDays: 5).count
            
        }
        else
        {
            return arrTimeSlots.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectioViewTime
        {
            let cell = self.collectioViewTime.dequeueReusableCell(withReuseIdentifier: "BookTimeCollectionViewCell", for: indexPath) as! BookTimeCollectionViewCell
            
             if indexPath.row == 0
            {
                 let objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                cell.lblName.text = objDate

            }
            else if indexPath.row == 1
            {
                let objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                cell.lblName.text = "\(objDate.dropLast(5))"
             }
            else
            {
                let objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                cell.lblName.text = "\(objDate.dropLast(5))"
            }
 
                 if self.strSelectedDay == indexPath.row
                {
                    cell.lblName.textColor = UIColor(red: 8/255, green: 54/255, blue: 75/255, alpha: 1)
//                    cell.mainView.backgroundColor = UIColor(red: 8/255, green: 54/255, blue: 75/255, alpha: 1)
                    cell.mainView.layer.borderColor = CGColor(red: 155/255, green: 192/255, blue: 60/255, alpha: 1)
                }
                else
                {
                    cell.lblName.textColor = .black
//                    cell.mainView.backgroundColor = UIColor.white
                    cell.mainView.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
                 }
 
            
            return cell
        }
        else
        {
            let cell = self.collectionViewSlots.dequeueReusableCell(withReuseIdentifier: "AvailableSlotCollectionViewCell", for: indexPath) as! AvailableSlotCollectionViewCell
            
            let dicData = arrTimeSlots[indexPath.row]
            
            let bidAccepted = dicData.fromTime ?? ""
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm:ss"
            let date1 = formatter.date(from: bidAccepted)
            let Dform = DateFormatter()
            Dform.dateFormat = "hh:mm a"
            var strDate = Dform.string(from: date1!)
            if bidAccepted == "12:00:00"
            {
                strDate = "12:00 AM"
            }
            let bidAccepted1 = dicData.toTime ?? ""
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "HH:mm:ss"
            let date11 = formatter1.date(from: bidAccepted1)
            let Dform1 = DateFormatter()
            Dform1.dateFormat = "hh:mm a"
            var strDate1 = Dform1.string(from: date11!)
            if bidAccepted1 == "12:00:00"
            {
                strDate1 = "12:00 AM"
            }
            cell.lblName.text = "\(strDate) - \(strDate1)"
            
            if strSelectedTimeSlot == indexPath.row
            {
//                cell.mainView.backgroundColor = UIColor(red: 8/255, green: 54/255, blue: 75/255, alpha: 1)
                cell.mainView.layer.borderColor = CGColor(red: 155/255, green: 192/255, blue: 60/255, alpha: 1)
            }
            else
            {
//                cell.lblName.textColor = UIColor(red: 8/255, green: 54/255, blue: 75/255, alpha: 1)
//                cell.mainView.backgroundColor = UIColor(red: 14/255, green: 190/255, blue: 126/255, alpha: 0.08)
                cell.mainView.layer.borderColor = CGColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 1)
                
             }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectioViewTime
        {
            strSelectedDay = indexPath.row
            strSelectedTimeSlot = -1
            
            

            if indexPath.row == 0
            {
                var objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                let selectDating =  objDate
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
                let date1 = formatter.date(from: Date.getCurrentDate())
                
                let Dform = DateFormatter()
                Dform.dateFormat = "EEEE"
                let strDate = Dform.string(from: date1!)
                
                let formatterAPI = DateFormatter()
                formatterAPI.dateFormat = "-MM-dd"
                let dateAPI = formatterAPI.string(from: date1!)
                
                let formatterAPI12 = DateFormatter()
                formatterAPI12.dateFormat = "dd-MM-yyyy"
                let dateAPI12 = formatterAPI12.string(from: date1!)
                
                self.strSelectDate = dateAPI
                self.strDate12 = dateAPI12
                
                callTimeSlotApi(strDate: strDate)

            }
            else if indexPath.row == 1
            {
                var objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                let selectDating = objDate
                
                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "en_US")
                formatter.dateFormat = "dd MMM yyyy"
                let date1 = formatter.date(from: selectDating.replacingOccurrences(of: "Tomorrow, ", with: ""))
                
                let Dform = DateFormatter()
                Dform.dateFormat = "EEEE"
                let strDate = Dform.string(from: date1!)
                
                let formatterAPI = DateFormatter()
                formatterAPI.dateFormat = "-MM-dd"
                let dateAPI = formatterAPI.string(from: date1!)

                let formatterAPI12 = DateFormatter()
                formatterAPI12.dateFormat = "dd-MM-yyyy"
                let dateAPI12 = formatterAPI12.string(from: date1!)
                
                self.strSelectDate = dateAPI
                self.strDate12 = dateAPI12
                
                callTimeSlotApi(strDate: strDate)

            }
            else
            {
                var objDate = Date.getDates(forLastNDays: 5)[indexPath.row]
                
                let selectDating = objDate
 
                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "en_US")
                formatter.dateFormat = "EEE, dd MMM yyyy"
                let date1 = formatter.date(from: selectDating)
                
                let Dform = DateFormatter()
                Dform.dateFormat = "EEEE"
                let strDate = Dform.string(from: date1!)
                
                let formatterAPI = DateFormatter()
                formatterAPI.dateFormat = "-MM-dd"
                let dateAPI = formatterAPI.string(from: date1!)
                
                let formatterAPI12 = DateFormatter()
                formatterAPI12.dateFormat = "dd-MM-yyyy"
                let dateAPI12 = formatterAPI12.string(from: date1!)
                
                self.strSelectDate = dateAPI
                self.strDate12 = dateAPI12
                
                callTimeSlotApi(strDate: strDate)

            }
            
            
        }
        else if collectionView == collectionViewSlots
        {
            strSelectedTimeSlot = indexPath.row
            let dicData = arrTimeSlots[indexPath.row]
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US")
            formatter.dateFormat = "HH:mm:ss"
            let date1 = formatter.date(from: dicData.fromTime ?? "")
            
            let Dform = DateFormatter()
            Dform.dateFormat = "hh:mm a"
            let fromTime = Dform.string(from: date1!)
            
            
            let formatter1 = DateFormatter()
            formatter1.locale = Locale(identifier: "en_US")
            formatter1.dateFormat = "HH:mm:ss"
            let date11 = formatter1.date(from: dicData.toTime ?? "")
            
            let Dform1 = DateFormatter()
            Dform1.dateFormat = "hh:mm a"
            let fromTime1 = Dform1.string(from: date11!)
            
            strSelectTime = "\(fromTime) - \(fromTime1)"
        }
       
        collectioViewTime.reloadData()
        collectionViewSlots.reloadData()
        
    }
    
    //MARK: - API callSpecialityDataApi
    
    func callBookNowAPI()
    {
        APIClient.sharedInstance.showIndicator()
        
        let para = ["patient_id":appDelegate?.dicUserLoginData.id ?? "","doctor_id":self.strDoctor_id,"date":"\(Calendar(identifier: .gregorian).dateComponents([.year], from: Date()).year!)\(self.strSelectDate)","time":self.strSelectTime,"payment_mode":"COD","consultant_fee":self.dicDrDetail.consultantCharge ?? ""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(BOOK_APPOINTMENT, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        vc.isMedical = false
                        vc.strTitle = message ?? ""
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func callDoctorDetailsDataApi()
    {
        APIClient.sharedInstance.showIndicator()
        
        let para = ["doctor_id": strDoctor_id]
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(DOCTORS_DETAILS, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
 

            
            if error == nil{
                APIClient.sharedInstance.hideIndicator()
                
                if let dicResponse = response as? NSDictionary{
                    if let arraData = dicResponse.value(forKey: "data") as? NSArray{
                        
                        let dicData = arraData[0] as! NSDictionary
                        let dicDetails = GFCDoctorDetailsData(fromDictionary: dicData)
                        self.dicDrDetail = dicDetails
                        
                        self.callCountryListAPI()
                        
                        let url = URL(string: self.dicDrDetail.imageUrl ?? "")
                        self.imgDr.sd_imageIndicator = SDWebImageActivityIndicator.gray
                        self.imgDr.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
                        
//                        self.lblDrPhone.text = self.dicDrDetail.mobileNo ?? ""
                        self.lblHospitalnmae.text = self.dicDrDetail.hospitalName ?? ""
 
                        if dicDetails.doctorName == ""
                        {
                            self.lblName.text = "N/A"
                        }
                        else
                        {
                            self.lblName.text = dicDetails.doctorName ?? "N/A"
                        }
                        
                        if dicDetails.descriptionField == ""
                        {
                            self.lblDes.text = "N/A"
                        }
                        else
                        {
                            self.lblDes.text = dicDetails.descriptionField ?? "N/A"
                        }
                        
                        if dicDetails.qualification == ""
                        {
                            self.lblqualification.text = "N/A"
                        }
                        else
                        {
                            self.lblqualification.text = dicDetails.qualification ?? "N/A"
                        }
                        
                        if dicDetails.rating == ""
                        {
                            self.lblRating.text = "0"
                        }
                        else
                        {
                            let rating = Double(dicDetails.rating ?? "")?.round(to: 1)
                            
                            self.lblRating.text = "\(rating ?? 0.0)"
                        }
                        self.lblconsultant_charge.text = "Rs. \(self.dicDrDetail.consultantCharge ?? "")"
//                        
                        self.lblspecialization_name.text = self.dicDrDetail.specializationName ?? ""
                    }
                }
            }
        }
    }
    
    
    func callTimeSlotApi(strDate: String)
    {
      //  APIClient.sharedInstance.showIndicator()
        
        var dayOf = 1
        
        if strDate == "Monday"
        {
            dayOf = 1
        }
        else if strDate == "Tuesday"
        {
            dayOf = 2
        }
        else if strDate == "Wednesday"
        {
            dayOf = 3
        }
        else if strDate == "Thursday"
        {
            dayOf = 4
        }
        else if strDate == "Friday"
        {
            dayOf = 5
        }
        else if strDate == "Saturday"
        {
            dayOf = 6
        }
        else if strDate == "Sunday"
        {
            dayOf = 7
        }
        
        let para = ["doctor_id": strDoctor_id,"available_day": "\(dayOf ?? 0)","date":self.strDate12]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(TIME_SLOT, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
 
            if error == nil {
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                
                APIClient.sharedInstance.hideIndicator()

                if IsSuccess == "success"
                {
                    let arrData = response?.value(forKey: "data") as? NSArray
                    self.arrTimeSlots.removeAll()
                    for obj in arrData!
                    {
                        let objData = GFCTimeSlotData(fromDictionary: obj as! NSDictionary)
                        
                        if objData.fromTime != "00:00:00"
                        {
                            self.arrTimeSlots.append(objData)
                        }
                        else
                        {
                            objData.fromTime = "12:00:00"
                            objData.toTime = "12:00:00"
                            self.arrTimeSlots.append(objData)
                        }
                        
                    }
                    if self.arrTimeSlots.count > 0
                    {
                        
                        let objTimeCount = self.arrTimeSlots.count
                        
                        var objCoount = 0.0
                        
                        if objTimeCount % 2 == 0
                        {
                            objCoount = ceil(Double((objTimeCount)/2))
                        }
                        else
                        {
                            objCoount = ceil(Double((objTimeCount)/2)+0.5)
                           
                        }
                        
                        let totalHeight = Int(objCoount) * 40
                        
                        self.viewASlots.constant = CGFloat(totalHeight) + 25.0
                        
                        self.tblViewMain.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540 + CGFloat(totalHeight))
                        self.scrollMainView.reloadInputViews()
                        
                        self.collectionViewSlots.isHidden = false
                        self.lblNoA.isHidden = true
                    }
                    else
                    {
                        self.viewASlots.constant = 50.0
                            
                        self.tblViewMain.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
                        self.scrollMainView.reloadInputViews()
                        
                        self.collectionViewSlots.isHidden = true
                        self.lblNoA.isHidden = false
                        
                    }
                    self.collectionViewSlots.reloadData()
                }
                else
                {
                    self.viewASlots.constant = 50.0
                        
                    self.tblViewMain.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
                    self.scrollMainView.reloadInputViews()
                    
                    self.collectionViewSlots.isHidden = true
                    self.lblNoA.isHidden = false
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()

            }
        }
    }
    
    func callCountryListAPI()
    {
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if self.dicDrDetail.countryId == dicData.countryId
                            {
                                self.lblAddress.text = "\(self.dicDrDetail.address ?? ""), \(self.dicDrDetail.cityName ?? ""), \(self.dicDrDetail.stateName ?? ""), \(self.dicDrDetail.countryName ?? "")"
                            }
                        }
                    }
                }
            }
        }
    }
    
}
extension Date {
    static func getDates(forLastNDays nDays: Int) -> [String] {
        let cal = NSCalendar.current
        // start with today
        var date = cal.startOfDay(for: Date())
        
        var arrDates = [String]()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM"
        let dateString = dateFormatter.string(from: date)
        arrDates.append("Today, \(dateString)")
        
        let dateFormatter2 = DateFormatter()
        var yesterday = Calendar.current.date(byAdding: .day, value: +1, to: date)
        dateFormatter2.dateFormat = "dd MMM yyyy"
        let dateString2 = dateFormatter2.string(from: yesterday!)
        arrDates.append("Tomorrow, \(dateString2)")
        
        for _ in 1 ... nDays {
            // move back in time by one day:
            yesterday = cal.date(byAdding: Calendar.Component.day, value: 1, to: yesterday!)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEE, dd MMM yyyy"
            let dateString = dateFormatter.string(from: yesterday!)
            arrDates.append(dateString)
        }
        
        print(arrDates)
        return arrDates
    }
}
class BookTimeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var mainView: UIView!
}
class AvailableSlotCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var mainView: UIView!
}
extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"

        return dateFormatter.string(from: Date())

    }
}
