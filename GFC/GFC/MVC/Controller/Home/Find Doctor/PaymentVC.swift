//
//  PaymentVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 08/05/24.
//

import UIKit
import Quickblox
class PaymentVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.dataSource = self
        tblView.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedNetbanking(_ sender: Any) {
    }
    @IBAction func clickedUpi(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingVC") as! BookingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func clickedPay(_ sender: Any) {
    }
    
}

extension PaymentVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "PaymentCell") as! PaymentCell
         return cell
    }
    
    
}

class PaymentCell : UITableViewCell {
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblEDate: UILabel!
    
}
