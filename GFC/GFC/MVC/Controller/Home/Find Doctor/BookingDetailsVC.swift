//
//  BookingDetailsVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 08/05/24.
//

import UIKit
import Quickblox
import QuickbloxWebRTC
import PushKit

 class BookingDetailsVC: UIViewController {
        
    private var users = Users()
    
    private let authModule = AuthModule()
    private let connection = ConnectionModule()
    private var splashVC: SplashScreenViewController? = nil
    private let callHelper = CallHelper()
    
    private var callViewController: CallViewController? = nil
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDegree: UILabel!
    @IBOutlet weak var lblCFee: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblRatng: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblC1Fee: UILabel!
    @IBOutlet weak var lblOnlineFee: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblHospitalNAme: UILabel!
    @IBOutlet weak var lblSpe: UILabel!
        
    @IBOutlet weak var uiviewUp: UIView!
    @IBOutlet weak var uivireMid: UIView!
    @IBOutlet weak var uiviewLas: UIView!
    
     @IBOutlet weak var btnProceedToPay: UIButton!
     
     
    var dicDrDetail = GFCDoctorDetailsData()
    
    var strSelectDate = ""
    var strSelectTime = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnProceedToPay.isHidden = true
        
        lblDate.text = strSelectDate
        lblTime.text = strSelectTime
        
        lblOnlineFee.text = "Rs. 10"
        
        let total = (Double(dicDrDetail.consultantCharge ?? "") ?? 0.0) + (Double(10.0))
        
        lblTotal.text = "Rs. \(total)"
        
        let url = URL(string: self.dicDrDetail.imageUrl ?? "")
        self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
        
//                        self.lblDrPhone.text = self.dicDrDetail.mobileNo ?? ""
        self.lblHospitalNAme.text = self.dicDrDetail.hospitalName ?? ""

        if dicDrDetail.doctorName == ""
        {
            self.lblName.text = "N/A"
        }
        else
        {
            self.lblName.text = dicDrDetail.doctorName ?? "N/A"
        }
        
        if dicDrDetail.qualification == ""
        {
            self.lblType.text = "N/A"
        }
        else
        {
            self.lblType.text = dicDrDetail.qualification ?? "N/A"
        }
        
        if dicDrDetail.rating == ""
        {
            self.lblRatng.text = "0"
        }
        else
        {
            let rating = Double(dicDrDetail.rating ?? "")?.round(to: 1)
            
            self.lblRatng.text = "\(rating ?? 0.0)"
        }
        
        self.lblCFee.text = "Rs. \(self.dicDrDetail.consultantCharge ?? "")"
        self.lblC1Fee.text = "Rs. \(self.dicDrDetail.consultantCharge ?? "")"
//
        self.lblDegree.text = self.dicDrDetail.specializationName ?? ""
        
        
//        connection.delegate = self
//        
//        connection.activateAutomaticMode()
//        authModule.delegate = self
//        
//        callHelper.delegate = self
        
        uiviewUp.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.19), alpha: 1, x: 1, y: 1, blur: 5, spread: 0)
        uivireMid.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.19), alpha: 1, x: 1, y: 1, blur: 5, spread: 0)
        uiviewLas.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.19), alpha: 1, x: 1, y: 1, blur: 5, spread: 0)
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
//        CallPermissions.check(with: .video, presentingViewController: self) { granted in
//            
//        }
    }
    
    
    @IBAction func clickedPay(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BookingPopVC") as! BookingPopVC
        let home = UINavigationController(rootViewController: vc)
        home.modalPresentationStyle = .overFullScreen
        home.navigationBar.isHidden = true
        self.present(home, animated: false)
    }
    @IBAction func clickedVideoCall(_ sender: Any) {
       // call(with: QBRTCConferenceType.video)
        
    }
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
     @IBAction func clickedFIBB(_ sender: Any) {
         callGetPaymentTokenAPI()
     }
     
    
    private func call(with conferenceType: QBRTCConferenceType) {
        if connection.established == false {
          //  showNoInternetAlert { [weak self] (action) in
                self.connection.establish()
          //  }
            return
        }
        
        guard callHelper.registeredCallId?.isEmpty == true else {
            return
        }
        
        CallPermissions.check(with: conferenceType, presentingViewController: self) { [weak self] granted in
            guard let self = self, granted == true else {
                return
            }
            self.connection.activateCallMode()
            var callMembers: [NSNumber: String] = [:]
            
            callMembers[NSNumber(value: Int(dicDrDetail.quickbloxId ?? "") ?? 0)] = dicDrDetail.doctorName ?? ""
            
            let hasVideo = conferenceType == .video
            self.callHelper.registerCall(withMembers: callMembers, hasVideo: hasVideo)
        }
    }
     
     // MARK: - callingAPI
     func callGetPaymentTokenAPI() {
         APIClient.sharedInstance.showIndicator()
         
//         let grant_type = "client_credentials"
//         let client_id = "gfc"
//         let client_secret = "59a3a836-4b60-47df-a7ac-a91493c43e73"
         
         let grant_type = "client_credentials"
         let client_id = "gfc-company"
         let client_secret = "8cea33a0-fb2e-48a0-996a-6bf8af36cb2d"

         
         let param = ["grant_type":grant_type, "client_id":client_id,"client_secret":client_secret]
         
         print(param)
         
         APIClient.sharedInstance.MakeAPICallWithOutHeaderPostPayment("", parameters: param) { response, error, statusCode in
             
             print("STATUS CODE \(String(describing: statusCode))")
             print("RESPONSE \(String(describing: response))")
             
             if error == nil {
                 APIClient.sharedInstance.hideIndicator()
                 
                 if statusCode == 200 {
                     
                     let access_token = response?.value(forKey: "access_token") as? String
                     let token_type = response?.value(forKey: "token_type") as? String
                     
                     appDelegate?.payment_access_token = "\(token_type ?? "") \(access_token ?? "")"
                     
                     self.callPaymentOpenAPI()
                     
                 } else {
                     APIClient.sharedInstance.hideIndicator()
                 }
             } else {
                 APIClient.sharedInstance.hideIndicator()
             }
         }
     }
     
     func callPaymentOpenAPI() {
         
         let total = (Double(dicDrDetail.consultantCharge ?? "") ?? 0.0) + (Double(10.0))
         
         let monetaryValue = NSMutableDictionary()
         monetaryValue.setValue(total, forKey: "amount")
         monetaryValue.setValue("IQD", forKey: "currency")
         
         let dict = NSMutableDictionary()
         dict.setValue("https://fib.prod.fib.iq/protected/v1/payments/:{paymentId}/status", forKey: "statusCallbackUrl")
         dict.setValue("Lorem ipsum dolor sit amet.", forKey: "description")
         dict.setValue("PT8H6M12.345S", forKey: "expiresIn")
         dict.setValue(monetaryValue, forKey: "monetaryValue")
         
         print(dict)
         
         let _url = "https://fib.prod.fib.iq/protected/v1/payments" as NSString
         
         WebParserWS.fetchDataWithURL(url: _url, type: .TYPE_POST_RAWDATA, ServiceName: "payments", bodyObject: dict, delegate: self, isShowProgress: true)
         
     }
     
}

extension BookingDetailsVC: responseDelegate {
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        print(Response)
        
        DispatchQueue.main.async {
            let dicResponse = Response as! NSDictionary
            
            if ServiceName == "payments" {
                
                let personalAppLink = dicResponse.value(forKey: "personalAppLink") as? String
                
                if let url = URL(string: personalAppLink ?? "") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        }
    }
    
    
}

// MARK: - AuthModuleDelegate
extension BookingDetailsVC: AuthModuleDelegate {
    func authModule(_ authModule: AuthModule, didLoginUser user: QBUUser) {
        Profile.synchronize(withUser: user)
        connection.establish()
        
    }
    
    func authModuleDidLogout(_ authModule: AuthModule) {
        connection.deactivateAutomaticMode()
        
    }
    
    func authModule(_ authModule: AuthModule, didReceivedError error: ErrorInfo) {
        showUnAuthorizeAlert(message: "\(error.statusCode) \(error.info)" , logoutAction: { [weak self] logoutAction in
            self?.logout()
        }, tryAgainAction: { [weak self] tryAgainAction in
            let profile = Profile()
            self?.authModule.login(fullName: profile.fullName, login: profile.login)
        })
    }
    
    private func logout() {
        if connection.established == false {
          //  showNoInternetAlert { [weak self] (action) in
            self.connection.establish()
          //  }
            return
        }
        
    }
}


// MARK: - ConnectionModuleDelegate
extension BookingDetailsVC: ConnectionModuleDelegate {
    func connectionModuleWillConnect(_ connectionModule: ConnectionModule) {
        showAnimatedAlertView(nil, message: ConnectionConstant.connectingState)
    }
    
    func connectionModuleDidConnect(_ connectionModule: ConnectionModule) {
        hideAlertView()
    }
    
    func connectionModuleDidNotConnect(_ connectionModule: ConnectionModule, error: Error) {
        if error._code.isNetworkError {
            //showNoInternetAlert { [weak self] (action) in
            self.connection.establish()
           // }
            return
        }
        showAlertView(nil, message: error.localizedDescription, handler: nil)
    }
    
    func connectionModuleWillReconnect(_ connectionModule: ConnectionModule) {
        showAnimatedAlertView(nil, message: ConnectionConstant.reconnectingState)
    }
    
    func connectionModuleDidReconnect(_ connectionModule: ConnectionModule) {
        hideAlertView()
    }
    
    func connectionModuleTokenHasExpired(_ connectionModule: ConnectionModule) {
        let profile = Profile()
        authModule.login(fullName: profile.fullName, login: profile.login)
    }
}


// MARK: - CallHelperDelegate
extension BookingDetailsVC: CallHelperDelegate {
    func helper(_ helper: CallHelper, didAcceptCall callId: String) {
        self.helper(helper, showCall: callId)
    }
    
    func helper(_ helper: CallHelper, didRegisterCall callId: String, mediaListener: MediaListener, mediaController: MediaController, direction: CallDirection, members: [NSNumber : String], hasVideo: Bool) {
        
        connection.activateCallMode()
        
        callViewController = hasVideo == true ? Screen.videoCallViewController() : Screen.audioCallViewController()
        
        callViewController?.setupWithCallId(callId, members: members, mediaListener: mediaListener, mediaController: mediaController, direction: direction)
        
        if direction == .outgoing {
            self.helper(helper, showCall: callId)
            return
        }
        let usersIDs = Array(members.keys)
        users.users(usersIDs) { [weak self] (users, error) in
            guard let self = self, let users = users else {
                return
            }
            
            let profile = Profile()
            
            var callMembers: [NSNumber : String] = [:]
            for user in users {
                if profile.ID != user.id
                {
                    callMembers[NSNumber(value: user.id)] = user.fullName ?? "User"
                }
            }
            self.callViewController?.callInfo.updateWithMembers(callMembers)
            let title = Array(callMembers.values).joined(separator: ", ")
            helper.updateCall(callId, title: title)
        }
    }
    
    func helper(_ helper: CallHelper, didUnregisterCall callId: String) {
        connection.deactivateCallMode()
        guard let callViewController = callViewController else {
            return
        }
        
        callViewController.dismiss(animated: false) {
            self.callViewController = nil
        }
    }
    
    //Internal Method
    private func helper(_ helper: CallHelper, showCall callId: String) {
        guard let callViewController = self.callViewController,
              let callViewControllerCallId = callViewController.callInfo.callId,
              callViewControllerCallId == callId else {
            return
        }
        hideAlertView()
        
        let navVC = UINavigationController(rootViewController: callViewController)
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: false)
        
        users.selected.removeAll()
        callViewController.hangUp = { (callId) in
            helper.unregisterCall(callId, userInfo: ["hangup": "hang up"])
        }
    }
    
}
