//
//  SentQueryVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 28/02/23.
//


import UIKit
import MobileCoreServices
import SDWebImage
import BSImagePicker
import Photos

class SentQueryVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDegree: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var lblTDoctorDetail: UILabel!
    @IBOutlet weak var lblConstantionFee: UILabel!
    @IBOutlet weak var lblTAddress: UILabel!
    @IBOutlet weak var lblTDescription: UILabel!
    
    @IBOutlet weak var btnTUpload: UIButton!
    
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var viewDescription: UIView! // 358
    
    @IBOutlet weak var lblHospitalNameq: UILabel!
    
    @IBOutlet weak var tblViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblTHospitalNAme: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var dicDrQueryDetail = GFCDoctorDetailsData()
    
    var imagePicker: ImagePicker!
    var selectedImg: String?
    
    var imagePickerNew = UIImagePickerController()
    
    var arrUploadDocBOL = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTDoctorDetail.text = "Doctor Details".localizeString(string: Language.shared.currentAppLang)
        lblConstantionFee.text = "Consultation Fee".localizeString(string: Language.shared.currentAppLang)
        lblTHospitalNAme.text = "Hospital Name".localizeString(string: Language.shared.currentAppLang)
        lblTAddress.text = "Address".localizeString(string: Language.shared.currentAppLang)
        lblTDescription.text = "Description".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            lblName.textAlignment = .left
        }else{
            lblName.textAlignment = .right
        }
        
        btnTUpload.setTitle("Upload your medical reports".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        tblViewHeight.constant = 0

        tblView.delegate = self
        tblView.dataSource = self
        
        let url = URL(string: self.dicDrQueryDetail.imageUrl ?? "")
        self.imgPic.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgPic.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
        
        callCountryListAPI()
        
        lblPhone.text = self.dicDrQueryDetail.mobileNo ?? ""
        lblHospitalNameq.text = self.dicDrQueryDetail.hospitalName ?? ""
        
        if dicDrQueryDetail.rating == "0"
        {
            lblRating.text = "0"
        }
        else
        {
            let rating = Double(dicDrQueryDetail.rating ?? "")?.round(to: 1)
            
            lblRating.text = "\(rating ?? 0.0)"
        }

        lblName.text = dicDrQueryDetail.doctorName ?? ""
   
        if dicDrQueryDetail.specializationName == ""
        {
            lblType.text = "N/A".localizeString(string: Language.shared.currentAppLang)
        }
        else
        {
            lblType.text = dicDrQueryDetail.specializationName ?? ""
        }
        
        if dicDrQueryDetail.qualification == ""
        {
            lblDegree.text = "N/A".localizeString(string: Language.shared.currentAppLang)
        }
        else
        {
            lblDegree.text = dicDrQueryDetail.qualification ?? ""
        }
        
        if dicDrQueryDetail.descriptionField == ""
        {
            lblDescription.text = "N/A".localizeString(string: Language.shared.currentAppLang)
        }
        else
        {
            lblDescription.text = dicDrQueryDetail.descriptionField ?? ""
        }
        
        lblFee.text = "Rs. \(dicDrQueryDetail.consultantCharge ?? "")"
        
        // Do any additional setup after loading the view.
    }
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedUpload(_ sender: Any) {
        if self.arrUploadDocBOL.count != 0
        {
            myImageUploadRequest(imgKey: "image")
        }
        else
        {
            self.view.makeToast("Please upload query docs".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Please upload query docs".localizeString(string: Language.shared.currentAppLang))
        }
    }
    
    @IBAction func clickedOpenPic(_ sender: Any) {
        let alert1 = UIAlertController(title: "Choose Photo".localizeString(string: Language.shared.currentAppLang), message: nil, preferredStyle: .actionSheet)
        
        alert1.addAction(UIAlertAction(title: "Camera".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert1.addAction(UIAlertAction(title: "Gallery".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert1.addAction(UIAlertAction.init(title: "Cancel".localizeString(string: Language.shared.currentAppLang), style: .cancel, handler: nil))
        
        self.present(alert1, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePickerNew.delegate = self
            imagePickerNew.sourceType = UIImagePickerController.SourceType.camera
            imagePickerNew.allowsEditing = true
            imagePickerNew.mediaTypes = [kUTTypeImage as String]
            self.present(imagePickerNew, animated: true, completion: nil)
        }
        else
        {
            
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("You don't have camera")
        }
    }
    
    func openGallary()
    {
        
//        imagePickerNew.delegate = self
//        imagePickerNew.sourceType = UIImagePickerController.SourceType.photoLibrary
//        imagePickerNew.allowsEditing = true
//        imagePickerNew.mediaTypes = ["public.image"]
//        self.present(imagePickerNew, animated: true, completion: nil)
        
        self.multipleImgAdd()
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        {
            let imageurl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            selectedImg = imageurl?.lastPathComponent
            
            self.arrUploadDocBOL.add(image)
            
            self.tblView.reloadData()
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            let imageurl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            
            self.arrUploadDocBOL.add(image)
            
            self.tblView.reloadData()
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func multipleImgAdd()
    {
        let imagePicker = ImagePickerController()
        imagePicker.settings.selection.max = 10 // Set the maximum number of images allowed to be selected
        imagePicker.albumButton.tintColor = .green // Set the color of the album button
        imagePicker.cancelButton.tintColor = .red // Set the color of the cancel button
        
        presentImagePicker(imagePicker, animated: true, select: { (asset: PHAsset) -> Void in
            // User selected an asset.
        }, deselect: { (asset: PHAsset) -> Void in
            // User deselected an asset.
        }, cancel: { (assets: [PHAsset]) -> Void in
            // User cancelled the selection.
        }, finish: { (assets: [PHAsset]) -> Void in
            // User finished selecting assets.
            for asset in assets {
                self.fetchImage(for: asset)
            }
        }, completion: nil)
    }
    
    func fetchImage(for asset: PHAsset) {
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        
        PHImageManager.default().requestImage(for: asset, targetSize: CGSize(width: 300, height: 300), contentMode: .aspectFit, options: options, resultHandler: { image, _ in
            if let image = image {
                // Handle the selected image
                print("Selected image: \(image)")
                
                self.arrUploadDocBOL.add(image)
                
                self.tblViewHeight.constant = CGFloat(45 * self.arrUploadDocBOL.count)
                self.tblView.reloadData()
            }
        })
    }

    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            
            ctx.cgContext.drawPDFPage(page)
        }
        
        return img
    }
    
    func callCountryListAPI()
    {
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if self.dicDrQueryDetail.countryId == dicData.countryId
                            {
                                self.lblAddress.text = "\(self.dicDrQueryDetail.address ?? ""), \(self.dicDrQueryDetail.cityName ?? ""), \(self.dicDrQueryDetail.stateName ?? ""), \(self.dicDrQueryDetail.countryName ?? "")"

                            }
                        }
                    }
                }
            }
        }
    }
}

extension SentQueryVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUploadDocBOL.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "UploadImgTblCell") as! UploadImgTblCell
                
        cell.lblName.textAlignment = .center
        cell.lblName.text = "/data/user/gfc\(random(digits: 150)).jpg"
        
        cell.btnClose.tag = indexPath.row
        cell.btnClose.addTarget(self, action: #selector(clickedRemove(_ :)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    
    @objc func clickedRemove(_ sender: UIButton)
    {
        let dicData = arrUploadDocBOL[sender.tag] as? UIImage
        self.arrUploadDocBOL.remove(dicData!)
        
        self.tblViewHeight.constant = CGFloat(45 * self.arrUploadDocBOL.count)
        
        self.tblView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrUploadDocBOL[indexPath.row] as? UIImage
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
        vc.modalPresentationStyle = .overFullScreen
        vc.isShowingImage = true
        vc.strImage = dicData!
        self.present(vc, animated: false, completion: nil)
        
    }
    
    func random(digits:Int) -> String {
        var number = String()
        for _ in 1...digits {
           number += "\(Int.random(in: 1...9))"
        }
        return number
    }
    
    func myImageUploadRequest(imgKey: String) {
        
        APIClient.sharedInstance.showIndicator()
        
        let myUrl = NSURL(string: BASE_URL + SENT_QUERY);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let currentDate = Date()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let formattedDate = dateFormatter.string(from: currentDate)
        
        let params = ["patient_id": "\(appDelegate?.dicUserLoginData.id ?? "")","doctor_id": self.dicDrQueryDetail.id ?? "","date":formattedDate]
        
        let boundary = generateBoundaryString()
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = createBodyWithParameters(parameters: params, filePathKey: imgKey, boundary: boundary, imgKey: imgKey) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            APIClient.sharedInstance.hideIndicator()
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            
            // print reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("response data = \(responseString!)")
            
            do{
                
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                    
                    // try to read out a string array
                    if let dicData = json["data"] as? NSDictionary
                    {
                        DispatchQueue.main.async {
                           
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
                            let homeNavigation = UINavigationController(rootViewController: vc)
                            homeNavigation.modalPresentationStyle = .overFullScreen
                            homeNavigation.navigationBar.isHidden = true
                            vc.strTitle = "Send Query Uploaded Successfully!"
                            vc.isMedical = false
                            self.present(homeNavigation, animated: false)
                        }
                    }
                    
                }
                
            }catch{
                
            }
            
        }
        
        task.resume()
    }
    
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, boundary: String, imgKey: String) -> NSData {
        
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "\(imgKey).jpg"
        let mimetype = "image/jpeg"
        
        for (index,objImg) in arrUploadDocBOL.enumerated()
        {
            let imageData =  (objImg as? UIImage)?.jpegData(compressionQuality: 1)
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"image[\(index)]\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(imageData! as Data)
            body.appendString(string: "\r\n")
            body.appendString(string: "--\(boundary)--\r\n")
            
        }
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func randomString(of length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var s = ""
        for _ in 0 ..< length {
            s.append(letters.randomElement()!)
        }
        return s
    }
    
}

class UploadImgTblCell: UITableViewCell
{
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnClose: UIButton!
}
