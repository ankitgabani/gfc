//
//  BookingVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 09/05/24.
//

import UIKit
import Quickblox
class BookingVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.dataSource = self
        tblView.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension BookingVC: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 12
        }
        else
        {
            return 15
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "BookingCell") as! BookingCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let headerView = Bundle.main.loadNibNamed("BookingHeaderView", owner: self, options: [:])?.first as! BookingHeaderView
            
            headerView.lblUpcoming.text = "Upcoming"
            
            return headerView
            
        }
        else
        {
            
            let headerView = Bundle.main.loadNibNamed("BookingHeaderView", owner: self, options: [:])?.first as! BookingHeaderView
            headerView.lblUpcoming.text = "Past"
            
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
}


class BookingCell : UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDateS: UILabel!
    @IBOutlet weak var lblEdate: UILabel!
    
}
