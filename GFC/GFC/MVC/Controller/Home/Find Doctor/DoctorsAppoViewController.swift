//
//  DoctorsAppoViewController.swift
//  GFC Globalhealth
//
//  Created by iMac on 08/10/22.
//

import UIKit
import DropDown
import SDWebImage

class DoctorsAppoViewController: UIViewController {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var txtFilter: UITextField!
    @IBOutlet weak var btnDrop: UIButton!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var viewNoData: UIView!
    
    @IBOutlet weak var lblTFilter: UILabel!
    @IBOutlet weak var lblTSpecil: UILabel!
    
    @IBOutlet weak var btnTApply: UIButton!
    
    @IBOutlet weak var lblTDoctor: UILabel!
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var dropDown = DropDown()
    
    var arrSpeciality: [GFCSpecialitySpecialityData] = [GFCSpecialitySpecialityData]()
    
    var arrSpecialityByDoctor: [GFCSpecialityByDoctorData] = [GFCSpecialityByDoctorData]()
    
    var strSpecialityID = ""
    var strSpecialization = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTDoctor.text = "Doctors".localizeString(string: Language.shared.currentAppLang)
        lblTFilter.text = "Filter".localizeString(string: Language.shared.currentAppLang)
        lblTSpecil.text = "Specialization".localizeString(string: Language.shared.currentAppLang)
        lblTNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)

        btnTApply.setTitle("Apply".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        tblView.delegate = self
        tblView.dataSource = self
        
        viewBg.isHidden = true
        
        self.txtFilter.text = strSpecialization
        
        callSpeciality()
        
        self.viewNoData.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    func setDropDown()
    {
        let arrSpeci = NSMutableArray()
        arrSpeci.add("Select")
        
        for obj in arrSpeciality
        {
            arrSpeci.add(obj.specialization ?? "")
        }
        
        dropDown.dataSource = arrSpeci as! [String]
        dropDown.anchorView = btnDrop
        dropDown.direction = .bottom
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtFilter.text = item
            
            if index != 0
            {
                let dicData = arrSpeciality[index-1]
                self.strSpecialityID = dicData.id ?? ""
                
                callSpecialityByDoctor(specilization_id: strSpecialityID)
            }
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: btnDrop.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: -btnDrop.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.selectedTextColor = .black
        dropDown.reloadAllComponents()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func clickedOpenFilter(_ sender: Any) {
        viewBg.isHidden = false
        
    }
    @IBAction func clickedDropOpen(_ sender: Any) {
        
        dropDown.show()
        
    }
    
    @IBAction func clickedCloseView(_ sender: Any) {
        
        viewBg.isHidden = true
        
    }
    @IBAction func clickedApply(_ sender: Any) {
        viewBg.isHidden = true
        callSpecialityByDoctor(specilization_id: self.strSpecialityID)
        self.tblView.reloadData()
    }
    
    func callSpeciality()
    {
        APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_SPECIALITY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    self.arrSpeciality.removeAll()
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        
                        let arrSpeciality = dic?.value(forKey: "specialityData") as? NSArray
                        
                        for objSpeciality in arrSpeciality!
                        {
                            let dicSpeciality = GFCSpecialitySpecialityData(fromDictionary: objSpeciality as! NSDictionary)
                            self.arrSpeciality.append(dicSpeciality)
                        }
                        
                        if self.arrSpeciality.count > 0
                        {
                            self.tblView.isHidden = false
                            self.viewNoData.isHidden = true
                        }
                        else
                        {
                            self.viewNoData.isHidden = false
                            self.tblView.isHidden = true
                        }
                        
                        self.callSpecialityByDoctor(specilization_id: self.strSpecialityID)
                        
                        self.setDropDown()
                    }
                    else
                    {
                        self.viewNoData.isHidden = false
                        self.tblView.isHidden = true
                    }
                }
            }
        }
    }
    
    func callSpecialityByDoctor(specilization_id:String)
    {
        APIClient.sharedInstance.showIndicator()
        
        let para = ["specilization_id":specilization_id]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_SPECIALITYBY_DOC, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    self.arrSpecialityByDoctor.removeAll()
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                       
                        for objSpecialityDoc in arrData!
                        {
                            let dicSpecialityByDoctor = GFCSpecialityByDoctorData(fromDictionary: objSpecialityDoc as! NSDictionary)
                            self.arrSpecialityByDoctor.append(dicSpecialityByDoctor)
                        }
                        
                        if self.arrSpecialityByDoctor.count > 0
                        {
                            self.tblView.isHidden = false
                            self.viewNoData.isHidden = true
                        }
                        else
                        {
                            self.viewNoData.isHidden = false
                            self.tblView.isHidden = true
                        }
                        
                        self.tblView.reloadData()
                    }
                    else
                    {
                        self.viewNoData.isHidden = false
                        self.tblView.isHidden = true
                    }
                }
            }
        }
    }
    

}

extension DoctorsAppoViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSpecialityByDoctor.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "DoctorAppTblViewCell") as! DoctorAppTblViewCell
        
        let dicData = arrSpecialityByDoctor[indexPath.row]
        
        cell.lblTConsultationFee.text = "Consultation Fee".localizeString(string: Language.shared.currentAppLang)
        
        cell.btnBook.setTitle("Book Now".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        cell.lblName.text = dicData.doctorName ?? ""
        cell.lblType.text = dicData.specializationName ?? ""
        cell.lblProc.text = "Rs.\(dicData.consultantCharge ?? "")"
        cell.lblDegree.text = dicData.qualification ?? ""
        
        let url = URL(string: "https://gfcglobalhealth.com/software/assets/uploads/doctor_profile/" + dicData.image ?? "")
        cell.imgPic.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPic.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
        
        cell.btnBook.tag = indexPath.row
        cell.btnBook.addTarget(self, action: #selector(clickedBook(_:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    @objc func clickedBook(_ sender: UIButton)
    {
        let dicData = arrSpecialityByDoctor[sender.tag]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorDetailsVC") as! DoctorDetailsVC
        vc.strDoctor_id = dicData.id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

class DoctorAppTblViewCell: UITableViewCell
{
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblProc: UILabel!
    @IBOutlet weak var btnBook: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDegree: UILabel!
    
    @IBOutlet weak var lblTConsultationFee: UILabel!
    
}
