//
//  OrderDetailVC.swift
//  GFC Globalhealth
//
//  Created by Code on 02/03/23.
//

import UIKit
import Quickblox
class OrderDetailVC: UIViewController {
    
    
    @IBOutlet weak var viewAll: UIView!
    @IBOutlet weak var viewCancell: UIView!
    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderPrice: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAddressType: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var viewROrdered: UIView!
    @IBOutlet weak var viewLOrdered: UIView!
    @IBOutlet weak var viewRConfirm: UIView!
    @IBOutlet weak var viewLConfirm: UIView!
    @IBOutlet weak var viewRShipped: UIView!
    @IBOutlet weak var viewLShipped: UIView!
    @IBOutlet weak var viewROutOf: UIView!
    @IBOutlet weak var viewLOutOf: UIView!
    @IBOutlet weak var viewRDeliverd: UIView!
    
    @IBOutlet weak var lblTOrderDetail: UILabel!
    @IBOutlet weak var lblTOrderId: UILabel!
    @IBOutlet weak var lblTPaymentMode: UILabel!
    @IBOutlet weak var lblTAddress: UILabel!
    
    // OrderStatus outlet
    
    @IBOutlet weak var lblTOrdered: UILabel!
    @IBOutlet weak var lblTYouHave: UILabel!
    @IBOutlet weak var lblYouHaveCan: UILabel!
    @IBOutlet weak var lblTOrderCan: UILabel!
    
    @IBOutlet weak var lblTOrderConfirmed: UILabel!
    @IBOutlet weak var lblTYourOrder: UILabel!
    
    @IBOutlet weak var lblTShipped: UILabel!
    @IBOutlet weak var lblTOrderShipped: UILabel!
    
    @IBOutlet weak var lblTOutofDelivery: UILabel!
    @IBOutlet weak var lblTOrderIsOut: UILabel!
    
    @IBOutlet weak var lblTDelivered: UILabel!
    @IBOutlet weak var lblTYourOrderDelivered: UILabel!
    
    @IBOutlet weak var lblTCancel: UILabel!
    @IBOutlet weak var lbltOrderCancel: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var objOrderDetails = GFCOrderHistoryData()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTOrderDetail.text = "Order Detail".localizeString(string: Language.shared.currentAppLang)
        lblTOrderId.text = "Order Id :".localizeString(string: Language.shared.currentAppLang)
        lblTPaymentMode.text = "Payment Mode:".localizeString(string: Language.shared.currentAppLang)
        lblTAddress.text = "Address".localizeString(string: Language.shared.currentAppLang)
        lblTOrdered.text = "Ordered".localizeString(string: Language.shared.currentAppLang)
        lblTOrderCan.text = "Ordered".localizeString(string: Language.shared.currentAppLang)
        lblTOrderConfirmed.text = "Ordered Confirmed".localizeString(string: Language.shared.currentAppLang)
        lblTShipped.text = "Shipped".localizeString(string: Language.shared.currentAppLang)
        lblTOutofDelivery.text = "Out For Delivery".localizeString(string: Language.shared.currentAppLang)
        lblTDelivered.text = "Delivered".localizeString(string: Language.shared.currentAppLang)
        lblTYouHave.text = "You have placed your order.".localizeString(string: Language.shared.currentAppLang)
        lblYouHaveCan.text = "You have placed your order.".localizeString(string: Language.shared.currentAppLang)
        lblTYourOrder.text = "Your order has been accepted.".localizeString(string: Language.shared.currentAppLang)
        lblTOrderShipped.text = "Your order has been shippped.".localizeString(string: Language.shared.currentAppLang)
        lblTOrderIsOut.text = "Order is out for delivery.".localizeString(string: Language.shared.currentAppLang)
        lblTYourOrderDelivered.text = "Your order has been delivered.".localizeString(string: Language.shared.currentAppLang)
        lblTCancel.text = "Cancelled".localizeString(string: Language.shared.currentAppLang)
        lbltOrderCancel.text = "Your order has been cancelled.".localizeString(string: Language.shared.currentAppLang)
 
        tblView.delegate = self
        tblView.dataSource = self
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.callMyOrderListAPI()
    }
    
    func setUp()
    {
        lblOrderId.text = objOrderDetails.order.orderId ?? ""
        lblOrderPrice.text = "₹ \(objOrderDetails.order.amount ?? "")"
        lblPaymentMode.text = objOrderDetails.order.paymentMode ?? ""
        lblAddress.text = "\( objOrderDetails.order.address ?? ""),\(objOrderDetails.order.city ?? ""), \(objOrderDetails.order.state ?? ""), \(objOrderDetails.order.pincode ?? "")"
        lblAddressType.text = objOrderDetails.order.title ?? ""
        lblOrderStatus.text = objOrderDetails.order.orderStatus ?? ""
        
        let bidAccepted = objOrderDetails.order.created ?? ""
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date1 = formatter.date(from: bidAccepted)
        let Dform = DateFormatter()
        Dform.dateFormat = "MMM dd, yyyy"
        let strDate = Dform.string(from: date1!)
        self.lblDate.text = strDate
        
        if objOrderDetails.order.orderStatus == ""
        {
            viewROrdered.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLOrdered.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRConfirm.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLConfirm.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRShipped.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLShipped.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewROutOf.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLOutOf.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRDeliverd.backgroundColor = UIColor.init(hexString: "D8D8D8")
            
        }
        else if objOrderDetails.order.orderStatus == "pending"
        {
            viewROrdered.backgroundColor = UIColor.init(hexString: "08364B")
            viewLOrdered.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRConfirm.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLConfirm.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRShipped.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLShipped.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewROutOf.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLOutOf.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRDeliverd.backgroundColor = UIColor.init(hexString: "D8D8D8")
            
        }
        else if objOrderDetails.order.orderStatus == "confirmed"
        {
            viewROrdered.backgroundColor = UIColor.init(hexString: "08364B")
            viewLOrdered.backgroundColor = UIColor.init(hexString: "08364B")
            viewRConfirm.backgroundColor = UIColor.init(hexString: "08364B")
            viewLConfirm.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRShipped.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLShipped.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewROutOf.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLOutOf.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRDeliverd.backgroundColor = UIColor.init(hexString: "D8D8D8")
        }
        else if objOrderDetails.order.orderStatus == "shipped"
        {
            viewROrdered.backgroundColor = UIColor.init(hexString: "08364B")
            viewLOrdered.backgroundColor = UIColor.init(hexString: "08364B")
            viewRConfirm.backgroundColor = UIColor.init(hexString: "08364B")
            viewLConfirm.backgroundColor = UIColor.init(hexString: "08364B")
            viewRShipped.backgroundColor = UIColor.init(hexString: "08364B")
            viewLShipped.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewROutOf.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewLOutOf.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRDeliverd.backgroundColor = UIColor.init(hexString: "D8D8D8")
        }
        else if objOrderDetails.order.orderStatus == "on_the_way"
        {
            viewROrdered.backgroundColor = UIColor.init(hexString: "08364B")
            viewLOrdered.backgroundColor = UIColor.init(hexString: "08364B")
            viewRConfirm.backgroundColor = UIColor.init(hexString: "08364B")
            viewLConfirm.backgroundColor = UIColor.init(hexString: "08364B")
            viewRShipped.backgroundColor = UIColor.init(hexString: "08364B")
            viewLShipped.backgroundColor = UIColor.init(hexString: "08364B")
            viewROutOf.backgroundColor = UIColor.init(hexString: "08364B")
            viewLOutOf.backgroundColor = UIColor.init(hexString: "D8D8D8")
            viewRDeliverd.backgroundColor = UIColor.init(hexString: "D8D8D8")
        }
        else if objOrderDetails.order.orderStatus == "delivered"
        {
            viewROrdered.backgroundColor = UIColor.init(hexString: "08364B")
            viewLOrdered.backgroundColor = UIColor.init(hexString: "08364B")
            viewRConfirm.backgroundColor = UIColor.init(hexString: "08364B")
            viewLConfirm.backgroundColor = UIColor.init(hexString: "08364B")
            viewRShipped.backgroundColor = UIColor.init(hexString: "08364B")
            viewLShipped.backgroundColor = UIColor.init(hexString: "08364B")
            viewROutOf.backgroundColor = UIColor.init(hexString: "08364B")
            viewLOutOf.backgroundColor = UIColor.init(hexString: "08364B")
            viewRDeliverd.backgroundColor = UIColor.init(hexString: "08364B")
        }
        else if objOrderDetails.order.orderStatus == "cancel"
        {
            
            viewCancell.isHidden = false
            viewAll.isHidden = true
            
            mainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 350)
            tblView.reloadData()
        }
        
    }
    // 08364B
    // D8D8D8
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension OrderDetailVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objOrderDetails.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "OrderTblCell") as! OrderTblCell
        
        let dicData = objOrderDetails.products[indexPath.row]
        
        cell.lblHad.text = "Products".localizeString(string: Language.shared.currentAppLang)
        cell.lblTOuality.text = "Quantity :".localizeString(string: Language.shared.currentAppLang)

        cell.lblVariant.text = "\("Varient:".localizeString(string: Language.shared.currentAppLang)) \(dicData.varient ?? "")"
        
        if dicData.rating == "0"
        {
            cell.btnRateus.isUserInteractionEnabled = true
            cell.btnRateus.setTitle("Rate Us".localizeString(string: Language.shared.currentAppLang), for: .normal)
        }
        else
        {
            let rating = Double(dicData.rating ?? "")?.round(to: 1)
            
            cell.btnRateus.isUserInteractionEnabled = false
            cell.btnRateus.setTitle("\(rating ?? 0.0)", for: .normal)
        }
        
        if indexPath.row == 0
        {
            cell.lblHad.isHidden = false
            cell.lblHadBottom.constant = 15
            cell.lblHadeight.constant = 17
            
            cell.lblTopCont.constant = 30
        }
        else
        {
            cell.lblHad.isHidden = true
            cell.lblHadBottom.constant = 0
            cell.lblHadeight.constant = 0
            
            cell.lblTopCont.constant = 15
        }
        cell.lblProductName.text = dicData.name ?? ""
        cell.lblproductQuantity.text = dicData.quantity ?? ""
        cell.lblProductPrice.text = "₹ \(dicData.price ?? "")"
        
        var media_link_url = dicData.image ?? ""
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        cell.imgProduct.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        cell.btnRateus.tag = indexPath.row
        cell.btnRateus.addTarget(self, action: #selector(clickedRate(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func clickedRate(_ sender: UIButton)
    {
        let dicData = objOrderDetails.products[sender.tag]
        
        let arrInfo = NSMutableArray()
        
        let dicInfo = NSMutableDictionary()
        dicInfo.setValue(appDelegate?.dicUserLoginData.name ?? "", forKey: "name")
        dicInfo.setValue(appDelegate?.dicUserLoginData.email ?? "", forKey: "email")
        dicInfo.setValue("product", forKey: "appointment_type")
        dicInfo.setValue(appDelegate?.dicUserLoginData.id ?? "", forKey: "userid")
        dicInfo.setValue(self.objOrderDetails.order.orderId ?? "", forKey: "appointment_id")
        dicInfo.setValue(dicData.pId ?? "", forKey: "id")
        
        arrInfo.add(dicInfo)
        
        let mainS = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainS.instantiateViewController(withIdentifier: "NewRatingVC") as! NewRatingVC
        vc.arrAllInfo = arrInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func callMyOrderListAPI()
    {
        
        let para = ["patientId":appDelegate?.dicUserLoginData.id ?? "","pageNo":"10000","token":"123456"]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_ORDER_H, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                       
                        for obj in arrData!
                        {
                            let dicData = GFCOrderHistoryData(fromDictionary: obj as! NSDictionary)
                            
                            if dicData.order.orderId == self.objOrderDetails.order.orderId
                            {
                                self.objOrderDetails = dicData
                                
                                self.tblView.reloadData()
                            }
                            
                        }
                    }
                }
            }
        }
    }
}

class OrderTblCell: UITableViewCell
{
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblproductQuantity: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblHad: UILabel!
    
    @IBOutlet weak var lblHadBottom: NSLayoutConstraint! //15
    
    @IBOutlet weak var lblHadeight: NSLayoutConstraint! //17
    
    @IBOutlet weak var lblTopCont: NSLayoutConstraint! //30
    @IBOutlet weak var lblTOuality: UILabel!
    
    @IBOutlet weak var btnRateus: UIButton!
    @IBOutlet weak var lblVariant: UILabel!
    
}
