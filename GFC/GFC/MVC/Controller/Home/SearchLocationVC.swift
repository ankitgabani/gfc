//
//  SearchLocationVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 01/05/23.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps

protocol LocationListingScreenDelegate {
    func reloadHomeScreen(objPlaceData : PlaceData)
}

struct PlaceData{
    var fullAddress : String?
    var firstName : String?
    var secondName : String?
    var lattitude : Double?
    var longitude : Double?
    var country_name : String?
    
    init() {
        fullAddress = ""
        firstName = ""
        secondName = ""
        lattitude = 0
        longitude = 0
        country_name = ""
    }
}


class SearchLocationVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var lblAreaRegion: UILabel!
    
    
    var resultsArray = [PlaceData]()

    var locationManager = CLLocationManager()
    
    var gmsFetcher: GMSAutocompleteFetcher!
    var delegateAddress: LocationListingScreenDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        tblView.delegate = self
        tblView.dataSource = self
        
        self.txtSearch.delegate = self
       
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }


        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
    }


}

extension SearchLocationVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultsArray != nil{
            return resultsArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if resultsArray.count > 0{
            //  viewNoLocation.isHidden = true
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "LocationCell") as! LocationCell
            
            let objPlaceData = resultsArray[indexPath.row]
            cell.lblName1.text = objPlaceData.firstName ?? ""
            cell.lblName2.text = objPlaceData.fullAddress ?? ""
            
            return cell
        }
        return UITableViewCell()

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
        if resultsArray.count > 0
        {
            let objPlaceData = resultsArray[indexPath.row]
            self.delegateAddress?.reloadHomeScreen(objPlaceData: objPlaceData)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}

extension SearchLocationVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtSearch.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            
            let mergeString = "\(textField.text!)\(string)"
            var finalString = ""
            if string != ""{
                finalString = mergeString
            }else{
                finalString = String(mergeString.dropLast())
            }
            print("FinalString\(finalString)")
            if finalString == ""{
                resultsArray = [PlaceData]()
                
                self.tblView.reloadData()
                
            }else{
                gmsFetcher.sourceTextHasChanged(finalString)
            }
            
        }
        else
        {
            
            
        }
        
        return true
    }
}

extension SearchLocationVC : GMSAutocompleteFetcherDelegate{
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        self.resultsArray.removeAll()
        for prediction in predictions {
            var placeDataObj = PlaceData()
            if let prediction = prediction as GMSAutocompletePrediction?{
                
                let placeId = prediction.placeID
                
                let placeClient = GMSPlacesClient()
                
                placeClient.lookUpPlaceID(prediction.placeID) { (place, error) -> Void in
                    if let error = error {
                        //show error
                        return
                    }
                    
                    if let place = place {
                        placeDataObj.fullAddress = prediction.attributedFullText.string
                        placeDataObj.firstName = prediction.attributedPrimaryText.string
                        placeDataObj.secondName = prediction.attributedSecondaryText?.string
                        placeDataObj.lattitude = place.coordinate.latitude
                        placeDataObj.longitude = place.coordinate.longitude
                        placeDataObj.country_name = place.formattedAddress
                    } else {
                        //show error
                    }
                    self.resultsArray.append(placeDataObj)
                    
                    self.tblView.reloadData()
                }
            }
            
        }
        
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print(error.localizedDescription)
    }
    
    
}

class LocationCell: UITableViewCell
{
    @IBOutlet weak var lblName1: UILabel!
    @IBOutlet weak var lblName2: UILabel!
}
