//
//  MapVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 26/04/23.
//

import UIKit
import CoreLocation
import MapKit
import GoogleMaps
import Alamofire


class MapVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, LocationListingScreenDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var lblCurrentLocation: UILabel!
    
    @IBOutlet weak var lblTAddAddress: UILabel!
    @IBOutlet weak var lblTUseCurrentLocation: UILabel!
    
    @IBOutlet weak var btnTSubmit: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    let marker: GMSMarker = GMSMarker()
    
    var locationManager = CLLocationManager()
    
    var objLatitude = ""
    var objLongitude = ""
    
    var strAddress = ""
    
    var isBookLab = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTAddAddress.text = "Add Address".localizeString(string: Language.shared.currentAppLang)
        lblTUseCurrentLocation.text = "Use Current Location".localizeString(string: Language.shared.currentAppLang)
        
        btnTSubmit.setTitle("Submit".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        mapView.delegate = self
        
        mapView.padding = UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 0)
        
        DispatchQueue.main.async {
            if CLLocationManager.locationServicesEnabled(){
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.distanceFilter = 10
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.requestAlwaysAuthorization()
                self.locationManager.startUpdatingLocation()
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    func reloadHomeScreen(objPlaceData: PlaceData) {
        marker.icon = UIImage(named: "placeholder") // Marker icon
        marker.position = CLLocationCoordinate2D(latitude: objPlaceData.lattitude ?? 0.0, longitude: objPlaceData.longitude ?? 0.0) // CLLocationCoordinate2D
        
        txtLocation.text = objPlaceData.fullAddress
        //lblCurrentLocation.text = objPlaceData.fullAddress
        
        objLatitude = "\(objPlaceData.lattitude ?? 0.0)"
        objLongitude = "\(objPlaceData.longitude ?? 0.0)"
        
        DispatchQueue.main.async { // Setting marker on mapview in main thread.
            self.marker.map = self.mapView // Setting marker on Mapview
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: objPlaceData.lattitude ?? 0.0, longitude: objPlaceData.longitude ?? 0.0, zoom: 17.0)
        self.mapView?.animate(to: camera)
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickedSubmit(_ sender: Any) {
        if isBookLab == true
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewAddressViewController") as! AddNewAddressViewController
            vc.isBookLab = true
            vc.strCurrentLocation = self.lblCurrentLocation.text ?? ""
            vc.strTitle = "Add Address".localizeString(string: Language.shared.currentAppLang)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewAddressViewController") as! AddNewAddressViewController
            vc.isBookLab = false
            vc.strCurrentLocation = self.lblCurrentLocation.text ?? ""
            vc.strTitle = "Add Address".localizeString(string: Language.shared.currentAppLang)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    @IBAction func clickedSearchLocation(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchLocationVC") as! SearchLocationVC
        vc.delegateAddress = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // MARK: - call API
    
    
    // MARK: - Googlemap delegate Method
    
    private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error" + error.description)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        marker.position = position.target
        self.objLatitude = String(marker.position.latitude)
        self.objLongitude = String(marker.position.longitude)
        self.perform(#selector(self.updateAddress), with: nil, afterDelay: 1.0)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        marker.icon = UIImage(named: "placeholder") // Marker icon
        marker.position = CLLocationCoordinate2D(latitude: location?.coordinate.latitude ?? 0.0, longitude: location?.coordinate.longitude ?? 0.0) // CLLocationCoordinate2D
        
        objLatitude = "\(location?.coordinate.latitude ?? 0.0)"
        objLongitude = "\(location?.coordinate.longitude ?? 0.0)"
        
        DispatchQueue.main.async { // Setting marker on mapview in main thread.
            self.marker.map = self.mapView // Setting marker on Mapview
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        self.mapView?.animate(to: camera)
        
        getAddressFromLatLong(pdblLatitude: "\((location?.coordinate.latitude)!)", withLongitude: "\((location?.coordinate.longitude)!)")
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        //   self.locationManager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        
        marker.position = coordinate
        
        self.marker.map = self.mapView
        
        self.objLatitude = String(coordinate.latitude)
        self.objLongitude = String(coordinate.longitude)
        
        getAddressFromLatLong(pdblLatitude: String((coordinate.latitude)), withLongitude: String((coordinate.longitude)))
        
    }
    
    @objc func updateAddress(){
        self.getAddressFromLatLong(pdblLatitude: String((marker.position.latitude)), withLongitude: String(( marker.position.longitude)))
    }
    
    func getAddressFromLatLong(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        
        latLong(lat: lat ?? 0.0, long: lon ?? 0.0)
    }
    
    func getAddressFromLatLon(latitude: Double, longitude: Double, apiKey: String, completion: @escaping (String?) -> Void) {
        let urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(apiKey)"

        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completion(nil)
                return
            }

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]

                if let results = json?["results"] as? [[String: Any]], let firstResult = results.first, let formattedAddress = firstResult["formatted_address"] as? String {
                    completion(formattedAddress)
                } else {
                    completion(nil)
                }
            } catch {
                completion(nil)
            }
        }.resume()
    }
    
    func latLong(lat: Double,long: Double)  {

//        let geoCoder = CLGeocoder()
//        let location = CLLocation(latitude: lat , longitude: long)
//
        getAddressFromLatLon(latitude: lat, longitude: long, apiKey: "AIzaSyCaw_s4MWxJOePq8VqDmPy7eRlKCez8o6k") { address in
            if let address = address {
                print("Address: \(address)")
                
                DispatchQueue.main.async {
                    self.lblCurrentLocation.text = address
                }
                
                
            } else {
                print("Unable to get address.")
            }
        }
        
//        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
//
//            print("Response GeoLocation : \(placemarks)")
//            var placeMark: CLPlacemark!
//            placeMark = placemarks?[0]
//
//            if placemarks != nil
//            {
//                if let country = placeMark.addressDictionary!["Country"] as? String {
//                    print("Country :- \(country)")
//                    // City
//                    if let city = placeMark.addressDictionary!["City"] as? String {
//                        print("City :- \(city)")
//                        // State
//                        if let state = placeMark.addressDictionary!["State"] as? String{
//                            print("State :- \(state)")
//                            // Street
//                            if let street = placeMark.addressDictionary!["Street"] as? String{
//                                print("Street :- \(street)")
//                                let str = street
//                                let streetNumber = str.components(
//                                    separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
//                                print("streetNumber :- \(streetNumber)" as Any)
//
//                                // ZIP
//                                if let zip = placeMark.addressDictionary!["ZIP"] as? String{
//                                    print("ZIP :- \(zip)")
//                                    // Location name
//                                    if let locationName = placeMark?.addressDictionary?["Name"] as? String {
//                                        print("Location Name :- \(locationName)")
//                                        // Street address
//                                        if let thoroughfare = placeMark?.addressDictionary!["Thoroughfare"] as? NSString {
//                                            print("Thoroughfare :- \(thoroughfare)")
//
//                                            self.lblCurrentLocation.text = "\(locationName), \(street), \(city), \(zip), \(country)"
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        })
    }
    
}

