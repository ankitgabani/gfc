//
//  MyOrderVC.swift
//  GFC Globalhealth
//
//  Created by Code on 02/03/23.
//

import UIKit
import Quickblox
class MyOrderVC: UIViewController {

    @IBOutlet weak var TblViewMyOrder: UITableView!
    @IBOutlet weak var viewNoData: UIView!
    
    @IBOutlet weak var lblTMyOrder: UILabel!
    
    @IBOutlet weak var lblNoOrde: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    var arrProduct: [GFCOrderHistoryProduct] = [GFCOrderHistoryProduct]()
    
    var dicOrderList = GFCOrderHistoryOrder()
    
    var arrData: [GFCOrderHistoryData] = [GFCOrderHistoryData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTMyOrder.text = "My Orders".localizeString(string: Language.shared.currentAppLang)
        lblNoOrde.text = "No orders yet".localizeString(string: Language.shared.currentAppLang)

        TblViewMyOrder.delegate = self
        TblViewMyOrder.dataSource = self

        callMyOrderListAPI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        appDelegate?.setUpSideMenu()
    }
    
    func callMyOrderListAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["patientId":appDelegate?.dicUserLoginData.id ?? "","pageNo":"10000","token":"123456"]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_ORDER_H, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                       
                        for obj in arrData!
                        {
                            let dicData = GFCOrderHistoryData(fromDictionary: obj as! NSDictionary)
                            self.arrData.append(dicData)
                        }
                        self.TblViewMyOrder.isHidden = false
                        self.viewNoData.isHidden = true
                    }
                    else
                    {
                        if self.arrData.count == 0
                        {
                            self.TblViewMyOrder.isHidden = true
                            self.viewNoData.isHidden = false
                        }
                        else
                        {
                            self.TblViewMyOrder.isHidden = false
                            self.viewNoData.isHidden = true
                        }
                    }
                    self.TblViewMyOrder.reloadData()
                }
            }
        }
    }
    

}
extension MyOrderVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.TblViewMyOrder.dequeueReusableCell(withIdentifier: "MyOrderListTblViewCell") as! MyOrderListTblViewCell
        
        let dicData = arrData[indexPath.row].order

        cell.lblPrice.text = "₹ \(dicData?.amount ?? "")"
        cell.lblOrderId.text = dicData?.orderId ?? ""
        cell.lblProductStatus.text = dicData?.orderStatus.capitalizingFirstLetter()
        
        cell.lblTOrderId.text = "Order Id :".localizeString(string: Language.shared.currentAppLang)
 
        cell.lblProductQuntity.text = "(\(arrData[indexPath.row].products.count ?? 0) Products)"
        
        let bidAccepted = dicData?.created ?? ""
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date1 = formatter.date(from: bidAccepted)
        let Dform = DateFormatter()
        Dform.dateFormat = "MMM dd, yyyy"
        let strDate = Dform.string(from: date1!)
        cell.lblDate.text = strDate
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
        vc.objOrderDetails = arrData[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}
class MyOrderListTblViewCell: UITableViewCell{
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblProductQuntity: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblProductStatus: UILabel!
    
    @IBOutlet weak var lblTOrderId: UILabel!
    
}

extension String {
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
    }
}
