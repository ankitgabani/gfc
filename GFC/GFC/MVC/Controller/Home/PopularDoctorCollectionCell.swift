//
//  PopularDoctorCollectionCell.swift
//  GFC
//
//  Created by Parth Anghan on 21/10/21.
//

import UIKit
import HCSStarRatingView

class PopularDoctorCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var lblDrName: UILabel!
    @IBOutlet weak var lblWork: UILabel!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var lblSp: UILabel!
}
