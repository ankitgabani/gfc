//
//  MyCartViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 08/04/22.
//

import UIKit
import GMStepper

class MyCartViewController: UIViewController {

    @IBOutlet weak var productTableView: UITableView!
    @IBOutlet weak var lblPriceCount: UILabel!
    @IBOutlet weak var viewNoDataFound: UIView!
    @IBOutlet weak var btnChackout: UIButton!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewBGMain: UIView!
    
    @IBOutlet weak var lblTCart: UILabel!
    @IBOutlet weak var lblTTotalPrice: UILabel!
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var arrCartList: [GFCCartListCartProductList] = [GFCCartListCartProductList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTCart.text = "Cart".localizeString(string: Language.shared.currentAppLang)
        lblTTotalPrice.text = "Total Price".localizeString(string: Language.shared.currentAppLang)
        lblTNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        
        btnChackout.setTitle("Checkout".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        viewBG.isHidden = true
        viewBGMain.isHidden = true

        productTableView.delegate = self
        productTableView.dataSource = self
        
        callCartListAPI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.myNotificationKeyPopUp, object: nil)
       
    }
    
    @objc func onNotification(notification:Notification)
    {
        viewBG.isHidden = false
        viewBGMain.isHidden = false
    }

    
    @IBAction func onBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func clickedOk(_ sender: Any) {
        viewBG.isHidden = true
        viewBGMain.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @objc func stepperValueDidChanges(sender: GMStepper){
        print(sender.value, terminator: "")

    }
    @IBAction func btnCheckOutClicked(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "BookTestPopupViewController") as! BookTestPopupViewController

        controller.modalPresentationStyle = .overCurrentContext
        controller.parentVC = self
        controller.isOpenFromLab = false
        controller.strTopTitle = "Place Order".localizeString(string: Language.shared.currentAppLang)
        controller.strBtnTitle = "Place Order".localizeString(string: Language.shared.currentAppLang)
        controller.strPrice = lblPriceCount.text ?? ""
        controller.dicCart = arrCartList
        let navController = UINavigationController(rootViewController: controller) //Add navigation controller
        navController.navigationBar.isHidden = true
        navController.modalPresentationStyle = .overFullScreen

        self.present(navController, animated: false, completion: nil)
    }
    
    // MARK: - call API
    
    func callCartListAPI()
    {
        let para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? ""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CART_ITEM, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    let sub_total = response?.value(forKey: "sub_total") as? Int
                    let offer = response?.value(forKey: "offer") as? String
                    let total = response?.value(forKey: "total") as? Int
                    
                    self.lblPriceCount.text = "\(appDelegate?.currentCountry.currency ?? "") \(total ?? 0)"
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "cartProductList") as? NSArray
                        self.arrCartList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCartListCartProductList(fromDictionary: obj as! NSDictionary)
                            self.arrCartList.append(dicData)
                        }
                        
                        if self.arrCartList.count > 0
                        {
                            self.productTableView.isHidden = false
                            self.viewNoDataFound.isHidden = true
                            self.btnChackout.backgroundColor = UIColor(red: 23/255, green: 52/255, blue: 73/255, alpha: 1)
                            self.btnChackout.isUserInteractionEnabled = true
                        }
                        else
                        {
                            self.productTableView.isHidden = true
                            self.viewNoDataFound.isHidden = false
                            self.btnChackout.backgroundColor = UIColor(red: 125/255, green: 125/255, blue: 125/255, alpha: 1)
                            self.btnChackout.isUserInteractionEnabled = false
                        }
                        self.productTableView.reloadData()
                    }
                }
            }
        }
    }

    func callDeleteCartAPI(cart_id:String)
    {
        let para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? "","cart_id":cart_id]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(DELETE_CART, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String

                    if status == "success"
                    {
                        self.view.makeToast("Item deleted from cart".localizeString(string: Language.shared.currentAppLang))
                        self.callCartListAPI()
                    }
                    else
                    {
                        self.view.makeToast(message)
                    }
                }
            }
        }
    }
    
}

extension MyCartViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCartList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartProductTableViewCell") as! CartProductTableViewCell
        
        let dicData = arrCartList[indexPath.row]
        
        
        cell.LblProductName.text = dicData.productName ?? ""
        
        
        if dicData.size != ""
        {
            cell.lblProductSize.text = "\("Size :".localizeString(string: Language.shared.currentAppLang)) \(dicData.size ?? "")"
            cell.lblPice.text = "\(appDelegate?.currentCountry.currency ?? "") \(dicData.discountedPrice ?? "")"
        }
        else if dicData.size1 != ""
        {
            cell.lblProductSize.text = "\("Size :".localizeString(string: Language.shared.currentAppLang)) \(dicData.size1 ?? "")"
            cell.lblPice.text = "\(appDelegate?.currentCountry.currency ?? "") \(dicData.discountedPrice1 ?? "")"
        }
        else if dicData.size2 != ""
        {
            cell.lblProductSize.text = "\("Size :".localizeString(string: Language.shared.currentAppLang)) \(dicData.size2 ?? "")"
            cell.lblPice.text = "\(appDelegate?.currentCountry.currency ?? "") \(dicData.discountedPrice2 ?? "")"
        }
        else if dicData.size3 != ""
        {
            cell.lblProductSize.text = "\("Size :".localizeString(string: Language.shared.currentAppLang)) \(dicData.size3 ?? "")"
            cell.lblPice.text = "\(appDelegate?.currentCountry.currency ?? "") \(dicData.discountedPrice3 ?? "")"
        }
        
        
        cell.lblCount.text = "\(dicData.quantity ?? "")"
       
        var media_link_url = dicData.image ?? ""
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        cell.imgProduct.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        cell.viewMain.layer.applySketchShadow(color: .blue, alpha: 0.2, x: 0, y: 0, blur: 8, spread: 0)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(clickedDeleteItem(_:)), for: .touchUpInside)
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(clickedAddItem(_:)), for: .touchUpInside)
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(clickedRemoveItem(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func clickedDeleteItem(_ sender: UIButton)
    {
        let dicData = arrCartList[sender.tag]
        
        callDeleteCartAPI(cart_id: dicData.cartId ?? "")
    }
    
    @objc func clickedAddItem(_ sender: UIButton)
    {
        let dicData = arrCartList[sender.tag]
        
        let index = sender.tag
        let cell = productTableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as! CartProductTableViewCell
        
        var quantity = Int(dicData.quantity ?? "") ?? 0
        quantity = quantity + 1
        
        cell.lblCount.text = "\(Int(quantity))"
        
        dicData.quantity = "\(Int(quantity))"
        
        callUpdateCartAPI(objData: dicData, objQty: "\(Int(quantity))")
    }
    
    @objc func clickedRemoveItem(_ sender: UIButton)
    {
        let dicData = arrCartList[sender.tag]
        
        let index = sender.tag
        let cell = productTableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as! CartProductTableViewCell
        
        var quantity = Int(dicData.quantity ?? "") ?? 0
        
        if quantity != 1
        {
            quantity = quantity - 1
            
            cell.lblCount.text = "\(Int(quantity))"
            
            dicData.quantity = "\(Int(quantity))"
            
            callUpdateCartAPI(objData: dicData, objQty: "\(Int(quantity))")
        }
      
    }
   
    func callUpdateCartAPI(objData: GFCCartListCartProductList,objQty: String)
    {
      
        let para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? "","productId":objData.pId ?? "","quantity":objQty,"varient":objData.varient ?? ""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(UPDATR_CART_ITEM, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    self.callCartListAPI()
                 }
            }
        }
    }
    
}

class CartProductTableViewCell: UITableViewCell{
    
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var LblProductName: UILabel!
    @IBOutlet weak var lblProductSize: UILabel!
    @IBOutlet weak var stepper: UIView!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblPice: UILabel!
    @IBOutlet weak var viewMain: UIView!
    
}
