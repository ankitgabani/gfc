//
//  PharmayCategoryViewController.swift
//  GFC
//
//  Created by iMac on 15/03/22.
//

import UIKit
import SDWebImage
import DropDown

class PharmayCategoryViewController: UIViewController {
    
    @IBOutlet weak var pharmacyCategoryCollectionView: UICollectionView!
    @IBOutlet weak var lvlCartCount: UILabel!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var btnDrop: UIButton!
   
    @IBOutlet weak var countryDropView: UIView!
    
    @IBOutlet weak var lblTLocation: UILabel!
    @IBOutlet weak var lblTCategories: UILabel!
    
    
    let sectionInsetsCat = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    let itemsPerRowCat: CGFloat = 3
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.sectionInsetsCat.left * (self.itemsPerRowCat + 1)
            let availableWidth = self.pharmacyCategoryCollectionView.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.itemsPerRowCat
            
            _flowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
            _flowLayout.minimumInteritemSpacing = 0
            _flowLayout.minimumLineSpacing = 0
        }
        
        // edit properties here
        return _flowLayout
    }
    
    var arrPharmacyCat: GFCPharmacyCat?
    
    let dropDownCoutnry = DropDown()
    
    var arrCountryList: [GFCCountryListData] = [GFCCountryListData]()
    
    var strCountry = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTLocation.text = "Location".localizeString(string: Language.shared.currentAppLang)
        lblTCategories.text = "Categories".localizeString(string: Language.shared.currentAppLang)
        
        pharmacyCategoryCollectionView.dataSource = self
        pharmacyCategoryCollectionView.delegate = self
        pharmacyCategoryCollectionView.collectionViewLayout = flowLayout
 
    }
 
    func setDropDownCountry()
    {
        let Country = NSMutableArray()
        
        for obj in arrCountryList
        {
            Country.add(obj.countryName ?? "")
        }
        
        if Language.shared.isArabic{
            self.dropDownCoutnry.anchorView = countryDropView
        }
        else{
            self.dropDownCoutnry.anchorView = txtCountry
        }
        
        dropDownCoutnry.dataSource = Country as! [String]
        dropDownCoutnry.anchorView = btnDrop
        dropDownCoutnry.direction = .bottom
        
        dropDownCoutnry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCountry.text = item
            
            for obj in arrCountryList
            {
                if obj.countryName == item
                {
                    self.strCountry = obj.countryId ?? ""
                    appDelegate?.currentCountry = obj
                }
            }
            
            self.callPharmacyCategoryAPI()
        }
        
        dropDownCoutnry.bottomOffset = CGPoint(x: 0, y: btnDrop.bounds.height)
        dropDownCoutnry.topOffset = CGPoint(x: 0, y: -btnDrop.bounds.height)
        dropDownCoutnry.dismissMode = .onTap
        dropDownCoutnry.textColor = UIColor.darkGray
        dropDownCoutnry.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCoutnry.selectionBackgroundColor = UIColor.clear
        dropDownCoutnry.selectedTextColor = .black
        dropDownCoutnry.reloadAllComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        callCountryListAPI()
        
        self.callPharmacyCategoryAPI()
        callCartListAPI()
    }
    
    @IBAction func clickedCountry(_ sender: Any) {
        dropDownCoutnry.show()
    }
    
    @IBAction func clickedSearch(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchProductVC") as! SearchProductVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onBtnCart(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- API
    
    func callPharmacyCategoryAPI() {
        
        let param = ["token": "12345678","country":appDelegate?.currentCountry.countryId ?? ""]
        
        APIClient.sharedInstance.requestWith(method: .post, endPoint: PHARMACY_CATEGORY, loader: true, params: param, model: GFCPharmacyCat.self, success: { (result) in
            
            print(result)
            if let model = result as? GFCPharmacyCat{
                self.arrPharmacyCat = model
                
                self.pharmacyCategoryCollectionView.reloadData()
            }
        }, failure: { (error) in
            self.view.makeToast("Something went wrong!")
        })
    }
    
    func callCartListAPI()
    {
        let para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? ""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CART_ITEM, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let cartProductList = response?.value(forKey: "cartProductList") as? NSArray

                    if status == "success"
                    {
                        self.lvlCartCount.text = "\(cartProductList?.count ?? 0)"
                    }
                }
            }
        }
    }
    
    func callCountryListAPI()
    {
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCountryList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if dicData.status == "1"
                            {
                                self.arrCountryList.append(dicData)
                            }
                            
                        }
                        
                        if self.arrCountryList.count > 0
                        {
                            if appDelegate?.currentCountry.countryName == nil
                            {
                                self.strCountry = self.arrCountryList[0].countryId ?? ""
                                self.txtCountry.text = self.arrCountryList[0].countryName ?? ""
                                appDelegate?.currentCountry = self.arrCountryList[0]
                                self.callPharmacyCategoryAPI()
                            }
                            else
                            {
                                self.txtCountry.text = appDelegate?.currentCountry.countryName ?? ""
                                self.strCountry = appDelegate?.currentCountry.countryId ?? ""
                                self.callPharmacyCategoryAPI()
                            }
                        }
                        
                        self.setDropDownCountry()
                    }
                }
            }
        }
    }
    
}

extension PharmayCategoryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPharmacyCat?.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PharmacyCategoryCollectionViewCell", for: indexPath) as! PharmacyCategoryCollectionViewCell
        
        let dicData = arrPharmacyCat?.data?[indexPath.item]
        
        var imgBody = dicData?.image ?? ""
        
        imgBody = imgBody.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: "\(imgBody)")
        cell.categoryImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.categoryImage.sd_setImage(with: url, placeholderImage: UIImage(named: "app_logo"))
        
        cell.lblTitle.text = dicData?.pharmacyCateName ?? ""
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PharmacySubCatViewController") as! PharmacySubCatViewController
        controller.categoryID = arrPharmacyCat?.data?[indexPath.item].pharmacyCateID ?? ""
        controller.strCountryID = self.strCountry
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width / 3 - 3, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    
}

class PharmacyCategoryCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
}



// MARK: - Welcome
struct GFCPharmacyCat: Codable {
    let status, message: String?
    let data: [Datum]?
}

// MARK: - Datum
struct Datum: Codable {
    let pharmacyCateID, pharmacyCateName, pharmacyCateDescription: String?
    let image: String?
    let createdAt, status: String?
    
    enum CodingKeys: String, CodingKey {
        case pharmacyCateID = "pharmacy_cate_id"
        case pharmacyCateName = "pharmacy_cate_name"
        case pharmacyCateDescription = "pharmacy_cate_description"
        case image, createdAt, status
    }
}

