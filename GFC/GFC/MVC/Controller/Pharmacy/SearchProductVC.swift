//
//  SearchProductVC.swift
//  GFC Globalhealth
//
//  Created by iMac on 13/03/23.
//

import UIKit
import DropDown

class SearchProductVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var txtConyrt: UITextField!
    @IBOutlet weak var collectionSearch: UICollectionView!
    
    @IBOutlet weak var lblTLocation: UILabel!
    
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    let sectionInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
    let sitemsPerRow : CGFloat = 2
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.sectionInsets.left * (self.sitemsPerRow + 1)
            let availableWidth = self.collectionSearch.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.sitemsPerRow
            
            _flowLayout.itemSize = CGSize(width: widthPerItem, height: 225)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
            _flowLayout.minimumInteritemSpacing = 10
            _flowLayout.minimumLineSpacing = 10
        }
        
        // edit properties here
        return _flowLayout
    }
    
    var arrCountryList: [GFCCountryListData] = [GFCCountryListData]()
    
    var arrSearchProduct: [GFCSearchProductData] = [GFCSearchProductData]()
    
    var strCountryID = ""
    
    let dropDownCountry = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            txtSearch.textAlignment = .left
        }else{
            txtSearch.textAlignment = .right
        }

        collectionSearch.delegate = self
        collectionSearch.dataSource = self
        collectionSearch.collectionViewLayout = flowLayout
        
       // callCountryListAPI()
        
        self.txtSearch.delegate = self
        self.txtSearch.addTarget(self, action: #selector(searchProductAsPerText(_ :)), for: .editingChanged)
        
        // Do any additional setup after loading the view.
    }
    @objc func searchProductAsPerText(_ textfield:UITextField)
    {
        
        if textfield.text == ""
        {
            self.viewNoData.isHidden = false
            self.arrSearchProduct.removeAll()
            collectionSearch.reloadData()
        }
        else
        {
            callSearchProductAPI(country: appDelegate?.currentCountry.countryId ?? "", product_name: textfield.text ?? "")
        }
    }
    
    func setDropDownCountry()
    {
        let Country = NSMutableArray()
        
        for obj in arrCountryList
        {
            Country.add(obj.countryName ?? "")
        }
        
        dropDownCountry.dataSource = Country as! [String]
        dropDownCountry.anchorView = btnDrop
        dropDownCountry.direction = .bottom
        
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtConyrt.text = item
            
            for obj in arrCountryList
            {
                if obj.countryName == item
                {
                    self.strCountryID = obj.countryId ?? ""
                    appDelegate?.currentCountry = obj
                }
            }
        }
        dropDownCountry.bottomOffset = CGPoint(x: 0, y: btnDrop.bounds.height)
        dropDownCountry.topOffset = CGPoint(x: 0, y: -btnDrop.bounds.height)
        dropDownCountry.dismissMode = .onTap
        dropDownCountry.textColor = UIColor.darkGray
        dropDownCountry.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCountry.selectionBackgroundColor = UIColor.clear
        dropDownCountry.selectedTextColor = .black
        dropDownCountry.reloadAllComponents()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickedCountry(_ sender: Any) {
        dropDownCountry.show()
    }
    
    //MARK: - API
    
    func callCountryListAPI()
    {
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCountryList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if dicData.status == "1"
                            {
                                self.arrCountryList.append(dicData)
                            }
                            
                        }
                        
                        if self.arrCountryList.count > 0
                        {
                            if appDelegate?.currentCountry.countryName == nil
                            {
                                self.strCountryID = self.arrCountryList[0].countryId ?? ""
                                self.txtConyrt.text = self.arrCountryList[0].countryName ?? ""
                                appDelegate?.currentCountry = self.arrCountryList[0]
                                 
                            }
                            else
                            {
                                 self.txtConyrt.text = appDelegate?.currentCountry.countryName ?? ""
                                self.strCountryID = appDelegate?.currentCountry.countryId ?? ""
                                 
                            }
                        }
                        
                        self.setDropDownCountry()
                
                    }
                }
            }
        }
    }
 
    func callSearchProductAPI(country:String,product_name:String)
    {
        let para = ["country":country,"product_name":product_name]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(SEARCH_PRODUCT, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrSearchProduct.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCSearchProductData(fromDictionary: obj as! NSDictionary)
                            self.arrSearchProduct.append(dicData)
                        }
                        self.collectionSearch.reloadData()
                        
                        if self.arrSearchProduct.count == 0
                        {
                            self.viewNoData.isHidden = false
                        }
                        else
                        {
                            self.viewNoData.isHidden = true
                        }
                        
                    }
                    else
                    {
                        self.viewNoData.isHidden = false
                    }
                }
                else
                {
                    self.viewNoData.isHidden = false
                }
            }
        }
    }

}

extension SearchProductVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSearchProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionSearch.dequeueReusableCell(withReuseIdentifier: "SearchProductCell", for: indexPath) as! SearchProductCell
        
        let dicData = arrSearchProduct[indexPath.row]
        
        cell.lblName.text = dicData.name ?? ""
        cell.lblPrice.text = "₹ \(dicData.discountedPrice ?? "")"
      
        if dicData.rating == ""
        {
            cell.lblRating.text = "0"
        }
        else
        {
            let rating = Double(dicData.rating ?? "")?.round(to: 1)
            cell.lblRating.text = "\(rating ?? 0.0)"
        }
        
        let attributedText = NSAttributedString(string: "₹ \(dicData.price ?? "")",
            attributes: [.strikethroughStyle: NSUnderlineStyle.single.rawValue]
        )
        cell.lblSPrice.attributedText = attributedText
        
        var media_link_url = dicData.image ?? ""
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dicData = arrSearchProduct[indexPath.row]
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        controller.pID = dicData.pId ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
}

class SearchProductCell: UICollectionViewCell
{
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSPrice: UILabel!
    
    @IBOutlet weak var lblRating: UILabel!
}
