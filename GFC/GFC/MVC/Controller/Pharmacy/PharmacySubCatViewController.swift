//
//  PharmacySubCatViewController.swift
//  GFC
//
//  Created by iMac on 15/03/22.
//

import UIKit
import SDWebImage
import DropDown

class PharmacySubCatViewController: UIViewController {

    
    @IBOutlet weak var viewNoDate: UIView!
    
    @IBOutlet weak var TablesubCategory: UITableView!
    @IBOutlet weak var collectionChildCategory: UICollectionView!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var lblCartCount: UILabel!
    
    @IBOutlet weak var lbltLocation: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    let SocityNoticesectionInsetsCat = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    let SocityNoticeitemsPerRowCat: CGFloat = 2
    
    var SocityNoticeflowLayout: UICollectionViewFlowLayout {
        let _SocityNoticeflowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            
            let mainViewWidth = self.view.frame.width - 100
            let paddingSpace = self.SocityNoticesectionInsetsCat.left * (self.SocityNoticeitemsPerRowCat + 1)
            let availableWidth = mainViewWidth - paddingSpace
            let widthPerItem = availableWidth / self.SocityNoticeitemsPerRowCat
            
            _SocityNoticeflowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem)
            
            _SocityNoticeflowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            _SocityNoticeflowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
            _SocityNoticeflowLayout.minimumInteritemSpacing = 5
            _SocityNoticeflowLayout.minimumLineSpacing = 5
            
        }
        
        // edit properties here
        return _SocityNoticeflowLayout
    }
    
    var categoryID = ""
    var arrPharmacySubCat: [GFCPharmacySubCatData] = [GFCPharmacySubCatData]()
  
    var arrPharmacyChildCat: [GFCPharmacySubCatData] = [GFCPharmacySubCatData]()
    
    var arrCountryList: [GFCCountryListData] = [GFCCountryListData]()
    
    var arrCartList: [GFCCartListCartProductList] = [GFCCartListCartProductList]()


    var strCountryID = ""
    
    let dropDownCountry = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }

        TablesubCategory.dataSource = self
        TablesubCategory.delegate = self
        collectionChildCategory.dataSource = self
        collectionChildCategory.delegate = self
        collectionChildCategory.collectionViewLayout = SocityNoticeflowLayout
     
   }
    
    override func viewWillAppear(_ animated: Bool) {
        
        callPharmacySubCategoryAPI()
        
        callPharmacySubChildAPI(childID: self.categoryID)
        
       callCountryListAPI()
        
        callCartListAPI()
        
        setDropDownCountry()
    }
    
    func setDropDownCountry()
    {
        let Country = NSMutableArray()
        
        for obj in arrCountryList
        {
            Country.add(obj.countryName ?? "")
        }
        
        dropDownCountry.dataSource = Country as! [String]
        dropDownCountry.anchorView = btnDrop
        dropDownCountry.direction = .bottom
        
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCountry.text = item
            
            for obj in arrCountryList
            {
                if obj.countryName == item
                {
                    self.strCountryID = obj.countryId ?? ""
                    appDelegate?.currentCountry = obj
                }
            }
        }
        dropDownCountry.bottomOffset = CGPoint(x: 0, y: btnDrop.bounds.height)
        dropDownCountry.topOffset = CGPoint(x: 0, y: -btnDrop.bounds.height)
        dropDownCountry.dismissMode = .onTap
        dropDownCountry.textColor = UIColor.darkGray
        dropDownCountry.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCountry.selectionBackgroundColor = UIColor.clear
        dropDownCountry.selectedTextColor = .black
        dropDownCountry.reloadAllComponents()
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onBtnCart(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func clickedCountry(_ sender: Any) {
        dropDownCountry.show()
    }
    @IBAction func clickedSearch(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchProductVC") as! SearchProductVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK: - API
    
    func callCountryListAPI()
    {
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCountryList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if dicData.status == "1"
                            {
                                self.arrCountryList.append(dicData)
                            }
                        }
                        
                        if self.arrCountryList.count > 0
                        {
                            if appDelegate?.currentCountry.countryName == nil
                            {
                                self.strCountryID = self.arrCountryList[0].countryId ?? ""
                                self.txtCountry.text = self.arrCountryList[0].countryName ?? ""
                                appDelegate?.currentCountry = self.arrCountryList[0]
                            }
                            else
                            {
                                 self.txtCountry.text = appDelegate?.currentCountry.countryName ?? ""
                                self.strCountryID = appDelegate?.currentCountry.countryId ?? ""
                             }
                        }
 
                        self.setDropDownCountry()
                
                    }
                }
            }
        }
    }
    
    func callPharmacySubCategoryAPI()
    {
        let para = ["token":"5151","categoryId":self.categoryID]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(PHARMACY_SUBCATEGORY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrPharmacySubCat.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCPharmacySubCatData(fromDictionary: obj as! NSDictionary)
                            self.arrPharmacySubCat.append(dicData)
                        }
                        self.TablesubCategory.reloadData()
                    }
                }
            }
        }
    }
    
    func callPharmacySubChildAPI(childID: String)
    {
        let para = ["token":"5151","subCategoryId":childID]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(PHARMACY_SUBCHILD, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrPharmacyChildCat.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCPharmacySubCatData(fromDictionary: obj as! NSDictionary)
                            self.arrPharmacyChildCat.append(dicData)
                        }
                        self.collectionChildCategory.reloadData()
                    }
                    
                    
                    if self.arrPharmacyChildCat.count == 0
                    {
                        self.viewNoDate.isHidden = false
                    }
                    else
                    {
                        self.viewNoDate.isHidden = true
                    }
                        
                    
                }
            }
        }
    }
    
    func callCartListAPI()
    {
        let para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? ""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CART_ITEM, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let cartProductList = response?.value(forKey: "cartProductList") as? NSArray

                    if status == "success"
                    {
                        self.lblCartCount.text = "\(cartProductList?.count ?? 0)"
                    }
                }
            }
        }
    }

}

extension PharmacySubCatViewController : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPharmacySubCat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryTableViewCell") as! SubCategoryTableViewCell
        
        let dicData = arrPharmacySubCat[indexPath.row]
        
        cell.lblTitle.text = dicData.name ?? ""
        
        var media_link_url = dicData.imageUrl ?? ""
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        cell.imageSubCategory.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrPharmacySubCat[indexPath.row]
        callPharmacySubChildAPI(childID: dicData.categoryId ?? "")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
}

extension PharmacySubCatViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPharmacyChildCat.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PharmacyChildCollectionViewCell", for: indexPath) as! PharmacyChildCollectionViewCell

        let dicData = arrPharmacyChildCat[indexPath.row]
        
        cell.lblTitle.text = dicData.name ?? ""
        
        var media_link_url = dicData.imageUrl ?? ""
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        cell.categoryImage.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let dicData = arrPharmacyChildCat[indexPath.row]
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PharmacyProductViewController") as! PharmacyProductViewController
        controller.strCateID = dicData.id ?? ""
        controller.catChildName = dicData.name ?? ""
        controller.strCountryID = self.strCountryID
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
   
    
    
}
class SubCategoryTableViewCell: UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageSubCategory: UIImageView!
}

class PharmacyChildCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
}
