//
//  PharmacyProductViewController.swift
//  GFC
//
//  Created by iMac on 15/03/22.
//

import UIKit
import  SDWebImage
import DropDown

class PharmacyProductViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collcetionViewProducts: UICollectionView!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var viewNoData: UIView!
    
    @IBOutlet weak var lblTLocation: UILabel!
    @IBOutlet weak var lblTNoData: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    let sitemsPerRow : CGFloat = 2
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.sectionInsets.left * (self.sitemsPerRow + 1)
            let availableWidth = self.collcetionViewProducts.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.sitemsPerRow
            
            _flowLayout.itemSize = CGSize(width: widthPerItem, height: 225)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
            _flowLayout.minimumInteritemSpacing = 10
            _flowLayout.minimumLineSpacing = 10
        }
        
        // edit properties here
        return _flowLayout
    }
    
    var arrProducts : PharmacyProductsModel?
    var catChildName = ""
    
    var arrCountryList: [GFCCountryListData] = [GFCCountryListData]()
    
    var arrPharmacyList: [GFCPharmacyProductData] = [GFCPharmacyProductData]()
    
    let dropDownCountry = DropDown()
    var strCountryID = ""
    
    var strCateID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        
        lblTitle.text = "\("All Products Of".localizeString(string: Language.shared.currentAppLang)) \(catChildName)"
        collcetionViewProducts.dataSource = self
        collcetionViewProducts.delegate = self
        collcetionViewProducts.collectionViewLayout = flowLayout
        
        self.viewNoData.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        callCountryListAPI()
        
        self.callPharmacyListAPI(categoryId: self.strCateID, country: self.strCountryID)
        callCartListAPI()
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnCart(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func clickedCountry(_ sender: Any) {
        dropDownCountry.show()
    }
    
    func setDropDownCountry()
    {
        let Country = NSMutableArray()
        
        for obj in arrCountryList
        {
            Country.add(obj.countryName ?? "")
        }
        
        dropDownCountry.dataSource = Country as! [String]
        dropDownCountry.anchorView = btnDrop
        dropDownCountry.direction = .bottom
        
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCountry.text = item
            
            for obj in arrCountryList
            {
                if obj.countryName == item
                {
                    self.strCountryID = obj.countryId ?? ""
                    appDelegate?.currentCountry = obj
                }
            }
            callPharmacyListAPI(categoryId: self.strCateID, country: self.strCountryID)
        }
        
        dropDownCountry.bottomOffset = CGPoint(x: 0, y: btnDrop.bounds.height)
        dropDownCountry.topOffset = CGPoint(x: 0, y: -btnDrop.bounds.height)
        dropDownCountry.dismissMode = .onTap
        dropDownCountry.textColor = UIColor.darkGray
        dropDownCountry.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCountry.selectionBackgroundColor = UIColor.clear
        dropDownCountry.selectedTextColor = .black
        dropDownCountry.reloadAllComponents()
    }
    //MARK: - API
    
    func callPharmacyListAPI(categoryId:String,country:String)
    {
        let para = ["token":"652566","categoryId":categoryId,"country":country]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(PHARMACY_PRODUCTS, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrPharmacyList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCPharmacyProductData(fromDictionary: obj as! NSDictionary)
                            self.arrPharmacyList.append(dicData)
                        }
                        
                        if self.arrPharmacyList.count == 0
                        {
                            self.viewNoData.isHidden = false
                            self.collcetionViewProducts.isHidden = true
                        }
                        else
                        {
                            self.viewNoData.isHidden = true
                            self.collcetionViewProducts.isHidden = false
                        }
                        
                        self.collcetionViewProducts.reloadData()
                    }
                }
            }
        }
    }
    
    func callCountryListAPI()
    {
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCountryList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            
                            if dicData.status == "1"
                            {
                                self.arrCountryList.append(dicData)
                            }
                            
                        }
                        
                        if self.arrCountryList.count > 0
                        {
                            if appDelegate?.currentCountry.countryName == nil
                            {
                                self.strCountryID = self.arrCountryList[0].countryId ?? ""
                                self.txtCountry.text = self.arrCountryList[0].countryName ?? ""
                                appDelegate?.currentCountry = self.arrCountryList[0]
                                self.callPharmacyListAPI(categoryId: self.strCateID, country: self.strCountryID)
                            }
                            else
                            {
                                self.txtCountry.text = appDelegate?.currentCountry.countryName ?? ""
                                self.strCountryID = appDelegate?.currentCountry.countryId ?? ""
                                self.callPharmacyListAPI(categoryId: self.strCateID, country: self.strCountryID)
                            }
                        }
                        
                        self.setDropDownCountry()
                    }
                }
            }
        }
    }
    
    func callCartListAPI()
    {
        let para = ["token":"54545","patientId":appDelegate?.dicUserLoginData.id ?? ""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CART_ITEM, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let cartProductList = response?.value(forKey: "cartProductList") as? NSArray
                    
                    if status == "success"
                    {
                        self.lblCartCount.text = "\(cartProductList?.count ?? 0)"
                    }
                }
            }
        }
    }
    
    
}

extension PharmacyProductViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPharmacyList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PharmacyProductsCollectionViewCell", for: indexPath) as! PharmacyProductsCollectionViewCell
        
        let dicData = arrPharmacyList[indexPath.row]
        
        cell.lblTiitle.text = dicData.pharmacy ?? ""
        cell.lblPrice.text = "\(appDelegate?.currentCountry.currency ?? "") \(dicData.discountedPrice ?? "")"
        
        if dicData.rating == ""
        {
            cell.lblRating.text = "0"
        }
        else
        {
            let rating = Double(dicData.rating ?? "")?.round(to: 1)
            cell.lblRating.text = "\(rating ?? 0.0)"
        }
        
        var media_link_url = dicData.image ?? ""
        media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
        cell.imageMain.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "\(appDelegate?.currentCountry.currency ?? "") \(dicData.price ?? "")")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
        cell.lblLowPrice.attributedText = attributeString
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let dicData = arrPharmacyList[indexPath.row]
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        controller.pID = dicData.pId ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
}

class PharmacyProductsCollectionViewCell: UICollectionViewCell{
    
    override func awakeFromNib() {
        super.awakeFromNib()

        viewRating.layer.cornerRadius = 9
        viewRating.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    @IBOutlet weak var imageMain: UIImageView!
    @IBOutlet weak var lblTiitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblStar: UILabel!
    @IBOutlet weak var lblLowPrice: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var viewRating: UIView!
}

// MARK: - PharmacyProductsModel
struct PharmacyProductsModel: Codable {
    let status, message: String?
    let data: [PharmacyProductsDatum]?
}

// MARK: - PharmacyProductsDatum
struct PharmacyProductsDatum: Codable {
    let pID, name: String?
    let category, subCategoryName, childCategoryName: String?
    let pharmacy, productStock, moqPerUser, price: String?
    let image: String?
    let createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case pID = "pId"
        case name, category, subCategoryName, childCategoryName, pharmacy, productStock
        case moqPerUser = "moq_per_user"
        case price, image, createdAt
    }
}
