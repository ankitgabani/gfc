//
//  AddNewAddressViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 18/04/22.
//

import UIKit
import DialCountries
import DropDown

class AddNewAddressViewController: UIViewController {
    
    @IBOutlet weak var lblC_Code: UITextField!
    @IBOutlet weak var viewTargetCountry: UIView!
    
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtLocality: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblFullnameError: UILabel!
    @IBOutlet weak var lblPhoneNumberError: UILabel!
    @IBOutlet weak var lblAddressError: UILabel!
    @IBOutlet weak var lblLocalityError: UILabel!
    @IBOutlet weak var lblCityError: UILabel!
    @IBOutlet weak var lblStateError: UILabel!
    @IBOutlet weak var lblZipError: UILabel!
    
    @IBOutlet weak var homeView: UIView!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var officeMainView: UIView!
    @IBOutlet weak var officeeBtn: UIButton!
    @IBOutlet weak var lblCurrentLocation: UILabel!
    
    @IBOutlet weak var viewCurrentLoc: UIView!
    @IBOutlet weak var heightCurrentLoc: NSLayoutConstraint! // 80
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var lblTAddAddress: UILabel!
    @IBOutlet weak var lblTContactDetail: UILabel!
    
    @IBOutlet weak var lblTAddressDetail: UILabel!
    @IBOutlet weak var lblTUseCuurentLocation: UILabel!
    @IBOutlet weak var lblTAddressType: UILabel!
    
    @IBOutlet weak var lblTHome: UILabel!
    @IBOutlet weak var lblTOffice: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    var dicData = GFCAddressListData()
    
    var selectedAddressType = String()
    var isUpdate = false
    
    var strTitle = ""
    
    var strCurrentLocation = ""
    
    var isBookLab = false
    
    var arrCountryList = NSMutableArray()
    
    let dropDownCountry = DropDown()
    let dropDownState = DropDown()
    let dropDownCity = DropDown()
    
    var country_id = ""
    var state_id = ""
    var city_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTitle.text = strTitle.localizeString(string: Language.shared.currentAppLang)
        
        lblCurrentLocation.text = strCurrentLocation
        
        lblTContactDetail.text = "Contact Detail".localizeString(string: Language.shared.currentAppLang)
        lblTAddressDetail.text = "Address Detail".localizeString(string: Language.shared.currentAppLang)
        lblTAddressType.text = "Address Type".localizeString(string: Language.shared.currentAppLang)
        lblTUseCuurentLocation.text = "Use Current Location".localizeString(string: Language.shared.currentAppLang)
        lblTHome.text = "Home".localizeString(string: Language.shared.currentAppLang)
        lblTOffice.text = "Office".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            txtFullName.textAlignment = .left
            txtPhoneNumber.textAlignment = .left
            txtAddress.textAlignment = .left
            txtLocality.textAlignment = .left
            txtCity.textAlignment = .left
            txtZipCode.textAlignment = .left
            txtState.textAlignment = .left
            txtCountry.textAlignment = .left
        }else{
            txtFullName.textAlignment = .right
            txtPhoneNumber.textAlignment = .right
            txtAddress.textAlignment = .right
            txtLocality.textAlignment = .right
            txtCity.textAlignment = .right
            txtZipCode.textAlignment = .right
            txtState.textAlignment = .right
            txtCountry.textAlignment = .right
        }

        if isUpdate == true
        {
            btnNext.setTitle("Update".localizeString(string: Language.shared.currentAppLang), for: .normal)
            
            txtFullName.text = dicData.name ?? ""
            txtPhoneNumber.text = dicData.mobile ?? ""
            txtAddress.text = dicData.address ?? ""
            txtLocality.text = ""
            txtCity.text = dicData.city ?? ""
            txtState.text = dicData.state ?? ""
            txtZipCode.text = dicData.pincode ?? ""
            txtCountry.text = dicData.country ?? ""
            txtLocality.text = dicData.locality ?? ""
            
            viewCurrentLoc.isHidden = true
            heightCurrentLoc.constant = 0
            
            if dicData.title == "home"
            {
                homeBtn.isSelected = true
                officeeBtn.isSelected = false
                selectedAddressType = "home"
                homeView.borderColor = .black
                officeMainView.borderColor = .lightGray
            }
            else
            {
                homeBtn.isSelected = false
                officeeBtn.isSelected = true
                selectedAddressType = "office"
                officeMainView.borderColor = .black
                homeView.borderColor = .lightGray
            }
        }
        else
        {
            txtFullName.placeholder = "Full Name".localizeString(string: Language.shared.currentAppLang)
            txtPhoneNumber.placeholder = "Phone Number".localizeString(string: Language.shared.currentAppLang)
            txtAddress.placeholder = "Address (House No, Building, Street, Area)".localizeString(string: Language.shared.currentAppLang)
            txtLocality.placeholder = "Locality, Town".localizeString(string: Language.shared.currentAppLang)
            txtCity.placeholder = "City, District".localizeString(string: Language.shared.currentAppLang)
            txtZipCode.placeholder = "Zip".localizeString(string: Language.shared.currentAppLang)
            txtState.placeholder = "State".localizeString(string: Language.shared.currentAppLang)
            txtCountry.placeholder = "Country".localizeString(string: Language.shared.currentAppLang)
            
            btnNext.setTitle("Next".localizeString(string: Language.shared.currentAppLang), for: .normal)
            
            viewCurrentLoc.isHidden = false
            heightCurrentLoc.constant = 80

            btnHomeClicked(UIButton())
        }
        
        txtFullName.delegate = self
        txtPhoneNumber.delegate = self
        txtAddress.delegate = self
        txtLocality.delegate = self
        txtCity.delegate = self
        txtState.delegate = self
        txtZipCode.delegate = self
        
        
        callGetCountryAPI()
    }
    
    
    func setDropDownSelectCountry() {
        
        let arrCountry = NSMutableArray()
        
        for obj in self.arrCountryList
        {
            let country_name = (obj as? NSDictionary)?.value(forKey: "country_name") as? String
            
            arrCountry.add(country_name!)
        }
        
        dropDownCountry.dataSource = arrCountry as! [String]
        dropDownCountry.anchorView = viewTargetCountry
        dropDownCountry.direction = .any
        
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCountry.text = item
            
            for obj in self.arrCountryList
            {
                let country_name = (obj as? NSDictionary)?.value(forKey: "country_name") as? String
                let country_id = (obj as? NSDictionary)?.value(forKey: "country_id") as? String
                
                if country_name == item
                {
                    self.country_id = country_id ?? ""
                }
            }
        }
        
        dropDownCountry.bottomOffset = CGPoint(x: 0, y: viewTargetCountry.bounds.height)
        dropDownCountry.topOffset = CGPoint(x: 0, y: -viewTargetCountry.bounds.height)
        dropDownCountry.dismissMode = .onTap
        dropDownCountry.textColor = UIColor.darkGray
        dropDownCountry.backgroundColor = UIColor.white
        dropDownCountry.selectionBackgroundColor = UIColor.clear
        dropDownCountry.selectedTextColor = .black
        dropDownCountry.reloadAllComponents()
    }
    
    
    @IBAction func clickedCode_(_ sender: Any) {
        let controller = DialCountriesController(locale: Locale(identifier: "ae"))
            controller.delegate = self
            controller.show(vc: self)
    }
    
    @IBAction func onBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHomeClicked(_ sender: UIButton) {
        if !sender.isSelected {
            homeBtn.isSelected = true
            officeeBtn.isSelected = false
            selectedAddressType = "home"
            homeView.borderColor = .black
            officeMainView.borderColor = .lightGray
        }
        
    }
    
    @IBAction func clickeCountryCode(_ sender: Any) {
        dropDownCountry.show()
    }
    
    
    @IBAction func btnOfficeClicked(_ sender: UIButton) {
        if !sender.isSelected {
            homeBtn.isSelected = false
            officeeBtn.isSelected = true
            selectedAddressType = "office"
            officeMainView.borderColor = .black
            homeView.borderColor = .lightGray
        }
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        if txtFullName.text == ""{
            lblFullnameError.isHidden = false
            lblFullnameError.text = "Please enter Full Name".localizeString(string: Language.shared.currentAppLang)
            return
        }
        if txtPhoneNumber.text == ""{
            lblPhoneNumberError.isHidden = false
            lblPhoneNumberError.text = "Enter Phone Number".localizeString(string: Language.shared.currentAppLang)
            return
        }
        if (self.txtPhoneNumber.text?.count ?? 0) < 8
        {
            self.view.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))

            return
        }
        if txtAddress.text == ""{
            lblAddressError.isHidden = false
            lblAddressError.text = "Please enter your address".localizeString(string: Language.shared.currentAppLang)
            return
        }
        if txtLocality.text == ""{
            lblLocalityError.isHidden = false
            lblLocalityError.text = "Please enter your town".localizeString(string: Language.shared.currentAppLang)
            return
        }
        if txtCity.text == ""{
            lblCityError.isHidden = false
            lblCityError.text = "Please enter your City".localizeString(string: Language.shared.currentAppLang)
            return
        }
        if txtState.text == ""{
            lblStateError.isHidden = false
            lblStateError.text = "Please enter your state".localizeString(string: Language.shared.currentAppLang)
            return
        }
        if txtCountry.text == ""{
            self.view.makeToast("Please select country".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Please select country".localizeString(string: Language.shared.currentAppLang))

            return
        }
        if txtZipCode.text == ""{
            lblZipError.isHidden = false
            lblZipError.text = "Please enter Ziip code".localizeString(string: Language.shared.currentAppLang)
            return
        }
        if selectedAddressType == ""{
            return
        }
        
        if isUpdate == true
        {
            callUpdateAddressAPI()
        }
        else
        {
            callAddAddressAPI()
        }
    }
    
    func removeErrorView(_ textField: UITextField){
        if textField == txtFullName{
            lblFullnameError.isHidden = true
        }else if textField == txtPhoneNumber{
            lblPhoneNumberError.isHidden = true
        }else if textField == txtAddress{
            lblAddressError.isHidden = true
        }else if textField == txtLocality{
            lblLocalityError.isHidden = true
        }else if textField == txtCity{
            lblCityError.isHidden = true
        }else if textField == txtState{
            lblStateError.isHidden = true
        }else if textField == txtZipCode{
            lblZipError.isHidden = true
        }
    }
    
    func callAddAddressAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let name = self.txtFullName.text ?? ""
        let mobile = self.txtPhoneNumber.text ?? ""
        let title = self.selectedAddressType
        let address = self.txtAddress.text ?? ""
        let city = self.txtCity.text ?? ""
        let state = self.txtState.text ?? ""
        let pincode = self.txtZipCode.text ?? ""
        let country = self.txtCountry.text ?? ""
        let patient_id = appDelegate?.dicUserLoginData.id ?? ""
        let locality = txtLocality.text ?? ""
        
        let param = ["name": name,
                     "mobile": mobile,
                     "title": title,
                     "address": address,
                     "city": city,
                     "state": state,
                     "pincode": pincode,
                     "country": self.country_id,
                     "patient_id": patient_id,
                     "token": "12345",
                     "lat":"39.989098",
                     "long":"103.989098",
                     "locality":locality ?? ""
        ]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(SAVE_ADDRESS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                APIClient.sharedInstance.hideIndicator()
                
                if IsSuccess != "error" {
                    
                    if let responseUser = response
                    {
                        
                        self.view.makeToast(message)
                        
                        let windows = UIApplication.shared.windows
                        
                        windows.last?.makeToast(message)
                        
                        if self.isBookLab == true
                        {
                            self.navigationController?.popToRootViewController(animated: false)
                        }
                        else
                        {
                            
                            let controllers = self.navigationController?.viewControllers
                            for vc in controllers! {
                                if vc is AddressListViewController {
                                    _ = self.navigationController?.popToViewController(vc as! AddressListViewController, animated: true)
                                    
                                }
                            }
                            
                        }
                    }
                    else
                    {
                        self.view.makeToast(message)
                        let windows = UIApplication.shared.windows
                        windows.last?.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                    let windows = UIApplication.shared.windows
                    windows.last?.makeToast(message)
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                
                print("error \(String(describing: error))")
            }
        })
    }

    func callGetCountryAPI() {
            
           // APIClient.sharedInstance.showIndicator()
            
            let param = ["":""]
            
            APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_COUNTRY, parameters: param, completionHandler: { (response, error, statusCode) in
                
                if error == nil {
                    print("STATUS CODE \(String(describing: statusCode))")
                    print("Response \(String(describing: response))")
                    
                    let message = response?["message"] as? String ?? ""
                    let IsSuccess = response?["status"] as? String ?? ""
                    APIClient.sharedInstance.hideIndicator()
                    
                    if IsSuccess != "error" {
                        
                        if let responseUser = response {
                            
                            let arrData = response?.value(forKey: "data") as? NSArray
                            
                            self.arrCountryList = arrData?.mutableCopy() as! NSMutableArray
                            
                            if self.isUpdate == true
                            {
                                for obj in self.arrCountryList
                                {
                                    let country_name = (obj as? NSDictionary)?.value(forKey: "country_name") as? String
                                    let country_id = (obj as? NSDictionary)?.value(forKey: "country_id") as? String
                                    
                                    if country_name == self.dicData.country
                                    {
                                        self.country_id = country_id ?? ""
                                    }
                                }
                            }
                            
                            self.setDropDownSelectCountry()
                            
                            
                        } else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    let message = response?["message"] as? String ?? ""
                    self.view.makeToast(message)
                    
                    print("error \(String(describing: error))")
                }
            })
        }
    
    func callUpdateAddressAPI() {
        
        APIClient.sharedInstance.showIndicator()
        
        let name = txtFullName.text ?? ""
        let mobile = txtPhoneNumber.text ?? ""
        let title = selectedAddressType
        let address = txtAddress.text ?? ""
        let city = txtCity.text ?? ""
        let state = txtState.text ?? ""
        let pincode = txtZipCode.text ?? ""
        let country = txtCountry.text ?? ""
        let patient_id = appDelegate?.dicUserLoginData.id ?? ""
        let locality = txtLocality.text ?? ""
        
        let param = ["name": name,
                     "mobile": mobile,
                     "title": title,
                     "address": address,
                     "city": city,
                     "state": state,
                     "pincode": pincode,
                     "country_id": self.country_id,
                     "patient_id": patient_id,
                     "token": "12345",
                     "lat":"39.989098",
                     "long":"103.989098",
                     "address_id":dicData.id ?? "",
                     "locality":locality ?? ""
        ]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithOutAuthHeaderPostDic(UPDATE_ADDRESS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["status"] as? String ?? ""
                APIClient.sharedInstance.hideIndicator()
                
                if IsSuccess != "error" {
                    
                    if let responseUser = response
                    {
                        
                        self.view.makeToast(message)
                        
                        let windows = UIApplication.shared.windows
                        windows.last?.makeToast(message)
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    if message == "Something went wrong"
                    {
                        self.view.makeToast("Address updated successfully".localizeString(string: Language.shared.currentAppLang))
                        
                        let windows = UIApplication.shared.windows
                        windows.last?.makeToast("Address updated successfully".localizeString(string: Language.shared.currentAppLang))
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        self.view.makeToast(message)

                    }
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
                
                print("error \(String(describing: error))")
            }
        })
    }
    
    
}

extension AddNewAddressViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        removeErrorView(textField)
    }
}

extension AddNewAddressViewController : DialCountriesControllerDelegate {
    
    func didSelected(with country: Country) {
//        JSN.log("selected country ===>%@", country.dialCode)
//        JSN.log("selected country flag ===>%@", country.flag)
      //  self.txtCountryCode.text =  "+\(country.dialCode ?? "+91")"
        
        self.lblC_Code.text = country.dialCode ?? "+91"
        
    }
    
}

@IBDesignable
extension UITextField {
    
    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }
    
    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}

