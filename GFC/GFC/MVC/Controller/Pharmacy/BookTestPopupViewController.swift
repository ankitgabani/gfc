//
//  BookTestPopupViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 18/04/22.
//

import UIKit
import DropDown

extension Notification {
    
    public static let myNotificationKeyPopUp = Notification.Name(rawValue: "myNotificationKeyPopUp")
}


class BookTestPopupViewController: UIViewController {

    @IBOutlet weak var btnNullAddress: UIButton!
    @IBOutlet weak var btnAddNewAddres: UIButton!
    
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var lblBtnTitle: UILabel!
    //MARK: - Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var containerViewCenterYConstrain: NSLayoutConstraint!
    @IBOutlet weak var trasperentView: UIView!
    
    @IBOutlet weak var stackBook: UIStackView!
    
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var selectedAppointmentStack: UIStackView!
 
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var btnNewAddress: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var viewB: UIView!
    
    @IBOutlet weak var lblAppointmentDate: UILabel!
    @IBOutlet weak var lblTimeSlots: UILabel!
    @IBOutlet weak var lblNoPerson: UILabel!
    
    @IBOutlet weak var imgCOD: UIImageView!
    @IBOutlet weak var imgOnline: UIImageView!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var heightMain: NSLayoutConstraint! // 408
    
    @IBOutlet weak var lblTAppintmentDate: UILabel!
    @IBOutlet weak var lblTTimeSlot: UILabel!
    @IBOutlet weak var lblTNoOfPerson: UILabel!
    @IBOutlet weak var lblTPaymentMode: UILabel!
    
    @IBOutlet weak var lblTCashOnDelivery: UILabel!
    @IBOutlet weak var lblTOnline: UILabel!
    
    @IBOutlet weak var lbltPrice: UILabel!
    
    var arrAddressList: [GFCAddressListData] = [GFCAddressListData]()
    
    var completionHandler: ((Bool)->Void)?
    var selectedType = String()
    var parentVC = UIViewController()
    
    var isOpenFromLab = false
    
    var strTopTitle = ""
    
    var strBtnTitle = ""
    
    var dicBookTestData = GFCLabCateListList()
    
    var strNoPerson = ""
    
    var strBookingDate = ""
    
    var strStartTime = ""
    
    var strEndTime = ""
        
    var strPrice = ""
    
    var strCartIDs = ""
    
    var arrCartIDs = NSMutableArray()

    var dicCart: [GFCCartListCartProductList] = [GFCCartListCartProductList]()
    
    var strType = ""
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLbl.text = strTopTitle.localizeString(string: Language.shared.currentAppLang)
        
        lblBtnTitle.text = strBtnTitle.localizeString(string: Language.shared.currentAppLang)
        
        strType = ""
        imgCOD.image = UIImage(named: "unselectedbutton")
        imgOnline.image = UIImage(named: "unselectedbutton")
        
        tblView.isHidden = true
        
        tblView.delegate = self
        tblView.dataSource = self
        
        if strTopTitle == "Book Test"
        {
            let bidAcceptedS = strBookingDate
            let formatterS = DateFormatter()
            formatterS.locale = Locale(identifier: "en_US")
            formatterS.dateFormat = "yyyy-MM-dd"
            let date1 = formatterS.date(from: bidAcceptedS)
            let Dform = DateFormatter()
            Dform.dateFormat = "EEE, MMM d"
            let strDate = Dform.string(from: date1!)

            lblAppointmentDate.text = strDate
            
            lblNoPerson.text = strNoPerson
            
            lblTimeSlots.text = "\(strStartTime) - \(strEndTime)"
            
            let price =  "\(dicBookTestData.discountedPrice ?? "")"
            
            let person = "\(strNoPerson.replacingOccurrences(of: " Person", with: ""))"
            
            let objPrice = Double(price)! * Double(person)!
            
            lblTotalPrice.text = "₹ \(objPrice) (\(person) * \(price))"

            stackBook.isHidden = false
        }
        else if strTopTitle == "Place Order"
        {
            
            for objCart in dicCart
            {
                if arrCartIDs.contains(objCart.cartId ?? "") == false
                {
                    arrCartIDs.add(objCart.cartId ?? "")
                }
                
            }
            
            self.strCartIDs = arrCartIDs.componentsJoined(by: ",")
            
            let price =  "\(dicBookTestData.discountedPrice ?? "")"
            
//            let person = "\(strNoPerson.replacingOccurrences(of: " Person", with: ""))"
//
//            let objPrice = Double(price)! * Double(person)!
            
            lblTotalPrice.text = "₹ \(price)"
            
            stackBook.isHidden = true
            
        }
        //alert open animation
        showAlert()
        
        if isOpenFromLab{
            selectedAppointmentStack.isHidden = false
        }
        else{
            selectedAppointmentStack.isHidden = true
        }
        callAddressListListAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
 
        titleLbl.text = strTopTitle
        
        lblBtnTitle.text = strBtnTitle
        
        lblTAppintmentDate.text = "Appointment Date".localizeString(string: Language.shared.currentAppLang)
        lblTTimeSlot.text = "Time Slot".localizeString(string: Language.shared.currentAppLang)
        lblTNoOfPerson.text = "No Of Person".localizeString(string: Language.shared.currentAppLang)
        
        if Language.shared.isArabic == false{
            lblTCashOnDelivery.textAlignment = .left
        }else{
            lblTCashOnDelivery.textAlignment = .right
        }
        
        lblTCashOnDelivery.text = "Cash On Delivery".localizeString(string: Language.shared.currentAppLang)
        lblTPaymentMode.text = "Payment Mode".localizeString(string: Language.shared.currentAppLang)

        btnAddNewAddres.setTitle("Add New Address".localizeString(string: Language.shared.currentAppLang), for: .normal)
        strType = ""
        imgCOD.image = UIImage(named: "unselectedbutton")
        imgOnline.image = UIImage(named: "unselectedbutton")
        
        if strTopTitle == "Book Test".localizeString(string: Language.shared.currentAppLang)
        {
            let bidAcceptedS = strBookingDate
            let formatterS = DateFormatter()
            formatterS.locale = Locale(identifier: "en_US")
            formatterS.dateFormat = "yyyy-MM-dd"
            let date1 = formatterS.date(from: bidAcceptedS)
            let Dform = DateFormatter()
            Dform.dateFormat = "EEE, MMM d"
            let strDate = Dform.string(from: date1!)

            lblAppointmentDate.text = strDate
            
            lblNoPerson.text = strNoPerson.localizeString(string: Language.shared.currentAppLang)
            
            lblTimeSlots.text = "\(strStartTime) - \(strEndTime)"
            
            let price =  "\(dicBookTestData.discountedPrice ?? "")"
            
            let person = "\(strNoPerson.replacingOccurrences(of: " Person", with: ""))"
            
            let objPrice = Double(price)! * Double(person)!
            
            lblTotalPrice.text = "₹ \(objPrice) (\(person) * \(price))"

            stackBook.isHidden = false
            
            heightMain.constant = 408
        }
        else if strTopTitle == "Place Order".localizeString(string: Language.shared.currentAppLang)
        {
            
            for objCart in dicCart
            {
                if arrCartIDs.contains(objCart.cartId ?? "") == false
                {
                    arrCartIDs.add(objCart.cartId ?? "")
                }
                
            }
            
            self.strCartIDs = arrCartIDs.componentsJoined(by: ",")
            
            stackBook.isHidden = true
            lblTotalPrice.text = strPrice
            heightMain.constant = 350
        }
        //alert open animation
        showAlert()
        
        if isOpenFromLab{
            selectedAppointmentStack.isHidden = false
        }
        else{
            selectedAppointmentStack.isHidden = true
        }
        callAddressListListAPI()
    }
    
    //MARK: - Button Action
    
    @IBAction func clickedAddFreshAddress(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddNewAddressViewController") as! AddNewAddressViewController
            controller.strTitle = "Add Address".localizeString(string: Language.shared.currentAppLang)
            controller.isBookLab = true
            self.navigationController?.pushViewController(controller, animated: true)
        })
    }
    
 
    @IBAction func btnAddNewAddressClicked(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddNewAddressViewController") as! AddNewAddressViewController
            controller.isBookLab = true
            controller.strTitle = "Add Address".localizeString(string: Language.shared.currentAppLang)
            self.navigationController?.pushViewController(controller, animated: true)
        })
    }
    @IBAction func onClearBtn(_ sender: Any) {
        dismissAlert()
    }
    @IBAction func clickedOpenAdd(_ sender: Any) {
        if self.lblAddress.text == "Choose Address".localizeString(string: Language.shared.currentAppLang)
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
            vc.isBookLab = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            tblView.isHidden = false
        }
    }
    
    @IBAction func clickedAddNewAddress(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        vc.isBookLab = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func clickedPalce(_ sender: Any) {
        if strTopTitle == "Book Test".localizeString(string: Language.shared.currentAppLang)
        {
            if self.lblAddress.text == "Select Address".localizeString(string: Language.shared.currentAppLang)
            {
                self.view.makeToast("Please select address".localizeString(string: Language.shared.currentAppLang))
                let window  = UIApplication.shared.windows
                window.last?.makeToast("Please select address".localizeString(string: Language.shared.currentAppLang))
            }
            else if strType == ""
            {
                self.view.makeToast("Please payment mode".localizeString(string: Language.shared.currentAppLang))
                let window  = UIApplication.shared.windows
                window.last?.makeToast("Please payment mode".localizeString(string: Language.shared.currentAppLang))
            }
            else
            {
                if btnAddNewAddres.isHidden == true {
                    callTestBookingAPI()
                } else {
                    self.view.makeToast("Please add new address")
                }
            }
        }
        else
        {
            if self.lblAddress.text == "Select Address".localizeString(string: Language.shared.currentAppLang)
            {
                self.view.makeToast("Please select address".localizeString(string: Language.shared.currentAppLang))
                let window  = UIApplication.shared.windows
                window.last?.makeToast("Please select address".localizeString(string: Language.shared.currentAppLang))
            }
            else if strType == ""
            {
                self.view.makeToast("Select payment type".localizeString(string: Language.shared.currentAppLang))
                let window  = UIApplication.shared.windows
                window.last?.makeToast("Select payment type".localizeString(string: Language.shared.currentAppLang))
            }
            else
            {
                callPlaceOrderAPI()
            }
        }
        
    }
    @IBAction func clickedCloseTB(_ sender: Any) {
        tblView.isHidden = true
    }
    
    @IBAction func clickedCOD(_ sender: Any) {
        strType = "COD"
        imgCOD.image = UIImage(named: "selectedbutton")
        imgOnline.image = UIImage(named: "unselectedbutton")
    }
    @IBAction func clickedOnlien(_ sender: Any) {
        strType = "Online"
        imgOnline.image = UIImage(named: "selectedbutton")
        imgCOD.image = UIImage(named: "unselectedbutton")
        
    }
    
    
    //MARK: - call API
    
    func callAddressListListAPI()
    {
        let para = ["":""]
        
//        APIClient.sharedInstance.showIndicator()
        
        let _url = "patientId=\(appDelegate?.dicUserLoginData.id ?? "")&token=12212"
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_ADDRESS_LIST + _url, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrAddressList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCAddressListData(fromDictionary: obj as! NSDictionary)
                            self.arrAddressList.append(dicData)
                        }
                        
                        self.arrAddressList = self.arrAddressList.reversed()
                        
                        if self.arrAddressList.count > 0
                        {
                            
                            let obj = self.arrAddressList[0]
                            
                       //     appDelegate?.objSelectAddress = self.arrAddressList[0]
                            
                            self.lblAddress.text = "Select Address".localizeString(string: Language.shared.currentAppLang)
                            
                            self.viewAddress.isHidden = false
                            self.btnNewAddress.isHidden = true
                            self.btnNullAddress.isHidden = true
                            self.lblBtnTitle.alpha = 1
                            self.btnAddNewAddres.isHidden = false
                            self.btnDone.isUserInteractionEnabled = true
                        }
                        else
                        {
                            self.viewAddress.isHidden = true
                            self.btnNewAddress.isHidden = false
                            self.btnNullAddress.isHidden = false
                            self.lblBtnTitle.alpha = 0.5
                            self.btnAddNewAddres.isHidden = true
                            self.btnDone.isUserInteractionEnabled = false
                        }
                        self.tblView.reloadData()
                    }
                    else
                    {
                        self.lblAddress.text = "Choose Address"
                        self.btnNullAddress.isHidden = true
                        self.btnAddNewAddres.isHidden = true
                    }
                }
            }
        }
    }
    
    //MARK: - Alert dismiss/Present animation
    
    func showAlert(){
        //self.containerViewCenterYConstrain.constant = 1000
        self.trasperentView.alpha = 0
        self.view.layoutIfNeeded()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            UIView.animate(withDuration: 0.2) {
                //self.containerViewCenterYConstrain.constant = 0
                self.view.layoutIfNeeded()
                self.trasperentView.alpha = 0.4
            }
        }
    }
    
    func dismissAlert(){
        //containerViewCenterYConstrain.constant = 1000
        UIView.animate(withDuration: 0.01, animations: {
            self.trasperentView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (true) in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Labtest book API

extension BookTestPopupViewController
{
    func callTestBookingAPI()
    {
        
        APIClient.sharedInstance.showIndicator()
        
        let testId = dicBookTestData.testId ?? ""
        
        let patientId = appDelegate?.dicUserLoginData.id ?? ""
        
        let no_of_person = strNoPerson.replacingOccurrences(of: " Person", with: "")
        
        let booking_date = strBookingDate
        
        let slot_start_time = strStartTime
        
        let slot_end_time = strEndTime
        
        let name = appDelegate?.objSelectAddress.name ?? ""
        
        let mobile = appDelegate?.objSelectAddress.mobile ?? ""
        
        let title = appDelegate?.objSelectAddress.title ?? ""
        
        let address = appDelegate?.objSelectAddress.address ?? ""
        
        let city = appDelegate?.objSelectAddress.city ?? ""
        
        let state = appDelegate?.objSelectAddress.state ?? ""
        
        let pincode = appDelegate?.objSelectAddress.pincode ?? ""
        
        let lat = appDelegate?.objSelectAddress.lat ?? ""
        
        let long = appDelegate?.objSelectAddress.lng ?? ""
        
        let payment_mode = strType
        
        let amount = (dicBookTestData.discountedPrice ?? "").replacingOccurrences(of: "₹ ", with: "")
        
        let status = dicBookTestData.status ?? ""
        
        let country = appDelegate?.objSelectAddress.countryName ?? ""
        
        let lab_id = dicBookTestData.labId ?? ""
        
        let token = "123456"

        let para = ["testId":testId,"patientId":patientId,"no_of_person":no_of_person,"booking_date":booking_date,"slot_start_time":slot_start_time,"slot_end_time":slot_end_time,"name":name,"mobile":mobile,"title":title,"address":address,"city":city,"state":state,"pincode":pincode,"lat":lat,"long":long,"payment_mode":payment_mode,"amount":amount,"status":status,"country":"101","lab_id":lab_id,"token":token] as [String : Any]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(BOOK_TEST, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
 
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if status == "success"
                    {

                        NotificationCenter.default.post(name: Notification.myNotificationKeyPopUp, object: nil, userInfo:["text": ""]) // Notification
                        
                        self.dismiss(animated: false, completion: nil)
                    }
                    else
                    {
                        APIClient.sharedInstance.hideIndicator()
                        self.view.makeToast(message)
                    }
                }
                else
                {
                    APIClient.sharedInstance.hideIndicator()
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    func callPlaceOrderAPI()
    {
        
       // APIClient.sharedInstance.showIndicator()
                
        let patientId = appDelegate?.dicUserLoginData.id ?? ""
        
        let name = appDelegate?.objSelectAddress.name ?? ""
        
        let mobile = appDelegate?.objSelectAddress.mobile ?? ""
        
        let title = appDelegate?.objSelectAddress.title ?? ""
        
        let address = appDelegate?.objSelectAddress.address ?? ""
        
        let city = appDelegate?.objSelectAddress.city ?? ""
        
        let state = appDelegate?.objSelectAddress.state ?? ""
        
        let pincode = appDelegate?.objSelectAddress.pincode ?? ""
        
        let lat = appDelegate?.objSelectAddress.lat ?? ""
        
        let long = appDelegate?.objSelectAddress.lng ?? ""
        
        let payment_mode = strType
        
        let amount = (self.lblTotalPrice.text ?? "").replacingOccurrences(of: "\(appDelegate?.currentCountry.currency ?? "") ", with: "")
        
        let country = appDelegate?.objSelectAddress.countryName ?? ""
        
        let para = ["cartIds": self.strCartIDs,"patientId":patientId,"name":name,"mobile":mobile,"title":title,"address":address,"city":city,"state":state,"pincode":pincode,"lat":lat,"long":long,"payment_mode":payment_mode,"amount":amount,"country":"101"] as [String : Any]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(PLACE_ORDER, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
 
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        APIClient.sharedInstance.hideIndicator()
                        
                        NotificationCenter.default.post(name: Notification.myNotificationKeyPopUp, object: nil, userInfo:["text": ""]) // Notification
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
                        vc.strTitle = "Order placed successfully".localizeString(string: Language.shared.currentAppLang)
                        vc.isMedical = false
                        self.navigationController?.pushViewController(vc, animated: false)
                       
                    }
                    else
                    {
                        APIClient.sharedInstance.hideIndicator()
                        self.view.makeToast(message)
                    }
                }
                else
                {
                    APIClient.sharedInstance.hideIndicator()
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
}

extension BookTestPopupViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "AddressTblCell") as! AddressTblCell
        
        let dicData = arrAddressList[indexPath.row]
        
        cell.lblAddress.text = "\(dicData.name ?? "") (\(dicData.mobile ?? ""))\n(\(dicData.title ?? "")) \(dicData.address ?? "") \(dicData.city ?? "") \(dicData.state ?? "") \(dicData.pincode ?? "") \(dicData.countryName ?? "")"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrAddressList[indexPath.row]
        
        self.lblAddress.text = "\(dicData.name ?? "") (\(dicData.mobile ?? ""))\n(\(dicData.title ?? "")) \(dicData.address ?? "") \(dicData.city ?? "") \(dicData.state ?? "") \(dicData.pincode ?? "") \(dicData.countryName ?? "")"
        
        appDelegate?.objSelectAddress = dicData

        tblView.isHidden = true
    }
    
    
}

class AddressTblCell: UITableViewCell
{
    @IBOutlet weak var lblAddress: UILabel!    
}
