//
//  TreatmentListViewController.swift
//  GFC
//
//  Created by iMac on 28/03/22.
//

import UIKit
import DropDown
import SDWebImage

class TreatmentListViewController: UIViewController {
    
    @IBOutlet weak var treatmentListTblView: UITableView!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtTretment: UITextField!
    
    @IBOutlet weak var viewDropDon: UIView!
    @IBOutlet weak var viewDropTretmrnt: UIView!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var btnDropTre: UIButton!
    @IBOutlet weak var viewNoData: UIView!
    
    @IBOutlet weak var lblTLocation: UILabel!
    @IBOutlet weak var lblTTreatment: UILabel!
    
    @IBOutlet weak var btReset: UIButton!
    
    @IBOutlet weak var lblNoData: UILabel!
    
    let dropDownCountry = DropDown()
    let dropDownTretment = DropDown()

    var arrCountryList: [GFCCountryListData] = [GFCCountryListData]()
    
    var arrTreatmentList: [GFCTreatmentDropListData] = [GFCTreatmentDropListData]()
    
    var arrHospitalList: [GFCHospitalListData] = [GFCHospitalListData]()

    var strCountryID = ""
    
    var strTreatmentID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtTretment.text = "Select"
        
        lblTTreatment.text = "Treatments".localizeString(string: Language.shared.currentAppLang)
        lblTLocation.text = "Location".localizeString(string: Language.shared.currentAppLang)
        lblNoData.text = "No Data Found".localizeString(string: Language.shared.currentAppLang)
        
        btReset.setTitle("Reset".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        
        treatmentListTblView.delegate = self
        treatmentListTblView.dataSource = self
        
        setDropDownTreatment()
    }

    override func viewWillAppear(_ animated: Bool) {
        callTreatmentListAPI()
        callCountryListAPI()
        self.txtCountry.text = "Country"
        txtTretment.text = "Treatment"
        
        //        self.callHospitalListAPI(country_id:"101",treatment_id:"0")
    }
    
    @IBAction func DropDownOpenClicked(_ sender: Any) {
        
        dropDownCountry.show()
        
    }
    @IBAction func clickedTretmentDrop(_ sender: Any) {
        
        dropDownTretment.show()
        
    }
    
    @IBAction func clickedReset(_ sender: Any) {
        
        self.txtCountry.text = "India"
        self.callHospitalListAPI(country_id:"101",treatment_id:"0")
        
        txtTretment.text = "Select"
        
    }
    
    
    func setDropDownCountry()
    {
        let Country = NSMutableArray()
        
        for obj in arrCountryList
        {
            Country.add(obj.countryName ?? "")
        }
        
        dropDownCountry.dataSource = Country as! [String]
        dropDownCountry.anchorView = btnDrop
        dropDownCountry.direction = .bottom
        
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCountry.text = item
            
            for obj in arrCountryList
            {
                if obj.countryName == item
                {
                    self.strCountryID = obj.countryId ?? ""
                    //                    appDelegate?.currentCountry = obj
                }
            }
            self.callHospitalListAPI(country_id:self.strCountryID,treatment_id:self.strTreatmentID)
            
        }
        
        dropDownCountry.bottomOffset = CGPoint(x: 0, y: btnDrop.bounds.height)
        dropDownCountry.topOffset = CGPoint(x: 0, y: -btnDrop.bounds.height)
        dropDownCountry.dismissMode = .onTap
        dropDownCountry.textColor = UIColor.darkGray
        dropDownCountry.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCountry.selectionBackgroundColor = UIColor.clear
        dropDownCountry.selectedTextColor = .black
        
        dropDownCountry.reloadAllComponents()
    }
    
    func setDropDownTreatment()
    {
        let Treatment = NSMutableArray()
        Treatment.add("Select")
        
        
        for obj in arrTreatmentList
        {
            Treatment.add(obj.treatmentName ?? "")
        }
        
        dropDownTretment.dataSource = Treatment as! [String]
        dropDownTretment.anchorView = btnDropTre
        dropDownTretment.direction = .bottom
        
        dropDownTretment.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtTretment.text = item
            
            for obj in arrTreatmentList
            {
                if obj.treatmentName == item
                {
                    self.strTreatmentID = obj.treatmentId ?? ""
                }
            }
            
            if index == 0
            {
                self.strTreatmentID = ""
            }
            self.callHospitalListAPI(country_id:self.strCountryID,treatment_id:self.strTreatmentID)
        }
        
        dropDownTretment.bottomOffset = CGPoint(x: 0, y: btnDrop.bounds.height)
        dropDownTretment.topOffset = CGPoint(x: 0, y: -btnDrop.bounds.height)
        dropDownTretment.dismissMode = .onTap
        dropDownTretment.textColor = UIColor.darkGray
        dropDownTretment.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownTretment.selectionBackgroundColor = UIColor.clear
        dropDownTretment.selectedTextColor = .black
        dropDownTretment.reloadAllComponents()
    }
    
    func callCountryListAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(HOSPITAL_COUNTRY_, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCountryList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            self.arrCountryList.append(dicData)
                            
//                            if appDelegate?.dicUserLoginData.countryId == dicData.countryId
//                            {
//                                self.callHospitalListAPI(country_id:dicData.countryId ?? "",treatment_id:self.strTreatmentID)
//                            }
                        }
                        
                        self.txtCountry.text = self.arrCountryList[0].countryName ?? ""
                        self.strCountryID = self.arrCountryList[0].countryId ?? ""
                        
//                        if self.arrCountryList.count > 0
//                        {
//                            if appDelegate?.currentCountry.countryName == nil
//                            {
//                                self.strCountryID = self.arrCountryList[0].countryId ?? ""
//                                self.txtCountry.text = self.arrCountryList[0].countryName ?? ""
//                                appDelegate?.currentCountry = self.arrCountryList[0]
//
//                            }
//                            else
//                            {
//                                 self.txtCountry.text = appDelegate?.currentCountry.countryName ?? ""
//                                self.strCountryID = appDelegate?.currentCountry.countryId ?? ""
//                                self.callHospitalListAPI(country_id:self.strCountryID,treatment_id:self.strTreatmentID)
//                            }
//                        }
//
                        self.callHospitalListAPI(country_id: self.strCountryID,treatment_id:self.strTreatmentID)
                        
                        self.setDropDownCountry()
                    }
                }
            }
        }
    }
    
    func callTreatmentListAPI()
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_TREATMENT, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrTreatmentList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCTreatmentDropListData(fromDictionary: obj as! NSDictionary)
                            self.arrTreatmentList.append(dicData)
                        }
                        self.setDropDownTreatment()
                        
                    }
                }
            }
        }
    }
    
    func callHospitalListAPI(country_id:String,treatment_id:String)
    {
        // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        let _url = "?country_id=\(country_id)&treatment_id=\(treatment_id)&offset=0"
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_HOSPITAL_C_ID + _url, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    self.arrHospitalList.removeAll()
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        for obj in arrData!
                        {
                            let dicData = GFCHospitalListData(fromDictionary: obj as! NSDictionary)
                            self.arrHospitalList.append(dicData)
                        }
                    }
                    else
                    {
                        self.arrHospitalList.removeAll()
                    }
                    if self.arrHospitalList.count == 0
                    {
                        self.treatmentListTblView.isHidden = true
                        self.viewNoData.isHidden = false
                    }
                    else
                    {
                        self.treatmentListTblView.isHidden = false
                        self.viewNoData.isHidden = true
                    }
                    
                    self.treatmentListTblView.reloadData()
                }
                else
                {
                    self.arrHospitalList.removeAll()
                    self.treatmentListTblView.reloadData()
                }
            }
        }
    }
    
}

extension TreatmentListViewController: UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHospitalList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TreatmentTableViewCell") as! TreatmentTableViewCell
        
        let dicData = arrHospitalList[indexPath.row]
        
        cell.lblHospitalName.text = dicData.name ?? ""
        cell.lblHospitalDesc.text = dicData.descriptionField ?? "".htmlToString
        cell.lblHospitalLocation.text = dicData.address ?? ""
        
        let rating = Double(dicData.rating ?? "")?.round(to: 1)
        cell.lblRating.text = "\(rating ?? 0.0)"
        
        
        if dicData.images.count > 0
        {
            var media_link_url = "\(dicData.imageUrl ?? "")\(dicData.images[0].image ?? "")"
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.hospitalImageView.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
        }
        
        cell.lblHospitalDesc.numberOfLines = 3
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dicData = arrHospitalList[indexPath.row]
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "HospitalDetailViewController") as! HospitalDetailViewController
        controller.strHospitalID = dicData.id ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
    }
}


class TreatmentTableViewCell: UITableViewCell{
    
    @IBOutlet weak var hospitalImageView: UIImageView!
    @IBOutlet weak var lblHospitalName: UILabel!
    @IBOutlet weak var lblHospitalLocation: UILabel!
    @IBOutlet weak var lblHospitalDesc: UILabel!
    
    @IBOutlet weak var lblRating: UILabel!
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options:
                                            [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
