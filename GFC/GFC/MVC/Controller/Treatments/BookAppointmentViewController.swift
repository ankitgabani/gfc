//
//  BookAppointmentViewController.swift
//  GFC
//
//  Created by iMac on 28/03/22.
//

import UIKit
import DropDown

class BookAppointmentViewController: UIViewController {

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtMiddleName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtSelectCountry: UITextField!
    @IBOutlet weak var txtSelectDepartment: UITextField!
    @IBOutlet weak var txtSelectDoctor: UITextField!
    @IBOutlet weak var txtSelectTimeSlot: UITextField!
    
    
    @IBOutlet weak var maleMainView: UIView!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var feMaleMainView: UIView!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var otherMainView: UIView!
    @IBOutlet weak var otherBtn: UIButton!
    
    @IBOutlet weak var lblAppointment: UILabel!
    @IBOutlet weak var lblTGender: UILabel!
    @IBOutlet weak var lblTDepartment: UILabel!
    
    @IBOutlet weak var lblTMale: UILabel!
    @IBOutlet weak var lblTFemale: UILabel!
    @IBOutlet weak var lblTOther: UILabel!
    
    @IBOutlet weak var lblTWhatDay: UILabel!
    @IBOutlet weak var lblTWhatTime: UILabel!
    
    // day
    
    @IBOutlet weak var lblTMonday: UILabel!
    @IBOutlet weak var lblTTuesday: UILabel!
    @IBOutlet weak var lblTWednesday: UILabel!
    @IBOutlet weak var lblTThursday: UILabel!
    @IBOutlet weak var lblTFriday: UILabel!
    @IBOutlet weak var lblTSaturday: UILabel!
    
    // time
    
    @IBOutlet weak var lblTMorning: UILabel!
    @IBOutlet weak var lblTAftrenoon: UILabel!
    @IBOutlet weak var lblTEvening: UILabel!
    
    @IBOutlet weak var btnTBookAppointment: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    var selectedGender = ""
    let dropDownCoutnry = DropDown()
    
    let dropDownDepartment = DropDown()
    
    let dropDownDoctor = DropDown()
    
    let dropDownSelectTimeSlot = DropDown()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }

        // The view to which the drop down will appear on
        dropDownCoutnry.anchorView = txtSelectCountry // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        dropDownCoutnry.dataSource = ["India","England","USA","Austalia"]
        
        // The view to which the drop down will appear on
        dropDownDepartment.anchorView = txtSelectDepartment // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        dropDownDepartment.dataSource = ["India","England","USA","Austalia"]
        
        // The view to which the drop down will appear on
        dropDownDoctor.anchorView = txtSelectDoctor // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        dropDownDoctor.dataSource = ["India","England","USA","Austalia"]
        
        // The view to which the drop down will appear on
        dropDownSelectTimeSlot.anchorView = txtSelectTimeSlot // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        dropDownSelectTimeSlot.dataSource = ["India","England","USA","Austalia"]
    }
    
    @IBAction func btnSelectCountryDropDown(_ sender: Any) {
        dropDownCoutnry.show()
        // Action triggered on selection
        dropDownCoutnry.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtSelectCountry.text = item
            dropDownCoutnry.hide()
        }
    }
    
    @IBAction func btnSelectDepartmentDropDown(_ sender: Any) {
        dropDownDepartment.show()
        // Action triggered on selection
        dropDownDepartment.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtSelectDepartment.text = item
            dropDownDepartment.hide()
        }
    }
    
    @IBAction func btnSelectDoctorDropDown(_ sender: Any) {
        dropDownDoctor.show()
        // Action triggered on selection
        dropDownDoctor.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtSelectDoctor.text = item
            dropDownDoctor.hide()
        }
    }
    
    @IBAction func btnSelectTimeSlotDropDown(_ sender: Any) {
        dropDownSelectTimeSlot.show()
        // Action triggered on selection
        dropDownSelectTimeSlot.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtSelectTimeSlot.text = item
            dropDownSelectTimeSlot.hide()
        }
    }
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnMaleClicked(_ sender: UIButton) {
        if !sender.isSelected {
            maleBtn.isSelected = true
            femaleBtn.isSelected = false
            otherBtn.isSelected = false
            selectedGender = "Male"
            maleMainView.borderColor = .green
            feMaleMainView.borderColor = .black
            otherMainView.borderColor = .black
        }
    }
    @IBAction func btnFemaleClicked(_ sender: UIButton) {
        if !sender.isSelected {
            femaleBtn.isSelected = true
            maleBtn.isSelected = false
            otherBtn.isSelected = false
            selectedGender = "Female"
            feMaleMainView.borderColor = .green
            maleMainView.borderColor = .black
            otherMainView.borderColor = .black
        }
    }
    @IBAction func btnOtherClicked(_ sender: UIButton) {
        if !sender.isSelected {
            otherBtn.isSelected = true
            maleBtn.isSelected = false
            femaleBtn.isSelected = false
            selectedGender = "Other"
            otherMainView.borderColor = .green
            feMaleMainView.borderColor = .black
            maleMainView.borderColor = .black
        }
    }
    
    
    @IBAction func btnMondayClicked(_ sender: Any) {
    }
    @IBAction func btnTuesdayClicked(_ sender: Any) {
    }
    @IBAction func btnWednesdayClicked(_ sender: Any) {
    }
    
    
    @IBAction func btnThurdayClicked(_ sender: Any) {
    }
    @IBAction func btnFridayClicked(_ sender: Any) {
    }
    @IBAction func btnSaturdayClicked(_ sender: Any) {
    }
    
    @IBAction func btnMorningClicked(_ sender: Any) {
    }
    @IBAction func btnAfternoonClicked(_ sender: Any) {
    }
    @IBAction func btnEveningClicked(_ sender: Any) {
    }
    
    @IBAction func btnBookAppointmentClicked(_ sender: Any) {
        
        if txtFirstName.text == ""{
            self.view.makeToast("Enter First Name".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtMiddleName.text == ""{
            self.view.makeToast("Enter Middle Name".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtLastName.text == ""{
            self.view.makeToast("Enter Last Name".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if !AppUtilites.isValidEmail(testStr: txtEmail.text ?? ""){
            self.view.makeToast("Enter Valid Email".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtAddress.text == ""{
            self.view.makeToast("Enter Address".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtStreet.text == ""{
            self.view.makeToast("Enter Street".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtCity.text == ""{
            self.view.makeToast("Enter City".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtState.text == ""{
            self.view.makeToast("Enter State".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtSelectCountry.text == ""{
            self.view.makeToast("Select Country".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if selectedGender == ""{
            self.view.makeToast("Select Gender".localizeString(string: Language.shared.currentAppLang))
            return
        }
        
        if txtSelectDepartment.text == ""{
            self.view.makeToast("Select Department".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtSelectDoctor.text == ""{
            self.view.makeToast("Select Doctor".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtSelectTimeSlot.text == ""{
            self.view.makeToast("Select Time Slot".localizeString(string: Language.shared.currentAppLang))
            return
        }
        
    }
    
}
