//
//  HospitalDetailViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 29/04/22.
//

import UIKit
import SDWebImage

class HospitalDetailViewController: UIViewController {

    @IBOutlet weak var collectionViewOurTreatment: UICollectionView!
    @IBOutlet weak var collectionImg: UICollectionView!
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var llblAddress: UILabel!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    
    @IBOutlet weak var btnTGetAppoinment: UIButton!
    @IBOutlet weak var lblTDescription: UILabel!
    @IBOutlet weak var lblTOurTreatmetn: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var heightOutTreatment: NSLayoutConstraint!
    
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    let sitemsPerRow : CGFloat = 5
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.moviesectionInsets.left * (self.moviesitemsPerRow + 1)
            let availableWidth = self.collectionImg.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.moviesitemsPerRow
            
            _flowLayout.itemSize = CGSize(width: 60, height: 60)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _flowLayout.minimumInteritemSpacing = 5
            _flowLayout.minimumLineSpacing = 5
        }
        
        // edit properties here
        return _flowLayout
    }
    
    let moviesectionInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    let moviesitemsPerRow : CGFloat = 4
    
    var MoviesflowLayout: UICollectionViewFlowLayout {
        let _movieflowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            let paddingSpace = self.moviesectionInsets.left * (self.moviesitemsPerRow + 1)
            let availableWidth = self.collectionViewOurTreatment.frame.width - paddingSpace
            let widthPerItem = availableWidth / self.moviesitemsPerRow
            
            self.objHeight = Int(widthPerItem+5)
            _movieflowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem)
            
            _movieflowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            _movieflowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
            _movieflowLayout.minimumInteritemSpacing = 5
            _movieflowLayout.minimumLineSpacing = 5
        }
        
        // edit properties here
        return _movieflowLayout
    }

    var dicHospitalDetail = GFCHospitalDetailData()
    
    var arrhospital_imgArr = NSMutableArray()
    
    var arrTreatment: [GFCHospitalDetailTreatment] = [GFCHospitalDetailTreatment]()
    
    var strHospitalID = ""
    
    var strSelectImg = 0
    
    var strTretmentUrl_ = ""
    
    var objHeight = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTDescription.text = "Description".localizeString(string: Language.shared.currentAppLang)
        lblTOurTreatmetn.text = "OUR TREATMENT".localizeString(string: Language.shared.currentAppLang)
        
        btnTGetAppoinment.setTitle("Sent Query".localizeString(string: Language.shared.currentAppLang), for: .normal)

        collectionViewOurTreatment.delegate = self
        collectionViewOurTreatment.dataSource = self
        collectionViewOurTreatment.collectionViewLayout = MoviesflowLayout
        
        collectionImg.delegate = self
        collectionImg.dataSource = self
        collectionImg.collectionViewLayout = flowLayout
     
        callSpeciality()
        
        lblName.numberOfLines = 0
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGetAppointmentClicked(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "GetAppointmentViewController") as! GetAppointmentViewController
        controller.strHospitalID = self.strHospitalID
         self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func callSpeciality()
    {
        APIClient.sharedInstance.showIndicator()
        
        let para = ["token":"123456","hospitalId":self.strHospitalID]
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_HOSPITAL_DETAIL, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let dic = response?.value(forKey: "data") as? NSDictionary
                        
                        let treatment_imgURL = dic?.value(forKey: "treatment_imgURL") as? String
                        
                        self.strTretmentUrl_ = treatment_imgURL ?? ""
                        
                        let dicData = GFCHospitalDetailData(fromDictionary: dic!)
                        
                        self.dicHospitalDetail = dicData
                        
                        self.lblName.text = self.dicHospitalDetail.name ?? ""
                        self.llblAddress.text = self.dicHospitalDetail.address ?? ""
                        self.lblDes.text = self.dicHospitalDetail.descriptionField ?? "".htmlToString
                        
                        if let arrHospitalImg = dic?.value(forKey: "hospital_imgArr") as? NSArray
                        {
                            self.arrhospital_imgArr = arrHospitalImg.mutableCopy() as! NSMutableArray
                            
                            if (arrHospitalImg.count ?? 0)  > 0
                            {
                                var media_link_url = arrHospitalImg[0] as! String
                                media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
                                self.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
                            }

                            self.collectionImg.reloadData()
                        }
                        
                        if let arrTreatments = dic?.value(forKey: "treatments") as? NSArray
                        {
                            if (arrTreatments.count ?? 0)  > 0
                            {
                                for obj in arrTreatments
                                {
                                    let dicData = GFCHospitalDetailTreatment(fromDictionary: obj as! NSDictionary)
                                    self.arrTreatment.append(dicData)
                                }
                              
                            }
                        }
                        
                        let objCount = self.arrTreatment.count
                        
                        let dev = Double(objCount) / 4
                        
                        var objHeight = 0.0
                        
                        let str = "\(dev)"
                        
                        if let value = str.double {
                            objHeight = value + 0.5
                        } else {
                            objHeight = Double(dev)
                        }
                        
                        let totalHeight = Int(objHeight) * self.objHeight
                        
                        self.heightOutTreatment.constant = CGFloat(Int(totalHeight) + 20)
                        
                        self.collectionViewOurTreatment.reloadData()
                    }
                }
            }
        }
    }
   

}
extension HospitalDetailViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionImg
        {
            return arrhospital_imgArr.count
        }
        else
        {
            return arrTreatment.count
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionImg
        {
            let cell = collectionImg.dequeueReusableCell(withReuseIdentifier: "ImgCollectionCell", for: indexPath) as! ImgCollectionCell
            
            let dicData = arrhospital_imgArr[indexPath.row] as! String
            
            var media_link_url = dicData
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
            
            if strSelectImg == indexPath.row
            {
                
                cell.imgPic.layer.borderWidth = 1
            }
            else
            {
                cell.imgPic.layer.borderWidth = 0
            }
            
            
            return cell
        }
        else
        {
            let cell = collectionViewOurTreatment.dequeueReusableCell(withReuseIdentifier: "TreatmentCollectionViewCell", for: indexPath) as! TreatmentCollectionViewCell
            
            let dicData = arrTreatment[indexPath.row]
            
            let img = dicData.images
            
            if (img?.count ?? 0) > 0
            {
                var media_link_url = strTretmentUrl_ + (img?[0].imgName ?? "")
                media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
                cell.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
                
            }
            else
            {
                cell.imgPic.image = UIImage(named: "app_logo")
            }
            cell.lblName.text = dicData.treatmentName ?? ""
            
            return cell
        }
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.item)
        
        if collectionView == collectionImg
        {
            strSelectImg = indexPath.row
            let dicData = arrhospital_imgArr[indexPath.row] as! String
            var media_link_url = dicData
            media_link_url = (media_link_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
            self.imgPic.sd_setImage(with: URL.init(string: media_link_url), placeholderImage: UIImage(named: "app_logo"), options: [], completed: nil)
            collectionImg.reloadData()
        }
       
        
    }
}
class TreatmentCollectionViewCell: UICollectionViewCell{
  
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}

class ImgCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var imgPic: UIImageView!
}

extension StringProtocol {
    var double: Double? { Double(self) }
}
