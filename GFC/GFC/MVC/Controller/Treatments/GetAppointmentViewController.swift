//
//  GetAppointmentViewController.swift
//  GFC
//
//  Created by Ankit Gabani on 29/04/22.
//

import UIKit
import DropDown
import Toast_Swift
import MobileCoreServices

class GetAppointmentViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtPassport: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtSelectCountry: UITextField!
    @IBOutlet weak var txtAAddress: UITextView!
    @IBOutlet weak var imgPic: UIImageView!
    
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtAttendantName: UITextField!
    @IBOutlet weak var txtAttendantPhoneNumber: UITextField!
    @IBOutlet weak var txtAttendantRelation: UITextField!
    
    @IBOutlet weak var maleMainView: UIView!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var feMaleMainView: UIView!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var otherMainView: UIView!
    @IBOutlet weak var otherBtn: UIButton!
    
    @IBOutlet weak var viewButtomCoun: UIView!
    @IBOutlet weak var viewButtomState: UIView!
    @IBOutlet weak var viewButtomCity: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var ViewSelectProfilPic: UIView!
    
    @IBOutlet weak var lblTSuccessful: UILabel!
    
    @IBOutlet weak var btnTOk: UIButton!
    
    @IBOutlet weak var lblTBookAppointment: UILabel!
    @IBOutlet weak var lblTGender: UILabel!
    @IBOutlet weak var lblTMale: UILabel!
    @IBOutlet weak var lblTFemale: UILabel!
    @IBOutlet weak var lblTOther: UILabel!
    @IBOutlet weak var lblTAttendantDetail: UILabel!
    
    @IBOutlet weak var viewBG: UIView!
    
    @IBOutlet weak var btnTBookAppointment: UIButton!
    @IBOutlet weak var btnTSelectProfilePic: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    
    let dropDownCoutnry = DropDown()
    let dropDownState = DropDown()
    let dropDownCity = DropDown()
    var selectedGender = ""
    
    var arrCountryList: [GFCCountryListData] = [GFCCountryListData]()
    
    var arrStateList: [GFCStateListData] = [GFCStateListData]()
    
    var arrCityList: [GFCCityListData] = [GFCCityListData]()

    var strCountry = ""
    var strState = ""
    var strCity = ""
    
    var strHospitalID = ""
    
    var isUploadPic = false
    
    var imagePicker: ImagePicker!
    var selectedImg: String?
    
    var imagePickerNew = UIImagePickerController()
    
    var appointment_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Language.shared.isArabic == false{
            btnBack.setImage(UIImage(named: "Group"), for: .normal)
        }else{
            btnBack.setImage(UIImage(named: "A_Back"), for: .normal)
        }
        
        lblTBookAppointment.text = "Sent Query".localizeString(string: Language.shared.currentAppLang)
        lblTGender.text = "GENDER".localizeString(string: Language.shared.currentAppLang)
        lblTAttendantDetail.text = "ATTENDANT DETAIL".localizeString(string: Language.shared.currentAppLang)
        lblTMale.text = "Male".localizeString(string: Language.shared.currentAppLang)
        lblTFemale.text = "Female".localizeString(string: Language.shared.currentAppLang)
        lblTOther.text = "Other".localizeString(string: Language.shared.currentAppLang)
        lblTSuccessful.text = "successful!".localizeString(string: Language.shared.currentAppLang)
        lblTitle.text = "Query Sent Successfully.".localizeString(string: Language.shared.currentAppLang)
        
        btnTSelectProfilePic.setTitle("Select Profile Pic".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnTBookAppointment.setTitle("Sent Query".localizeString(string: Language.shared.currentAppLang), for: .normal)
        btnTOk.setTitle("OK".localizeString(string: Language.shared.currentAppLang), for: .normal)
        
        txtFirstName.placeholder = "Full Name".localizeString(string: Language.shared.currentAppLang)
        txtEmail.placeholder = "Email".localizeString(string: Language.shared.currentAppLang)
        txtPhoneNumber.placeholder = "Phone Number".localizeString(string: Language.shared.currentAppLang)
        txtAddress.placeholder = "Address".localizeString(string: Language.shared.currentAppLang)
        txtStreet.placeholder = "Street".localizeString(string: Language.shared.currentAppLang)
        txtSelectCountry.placeholder = "Select Country".localizeString(string: Language.shared.currentAppLang)
        txtState.placeholder = "Select State".localizeString(string: Language.shared.currentAppLang)
        txtCity.placeholder = "Select City".localizeString(string: Language.shared.currentAppLang)
        txtPassport.placeholder = "Passport".localizeString(string: Language.shared.currentAppLang)
        
        txtAttendantName.placeholder = "Attendant Full Name".localizeString(string: Language.shared.currentAppLang)
        txtAttendantPhoneNumber.placeholder = "Attendant Phone Number".localizeString(string: Language.shared.currentAppLang)
        txtAttendantRelation.placeholder = "Attendant Relation".localizeString(string: Language.shared.currentAppLang)

        txtAAddress.delegate = self
        txtAAddress.text = "Attendant Address".localizeString(string: Language.shared.currentAppLang)

        if Language.shared.isArabic == false{
            txtFirstName.textAlignment = .left
            txtEmail.textAlignment = .left
            txtPhoneNumber.textAlignment = .left
            txtAddress.textAlignment = .left
            txtStreet.textAlignment = .left
            txtSelectCountry.textAlignment = .left
            txtState.textAlignment = .left
            txtCity.textAlignment = .left
            txtPassport.textAlignment = .left
            
            txtAttendantName.textAlignment = .left
            txtAttendantPhoneNumber.textAlignment = .left
            txtAttendantRelation.textAlignment = .left
            txtAAddress.textAlignment = .left
        }else{
            txtFirstName.textAlignment = .right
            txtEmail.textAlignment = .right
            txtPhoneNumber.textAlignment = .right
            txtAddress.textAlignment = .right
            txtStreet.textAlignment = .right
            txtSelectCountry.textAlignment = .right
            txtState.textAlignment = .right
            txtCity.textAlignment = .right
            txtPassport.textAlignment = .right
            
            txtAttendantName.textAlignment = .right
            txtAttendantPhoneNumber.textAlignment = .right
            txtAttendantRelation.textAlignment = .right
            txtAAddress.textAlignment = .right
        }
        
        
        callCountryListAPI()
        viewBG.isHidden = true
        
        ViewSelectProfilPic.layer.applySketchShadow(color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2), alpha: 1, x: 0, y: 0, blur: 10, spread: 0)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtAAddress.text == "Attendant Address".localizeString(string: Language.shared.currentAppLang) {
            txtAAddress.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtAAddress.text.isEmpty {
            txtAAddress.text = "Attendant Address".localizeString(string: Language.shared.currentAppLang)
        }
    }
    
    func setDropDownCountry()
    {
        let Country = NSMutableArray()
        
        for obj in arrCountryList
        {
            Country.add(obj.countryName ?? "")
        }
        
        dropDownCoutnry.dataSource = Country as! [String]
        dropDownCoutnry.anchorView = viewButtomCoun
        dropDownCoutnry.direction = .bottom
        
        dropDownCoutnry.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtSelectCountry.text = item
            
            for obj in arrCountryList
            {
                if obj.countryName == item
                {
                    self.strCountry = obj.countryId ?? ""
                }
            }
            callStateListAPI(country_id: strCountry)

        }
        
        dropDownCoutnry.bottomOffset = CGPoint(x: 0, y: viewButtomCoun.bounds.height)
        dropDownCoutnry.topOffset = CGPoint(x: 0, y: -viewButtomCoun.bounds.height)
        dropDownCoutnry.dismissMode = .onTap
        dropDownCoutnry.textColor = UIColor.darkGray
        dropDownCoutnry.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCoutnry.selectionBackgroundColor = UIColor.clear
        dropDownCoutnry.selectedTextColor = .black

        dropDownCoutnry.reloadAllComponents()
    }
    
    func setDropDownState()
    {
        let State = NSMutableArray()
        
        for obj in arrStateList
        {
            State.add(obj.stateName ?? "")
        }
        
        dropDownState.dataSource = State as! [String]
        dropDownState.anchorView = viewButtomState
        dropDownState.direction = .bottom
        
        dropDownState.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtState.text = item
            
            for obj in arrStateList
            {
                if obj.stateName == item
                {
                    self.strState = obj.stateId ?? ""
                }
            }
            
            callCityListAPI(state_id: strState)

        }
        
        dropDownState.bottomOffset = CGPoint(x: 0, y: viewButtomState.bounds.height)
        dropDownState.topOffset = CGPoint(x: 0, y: -viewButtomState.bounds.height)
        dropDownState.dismissMode = .onTap
        dropDownState.textColor = UIColor.darkGray
        dropDownState.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownState.selectionBackgroundColor = UIColor.clear
        dropDownState.selectedTextColor = .black

        dropDownState.reloadAllComponents()
    }
    
    func setDropDownCity()
    {
        let City = NSMutableArray()
        
        for obj in arrCityList
        {
            City.add(obj.cityName ?? "")
        }
        
        dropDownCity.dataSource = City as! [String]
        dropDownCity.anchorView = viewButtomCity
        dropDownCity.direction = .bottom
        
        dropDownCity.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtCity.text = item
            
            for obj in arrCityList
            {
                if obj.cityName == item
                {
                    self.strCity = obj.cityId ?? ""
                }
            }
        }
        
        dropDownCity.bottomOffset = CGPoint(x: 0, y: viewButtomCity.bounds.height)
        dropDownCity.topOffset = CGPoint(x: 0, y: -viewButtomCity.bounds.height)
        dropDownCity.dismissMode = .onTap
        dropDownCity.textColor = UIColor.darkGray
        dropDownCity.backgroundColor = UIColor(red: 255/255, green:  255/255, blue:  255/255, alpha: 1)
        dropDownCity.selectionBackgroundColor = UIColor.clear
        dropDownCity.selectedTextColor = .black

        dropDownCity.reloadAllComponents()
    }
    
    
    @IBAction func btnSelectCountryDropDown(_ sender: Any) {
        dropDownCoutnry.show()
    }
    
    @IBAction func clickedState(_ sender: Any) {
        dropDownState.show()
    }
    @IBAction func clickedCity(_ sender: Any) {
        dropDownCity.show()
    }
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedOkBG(_ sender: Any) {
        viewBG.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let mainS = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainS.instantiateViewController(withIdentifier: "MedicalEditVC") as! MedicalEditVC
            vc.isFromHome = true
            vc.strAppointmentID = "\(self.appointment_id)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnMaleClicked(_ sender: UIButton) {
        if !sender.isSelected {
            maleBtn.isSelected = true
            femaleBtn.isSelected = false
            otherBtn.isSelected = false
            selectedGender = "Male"
            maleMainView.borderColor = .green
            feMaleMainView.borderColor = .black
            otherMainView.borderColor = .black
        }
    }
    @IBAction func btnFemaleClicked(_ sender: UIButton) {
        if !sender.isSelected {
            femaleBtn.isSelected = true
            maleBtn.isSelected = false
            otherBtn.isSelected = false
            selectedGender = "Female"
            feMaleMainView.borderColor = .green
            maleMainView.borderColor = .black
            otherMainView.borderColor = .black
        }
    }
    @IBAction func btnOtherClicked(_ sender: UIButton) {
        if !sender.isSelected {
            otherBtn.isSelected = true
            maleBtn.isSelected = false
            femaleBtn.isSelected = false
            selectedGender = "Other"
            otherMainView.borderColor = .green
            feMaleMainView.borderColor = .black
            maleMainView.borderColor = .black
        }
    }
    
    @IBAction func btnBookAppointmentClicked(_ sender: Any) {
        
        if txtFirstName.text == ""{
            self.view.makeToast("Enter Full Name".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtEmail.text == ""
        {
            self.view.makeToast("Enter email".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if !AppUtilites.isValidEmail(testStr: txtEmail.text ?? ""){
            self.view.makeToast("Enter Valid Email".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtPhoneNumber.text == ""
        {
            self.view.makeToast("Enter Phone Number".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if (self.txtPhoneNumber.text?.count ?? 0) < 8
        {
            self.view.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))
            let window = UIApplication.shared.windows
            window.last?.makeToast("Enter Valid Phone Number".localizeString(string: Language.shared.currentAppLang))

            return
        }
        if txtAddress.text == ""{
            self.view.makeToast("Enter Address".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtStreet.text == ""
        {
            self.view.makeToast("Enter Street".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtSelectCountry.text == ""{
            self.view.makeToast("Select Country".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtState.text == ""{
            self.view.makeToast("Select State".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtCity.text == ""{
            self.view.makeToast("Select City".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtPassport.text == ""{
            self.view.makeToast("Enter passport".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if selectedGender == ""{
            self.view.makeToast("Select Gender".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtAttendantName.text == ""{
            self.view.makeToast("Enter Attendant Full Name".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtAttendantPhoneNumber.text == ""
        {
            self.view.makeToast("Enter attendant phone number".localizeString(string: Language.shared.currentAppLang))
            return
        }
         if txtAttendantRelation.text == ""{
            self.view.makeToast("Enter attendant relation".localizeString(string: Language.shared.currentAppLang))
            return
        }
        if txtAAddress.text == ""{
            self.view.makeToast("Enter attendant address".localizeString(string: Language.shared.currentAppLang))
            return
        }
        else
        {
            if isUploadPic == true
            {
                myImageUploadRequest(imageToUpload: imgPic.image!, imgKey: "profileImage")
            }
            else
            {
                self.view.makeToast("Select image".localizeString(string: Language.shared.currentAppLang))
            }
        }
        
        
    }
    
    @IBAction func clickedSelectimg(_ sender: Any) {
        let alert1 = UIAlertController(title: "Choose Photo".localizeString(string: Language.shared.currentAppLang) , message: nil, preferredStyle: .actionSheet)

        alert1.addAction(UIAlertAction(title: "Camera".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { _ in
            self.openCamera()
        }))

        alert1.addAction(UIAlertAction(title: "Gallery".localizeString(string: Language.shared.currentAppLang), style: .default, handler: { _ in
            self.openGallary()
        }))

        alert1.addAction(UIAlertAction.init(title: "Cancel".localizeString(string: Language.shared.currentAppLang), style: .cancel, handler: nil))

        self.present(alert1, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePickerNew.delegate = self
            imagePickerNew.sourceType = UIImagePickerController.SourceType.camera
            imagePickerNew.allowsEditing = true
            imagePickerNew.mediaTypes = [kUTTypeImage as String]
            self.present(imagePickerNew, animated: true, completion: nil)
        }
        else
        {
            
            let windows = UIApplication.shared.windows
            windows.last?.makeToast("You don't have camera")
        }
    }
    
    func openGallary()
    {
        
        imagePickerNew.delegate = self
        imagePickerNew.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePickerNew.allowsEditing = true
        imagePickerNew.mediaTypes = ["public.image"]
        self.present(imagePickerNew, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        {
            let imageurl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            selectedImg = imageurl?.lastPathComponent
            
           

            DispatchQueue.main.async {
                self.isUploadPic = true
                self.imgPic.image = image
            }
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            let imageurl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
            
            selectedImg = imageurl?.lastPathComponent
            
            DispatchQueue.main.async {
                self.isUploadPic = true
                self.imgPic.image = image
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - call API
    
    func callCountryListAPI()
    {
       // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_COUNTRY, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCountryList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCountryListData(fromDictionary: obj as! NSDictionary)
                            self.arrCountryList.append(dicData)
                        }
                        self.setDropDownCountry()
                    }
                }
            }
        }
    }
    
    func callStateListAPI(country_id:String)
    {
       // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_STATE + country_id, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrStateList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCStateListData(fromDictionary: obj as! NSDictionary)
                            self.arrStateList.append(dicData)
                        }
                        self.setDropDownState()
                    }
                }
            }
        }
    }
    
    func callCityListAPI(state_id:String)
    {
       // APIClient.sharedInstance.showIndicator()
        
        let para = ["":""]
        
        print(para)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(GET_CITY + state_id, parameters: para) { response, error, statusCode in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                if statusCode == 200
                {
                    let status = response?.value(forKey: "status") as? String
                    let message = response?.value(forKey: "message") as? String
                    
                    if status == "success"
                    {
                        let arrData = response?.value(forKey: "data") as? NSArray
                        self.arrCityList.removeAll()
                        for obj in arrData!
                        {
                            let dicData = GFCCityListData(fromDictionary: obj as! NSDictionary)
                            self.arrCityList.append(dicData)
                        }
                        self.setDropDownCity()
                    }
                }
            }
        }
    }
    
    
    func myImageUploadRequest(imageToUpload: UIImage, imgKey: String) {
        
        APIClient.sharedInstance.showIndicator()
        
        let myUrl = NSURL(string: BASE_URL + GET_APPOINTMENT_CREATE);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let hospitalId = self.strHospitalID
        
        let patient_name = self.txtFirstName.text ?? ""
        
        let patient_email = self.txtEmail.text ?? ""
        
        let phone = self.txtPhoneNumber.text ?? ""
        
        let address = self.txtAddress.text ?? ""
        
        let patient_street = self.txtStreet.text ?? ""
        
        let patient_city = self.strCity
        
        let patient_state = self.strState
        
        let patient_country = self.strCountry
        
        let patient_passport = self.txtPassport.text ?? ""
        
        let gender = self.selectedGender
        
        let attendant_name = self.txtAttendantName.text ?? ""
        
        let attendant_phone = self.txtAttendantPhoneNumber.text ?? ""
        
        let attendant_address = self.txtAAddress.text ?? ""
        
        let relation = self.txtAttendantRelation.text ?? ""
        
        let patientId = appDelegate?.dicUserLoginData.id ?? ""
        
        let para = ["hospitalId":hospitalId,"patient_name":patient_name,"patient_email":patient_email,"phone":phone,"address":address,"patient_street":patient_street,"patient_city":patient_city,"patient_state":patient_state,"patient_country":patient_country,"patient_passport":patient_passport,"gender":gender,"services_interested_in":"12","attendant_name":attendant_name,"attendant_phone":attendant_phone,"attendant_address":attendant_address,"relation":relation,"patientId":patientId,"token":"123456"]
        
        let boundary = generateBoundaryString()
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let imageData = imageToUpload.jpegData(compressionQuality: 1)
        if imageData == nil  {
            return
        }
        
        request.httpBody = createBodyWithParameters(parameters: para, filePathKey: imgKey, imageDataKey: imageData! as NSData, boundary: boundary, imgKey: imgKey) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            APIClient.sharedInstance.hideIndicator()
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            
            // print reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("response data = \(responseString!)")
            
            do{
                
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                    
                    // try to read out a string array
                    if let message = json["message"] as? String
                    {
                        let dicData = json["data"] as? NSDictionary
                        
                        let appointment_id = dicData?.value(forKey: "appointment_id") as? Int
                        
                        self.appointment_id = appointment_id ?? 0
                        
                        
                        DispatchQueue.main.async {
                            self.viewBG.isHidden = false
                        }
                    }
                    
                }
                
            }catch{
                
            }
            
        }
        
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String, imgKey: String) -> NSData {
        
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "\(imgKey).jpg"
        let mimetype = "image/jpeg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\("profileImage")\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func randomString(of length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var s = ""
        for _ in 0 ..< length {
            s.append(letters.randomElement()!)
        }
        return s
    }
    
    
}
