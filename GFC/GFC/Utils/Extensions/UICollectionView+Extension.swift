//
//  UICollectionView+Extension.swift
//  GFC Globalhealth
//
//  Created by Ankit Gabani on 14/09/24.
//

import Foundation
import UIKit
import Quickblox

extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}
