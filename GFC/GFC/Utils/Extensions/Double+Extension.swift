//
//  Double+Extension.swift
//  GFC Globalhealth
//
//  Created by Ankit Gabani on 14/09/24.
//

import Foundation
import UIKit
import Quickblox

extension Double {
    func truncate(to places: Int) -> Double {
        return Double(Int((pow(10, Double(places)) * self).rounded())) / pow(10, Double(places))
    }
}
