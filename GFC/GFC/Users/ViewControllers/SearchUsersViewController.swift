//
//  SearchUsersViewController.swift
//  sample-chat-swift
//
//  Created by Injoit on 28.12.2021.
//  Copyright © 2021 QuickBlox. All rights reserved.
//

import UIKit
import Quickblox

class SearchUsersViewController: UserListViewController {
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, nonDisplayedUsers: [UInt], searchText: String) {
        self.searchText = searchText
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil, nonDisplayedUsers: nonDisplayedUsers)
    }
    required init?(coder: NSCoder) {
        fatalError("You must create this view controller with a user.")
    }
    
    //MARK: - Properties
    var searchText: String = "" {
        didSet {
            if searchText.count > 2 {
                searchUsers(searchText)
            }
        }
    }
    
    //MARK: - Public Methods
    override func fetchUsers() {
        searchUsers(searchText)
    }

    override func fetchNext() {
        
    }

    //MARK: - Private Methods
    private func searchUsers(_ name: String) {
        
    }
}

extension UserList {
    func search(_ name: String, pageNumber: UInt, completion: DownloadUsersCompletion?) {
        
    }
    
    func searchNext(_ name: String, completion: DownloadUsersCompletion?) {
         
    }
}
