//
//  Constants.swift
//  GFC
//
//  Created by Parth Anghan on 18/12/21.
//

import Foundation
import UIKit
import Quickblox
//MARK:- BASE_URL
let BASE_URL = "https://gfcglobalhealth.com/"

let GOOGLE_KEY = "AIzaSyCIx4rfzMxR295rXQmLso1NdAOTQraG-cA"


//Dev


//Live


//MARK:- End Point
let HOME_SCREEN = "PatientApi/homeScreen"

let LOGIN_API = "PatientApi/login"

let SIGNUP_API = "PatientApi/signUp"

let VERIFY_OTP = "PatientApi/verifyOtp"

let SEND_OTP = "PatientApi/sendOtp"

let FORGET_PASSWORD = "PatientApi/forgotPassword"

let RESET_PASSWORD = "PatientApi/resetPassword"

let PHARMACY_PRODUCTS = "PatientApi/pharmacy_products"

let PHARMACY_CATEGORY = "PatientApi/pharmacy_category"

let PHARMACY_SUBCATEGORY = "PatientApi/pharmacy_sub_category"

let PHARMACY_SUBCHILD = "PatientApi/pharmacy_child_category"

let LAB_TEST_CATEGORY = "PatientApi/pharmacy_child_category"

let GET_COUNTRY = "PatientApi/getCountry"

let GET_COUNTRY_LIST = "PatientApi/getCountryList"

let GET_STATE = "PatientApi/getStateByCountryId?country_id="

let GET_CITY = "PatientApi/getStateByStateId?state_id="

let GET_CITY_LIST = "PatientApi/getCity"

let GET_SPECIALITY = "PatientApi/getSpeciality"

let GET_SPECIALITYBY_DOC = "PatientApi/doctorsBySpeciality"

let GET_LAB_TEST_CATEGORY = "PatientApi/getLabTestCategory"

let EDIT_PROFILE = "PatientApi/editProfile"

let GET_ADDRESS_LIST = "PatientApi/getAddress?"

let EDIT_PROFILE_IMAGE = "PatientApi/editProfileImage"

let DOCTORS_DETAILS = "PatientApi/doctorsDetails"

let TIME_SLOT = "PatientApi/timeSlot"

let BOOK_APPOINTMENT = "PatientApi/bookAppointment"

let SEARCH_DOCTOR_LIST = "PatientApi/doctorSearchList"

let GET_DOCTOR_LIST = "PatientApi/getDoctors"

let GET_SERVIES = "PatientApi/getServices"

let SENT_QUERY = "PatientApi/sentQuery"

let SAVE_ADDRESS = "PatientApi/addresSave"

let UPDATE_ADDRESS = "PatientApi/updateaddress"

let DALETE_ADDRESS = "PatientApi/deleteaddress"

let GET_TREATMENT = "PatientApi/treatments"

let GET_HOSPITAL_C_ID = "PatientApi/hospitalByCountry"

let GET_HOSPITAL_DETAIL = "PatientApi/getHospitalsDetailsById"

let GET_APPOINTMENT_CREATE = "PatientApi/appointmentCreate"

let GET_CATE_LIST = "PatientApi/getLabTestsByCategory"

let BOOK_TEST = "PatientApi/bookTest"

let GET_APPOINTMENT = "PatientApi/getAppointment"

let GET_ORDER_H = "PatientApi/getOrderHistory"

let MEDICAL_FORM = "PatientApi/medicalForm"

let MEDICAL_FORM_DETAIL = "PatientApi/medicalFormDetail"

let TREATMENET_LIST = "PatientApi/treatmentDetail"

let TO_DO_NOTE = "PatientApi/toDoDetail"

let ADD_RATING = "PatientApi/addRating"

let PLAN_DETEIL = "PatientApi/planDetail"

let FLIGHT_DETEIL = "PatientApi/flightDetail"


let PAYMENT_HISTORY = "PatientApi/paymentDetail"

let CART_ITEM = "PatientApi/getCartItems"

let UPDATR_CART_ITEM = "PatientApi/addUpdateToCart"

let HOSPITAL_COUNTRY_ = "PatientApi/hospitalsCountry"


let SEARCH_PRODUCT = "PatientApi/searchProduct"

let PHARMACY_P_DETAIL = "PatientApi/pharmacy_productsDetails"

let DELETE_CART = "PatientApi/removeCartById"

let PLACE_ORDER = "PatientApi/placeorder"

// **********************************************************************

let appDelegate = UIApplication.shared.delegate as? AppDelegate
