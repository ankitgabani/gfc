//
//  AppColor.swift
//  Swoosh Rider
//
//  Created by Parth Anghan on 01/04/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import Foundation
import UIKit
import Quickblox
//#1181D9 Primary gradient-1
let PRIMARY_GARDIENT_1_COLOR = UIColor(hexString: "1181D9")


//#4DB1FF gradient-2
let PRIMARY_GARDIENT_2_COLOR = UIColor(hexString: "4DB1FF")


//#EEF4FC textbox
let TEXT_BOX_COLOR = UIColor(hexString: "EEF4FC")


//#7F88A2 grey text
let GREY_APP_COLOR = UIColor(hexString: "7F88A2")


//#121212 black text
let BLACK_APP_COLOR = UIColor(hexString: "121212")


//#E7EBF1 img placeholder & stock line
let IMG_PLACEGOLDER_STOCK_LINE_COLOR = UIColor(hexString: "E7EBF1")


//#EEF2FB line COLOR
let LINE_COLOR = UIColor(hexString: "EEF2FB")


//#21BF73 BTN COLOR
let BUTTON_COLOR = UIColor(hexString: "21BF73")


//#7F59B0 CARD-1 COLOR
let CARD_1_COLOR = UIColor(hexString: "7F59B0")


//#63C5EA CARD-2 COLOR
let CARD_2_COLOR = UIColor(hexString: "63C5EA")


//#EA4B8B CARD-3 COLOR
let CARD_3_COLOR = UIColor(hexString: "EA4B8B")


//#FCB424 CARD-4 COLOR
let CARD_4_COLOR = UIColor(hexString: "FCB424")

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)

        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }

        var color: UInt32 = 0
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask

        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0

        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return String(format:"#%06x", rgb)
    }
}

extension CALayer {
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0)
    {
        masksToBounds = false
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}

class RectangularDashedView: UIView {
    
    @IBInspectable var cornerRadius1: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius1
            layer.masksToBounds = cornerRadius1 > 0
        }
    }
    @IBInspectable var dashWidth: CGFloat = 0
    @IBInspectable var dashColor: UIColor = .clear
    @IBInspectable var dashLength: CGFloat = 0
    @IBInspectable var betweenDashesSpace: CGFloat = 0
    
    var dashBorder: CAShapeLayer?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        if cornerRadius1 > 0 {
            dashBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius1).cgPath
        } else {
            dashBorder.path = UIBezierPath(rect: bounds).cgPath
        }
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
}

