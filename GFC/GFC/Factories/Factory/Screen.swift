//
//  ModuleFactory.swift
//  sample-videochat-webrtc-swift
//
//  Created by Injoit on 01.09.2020.
//  Copyright © 2020 QuickBlox. All rights reserved.
//

import Foundation

final class Screen {
   
    class func videoCallViewController() -> VideoCallViewController? {
        return VideoCallViewController.controllerFromStoryboard(.call)
    }
    
    class func audioCallViewController() -> CallViewController? {
        return CallViewController.controllerFromStoryboard(.call)
    }
    
    class func dialogsViewController() -> DialogsViewController? {
        return DialogsViewController.controllerFromStoryboard(.dialogs)
    }
    
    class func dialogsSelectionViewController() -> DialogsSelectionViewController? {
        return DialogsSelectionViewController.controllerFromStoryboard(.dialogs)
    }
    
    class func createNewDialogViewController() -> CreateNewDialogViewController? {
        return CreateNewDialogViewController.controllerFromStoryboard(.dialogs)
    }
    
    class func userListViewController(nonDisplayedUsers: [UInt]) -> UserListViewController? {
        return UserListViewController(nibName: nil, bundle: nil, nonDisplayedUsers: nonDisplayedUsers)
    }
    
    class func searchUsersViewController(nonDisplayedUsers: [UInt], searchText: String) -> SearchUsersViewController? {
        return SearchUsersViewController(nibName: nil, bundle: nil, nonDisplayedUsers: nonDisplayedUsers, searchText: searchText)
    }
    
    class func usersInfoViewController(nonDisplayedUsers: [UInt]) -> UsersInfoViewController? {
        return UsersInfoViewController(nibName: nil, bundle: nil, nonDisplayedUsers: nonDisplayedUsers)
    }
    class func viewedByViewController(nonDisplayedUsers: [UInt]) -> ViewedByViewController? {
        return ViewedByViewController(nibName: nil, bundle: nil, nonDisplayedUsers: nonDisplayedUsers)
    }
    
    class func addOccupantsVC() -> AddOccupantsVC? {
        return AddOccupantsVC.controllerFromStoryboard(.chat)
    }
    
    class func chatViewController() -> ChatViewController? {
        return ChatViewController.controllerFromStoryboard(.chat)
    }
    
    class func selectAssetsViewController() -> SelectAssetsViewController? {
        return SelectAssetsViewController.controllerFromStoryboard(.chat)
    }
    
    class func infoTableViewController() -> InfoTableViewController? {
        return InfoTableViewController.controllerFromStoryboard(.infoApp)
    }
   
}

final class ChatButton {
     func accessoryButton() -> UIButton {
         let image = #imageLiteral(resourceName: "attachment_ic")
        return chatButton(with: image, size: CGSize(width: 32.0, height: 32.0))
    }
    
     func accessorySendButton() -> UIButton {
         let image = #imageLiteral(resourceName: "send")
         return chatButton(with: image, size: CGSize(width: 32.0, height: 28.0))
    }
    
    private func chatButton(with image: UIImage, size: CGSize) -> UIButton {
        let accessoryImage = image.withTintColor(#colorLiteral(red: 0.2216441333, green: 0.4713830948, blue: 0.9869660735, alpha: 1))
        let accessoryButton = UIButton(frame: CGRect(origin: .zero, size: size))
        accessoryButton.setImage(accessoryImage, for: .normal)
        accessoryButton.setImage(accessoryImage, for: .highlighted)
        accessoryButton.contentMode = .scaleAspectFit
        accessoryButton.backgroundColor = .clear
        accessoryButton.tintColor = #colorLiteral(red: 0.2216441333, green: 0.4713830948, blue: 0.9869660735, alpha: 1)
        return accessoryButton
    }
}
