"Select Thyroid"package com.gfc.globalhealth.retrofit

import com.gfc.globalhealth.modal.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface APiInterface {

    @FormUrlEncoded
    @POST("PatientApi/login")
    fun login(
        @Field("username") username: String,
        @Field("signuptype") signuptype: String,
        @Field("password") password: String
    ): Call<LoginModal>

    //email:nikhil.webdev@gmail.com
    //signuptype:2
    //name:Testing
    //g_id:2432
    //latitude:21.22245
    //longitude:31.2234
    //fcmToken:jhjih3483u43442
    @FormUrlEncoded
    @POST("PatientApi/login")
    fun googleLogin(
        @Field("email") email: String,
        @Field("signuptype") signuptype: String,
        @Field("name") name: String,
        @Field("g_id") g_id: String,
        @Field("latitude") latitude: String,
        @Field("longitude") longitude: String,
        @Field("fcmToken") fcmToken: String,
    ): Call<LoginModal>

    @FormUrlEncoded
    @POST("PatientApi/signUp")
    fun signUp(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("mobileNumber") mobileNumber: String,
        @Field("city_id") city_id: String,
        @Field("state_id") state_id: String,
        @Field("country_id") country_id: String,
        @Field("signuptype") signuptype: String,
        @Field("latitude") latitude: String,
        @Field("longitude") longitude: String,
        @Field("fcmToken") fcmToken: String,
    ): Call<SignUpModal>

    @FormUrlEncoded
    @POST("PatientApi/forgotPassword")
    fun forgotPassword(
        @Field("username") username: String,
    ): Call<ForgotPasswordModal>

    @FormUrlEncoded
    @POST("PatientApi/resetPassword")
    fun resetPassword(
        @Field("patientId") patientId: String,
        @Field("password") password: String,
        @Field("confirm_password") confirm_password: String,
    ): Call<ResetPasswordModal>

    @FormUrlEncoded
    @POST("PatientApi/pharmacy_category")
    fun getPharmacyCategory(
        @Field("token") token: String,
//        @Field("country") country_id: String,
    ): Call<PharmacyCategoryModal>

    @FormUrlEncoded
    @POST("PatientApi/pharmacy_sub_category")
    fun getPharmacySubCategory(
        @Field("token") token: String,
        @Field("categoryId") categoryId: String,
    ): Call<PharmacySubCategoryModal>

    @FormUrlEncoded
    @POST("PatientApi/pharmacy_child_category")
    fun getPharmacyChildCategory(
        @Field("token") token: String,
        @Field("subCategoryId") subCategoryId: String,
    ): Call<PharmacyChildCategoryModal>

    @FormUrlEncoded
    @POST("PatientApi/pharmacy_products")
    fun getPharmacyProduct(
        @Field("token") token: String,
        @Field("categoryId") categoryId: String,
        @Field("country") country_id: String,
    ): Call<PharmacyProductModal>

    @FormUrlEncoded
    @POST("PatientApi/pharmacy_productsDetails")
    fun getPharmacyProductDetail(
        @Field("pid") pid: String,
        @Field("token") token: String,
    ): Call<PharmacyProductDetailModal>

    @FormUrlEncoded
    @POST("PatientApi/searchProduct")
    fun searchProduct(
        @Field("product_name") search: String,
        @Field("country") country: String,
    ): Call<PharmacyProductModal>

    @FormUrlEncoded
    @POST("PatientApi/getLabTestCategory")
    fun getLabCategory(
        @Field("token") token: String,
    ): Call<LabCategoryModel>

    @FormUrlEncoded
    @POST("PatientApi/getLabTestsByCategory")
    fun getLabTests(
        @Field("token") token: String,
        @Field("categoryId") categoryId: String,
        @Field("country") country_id: String,
        @Field("name") name:String,
    ): Call<LabTestModel>

    @FormUrlEncoded
    @POST("PatientApi/addUpdateToCart")
    fun addToCart(
        @Field("patientId") patientId: String,
        @Field("token") token: String,
        @Field("productId") productId: String,
        @Field("quantity") quantity: String,
        @Field("varient") varient: String,
    ): Call<AddUpdateRemoveCartModel>

    @FormUrlEncoded
    @POST("PatientApi/removeCartById")
    fun removeCart(
        @Field("patientId") patientId: String,
        @Field("token") token: String,
        @Field("cart_id") cart_id: String,
    ): Call<AddUpdateRemoveCartModel>

    @FormUrlEncoded
    @POST("PatientApi/getCartItems")
    fun getCartItems(
        @Field("patientId") patientId: String,
        @Field("token") token: String,
    ): Call<CartDataModel>

    @GET("PatientApi/getCountry")
    fun getCountryList(): Call<CountryModel>

    @GET("PatientApi/getStateByCountryId")
    fun getStateList(
        @Query("country_id") country_id: String
    ): Call<StateModel>

    @GET("PatientApi/getStateByStateId")
    fun getCityList(
        @Query("state_id") state_id: String
    ): Call<CityModel>

    @GET("PatientApi/treatments")
    fun getTreatmentList(): Call<TreatmentListModel>

    @GET("PatientApi/hospitalByCountry")
    fun getHospitalList(
        @Query("country_id") country_id: String,
        @Query("treatment_id") treatmentId: String,
        @Query("offset") page: String
    ): Call<HospitalListModel>

    @FormUrlEncoded
    @POST("PatientApi/getHospitalsDetailsById")
    fun getHospitalDetail(
        @Field("token") token: String,
        @Field("hospitalId") hospitalId: String,
    ): Call<HospitalDetailModel>

    @GET("PatientApi/getAppointment")
    fun getAppointmentList(
        @Query("patientId") patientId: String
    ): Call<AppointmentListModel>

    @FormUrlEncoded
    @POST("PatientApi/addresSave")
    fun addAddress(
        @Field("name") name: String,
        @Field("mobile") mobile: String,
        @Field("title") title: String,
        @Field("address") address: String,
        @Field("city") city: String,
        @Field("state") state: String,
        @Field("pincode") pincode: String,
        @Field("lat") lat: String,
        @Field("long") long: String,
//       @Field("country") country: String,
        @Field("patient_id") patient_id: String,
        @Field("token") token: String,
    ): Call<AddAddressModel>

    @FormUrlEncoded
    @POST("update-address")
    fun updateAddress(
        @Field("address_id") address_id: String,
//        @Field("name") name: String,
//        @Field("mobile") mobile: String,
//        @Field("title") title: String,
        @Field("address") address: String,
        @Field("city") city: String,
        @Field("state") state: String,
        @Field("pincode") pincode: String,
//        @Field("lat") lat: String,
//        @Field("long") long: String,
        @Field("country") country: String,
//        @Field("patient_id") patient_id: String,
//        @Field("token") token: String,
    ): Call<AddAddressModel>

    @GET("PatientApi/getAddress")
    fun getAddressList(
        @Query("patientId") patientId: String,
        @Query("token") token: String
    ): Call<AddressListModel>

    @FormUrlEncoded
    @POST("delete-address")
    fun deleteAddress(
        @Field("address_id") address_id: String,
        @Field("token") token: String
    ): Call<DeleteAddressModel>

    @Multipart
    @POST("PatientApi/appointmentCreate")
    fun bookHospitalAppointment(
        @Part("hospitalId") hospitalId: RequestBody,
        @Part("patient_name") patient_name: RequestBody,
        @Part("patient_email") patient_email: RequestBody,
        @Part("phone") patient_phone: RequestBody,
       // @Part("disease_name") disease_name: RequestBody,
        @Part("address") patient_address: RequestBody,
        @Part("patient_street") patient_street: RequestBody,
        @Part("patient_city") patient_city: RequestBody,
        @Part("patient_state") patient_state: RequestBody,
        @Part("patient_country") patient_country: RequestBody,
        @Part("patient_passport") patient_passport: RequestBody,
        //Part("best_days") best_days: RequestBody
        //Part("best_time") best_time: RequestBody
        @Part("gender") gender: RequestBody,
        @Part("services_interested_in") services_interested_in: RequestBody,
        @Part("attendant_name") attendant_name: RequestBody,
        @Part("attendant_phone") attendant_phone: RequestBody,
        @Part("attendant_address") attendant_address: RequestBody,
        @Part("relation") relation: RequestBody,
        @Part("patientId") patientId: RequestBody,
        @Part("token") token: RequestBody,
        @Part profileImage: MultipartBody.Part,
    ): Call<BookHospitalModel>

    @Multipart
    @POST("PatientApi/medicalForm")
    fun medicalFormSubmit(
        @Part("patientId") patientId: RequestBody,
        @Part("appointment_id") appointment_id: RequestBody,
        @Part("diagnosis_name") diagnosis_name: RequestBody,
        @Part("diagnosis_time") diagnosis_time: RequestBody,
        @Part("age") age: RequestBody,
        @Part("weight") weight: RequestBody,
        @Part("hypertension") hypertension: RequestBody,
        @Part("thyroid") thyroid: RequestBody,
        @Part("asthma") asthma: RequestBody,
        @Part("liver_diseases") liver_diseases: RequestBody,
        @Part("renal_failure") renal_failure: RequestBody,
        @Part("cardio") cardio: RequestBody,
        @Part("alcoholism") alcoholism: RequestBody,
        @Part("smoking") smoking: RequestBody,
        @Part("drugs") drugs: RequestBody,
        @Part("allergy_medicine") allergy_medicine: RequestBody,
        @Part("family_history") family_history: RequestBody,
        @Part("other_history") other_history: RequestBody,
        @Part("diabetes") diabetes: RequestBody,
        @Part("blood_tansfusion") blood_tansfusion: RequestBody,
        @Part image: Array<MultipartBody.Part?>
    ): Call<SubmitMedicalFormModel>

    @FormUrlEncoded
    @POST("PatientApi/medicalFormDetail")
    fun getMedicalHistory(
        @Field("appointment_id") appointmentId: String,
    ): Call<MedicalFormModel>

    @FormUrlEncoded
    @POST("PatientApi/treatmentDetail")
    fun getTreatmentDetail(
        @Field("appointment_id") appointmentId: String,
    ): Call<TreatmentDetailModel>

    @FormUrlEncoded
    @POST("PatientApi/planDetail")
    fun getPlanDetail(
        @Field("appointment_id") appointmentId: String,
    ): Call<PlanDetailModel>

    @FormUrlEncoded
    @POST("PatientApi/flightDetail")
    fun getFlightDetail(
        @Field("appointment_id") appointmentId: String,
    ): Call<FlightDetailModel>

    @FormUrlEncoded
    @POST("PatientApi/toDoDetail")
    fun getToDoDetail(
        @Field("appointment_id") appointmentId: String,
    ): Call<ToDoDetailModel>

    @FormUrlEncoded
    @POST("PatientApi/paymentDetail")
    fun getPaymentDetail(
        @Field("appointment_id") appointmentId: String,
    ): Call<PaymentDetailModel>

    @FormUrlEncoded
    @POST("PatientApi/bookTest")
    fun bookLabTest(
        @Field("testId") testId: String,
        @Field("token") token: String,
        @Field("patientId") patientId: String,
        @Field("no_of_person") patient_count: String,
        @Field("booking_date") booking_date: String,
        @Field("slot_start_time") slot_start_time: String,
        @Field("slot_end_time") slot_end_time: String,
        @Field("name") name: String,
        @Field("mobile") mobile: String,
        @Field("title") title: String,
        @Field("address") address: String,
        @Field("city") city: String,
        @Field("state") state: String,
        @Field("pincode") pincode: String,
        @Field("lat") lat: String,
        @Field("long") long: String,
        @Field("payment_mode") payment_mode: String,
        @Field("amount") amount: String,
        @Field("status") status: String,
        @Field("country") country: String,
        @Field("lab_id") labId:String,

    ): Call<BookLabTestModel>

    @FormUrlEncoded
    @POST("PatientApi/placeorder")
    fun placePharmacyOrder(
        @Field("cartIds") cartIds: String,
        @Field("patientId") patientId: String,
        @Field("name") name: String,
        @Field("mobile") mobile: String,
        @Field("title") title: String,
        @Field("address") address: String,
        @Field("city") city: String,
        @Field("state") state: String,
        @Field("pincode") pincode: String,
        @Field("lat") lat: String,
        @Field("long") long: String,
        @Field("payment_mode") payment_mode: String,
        @Field("amount") amount: String,
        @Field("country") country: String,
    ): Call<PlacePharmacyOrderModel>

    @FormUrlEncoded
//    @POST("PatientApi/getBookingHistory")
    @POST("PatientApi/getOrderHistory")
    fun getPharmacyOrder(
        @Field("patientId") patientId: String,
        @Field("pageNo") pageNo: String,
        @Field("token") token: String,
    ): Call<PharmacyOrderListModel>

    @FormUrlEncoded
    @POST("PatientApi/getOrderDetails")
    fun getPharmacyOrderDetail(
        @Field("patientId") patientId: String,
        @Field("orderId") orderId: String,
        @Field("token") token: String,
    ): Call<PharmacyOrderDetailModel>

    @Multipart
    @POST("PatientApi/editProfileImage")
    fun updateProfilePic(
        @Part("patientId") patientId: RequestBody,
//        @Part("token") token: RequestBody,
        @Part profileImage: MultipartBody.Part,
    ): Call<ProfileUpdateModel>

    @FormUrlEncoded
    @POST("PatientApi/editProfile")
    fun updateProfile(
        @Field("patientId") patientId: String,
        @Field("name") name: String,
        @Field("phone") phone: String,
        @Field("email") email: String,
    ): Call<ProfileUpdateModel>

    @GET("PatientApi/getSpeciality")
    fun getSpeciality(): Call<SpecialitiesListModel>

    @FormUrlEncoded
    @POST("PatientApi/doctorsBySpeciality")
    fun getSpecialityDoc(
        @Field("specilization_id") specilization_id: String,
    ): Call<DoctorListModel>

    @FormUrlEncoded
    @POST("PatientApi/doctorsDetails")
    fun getDocDetail(
        @Field("doctor_id") doctor_id: String,
    ): Call<DoctorDetailModel>

    @FormUrlEncoded
    @POST("PatientApi/timeSlot")
    fun getDocTimeSlot(
        @Field("doctor_id") doctor_id: String,
        @Field("available_day") available_day: String,
    ): Call<TimeSlotListModel>

    @FormUrlEncoded
    @POST("PatientApi/bookAppointment")
    fun bookDoctorAppointment(
        @Field("patient_id") patient_id: String,
        @Field("doctor_id") doctor_id: String,
        @Field("date") date: String,
        @Field("time") time: String,
        @Field("payment_mode") payment_mode: String,
        @Field("consultant_fee") consultant_fee: String,
    ): Call<BookDoctorAppointmentModel>

    @FormUrlEncoded
    @POST("PatientApi/doctorSearchList")
    fun search(
        @Field("keyword") keyword: String,
        @Field("country") country: String,
    ): Call<SearchDoctorModel>

    @FormUrlEncoded
    @POST("PatientApi/getDoctors")
    fun doctorSearch(
        @Field("specialization") keyword: String,
    ):Call<DoctorSearchModel>

    @GET("PatientApi/getServices")
    fun getMainService(): Call<MainServiceListModel>

    @Multipart
    @POST("PatientApi/sentQuery")
    fun sentSubmit(
        @Part("patient_id") patientId: RequestBody,
        @Part("doctor_id") doctor_id: RequestBody,
        @Part image: Array<MultipartBody.Part?>
    ): Call<SentQueryData>

    @FormUrlEncoded
    @POST("PatientApi/editProfile")
    fun editProfile(
        @Field("patientId") patientId:String,
        @Field("mobileNumber") mobileNumber: String,
        @Field("email")email: String,
        @Field("name") name:String,
    ): Call<ProfileUpdateModel>

    //http://gfctechnologies.com/gfcglobalheath/PatientApi/

//    @FormUrlEncoded
//    @POST("verify-otp")
//    fun verifyOTP(
//        @Field("phone") phone: String,
//        @Field("otp_code") otp: String
//    ): retrofit2.Call<VerifyOtpModel>

}

8920638723